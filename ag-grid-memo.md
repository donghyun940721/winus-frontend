```javascript
const gridOptions: GridOptions = {
    defaultColDef: { // 그리드 내 모든 컬럼에 공통적으로 적용할 옵션을 정의 ColDef 타입 참고
    resizable: true, // true : 그리드 컬럼 width를 사용자가 직접 조절할 수 있음
    menuTabs: [],
    },
    headerHeight: 32, // 그리드 헤더 height값 정의
    rowHeight: 32, // 그리드 row height값 정의
    columnDefs: FORM_COLUMN_DEFS, // 그리드 컬럼 정의 ColDef[]타입 참고
    rowSelection: "multiple", // 그리드 selected시 1개 or 다중으로 설정하는 옵션
    rowModelType: "serverSide", // 그리드 랜더링 타입, client-side : pagination이 없는 그리드에 사용 / server-side : pagination을 사용하는 페이지에 사용
    onGridReady: onGridReady, // 그리드가 정상적으로 랜더링을 마쳤을때 호출되는 함수
    onPaginationChanged: onPaginationChanged, // 페이지네이션이 일어날때마다 호출되는 함수
    onRowSelected: onSelectionChanged, // 행이 선택될때마다 호출되는 함수
    onPasteEnd: onPasteEnd, // 붙여넣기가 실행된 이후 호출되는 함수
    pagination: true, // 페이지네이션을 사용할지 여부
    paginationPageSize: pagingOptions![0], // 페이지당 보여줄 row의 갯수
    cacheBlockSize: pagingOptions![0], //
    rowBuffer: 100, // ag-grid는 view에 보이는 요소 외, 사용자가 스크롤시 빈 화면이 보이는 것을 막기위해 기본적으로 앞,뒤 10개의 행을 더 랜더링 함, 해당 옵션을 사용하면 앞,뒤 랜더링하는 로우의 갯수를 조정 할 수 있음
    suppressRowVirtualisation: true, // ag-grid의 기본 최적화 동작인 view에 보이는 데이터만 랜더링하는 옵션을 제거
    suppressMaxRenderedRowRestriction: true, // suppressRowVirtualisation옵션은 행을 500개로 제한을 두고있음, 그 제한을 풀어버리는 옵션
    getRowId: (data) => { // 행이 갖고있는 고유 id, 행을 crud할 경우 필수요소 및 그리드를 핸들링 할 경우 필요함
    return data.data.UOM_ID;
    },
    enableRangeSelection: true, // drag를 통해 행을 선택할 및 복사할 수 있음
    suppressClickEdit: true, // editable:true를 사용하는 경우 해당 cell을 클릭했을때 편집모드로 되는 기본동작을 막음 -> 내장되어있는 edit cell을 사용하지 않고 cellRenderer로 별도로 만든 편집셀 컴포넌트를 사용
    suppressContextMenu: true, // cell을 우클릭시 기본적으로 나오는 context-menu가 안보이도록 제거, context-menu를 사용하지 않을 경우에 true
    suppressRowClickSelection: true, // 행 체크박스 또는 cell 클릭시 selected를 확성화 하지 않도록 해주는 옵션, 체크박스를 사용하는 경우 체크박스를 클릭해야지만 행이 selected됨
};
```

    suppressColumnsToolPanel // column 편집 패널에 보이지 않음
    suppressRowVirtualisation: true,
    suppressMaxRenderedRowRestriction: true,
    - 편집모드를 사용하는 페이지에서 해당 옵션을 true로 사용
    - page size가 최대 500개 이하인 페이지에서만 사용 (속도 이슈 - 필요시 추후 다른 방법 구현 필요)
