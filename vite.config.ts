import VueI18nPlugin from "@intlify/unplugin-vue-i18n/vite";
import vue from "@vitejs/plugin-vue";
import { dirname, resolve } from "node:path";
import { URL, fileURLToPath } from "node:url";
import { UserConfig, defineConfig, loadEnv } from "vite";
import VueDevTools from "vite-plugin-vue-devtools";

//@ts-ignore
import { GetProxDef } from "./src/constants/proxy";

export default ({ mode }) => {
  const env = loadEnv(mode, process.cwd());

  const config: UserConfig = {
    build: {
      sourcemap: true, // TODO :: 배포시점에 개발,운영 구분하여 적용
    },
    plugins: [
      vue(),
      VueI18nPlugin({
        include: resolve(dirname(fileURLToPath(import.meta.url)), "./src/locales/**"),
        runtimeOnly: false,
        strictMessage: false,
      }),
      VueDevTools(),
    ],
    resolve: {
      alias: {
        "@": fileURLToPath(new URL("./src", import.meta.url)),
      },
    },
    css: {
      preprocessorOptions: {
        scss: {
          additionalData: `
            @import "./public/assets/scss/_mixin.scss";
            @import "./public/assets/scss/_units.scss";
            @import "./public/assets/scss/_variable.scss";
          `,
        },
      },
    },
    server: {
      proxy: GetProxDef(env),
      cors: true,
      // strictPort: true,
      watch: {
        usePolling: true,
      },
    },
  };

  // if (env.VITE_TARGET_URL.includes("localhost") || env.VITE_TARGET_URL.includes("127.0.0.1")) {
  //   delete config.server;
  // }

  return defineConfig(config);
};
