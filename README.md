# 📍 Branch

* main : 통합 테스트 환경 브랜치
* release : 배포 브랜치
* feature-logisall : logisall 개발 통합 브랜치

# **🛠 Skill Set**

* **F/W : Vue3**
* **IDE : VS Code**
* **상태관리 : Pinia**
* **다국어 : i18n**
* **그리드 : AG-Grid**
* **기타 : Node.js, TypeScript, axios, lodash**

# ⚙ Install

**1. Node.js 설치**

**2. Repository Clone**

* 당사 보안 정책에 의해 하기 경로의 구분 명칭으로 구성한다. (node package에 대한 제약)
* ***<span lang="EN-US">C:\\project\\{</span><span style="color:#0052cc;">Repository구분</span><span lang="EN-US">}\\{</span><span style="color:#0052cc;">프로젝트명</span><span lang="EN-US">}</span>***
* <span lang="EN-US">(</span>예시<span lang="EN-US">) C:\\project\\winusplus-front\\winus-frontend</span>
<br>

**3. NPM 명령어 수행**

* npm install
<br>

**4. 실행**

* 실행 IP, Port 지정 (package.json) : script 의 "dev" 명령어에 host IP, Port 명시
* 실행 명령어 : npm run dev:logisall

```json
  "scripts": {
    "dev": "vite --host --mode dev",
    "dev:logisall": "vite --host 127.0.0.1 --port 8001 --mode logisall",
    "preview": "vite preview",
    "build": "vite build --mode prod"
  },
```