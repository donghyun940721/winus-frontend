/**
 * OZ Report 정보
 */
export interface IOZReportArgument {
    /** 리포트 URL 주소 */
    url:string;

    /** 팝업창 명칭 */
    title:string;

    /** 파일 명칭 (export 시 파일명) */
    fileName:string;             
    
    /** OZ Report (*.ozr) 파일 확장자 명시 O */
    reportName:string;             
    
    /** OZ Report (*.odi) 파일 확장자 명시 X */
    odiName:string;             
    
    /** 'preview': 미리보기 창, 'print': 프린트 미리보기 창 호출, 'export':PDF 추출  */
    viewerMode:string;             
    
    /** OZ Viewer 내 출력 1회 제한 (출력 후 비활성화) */
    printOnce:boolean;
}


export class OZReport {

    url         :string;
    title       :string;
    fileName    :string;
    reportName  :string;
    odiName     :string;
    viewerMode  :string;
    printOnce   :boolean;
    urlParams = new URLSearchParams();
    
    get URL() { return this.url ; }
    get URLString() { return this.url + '?' + this.GetQueryString(); }
    
    constructor(args: IOZReportArgument){
        this.url            = args.url ?? '/jsp/OZReport.jsp';
        this.title          = args.title;
        this.fileName       = args.fileName;
        this.reportName     = args.reportName;
        this.odiName        = args.odiName;
        this.viewerMode     = args.viewerMode;
        this.printOnce      = args.printOnce;

        this.SetReportInfo();
    }

    private SetReportInfo() {
        if(this.title       != undefined)   this.urlParams.set('title'        , this.title);
        if(this.fileName    != undefined)   this.urlParams.set('fileName'     , this.fileName);
        if(this.reportName  != undefined)   this.urlParams.set('reportName'   , this.reportName);
        if(this.odiName     != undefined)   this.urlParams.set('odiName'      , this.odiName);
        if(this.viewerMode  != undefined)   this.urlParams.set('viewerMode'   , this.viewerMode);
        if(this.printOnce   != undefined)   this.urlParams.set('printOnce'    , this.printOnce.toString());
    }


    /**
     * ODI 인자 설정
     * 
     * @param {Object} odiParams 
     */
    SetOdiParam(odiParams: any){
        this.urlParams.set('odiParams', JSON.stringify(odiParams));
    }


    /**
     * OZR 인자 설정
     * 
     * @param {Object} ozrParams 
     */
    SetOzrParam(ozrParams: any){
        this.urlParams.set('ozrParams', JSON.stringify(ozrParams));
    }


    /**
     * URL 인자 문자열 획득
     * 
     * @returns 
     */
    GetQueryString(){
        return this.urlParams.toString();
    }
}

/**
 * FormData에서 리포트(*.jsp)를 직접호출 하는 클래스
 * 
 * @class
 */
export class OZReportForm extends OZReport{

    get URL() { return this.url ; }
    get URLParams() { return this.urlParams;}

    constructor(args: IOZReportArgument){
        super({...args});
    }

    AppendParamToForm(formData: HTMLElement){
        for (const [key, value] of this.urlParams.entries()){

            const frmValue = document.createElement("input");
    
            frmValue.setAttribute("name", key);
            frmValue.setAttribute("value", value);
    
            formData.appendChild(frmValue);
    
            console.log(`name: ${key} || value: ${value}`);
        }

        return formData;
    }
}