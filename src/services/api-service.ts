/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:    api-service.ts
 *  Description:    서버와 데이터를 주고받기위한 Rest API를 정의하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { useLoadingStore, useTabsStore, userStore } from "@/store";
import axios from "axios";
import { useToast } from "vue-toastification";
import { UtilService } from "./util-service";

axios.defaults.withCredentials = true;

export const REST_SERVICE_BACK_URL = import.meta.env.VITE_TARGET_URL + import.meta.env.VITE_URL_PREFIX;
export const REST_SERVICE_PRODUCTION_URL = import.meta.env.VITE_FILE_URL + import.meta.env.VITE_URL_PREFIX;

export const PREFIX = import.meta.env.VITE_URL_PREFIX;

const LOGIN_URL = "/login_rn.action";
const LOGOUT_URL = "/logout_rn.action";

const authHeader = {
  'Authorization' : 'winus-api',
  "Content-Type": "application/json",
};
const formDataHeader = {
  "Content-Type": "multipart/form-data",
};
const excelExportHeader = {
  "Content-Type": "application/x-www-form-urlencoded",
  Accept: "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
};

export class ApiService {
  static async handleError(error: any) {
    // 에러 처리 로직
  }

  static excelExport(url: string, params: any, data: any) {
    useLoadingStore().addSpinner();
    return axios
      .post(url, data, { params, headers: excelExportHeader })
      .then((resp) => {
        setTimeout(() => {
          useLoadingStore().removeSpinner();
        }, 250);
        return resp.data;
      })
      .catch((error) => {
        useLoadingStore().removeSpinner();
        console.log("error called = ", error);
        this.handleError(error);
      });
  }

  static postFormData(url: string, method: string, params: any, data: any, spinnerForContent: boolean = true) {
    const spinnerPath = spinnerForContent ? useTabsStore().selectedTab.value?.path : null;
    if (method === "post") {
      useLoadingStore().addSpinner(spinnerPath);
      return axios
        .post(url, data, { params, headers: formDataHeader })
        .then((resp) => {
          if (!resp.data.status) {
            useToast().error(resp.data.result.MSG);
          }
          setTimeout(() => {
            useLoadingStore().removeSpinner(spinnerPath);
          }, 250);
          return resp.data;
        })
        .catch((error) => {
          useLoadingStore().removeSpinner(spinnerPath);
          console.log("error called = ", error);
          this.handleError(error);
        });
    }
  }

  private static async callApi(url: string, method: string, params: any, data: any, runSpinner: boolean, spinnerForContent: boolean, responseType?: "json" | "text" | "blob" | "arraybuffer") {
    const spinnerPath = spinnerForContent ? useTabsStore().selectedTab.value?.path : null;
    if (runSpinner) {
      useLoadingStore().addSpinner(spinnerPath);
    }

    if (
      (UtilService.isEmptyObject(userStore().getCurrentUser()) || UtilService.isEmptyObject(userStore().getWindowRoll()) || userStore().getExpire() < Date.now()) &&
      window.location.pathname !== "/login"
    ) {
      window.location.replace("/error");
      return;
    }
    url = `${PREFIX}` + url;
    if (method === "get") {
      return axios
        .get(url, { params, headers: authHeader })
        .then((resp) => {
          if (!resp.data.status) {
            useToast().error(resp.data.result?.MSG || resp.data.result);
          }
          if (runSpinner) {
            useLoadingStore().removeSpinner(spinnerPath);
          }
          return resp.data;
        })
        .catch((error) => {
          if (runSpinner) {
            useLoadingStore().removeSpinner(spinnerPath);
          }
          console.log("error called = ", error);
          this.handleError(error);
        });
    } else if (method === "post") {
      return axios
        .post(url, data, { params, headers: authHeader, responseType: responseType })
        .then((resp) => {
          if (!resp.data.status) {
            const message = resp.data.result?.MSG || resp.data.result?.message || resp.data.result;
            if (message) useToast().error(message);
          }
          if (runSpinner) {
            useLoadingStore().removeSpinner(spinnerPath);
          }
          return responseType === "blob" ? resp : resp.data;
        })
        .catch((error) => {
          if (error.message) {
            useToast().error(error.message);
          }
          if (runSpinner) {
            useLoadingStore().removeSpinner(spinnerPath);
          }
          console.log("error : ", error);
          console.log("error called = ", error);
          this.handleError(error);
        });
    }
  }

  static login(id: string, pw: string, svcid: string, opid: string) {
    return this.callApi(LOGIN_URL, "post", null, { USER_ID: id, PSWD: pw, svcid, opid }, true, false);
  }

  static logout() {
    return this.callApi(LOGOUT_URL, "post", null, null, true, false);
  }

  static getApi(url: string, params: any, runSpinner: boolean = true, spinnerForContent: boolean = true) {
    return this.callApi(url, "get", params, null, runSpinner, spinnerForContent);
  }

  static postApi(url: string, params: any, data: any, runSpinner: boolean = true, spinnerForContent: boolean = true, responseType: "json" | "text" | "blob" | "arraybuffer" = "json") {
    return this.callApi(url, "post", params, data, runSpinner, spinnerForContent, responseType);
  }
}
