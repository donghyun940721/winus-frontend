/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	util-service.ts
 *  Description:    검색조건(search.vue, search-container-input.vue) 컴포넌트 제어 유틸을 정의하는 스크립트
 *  Authors:        dhkim
 *  Update History:
 *                  2024.03. : Created by dhkim
 *
------------------------------------------------------------------------------*/
import { useOptionDataStore, useSearchConditionStore } from "@/store";
import type { ISearchInput } from "@/types/index";
import { cloneDeep, set } from "lodash";

export class SearchUtil {

  /**
   * 특정 키(id)의 인덱스 추출
   * 
   * @param searchInput 
   * @param contentId 
   * @returns 
   */
  static getIndex = (searchInput: ISearchInput[] | any, contentId: any) => {

    let target = cloneDeep(searchInput);

    if(!Array.isArray(searchInput)) {
      target = Array.from(target);
    }

    return target.findIndex((content:any) => {
      return Object.values(content.ids).includes(contentId);
    });
  }

  /**
   * 검색(입력) 조건 비활성화
   * 
   * @param searchInput 
   * @param contentId 
   * @param disabled 
   */
  static setDisabled = (searchInput: ISearchInput[] | any, contentId: any, disabled: boolean) => {

    const target = searchInput instanceof Array ? searchInput : searchInput.value;
    const index = this.getIndex(target, contentId);

    if("disabled" in target[index]){
      target[index].disabled = disabled ? "disabled" : "";
    }
  }

  /**
   * 검색(입력) 조건이 "option" 타입에 한 해서 자동선택값을 변경함.
   * (optionsAutoSelected를 사용하는 경우)
   * 
   * @param searchInput   : 검색(입력)조건 리스트
   * @param contentId     : 검색구분 ID값
   * @param allowed       : 변경 Index 값
   */
  static setAllowAutoSelected = (searchInput: ISearchInput[] | any, contentId: any, allowed: boolean) => {
    
    const target = searchInput instanceof Array ? searchInput : searchInput.value;
    const index = this.getIndex(target, contentId);

    if("optionsAutoSelected" in target[index]){
      target[index].optionsAutoSelected.allowAutoSelected = allowed;             // 변경 값으로 자동선택 허용
    }
  }

  /**
   * 검색(입력) 조건이 "option" 타입에 한 해서 자동선택값을 변경함.
   * (optionsAutoSelected를 사용하는 경우)
   * 
   * @param searchInput   : 검색(입력)조건 리스트
   * @param contentId     : 검색구분 ID값
   * @param changedIndex  : 변경 Index 값
   * @param allowAutoSelected  : 자동변경 여부 (API 형태 호출 시)
   */
  static setAutoSelectedKeyIndex = (searchInput: ISearchInput[] | any, contentId: any, changedIndex: number | string, allowAutoSelected?: boolean) => {
    
    const target = searchInput instanceof Array ? searchInput : searchInput.value;
    const index = this.getIndex(target, contentId);

    if(index < 0) { 
      console.log(`Not Found Index`);
      return;
    }

    if("optionsAutoSelected" in target[index]){
      target[index].optionsAutoSelected.autoSelectedKeyIndex = changedIndex;
      target[index].optionsAutoSelected.allowAutoSelected = allowAutoSelected ?? true;             // 변경 값으로 자동선택 되도록 변경.
    }
  }

  /**
   * 입력란을 공백으로 초기화
   * (OptionDataStore 공백)
   * 
   * @param searchInput   : 검색(입력)조건 리스트
   * @param contentId     : 검색구분 ID값
   */
  static setClear = (searchInput: ISearchInput[] | any, contentId: any) => {
    const optionInfo = new Object();

    SearchUtil.setAllowAutoSelected(searchInput, contentId, true);    // 값 변경의 갱신 대상으로 변경

    set(optionInfo, contentId, []);

    useOptionDataStore().setOptionData(optionInfo);
    useSearchConditionStore().changeSearchInputValue(contentId, "");
  }
}
