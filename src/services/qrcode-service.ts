import qrcode from "qrcode-generator";

export class QRCode {

  /**
   * 특정 값을 QR 바코드 Image 태그 HTML 문자열로 반환
   *
   * @params text : 변환하고자 하는 값
   * @parmas fontSize : QR코드 사이즈
   * @returns Image 태그 HTML 문자열
   */
  static Generator (text: string, fontSize: number = 3) {
    const QRCodeImageMaker = qrcode(4, "L");

    QRCodeImageMaker.addData(text);
    QRCodeImageMaker.make();

    return QRCodeImageMaker.createImgTag(fontSize);
  }

}