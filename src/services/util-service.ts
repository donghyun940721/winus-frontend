/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	util-service.ts
 *  Description:    Application들을 위한 다양한 Utility 기능들을 정의하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import App from "@/App.vue";
import { createApp } from "vue";
// 뷰컴포넌트 외부에서 스토어 사용
import { OPTION_PARAMS_LIST } from "@/constants/options-params-list";
import { winusMenus } from "@/constants/winus-menus";
import { useRowSpanCheckboxStore, useSearchConditionStore, useSelectedDataStore, useTabsStore, userStore } from "@/store";
import { IAvailableOptionsUtilParams, IWinusMenu } from "@/types";
import { createPinia } from "pinia";
import { ApiService, PREFIX } from "./api-service";
// import { getEventListeners } from "events";
import { i18n } from "@/i18n";
import type { ISearchInput } from "@/types/index";
import { useToast } from "vue-toastification";
// 뷰컴포넌트 외부에서 스토어 사용
const pinia = createPinia();
const app = createApp(App);
app.use(pinia);
const { t } = i18n.global;

export class UtilService {
  // isUnused, hidden 요소를 가진 인풋을 제외한 실제 화면에 보여지는 인풋의 갯수를 계산
  static getAvailableSearchInputCount(searchInputList: any[]) {
    let availableCount = 0;
    for (let i = 0; searchInputList.length > i; i++) {
      if (!(Object.hasOwn(searchInputList[i], "isUnused") || Object.hasOwn(searchInputList[i], "hidden"))) {
        availableCount++;
      }
    }
    return availableCount - 1;
  }

  /**
   * 검색조건 영역 화면에 보여질 행의 갯수에 맞는 인풋의 갯수를 리턴해주는 함수
   *
   * @param line              : 보여질 행의 갯수
   * @param searchInputList   : 각 화면에서 정의한 검색조건 정보
   * @param isList            : 검색조건 사용자 정의 사용 여부
   * @returns
   */
  static getShowInputCount(line: number, searchInputList: ISearchInput[], isList?: boolean): number | number[] {
    // line:number ---> 보여질 행의 갯수를 넣어준다.
    // isList?: boolean ----> 검색조건 사용자 정의를 활용하는 페이지에서 값을 true로 넣어준다.
    // 1줄에 위치할 인풋 요소의 총 합은 24
    const lengthRange = 24 * line;

    let currentRange = 0;
    let currentCount = 0;
    // isList === true 일때
    const currentList = [];

    for (let i = 0; searchInputList.length > i; i++) {
      if (!((Object.hasOwn(searchInputList[i], "hidden") && searchInputList[i].hidden) || Object.hasOwn(searchInputList[i], "isUnused"))) {
        const widthType = searchInputList[i].width;
        switch (widthType) {
          case "entire":
            currentRange += 24;
            break;
          case "half":
            currentRange += 12;
            break;
          case "triple":
            currentRange += 8;
            break;
          case "quarter":
            currentRange += 6;
            break;
        }
        isList ? currentList.push(i) : currentCount++;
        if (currentRange > lengthRange) {
          isList ? currentList.pop() : currentCount--;
          return isList ? currentList : currentCount - 1;
        }
      }
    }
    return isList ? currentList : currentCount - 1;
  }

  // grid데이터 요청시 정렬에 관한 파라미터 수정
  static updateSortParameter(params: any, sortParameterObject: any) {
    const sordDataArray = params.request.sortModel;
    if (sordDataArray.length > 0) {
      sortParameterObject.sidx = sordDataArray[0].colId;
      sortParameterObject.sord = sordDataArray[0].sort;
    }
  }

  static async getExcelFileByURL(url: string, apiParams: any) {
    const res = await ApiService.postApi(url, apiParams, null, true, true, "blob");
    if (res.status === 200) {
      UtilService.getExcelFile(res);
    }
  }

  // 재고관리 > 현재고조회 : 전체엑셀과 같이 응답시 엑셀 다운로드가 필요한 경우 호출
  static getExcelFile(response: any, fileName: string = "download.xlsx") {
    const contentDisposition = response.headers["content-disposition"];
    if (contentDisposition && contentDisposition.includes("filename=")) {
      const encodedFilename = contentDisposition.split("filename=")[1].split(";")[0].replace(/"/g, "");
      fileName = decodeURIComponent(encodedFilename);
    }
    // response로 받아온 데이터를 blob객체로 변환
    const blobObject = new Blob([response.data], { type: response.headers["content-type"] });
    // 데이터의 참조를 가리키는 새로운 객체 URL을 생성
    const objectUrl = window.URL.createObjectURL(blobObject);
    // 열어주는 과정이 필요하기 때문에 a태그나 window.open이 필요
    const link = document.createElement("a");
    link.href = objectUrl;
    link.setAttribute("download", fileName);
    document.body.appendChild(link);
    link.click();
    window.URL.revokeObjectURL(objectUrl);
  }

  /* 행병합을 사용하는 그리드인 경우, 병합 최대치로 rowBuffer값을 셋팅 */
  static setRowBuffer(gridOptions: any, pagingSize: any) {
    const rowSpanStore = useRowSpanCheckboxStore().getGridRowSpanRule();

    const keys = Object.keys(rowSpanStore);

    if (keys && keys.length > 0) {
      let rowBufferValue = 0;
      const currentPage = gridOptions.api?.paginationGetCurrentPage();
      const currentPageSize = pagingSize.value;

      const startIndex = currentPage ? currentPage! * currentPageSize : currentPage!;

      for (let i = 0; keys.length > i; i++) {
        if (keys[i].includes(`_${startIndex}`)) {
          if (rowSpanStore[keys[i]]?.count > rowBufferValue) {
            rowBufferValue = rowSpanStore[keys[i]]?.count;
          }
        }
      }

      gridOptions.api?.__updateProperty("rowBuffer", rowBufferValue, true);
    }
  }

  /* sessionStorage에 저장된 화주정보 가져오기 */
  static getCustDataInSessionStorage() {
    const sessionStorageParseData = JSON.parse(window.sessionStorage.getItem("userCustInfo") || "{}");
    const lcId = UtilService.getCurrentLocation();
    if (Object.hasOwn(sessionStorageParseData, lcId) && Object.keys(sessionStorageParseData[lcId]).length > 0) {
      useSelectedDataStore().selectedModalHandler({ owner: sessionStorageParseData[lcId] });
      return true;
    }

    return false;
  }

  /* zoom값이 변경될때 팝업마다 formHeight 높이를 수정해야될 값을 가져옴 */
  static getZoomHeight(pageId: string) {
    const currentZoom = "0.88";
    // let currentZoom = "1";

    if (pageId === "wmsop910_3-new-popup") {
      if (currentZoom === "0.88") {
        return 35;
      } else if (currentZoom === "1") {
        return 10;
      }
    } else if (pageId === "simple-shipping-order") {
      if (currentZoom === "0.88") {
        return 20;
      } else if (currentZoom === "1") {
        return 0;
      }
    } else if (pageId === "shipping-order") {
      if (currentZoom === "0.88") {
        return 30;
      } else if (currentZoom === "1") {
        return 5;
      }
    } else if (pageId === "receiving-order") {
      if (currentZoom === "0.88") {
        return 20;
      } else if (currentZoom === "1") {
        return -5;
      }
    } else if (pageId === "wmsst040e6") {
      if (currentZoom === "0.88") {
        return -85;
      } else if (currentZoom === "1") {
        return -100;
      }
    } else if (pageId === "wmsst040e3") {
      if (currentZoom === "0.88") {
        return -85;
      } else if (currentZoom === "1") {
        return -100;
      }
    } else if (pageId === "WMSOP030T") {
      if (currentZoom === "0.88") {
        return 20;
      } else if (currentZoom === "1") {
        return -10;
      }
    } else if (pageId === "wmsom120-new-popup") {
      if (currentZoom === "0.88") {
        return 30;
      } else if (currentZoom === "1") {
        return 10;
      }
    } else if (pageId === "re-location-modal") {
      if (currentZoom === "0.88") {
        return 0;
      } else if (currentZoom === "1") {
        return -25;
      }
    } else if (pageId === "wmsop000q2") {
      if (currentZoom === "0.88") {
        return 110;
      } else if (currentZoom === "1") {
        return 110;
      }
    } else if (pageId === "wmsop000q8") {
      if (currentZoom === "0.88") {
        return 65;
      } else if (currentZoom === "1") {
        return 65;
      }
    }
    return 0;
  }

  static setExpireDate(hour: number) {
    return Date.now() + 1000 * 60 * 60 * hour;
  }

  static convertLocale(locale: string) {
    let lang;
    if (locale === "KR") {
      lang = "korean";
    } else if (locale === "EN") {
      lang = "english";
    } else if (locale === "JA") {
      lang = "japanese";
    } else if (locale === "CH") {
      lang = "chinese";
    } else if (locale === "VN") {
      lang = "vietnam";
    } else if (locale === "ES") {
      lang = "spanish";
    }
    return lang;
  }

  static convertLocalNum(locale: string) {
    let lang;
    if (locale === "KR") {
      lang = "0";
    } else if (locale === "EN") {
      lang = "3";
    } else if (locale === "JA") {
      lang = "1";
    } else if (locale === "CH") {
      lang = "2";
    } else if (locale === "VN") {
      lang = "4";
    } else if (locale === "ES") {
      lang = "5";
    }
    return lang;
  }

  static isEmptyObject(obj: any) {
    let empty = false;
    if (!obj) {
      empty = true;
    } else if (Object.keys(obj).length === 0) {
      empty = true;
    }
    return empty;
  }

  /**
   * Object에 특정 키 값이 존재하는 지 여부 확인
   *
   * @param obj
   * @param key
   */
  static hasKey(obj: any, key: string) {
    if (obj == undefined || obj == null) return false;

    return Object.hasOwn(obj, key);
  }

  /**
   * 문자열 공백 기준에 따른 공백 여부 반환
   *
   * @param str
   * @returns {boolean}
   */
  static isEmptyString(str: string | undefined) {
    return str == "" || str == undefined || str == null;
  }

  static isEmptyArray(arr: Array<string>) {
    if (arr == undefined || arr == null) return true;
    if (arr.length == 0) return true;

    return arr.every(UtilService.isEmptyString);
  }

  static getTimeStamp() {
    return Date.now();
  }

  // select options 리턴해주는 함수
  static getOptionApiData(pageTitle: string) {
    const PAGE_DATA_LIST: any = OPTION_PARAMS_LIST;
    return { [`${pageTitle}`]: PAGE_DATA_LIST[pageTitle] };
  }

  // 현재 활성화된 로케이션값 리턴해주는 함수
  static getCurrentLocation() {
    return JSON.parse(userStore().getCurrentUser())["LC_ID"];
  }

  // 그리드 crud이후 값 업데이트 및 인풋 데이터 초기화 함수
  static reloadGridData(SelectedDataStore: any, SearchConditionStore: any, gridApi: any) {
    // SelectedDataStore.resetSelectedData();
    SearchConditionStore.setAvailableSearchBySearchCondition(true);
    gridApi.deselectAll();
  }

  //TODO: change pagination on every page.vue
  static getGridPagingOptions(type: string): Array<number> {
    let pagingOption = [] as any;
    if (type === "WMSMS080_shipment") {
      pagingOption = [20, 40, 60];
    } else if (
      type === "WMSCM011" ||
      type === "WMSMS100" ||
      type === "WMSMS040" ||
      type === "WMSMS030" ||
      type === "new-zone" ||
      type === "WMSST051_2" ||
      type === "WMSCM162" ||
      type === "WMSCM085" ||
      type === "WMSMS010Q1" ||
      type === "WMSMS011Q1" ||
      type === "WMSPL010" ||
      type === "WMSTG733_1" ||
      type === "WMSCM011_1" ||
      type === "WMSCM011_2" ||
      type === "WMSCM081" ||
      type === "WMSCM100" ||
      type === "WMSOP910_NEW_POPUP_TOP" ||
      type === "WMSST040T2" ||
      type === "WMSMS010_CONTEXT_MODAL" ||
      type === "WMSCM101" ||
      type === "WMSST050_INVENTORY_ADJUSTMENT_POPUP" ||
      type === "WMSST040T1" ||
      type === "WMSCM050" ||
      type === "WMSOP000Q3" ||
      type === "WMSOP000Q1" ||
      type === "WMSOP000Q9" ||
      type === "TMSYS030Q3" ||
      type === "wmsop000q5v2"
    ) {
      pagingOption = [100, 200, 300, 600];
    } else if (type === "WMSYS030") {
      pagingOption = [100];
    } else if (type === "WMSCM080Q5" || type === "WMSMS150") {
      pagingOption = [100, 200, 300];
    } else if (type === "WMSMS090") {
      pagingOption = [1000, 5000, 10000];
    } else if (type === "WMSCM091" || type === "WMSOM120" || type === "WMSCM091Q7" || type === "WMSMS090pop2") {
      pagingOption = [100, 200, 300, 500, 1000];
    } else if (type === "WMSCM080") {
      pagingOption = [100, 200, 300, 500, 1000, 2000];
    } else if (type === "WMSMS092_1" || type === "WMSMS092_2" || type === "WMSMS092_3") {
      pagingOption = [100, 200, 300, 500, 1000, 5000, 10000];
    } else if (type === "WMSMS010") {
      pagingOption = [100, 500, 1000, 2000];
    } else if (type === "WMSMS011") {
      pagingOption = [100, 500, 1000, 5000, 10000, 50000];
    } else if (type === "WMSMS080") {
      pagingOption = [100, 300, 500, 1000, 3000, 5000];
    } else if (type === "WMSST040E3") {
      pagingOption = [300, 500, 1000];
    } else if (type === "WMSST051_1") {
      pagingOption = [400, 600, 800, 1000, 2000, 5000, 10000, 20000, 50000, 100000, 200000, 300000];
    } else if (type === "WMSST020") {
      pagingOption = [1000, 2000, 5000, 10000, 20000, 30000, 40000];
    } else if (type === "WMSOP910_1") {
      pagingOption = [1000, 2000, 3000, 5000, 10000, 50000, 100000];
    } else if (type === "WMSST050" || type === "WMSST051_3" || type === "WMSST401" || type === "WMSST040" || type === "01_WMSST120") {
      pagingOption = [200, 300, 400];
    } else if (type === "WMSST051_4" || type === "WMSST051_5" || type === "TMSYS030" || type == "TMSYS030_2") {
      pagingOption = [1000, 2000, 3000];
    } else if (type === "WMSST055") {
      pagingOption = [200, 500, 1000, 2000, 5000, 10000, 20000, 50000];
    } else if (type === "WMSST010") {
      pagingOption = [100, 200, 300, 500, 1000, 5000, 10000];
    } else if (type === "TMSYS040" || type === "TMSYS010_1") {
      pagingOption = [500, 700, 900];
    } else if (type === "TMSYS010_2") {
      pagingOption = [500, 700, 1000];
    } else if (type === "WMSTG070") {
      pagingOption = [1000, 2000, 5000, 10000, 20000];
    } else if (type === "WMSOP910_1" || type === "WMSOP910_2" || type === "WMSOP910_3") {
      pagingOption = [1000, 2000, 3000, 5000, 10000, 100000];
    } else if (type === "barcodeMultiInput") {
      pagingOption = [10, 20, 30];
    } else if (type === "WMSOP910_NEW_POPUP_BOTTOM" || type === "WMSOP999T2") {
      pagingOption = [20, 40, 60];
    } else if (type === "WMSOP000Q12") {
      pagingOption = [500, 700, 1000, 2000, 5000, 100000, 50000, 100000];
    } else if (type === "02_WMSST120") {
      pagingOption = [300, 1000, 5000, 10000, 20000];
    } else if (type === "WMSMS085") {
      pagingOption = [100, 200, 300];
    }

    return pagingOption;
  }

  // grid data의 특정 column 값이 select-box의 옶션 값과 매칭시켜서 보여줘야하는 경우가 있음.
  static setGridColumnDataBySelectBoxOptionData(optionDatas: any, gridData: any) {
    if (gridData.status) {
      gridData.result.list.forEach((gridItem: any) => {
        //ITEM_GRP_ID와 POOL_GRP_ID 둘 다 가지고 있는 경우 위에 if문만 실행되어 조건 추가
        if (gridItem.ITEM_GRP_ID && !gridItem.POOL_GRP_ID) {
          optionDatas.ITEMGRP?.forEach((optionItem: any) => {
            if (optionItem.CODE_CD === gridItem.ITEM_GRP_ID) {
              gridItem.ITEM_GRP_NAME = optionItem.CODE_NM;
            }
          });
        } else if (gridItem.POOL_GRP_ID) {
          optionDatas.POOLGRP?.forEach((optionItem: any) => {
            if (optionItem.CODE_CD === gridItem.POOL_GRP_ID) {
              gridItem.POOL_GRP_NAME = optionItem.CODE_NM;
            }
          });
        } else if (gridItem.TOP_MENU_CD) {
          optionDatas.vrTopMenuCd?.forEach((optionItem: any) => {
            if (optionItem.code === gridItem.TOP_MENU_CD) {
              gridItem.TOP_MENU_CD = optionItem.codeNm;
            }
          });
        } else if (gridItem.LOC_TYPE) {
          optionDatas.LocType?.forEach((optionItem: any) => {
            if (optionItem.CODE_CD === gridItem.LOC_TYPE) {
              gridItem.LOC_TYPE = optionItem.CODE_NM;
            }
          });
        }
      });
    }
  }

  /**
   * search 영역의 params setting
   */
  static getGridParams(searchConditions: any, SelectedDataStore: any, SearchConditionStore: any) {
    const apiParams = {} as any;
    searchConditions?.forEach((searchItem: any) => {
      // search input인 경우
      if (searchItem.isSearch) {
        // select box value에 따라 열리는 search modal 이 다를경우에
        if (!searchItem.modalTypes) {
          for (let i = 0; searchItem.ids.length > i; i++) {
            apiParams[searchItem.ids[i]] =
              SelectedDataStore.getSelectedModalData()[searchItem.title]?.[searchItem.rowDataIds[i]] || SearchConditionStore.getSearchInputValue()[searchItem.ids[i]] || "";
          }
        } else {
          // if (modalType.value === 'product') {
          //   value = SelectedDataStore.selectedModalData.value[modalType.value]?.[data.rowDataIds[i]];
          // } else if (modalType.value === 'logistics-container') {
          //   value = SelectedDataStore.selectedModalData.value[modalType.value]?.[data.rowData2ndIds ? data.rowData2ndIds[i] : ''] || SearchConditionStore.searchInputValue.value[data.ids[i]];
          // }
          for (let i = 0; i < searchItem.modalTypes.length; i++) {
            for (let j = 0; j < searchItem.rowDataIds.length; j++) {
              if (i === 1) {
                apiParams[searchItem.ids[j]] =
                  SelectedDataStore.getSelectedModalData()[searchItem.modalTypes[i]]?.[searchItem.rowDataIds[j]] || SearchConditionStore.getSearchInputValue()[searchItem.ids[j]] || "";
              } else if (i === 2) {
                if (!apiParams[searchItem.ids[j]]) {
                  apiParams[searchItem.ids[j]] =
                    SelectedDataStore.getSelectedModalData()[searchItem.modalTypes[i]]?.[searchItem.rowData2ndIds[j]] || SearchConditionStore.getSearchInputValue()[searchItem.ids[j]] || "";
                }
              }
            }
          }
        }

        if (!searchItem.modalTypes) {
          if (searchItem.hiddenId) {
            apiParams[searchItem.hiddenId] =
              SelectedDataStore.getSelectedModalData()[searchItem.title]?.[searchItem.rowDataHiddenId] || SearchConditionStore.getSearchInputValue()[searchItem.hiddenId] || "";
          }
        } else {
          if (searchItem.hiddenId) {
            for (let i = 0; i < searchItem.modalTypes.length; i++) {
              if (i === 1) {
                apiParams[searchItem.hiddenId] =
                  SelectedDataStore.getSelectedModalData()[searchItem.modalTypes[i]]?.[searchItem.rowDataHiddenId] || SearchConditionStore.getSearchInputValue()[searchItem.hiddenId] || "";
              } else if (i === 2) {
                if (!apiParams[searchItem.hiddenId]) {
                  apiParams[searchItem.hiddenId] =
                    SelectedDataStore.getSelectedModalData()[searchItem.modalTypes[i]]?.[searchItem.rowData2ndHiddenId] || SearchConditionStore.getSearchInputValue()[searchItem.hiddenId] || "";
                }
              }
            }
          }
        }
      }
      // 그 외 text, select ... 의 경우
      else {
        for (let i = 0; searchItem.ids.length > i; i++) {
          apiParams[searchItem.ids[i]] = SearchConditionStore.getSearchInputValue()[searchItem.ids[i]] || "";
        }
        if (searchItem.hiddenId) {
          apiParams[searchItem.hiddenId] = SearchConditionStore.getSearchInputValue()[searchItem.hiddenId] || "";
        }
      }
    });
    return apiParams;
  }

  /**
   * 페이지의 저장, 삭제할때 body data 생성
   */
  static getBodyDataByGridAndInfo(pk: string, inputsContainers: any, columnDefs: any, selectedItems: any, needPrefix: boolean, type: string) {
    let prefix = "";
    if (needPrefix) {
      if (type === "save") {
        prefix = "I_";
      } else {
        prefix = "D_";
      }
    }
    const body = {} as any;
    if (inputsContainers) {
      /**
       * info 데이터 기준으로 body setting
       */
      inputsContainers.forEach((inputContainer: any) => {
        inputContainer.inputs.forEach((inputDef: any) => {
          selectedItems.forEach((selectedItem: any, i: number) => {
            inputDef.rowDataIds?.forEach((rowDataId: string, j: number) => {
              inputDef.ids?.forEach((id: string, k: number) => {
                if (j === k) {
                  let value;
                  if (selectedItem[rowDataId]) {
                    value = selectedItem[rowDataId] + "";
                  } else {
                    value = "";
                  }
                  body[id + i] = value;
                }
              });
            });
            if (inputDef.hiddenId) {
              body[inputDef.hiddenId + i] = selectedItem[inputDef.rowDataHiddenId] + "";
            }

            /**
             * 기타 data setting
             */
            let gubunType;
            if ((selectedItem[pk] + "").includes("temp")) {
              gubunType = "INSERT";
            } else {
              if (type === "save") {
                gubunType = "UPDATE";
              } else {
                gubunType = "DELETE";
              }
            }
            if (gubunType === "INSERT" || gubunType === "UPDATE") {
              body["I_ST_GUBUN" + i] = gubunType;
            } else {
              body["D_ST_GUBUN" + i] = gubunType;
            }
          });
        });
      });
    } else {
      /**
       * grid 데이터 기준으로 body setting
       */
      for (let i = 0; i < selectedItems.length; i++) {
        columnDefs.forEach((columnDef: any) => {
          if (columnDef.field) {
            body[prefix + columnDef.field + i] = selectedItems[i][columnDef.field];
          }
        });
        /**
         * 기타 data setting
         */
        let gubunType;
        if ((selectedItems[i][pk] + "").includes("temp")) {
          gubunType = "INSERT";
        } else {
          if (type === "save") {
            gubunType = "UPDATE";
          } else {
            gubunType = "DELETE";
          }
        }
        if (gubunType === "INSERT" || gubunType === "UPDATE") {
          body[prefix + "ST_GUBUN" + i] = gubunType;
        } else {
          body[prefix + "ST_GUBUN" + i] = gubunType;
        }
      }
    }

    if (prefix !== "") {
      if (type === "save") {
        body["I_selectIds"] = selectedItems.length + "";
        body["D_selectIds"] = 0 + "";
      } else {
        body["I_selectIds"] = 0 + "";
        body["D_selectIds"] = selectedItems.length + "";
      }
    } else {
      body["selectIds"] = selectedItems.length + "";
    }
    return body;
  }

  // info 영역 require=true 인풋의 유효성 검사
  static inputRequiredValidation(infoInput: any, selectedItems: any) {
    const requiredInputItems: any[] = [];
    const result = { status: true, toastKey: "" };
    for (const inputItemsContainer in infoInput) {
      for (let i = 0; infoInput[inputItemsContainer].inputs.length > i; i++) {
        if (infoInput[inputItemsContainer].inputs[i].required) {
          requiredInputItems.push(infoInput[inputItemsContainer].inputs[i]);
        }
      }
    }
    for (let i = 0; selectedItems.length > i; i++) {
      for (let j = 0; requiredInputItems.length > j; j++) {
        for (let k = 0; requiredInputItems[j].ids.length > k; k++) {
          const requireKey = requiredInputItems[j].rowDataIds[k];
          const selectedItem = selectedItems[i];
          if (!selectedItem[requireKey]) {
            if (!selectedItem[requiredInputItems[j].ids[k]]) {
              result.status = false;
              result.toastKey = requiredInputItems[j].title;
              return result;
            }
          }
        }
      }
    }
    return result;
  }

  // search영역  required 인풋의 유효성 검사
  static searchInputRequiredValidation(searchInputs: any, SearchConditionStore: any, SelectedDataStore: any) {
    let result = "";

    for (let i = 0; searchInputs.length > i; i++) {
      if (searchInputs[i].required) {
        // search input인 경우
        if (searchInputs[i].isSearch) {
          if (!Object.hasOwn(SelectedDataStore.getSelectedModalData(), searchInputs[i].title)) {
            result = searchInputs[i].title;
            return result;
          }
        } else {
          // 그 외 select-box, text, check-box...
          if (!Object.hasOwn(SearchConditionStore.getSearchInputValue(), searchInputs[i].ids[0])) {
            result = searchInputs[i].title;
            return result;
          }
        }
      }
    }
    return result;
  }

  // 서버에서 Grid totcal 개수 및 total page 개수를 잘못보내줄때가 있음. 그럴때 data total 개수 수정
  static resetGridDataTotalCount(gridData: any, requestRows: number) {
    let totCnt;
    if (
      (gridData.result.tPage > 1 && gridData.result.cPage === 1 && gridData.result.list.length < requestRows) ||
      (gridData.result.tPage === 1 && gridData.result.list.length !== gridData.result.totCnt)
    ) {
      totCnt = gridData.result.list.length;
    } else if (gridData.result.tPage > 1 && gridData.result.cPage === gridData.result.tPage) {
      totCnt = gridData.result.pageSize * (gridData.result.tPage - 1) + gridData.result.list.length;
    } else {
      totCnt = gridData.result.totCnt;
    }
    return totCnt;
  }

  /**
   * select box value에 따라 열리는 search modal 이 다를경우에 사용. search modal 타입을 변경해준다
   */
  static setModalTypeBySelectedValue(e: string) {
    let searchModalKey = "";
    if (!e) {
      searchModalKey = "product";
      return searchModalKey;
    }
    if (e === "G") {
      searchModalKey = "product";
    } else if (e === "P") {
      searchModalKey = "logistics-container";
    }
    return searchModalKey;
  }

  /**
   * Grid 데이터 조회시 unique한 값이 없을 경우 유니크한 RNUM값을 심어준다.
   */
  static setGridDatasRowNum(list: any) {
    if (list) {
      list.forEach((row: any, i: number) => {
        if (!row.RNUM) {
          row.RNUM = ++i;
        }
      });
    }
  }

  /**
   * 서버에서 가져온 data를 보여줄때 값을 변환해서 보여줘야하는 경우가 있음.
   */
  static convertDataValue(type: string, data: string, field?: string) {
    data = data === null || data === undefined ? "null" : data;
    data = data === "" ? " " : data;
    const termsName = field ? ["page", type, field, data].join(".") : ["page", type, data].join(".");
    return t(termsName);

    // if (type === "WMSST040") {
    //   if (data === "FF") {
    //     if (user.locale === "korean") {
    //       return "완료";
    //     } else if (user.locale === "english") {
    //       return "Complete";
    //     } else if (user.locale === "japanese") {
    //       return "完了";
    //     } else if (user.locale === "chinese") {
    //       return "[중]완료";
    //     } else if (user.locale === "vietnam") {
    //       return "[중]완료";
    //     } else if (user.locale === "spanish") {
    //       return "Completo";
    //     }
    //   } else if (data === "UF") {
    //     if (user.locale === "korean") {
    //       return "미완료";
    //     } else if (user.locale === "english") {
    //       return "Incomplete";
    //     } else if (user.locale === "japanese") {
    //       return "未完了";
    //     } else if (user.locale === "chinese") {
    //       return "[중]미완료";
    //     } else if (user.locale === "vietnam") {
    //       return "[중]미완료";
    //     } else if (user.locale === "spanish") {
    //       return "InCompleto";
    //     }
    //   } else if (data === "01") {
    //     return "기한초과";
    //   } else if (data === "02") {
    //     return "배송미스";
    //   } else if (data === "03") {
    //     return "리콜";
    //   } else if (data === "12") {
    //     if (user.locale === "korean") {
    //       return "재고이동";
    //     } else if (user.locale === "english") {
    //       return "Inventory Movement";
    //     } else if (user.locale === "japanese") {
    //       return "在庫移動";
    //     } else if (user.locale === "chinese" || user.locale === "vietnam") {
    //       return "[중]재고이동";
    //     } else if (user.locale === "spanish") {
    //       return "Movimiento De Inventarios";
    //     }
    //   } else if (data === "14") {
    //     if (user.locale === "korean") {
    //       return "랙보충";
    //     } else if (user.locale === "english") {
    //       return "Rack Replenishment";
    //     } else if (user.locale === "japanese") {
    //       return "ラック補充";
    //     } else if (user.locale === "chinese" || user.locale === "vietnam") {
    //       return "[중]랙보충";
    //     } else if (user.locale === "spanish") {
    //       return "Reposición De Estanterías";
    //     }
    //   } else if (data === "16") {
    //     return "파손";
    //   }
    // }
    // if (type === "WMSST055") {
    //   if (data === "G") {
    //     if (user.locale === "korean") {
    //       return "정상";
    //     } else if (user.locale === "english" || user.locale === "spanish") {
    //       return "Normal";
    //     } else if (user.locale === "japanese") {
    //       return "正常";
    //     } else if (user.locale === "chinese" || user.locale === "vietnam") {
    //       return "[중]정상";
    //     }
    //   } else if (data === "F") {
    //     return "불량";
    //   }
    //   if (data === "10") {
    //     if (user.locale === "korean") {
    //       return "출고";
    //     } else if (user.locale === "english") {
    //       return "Shipping";
    //     } else if (user.locale === "japanese") {
    //       return "出庫";
    //     } else if (user.locale === "chinese" || user.locale === "vietnam") {
    //       return "[중]출고";
    //     } else if (user.locale === "spanish") {
    //       return "Transporte";
    //     }
    //   } else if (data === "20") {
    //     if (user.locale === "korean" || user.locale === "japanese" || user.locale === "chinese" || user.locale === "vietnam") {
    //       return "보관";
    //     } else if (user.locale === "english") {
    //       return "Storage";
    //     } else if (user.locale === "spanish") {
    //       return "Almacenamiento";
    //     }
    //   } else if (data === "40") {
    //     if (user.locale === "korean") {
    //       return "출하";
    //     } else if (user.locale === "english") {
    //       return "Shipping";
    //     } else if (user.locale === "japanese") {
    //       return "出荷";
    //     } else if (user.locale === "chinese" || user.locale === "vietnam") {
    //       return "[중]출하";
    //     } else if (user.locale === "spanish") {
    //       return "Envíos";
    //     }
    //   } else if (data === "50") {
    //     if (user.locale === "korean" || user.locale === "japanese" || user.locale === "chinese" || user.locale === "vietnam") {
    //       return "불령품보관(반품)";
    //     } else if (user.locale === "english") {
    //       return "Retain Rejects (Return)";
    //     } else if (user.locale === "spanish") {
    //       return "Retener Rechazos (Devolución)";
    //     }
    //   } else if (data === "80") {
    //     if (user.locale === "korean" || user.locale === "japanese" || user.locale === "chinese" || user.locale === "vietnam") {
    //       return "폐기";
    //     } else if (user.locale === "english") {
    //       return "Disposal";
    //     } else if (user.locale === "spanish") {
    //       return "Eliminación";
    //     }
    //   } else if (data === "90") {
    //     if (user.locale === "korean") {
    //       return "임가공";
    //     } else if (user.locale === "english") {
    //       return "Repacking";
    //     } else if (user.locale === "japanese") {
    //       return "流通加工";
    //     } else if (user.locale === "chinese" || user.locale === "vietnam") {
    //       return "[중]임가공";
    //     } else if (user.locale === "spanish") {
    //       return "Reembalaje";
    //     }
    //   } else {
    //     return "";
    //   }
    // }
    // if (type === "WMSMS080") {
    //   if (data === null) {
    //     if (user.locale === "korean" || user.locale === "english" || user.locale === "japanese" || user.locale === "chinese" || user.locale === "vietnam" || user.locale === "spanish") {
    //       return "선택";
    //     }
    //   }
    //   if (data === "10") {
    //     if (user.locale === "korean") {
    //       return "출고";
    //     } else if (user.locale === "english") {
    //       return "Shipping";
    //     } else if (user.locale === "japanese") {
    //       return "出庫";
    //     } else if (user.locale === "chinese" || user.locale === "vietnam") {
    //       return "[중]출고";
    //     } else if (user.locale === "spanish") {
    //       return "Transporte";
    //     }
    //   } else if (data === "20") {
    //     if (user.locale === "korean" || user.locale === "japanese" || user.locale === "chinese" || user.locale === "vietnam") {
    //       return "보관";
    //     } else if (user.locale === "english") {
    //       return "Storage";
    //     } else if (user.locale === "spanish") {
    //       return "Almacenamiento";
    //     }
    //   } else if (data === "40") {
    //     if (user.locale === "korean") {
    //       return "출하";
    //     } else if (user.locale === "english") {
    //       return "Shipping";
    //     } else if (user.locale === "japanese") {
    //       return "出荷";
    //     } else if (user.locale === "chinese" || user.locale === "vietnam") {
    //       return "[중]출하";
    //     } else if (user.locale === "spanish") {
    //       return "Envíos";
    //     }
    //   } else if (data === "50") {
    //     if (user.locale === "korean" || user.locale === "japanese" || user.locale === "chinese" || user.locale === "vietnam") {
    //       return "불령품보관(반품)";
    //     } else if (user.locale === "english") {
    //       return "Retain Rejects (Return)";
    //     } else if (user.locale === "spanish") {
    //       return "Retener Rechazos (Devolución)";
    //     }
    //   } else if (data === "80") {
    //     if (user.locale === "korean" || user.locale === "japanese" || user.locale === "chinese" || user.locale === "vietnam") {
    //       return "폐기";
    //     } else if (user.locale === "english") {
    //       return "Disposal";
    //     } else if (user.locale === "spanish") {
    //       return "Eliminación";
    //     }
    //   } else if (data === "90") {
    //     if (user.locale === "korean") {
    //       return "임가공";
    //     } else if (user.locale === "english") {
    //       return "Repacking";
    //     } else if (user.locale === "japanese") {
    //       return "流通加工";
    //     } else if (user.locale === "chinese" || user.locale === "vietnam") {
    //       return "[중]임가공";
    //     } else if (user.locale === "spanish") {
    //       return "Reembalaje";
    //     }
    //   } else if (data === "") {
    //     return "";
    //   } else if (data === "02" && field === "LOC_STAT") {
    //     if (user.locale === "korean" || user.locale === "japanese" || user.locale === "chinese" || user.locale === "vietnam") {
    //       return "사용";
    //     } else if (user.locale === "english") {
    //       return "Use";
    //     } else if (user.locale === "spanish") {
    //       return "Usar";
    //     }
    //   }
    //   if (data === "01" && field === "CARRY_TYPE") {
    //     if (user.locale === "korean" || user.locale === "japanese" || user.locale === "chinese" || user.locale === "vietnam") {
    //       return "랙";
    //     } else if (user.locale === "english") {
    //       return "Rack";
    //     } else if (user.locale === "spanish") {
    //       return "Estante";
    //     }
    //   } else if (data === "02" && field === "CARRY_TYPE") {
    //     if (user.locale === "korean" || user.locale === "japanese" || user.locale === "chinese" || user.locale === "vietnam") {
    //       return "평치";
    //     } else if (user.locale === "english") {
    //       return "Flat";
    //     } else if (user.locale === "spanish") {
    //       return "¡Flatos!";
    //     }
    //   }
    // }
    // if (type === "WMSST050_POPUP") {
    //   if (data === "01") {
    //     return "(+)";
    //   } else if (data === "02") {
    //     return "(-)";
    //   } else if (data === "03") {
    //     if (user.locale === "korean" || user.locale === "chinese" || user.locale === "japanese") {
    //       return "UOM변경";
    //     } else if (user.locale === "english") {
    //       return "Change UOM";
    //     } else if (user.locale === "spanish") {
    //       return "Cambio De UOM";
    //     }
    //   }
    // }
  }

  //TODO page별 별도 처리가 아닌 전체 영역에서 적용이 가능한 기능으로 변경
  /*
   * Grid 데이터 조회후 그리드 pinnedBottomRowData 객체 생성
   */
  static setGridPinnedBottomRowData(gridRowDataList: any, columnField: any, pagingSize?: any) {
    const pinnedBottomRowData = Object.assign({}, columnField);
    const filedKeys = Object.keys(pinnedBottomRowData);
    for (let j = 0; filedKeys.length > j; j++) {
      // *** 재고관리/일별재고 페이지들, 일별재고상세 페이지 pagingSize 개수만큼 계산한 합계 값을 필요로 함
      const exceptionalMenu = ["WMSST051_1", "WMSST051_2", "WMSST051_3", "WMSST051_4", "WMSST051_5", "WMSST055"];
      // pagingSize가 gridRowDataList.length보다 작은 경우에만 ***
      // if (useTabsStore().selectedTab.value?.id === "WMSST051_1" || "WMSST051_2" || "WMSST051_3" || "WMSST051_4" || "WMSST051_5 " || "WMSST055") {
      if (exceptionalMenu.includes(useTabsStore().selectedTab.value!.id)) {
        if (pagingSize < gridRowDataList.length) {
          gridRowDataList.length = pagingSize;
        }
      }
      for (let i = 0; gridRowDataList.length > i; i++) {
        if (String(gridRowDataList[i][filedKeys[j]]) !== "0") {
          pinnedBottomRowData[filedKeys[j]] += Number(gridRowDataList[i][filedKeys[j]]);
        }
      }
      // 소수점 2자리 까지만 노출
      if (
        String(pinnedBottomRowData[filedKeys[j]]).includes(".") &&
        Number(String(pinnedBottomRowData[filedKeys[j]]).substring(String(pinnedBottomRowData[filedKeys[j]]).indexOf(".") + 1, String(pinnedBottomRowData[filedKeys[j]]).length)) > 2
      ) {
        pinnedBottomRowData[filedKeys[j]] = Number(pinnedBottomRowData[filedKeys[j]].toFixed(2));
      }
      // 1000단위 콤마
      // pinnedBottomRowData[filedKeys[j]] = Number(pinnedBottomRowData[filedKeys[j]]).toLocaleString();
    }
    return pinnedBottomRowData;
  }

  /*
   * 로우가 selected될때 해당 로우의 editable = true 인 컬럼의 상태를 editing으로 변경해준다.
   * isSingleSelected : 한번에 모든 행이 아닌 특정 1개의 행만에 대해 핸들링을 할 때
   */
  static setGridStartEditing(selectedItems: any, gridApi: any, disabledEditColumn?: any, isSingleSelected?: any, componentName?: string) {
    if (isSingleSelected) {
      const columns = gridApi?.getModel().columnModel.displayedColumns;
      for (let i = 0; columns?.length > i; i++) {
        if (Object.hasOwn(columns[i].colDef, "editable")) {
          const editColumn = { rowIndex: isSingleSelected.rowIndex, colKey: columns[i].colDef.field, rowPinned: null };

          gridApi?.startEditingCell(editColumn);
        }
      }
      for (let i = 0; columns?.length > i; i++) {
        if (!Object.hasOwn(columns[i].colDef, "pinned")) {
          gridApi.ensureColumnVisible(columns[i]);
          return;
        }
      }
      return;
    }

    if (selectedItems.length > 0) {
      const columns = gridApi?.getModel().columnModel.displayedColumns;
      for (let i = 0; selectedItems.length > i; i++) {
        const index = selectedItems[i].rowIndex;

        // disabledEditColumn : 신규일때만 edit상태가 활성화되는 edit컬럼 field값 배열
        for (let j = 0; columns?.length > j; j++) {
          if (disabledEditColumn && !String(selectedItems[i].data.RNUM).includes("temp") && componentName === "TMSYS040/form") {
            for (let k = 0; disabledEditColumn.length > k; k++) {
              if (columns[j].colDef.field !== disabledEditColumn[k]) {
                if (Object.hasOwn(columns[j].colDef, "editable")) {
                  const editColumn = { rowIndex: index, colKey: columns[j].colDef.field };
                  gridApi?.startEditingCell(editColumn);
                }
              }
            }
          } else if (disabledEditColumn && componentName === "wmsop910_3-new-popup" && Number(selectedItems[i].data.WORK_STAT || 0) >= 230) {
            if (!disabledEditColumn.includes(columns[j].colDef.field)) {
              if (Object.hasOwn(columns[j].colDef, "editable")) {
                const editColumn = { rowIndex: index, colKey: columns[j].colDef.field };
                gridApi?.startEditingCell(editColumn);
              }
            }
          } else {
            if (Object.hasOwn(columns[j].colDef, "editable")) {
              const editColumn = { rowIndex: index, colKey: columns[j].colDef.field, rowPinned: null };
              gridApi?.startEditingCell(editColumn);
            }
          }
        }
      }

      // edit시 횡이동 막기, 기준이 될 행 구하기(pinned가 없는 행중 가장 낮은 인덱스의 행)
      // for (let l = 0; columns?.length > l; l++) {
      //   if (!columns[l].colDef.hasOwnProperty("pinned")) {
      //     gridApi.ensureColumnVisible(columns[l]);
      //     return;
      //   }
      // }
    }
  }

  /*
   * api로 불러온 options들 중에서 일부만 보여주는 경우
   */
  static setAvailableOptions(apiResponseOptions: any, availableOptionsInfo: IAvailableOptionsUtilParams[]) {
    for (let i = 0; availableOptionsInfo.length > i; i++) {
      const filteredOptions = apiResponseOptions[availableOptionsInfo[i].inputId].filter((option: any) => {
        if (availableOptionsInfo[i].availableList.includes(option.code)) {
          return option;
        }
      });
      apiResponseOptions[availableOptionsInfo[i].inputId] = filteredOptions;
    }
  }

  /**
   * Object key, value 값을 비교한다.
   */
  static compareObjectKeyValue(obj1: any, obj2: any) {
    if (Object.entries(obj1).toString() === Object.entries(obj2).toString()) {
      return true;
    } else {
      return false;
    }
  }

  /**
   * 필수 입력 열에 대한 유효성 검사
   * (headerClass: "header-require")
   *
   * @param gridApi
   * @returns
   */
  static saveEditGridRequiredValidation(gridApi: any) {
    const rowDataList = gridApi.getSelectedNodes();

    const result = { key: "", name: "", line: null, status: true } as { [key: string]: any };

    const requireColumns = gridApi?.getModel().columnModel.displayedColumns.filter((column: any) => {
      if (Object.hasOwn(column.colDef, "headerClass") && column.colDef.headerClass === "header-require") {
        return column;
      }
    });

    for (let i = 0; rowDataList.length > i; i++) {
      for (let j = 0; requireColumns.length > j; j++) {
        if (!rowDataList[i].data[requireColumns[j].colDef.field]) {
          result.key = requireColumns[j].colDef.field;
          result.name = requireColumns[j].colDef.headerName;
          result.line = Number(rowDataList[i].rowIndex) + 1;
          result.status = false;
          // result.status = false;일때 key에 해당되는 컬럼으로 그리드 뷰 이동, 포커스
          if (!result.status) {
            const columns = gridApi?.getModel().columnModel.displayedColumns;
            for (let l = 0; columns?.length > l; l++) {
              if (columns[l].colDef.field === result.key) {
                gridApi.ensureColumnVisible(columns[l].colDef.field, "middle");
                gridApi.setFocusedCell(rowDataList[i].rowIndex, columns[l].colDef.field);
              }
            }
          }
          return result;
        }
      }
    }

    return result;
  }

  static getGridRowSpanRule(key: string | number, gridRowDataList: any[], startIndex: number) {
    const RowSpanRule: any = {};
    gridRowDataList.forEach((data: any, index: number) => {
      if (!RowSpanRule[`${data[key]}_${startIndex}`]) {
        RowSpanRule[`${data[key]}_${startIndex}`] = {
          data: data[key],
          count: 0,
          startIndex: index + startIndex,
        };
      }
      RowSpanRule[`${data[key]}_${startIndex}`].count++;
    });

    return RowSpanRule;
  }

  // 현재 날짜 리턴해주는 함수, yyyy-mm-dd 형식
  static getToday() {
    const today = new Date();

    const year: any = today.getFullYear(); // 년도
    const month: any = String(today.getMonth() + 1).length === 1 ? `0${today.getMonth() + 1}` : today.getMonth() + 1; // 월
    const date: any = String(today.getDate()).length === 1 ? `0${today.getDate()}` : today.getDate(); // 날짜
    return `${year}-${month}-${date}`;
  }

  // 현재 날짜 리턴해주는 함수, yyyy-mm-dd hh mm ss 형식
  static getTodayHhMmSs() {
    const today = new Date();

    const year: any = today.getFullYear(); // 년도
    const month: any = String(today.getMonth() + 1).length === 1 ? `0${today.getMonth()}` : today.getMonth() + 1; // 월
    const date: any = String(today.getDate()).length === 1 ? `0${today.getDate()}` : today.getDate(); // 날짜
    const currentHour = today.getHours();
    const currentMinute = today.getMinutes();
    const currentSecond = today.getSeconds();
    return `${year}-${month}-${date} ${currentHour}:${currentMinute}:${currentSecond}`;
  }

  // timestamp를 yyyy-mm-dd hh-mm-ss 형식으로 리턴
  static convertTimestampToDateFormat(timestamp: string) {
    const dateFormat = new Date(timestamp);

    let month: string | number = dateFormat.getMonth() + 1;
    let day: string | number = dateFormat.getDate();
    let hour: string | number = dateFormat.getHours();
    let minute: string | number = dateFormat.getMinutes();
    let second: string | number = dateFormat.getSeconds();

    month = month >= 10 ? month : "0" + month;
    day = day >= 10 ? day : "0" + day;
    hour = hour >= 10 ? hour : "0" + hour;
    minute = minute >= 10 ? minute : "0" + minute;
    second = second >= 10 ? second : "0" + second;

    //통계관리 - LOT별 재고현황 - '생성일자' 컬럼 변환 형식이 다름
    if (useTabsStore().selectedTab.value?.id === "WMSTG070") {
      return dateFormat.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second + ".0";
    } else {
      return dateFormat.getFullYear() + "-" + month + "-" + day + " " + hour + ":" + minute + ":" + second;
    }
  }

  /**
   * openWindowPopupBySearchModal
   */
  static openWindowPopupBySearchModal(page: string, option = { width: 1000, height: 850, top: 100, left: 200 }) {
    if (page === "WMSCM083") {
      // 기준관리 - 상품정보관리 -> INFO 영역의 '기본입고존', '기본출고존' 의 '신규' 팝업
      const name = "new-zone";
      if (screen.height > 850) {
        option.height = 850;
      } else {
        option.height += screen.height;
      }
      const windowOption = { name, option: "" };
      UtilService.setWindowSizeOption(windowOption, option);
      window.open("/new-zone", name, windowOption.option);
    }
  }

  /**
   * excelImport
   */
  static excelImport(info: any, name: string, option = { width: 850, height: 350, top: 100, left: 200 }) {
    if (info.templateUrl) {
      info.templateUrl = PREFIX + info.templateUrl;
    }
    const windowOption = { name, option: "" };
    UtilService.setWindowSizeOption(windowOption, option);
    localStorage.setItem("windowModal", JSON.stringify(info));
    window.open(name, name, windowOption.option);
  }

  /**
   * print 팝업
   */
  static printExport(url: string, name: string, option = { width: 1200, height: 800, top: 100, left: 200 }) {
    const windowOption = { url, name, option: "" };
    windowOption.url = PREFIX + windowOption.url;
    UtilService.setWindowSizeOption(windowOption, option);
    window.open(windowOption.url, windowOption.name, windowOption.option);
  }

  /**
   * Window 팝업 호출
   * - Barcode / 재고이동지시서 / 피킹 - 바코드출력 - 피킹리스트 팝업
   *
   * @param url
   * @param name
   * @param width
   * @param height
   * @param isOzReportUrl: 별도의 컴포넌트에 OzReport를 출력하거나, pdfViewer를 호출하는 경우 (true)
   * @returns
   */
  static cfn_openPop2(url: string, name: string, width: any, height: any, isOzReportUrl: boolean = true) {
    /**
     *
     * isOzReportUrl: boolean = false
     * 원본 소스에 바로 OzReport url로 팝업의 띄우는 경우와 ,
     * 별도의 컴포넌트를 사용해 view에 OzReport를 띄워주는 경우를 구분하기 위함 (pdfView.action 도 false로 지정).
     *
     */
    // Oz report 확인 시
    if (isOzReportUrl) {
      url = import.meta.env.VITE_OZ_URL + url;
    }

    if (UtilService.cfn_isUndefined(width)) {
      width = 616;
    }
    if (UtilService.cfn_isUndefined(height)) {
      height = 400;
    }

    let x = screen.availWidth;
    let y = screen.availHeight;
    let p;
    x = (x - width) / 2;
    y = (y - height) / 2;

    const status = "height=" + height + ",width=" + width + ",left=" + x + ",top=" + y + ",scrollbars=no,resizable=no,noresize";
    if (UtilService.cfn_isUndefined(name)) {
      p = window.open(url, "_blank", status);
    } else {
      p = window.open(url, name, status);
    }
    return p;
  }

  /*
   * template 관련 팝업    
  // 현재 로컬스토리지 -> 쿼리스트링 형식으로 값 전달 방법 수정중 .. . 추후 완료시 openWindowPopup함수로 통일 예정
   */
  static templateImport(name: string, option = { width: 850, height: 450, top: 100, left: 200 }) {
    const windowOption = { name, option: "" };
    UtilService.setWindowSizeOption(windowOption, option);
    const templateImportPopup = window.open(name, name, windowOption.option);
    return templateImportPopup;
  }

  /**
   * 기타 윈도우 팝업 open
   *   // 현재 로컬스토리지 -> 쿼리스트링 형식으로 값 전달 방법 수정중 .. . 추후 완료시 openWindowPopup함수로 통일 예정
   */
  static openWindowPopup(info: any, name: string, option = { width: 850, height: 600, top: 100, left: 200 }, localStorageName = "windowModal") {
    if (info?.saveUrl) {
      info.saveUrl = PREFIX + info.saveUrl;
    }
    const windowOption = { name, option: "" };
    UtilService.setWindowSizeOption(windowOption, option);
    localStorage.setItem(localStorageName, JSON.stringify(info));
    const openWindowPopup = window.open(name, name, windowOption.option);

    return openWindowPopup;
  }

  // 출력 window 실행 IE 일때
  //! IE 고려하지 않음 삭제 예정
  static previewIe_print(type?: string, printGb?: number) {
    if (type === "WMSTG070") {
      const printContent = document.getElementById("dataTable") as any;
      const windowUrl = "about:blank";
      const uniqueName = new Date();
      const windowName = "Print" + uniqueName.getTime();
      let windowSize = "width=780,height=800,scrollbars=1";
      if (printGb == 4) {
        alert("wow");
        windowSize = "width=980,height=800,scrollbars=1";
      }
      const printWindow = window.open(windowUrl, windowName, windowSize) as any;
      const printPreviewObject = '<object id="printPreviewElement" width=0 height=0 classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object>';

      printWindow.document.write(printContent.innerHTML);
      printWindow.document.write(printPreviewObject);
      printWindow.document.write("<script language=JavaScript>");
      printWindow.document.write("window.print();");
      printWindow.document.write("</" + "script>");
      printWindow.document.close();
      document.querySelectorAll("#dataTable")[0].innerHTML = "";
    } else {
      const printContent = document.getElementById("dataTable") as any;
      const windowUrl = "about:blank";
      const uniqueName = new Date();
      const windowName = "Print" + uniqueName.getTime();
      const printWindow = window.open(windowUrl, windowName, "width=780,height=800,scrollbars=1") as any;
      const printPreviewObject = '<object id="printPreviewElement" width=0 height=0 classid="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object>';

      printWindow.document.write(printContent.innerHTML);
      printWindow.document.write(printPreviewObject);
      printWindow.document.write("<script language=JavaScript>");
      printWindow.document.write("window.print();");
      printWindow.document.write("</" + "script>");
      printWindow.document.close();
      document.querySelectorAll("#dataTable")[0].innerHTML = "";
    }
  }

  // 바코드 등의 출력 팝업에서 사용
  static setComma(strParam: number) {
    let retValue = "";

    const str = String(strParam).replace(/,/g, "");

    for (let i = 1; i <= str.length; i++) {
      if (i > 1 && i % 3 == 1) {
        retValue = str.charAt(str.length - i) + "," + retValue;
      } else {
        retValue = str.charAt(str.length - i) + retValue;
      }
    }
    return retValue;
  }

  // static fn_getFooterData(footer) {
  //   var rowCount = footer.find("select").val();
  //   var currentPage = footer.find("input").val();
  //   var totalRowCount = footer.find("div[class='ui-paging-info-hidden']").text();
  //   var totalPage = footer.find("#sp_1_pjmap1").text();
  //   var res = {
  //     CurrentPage : currentPage,
  //     RowCount : rowCount,
  //     TotalRowCount : totalRowCount,
  //     TotalPage : totalPage,
  //   }
  //   return res;
  // }

  // info영역에 현재 스크롤 유무를 확인
  static checkInfoContainerHasScroll(infoContainerElement: HTMLElement | null, fullHeightElement: HTMLElement | null) {
    if (infoContainerElement && fullHeightElement) {
      setTimeout(() => {
        // 인풋영역 show / hidden 이벤트 트렌지션 때문에 setTimeout (css transition: all 0.5s)
        if (fullHeightElement) {
          if (fullHeightElement.scrollHeight > fullHeightElement.clientHeight) {
            infoContainerElement.classList.add("info-scroll-true");
          } else {
            infoContainerElement.classList.remove("info-scroll-true");
          }
        }
      }, 600);
    }
  }
  // info영역 스크롤이 최 하단에 위치할 때 more아이콘 제거
  static checkInfoContainerScrollPosition(infoContainerElement: HTMLElement | null, fullHeightElement: HTMLElement | null, remove?: "remove") {
    if (infoContainerElement && fullHeightElement) {
      const event = () => {
        if (fullHeightElement.scrollHeight - fullHeightElement.scrollTop - 30 < fullHeightElement.clientHeight) {
          infoContainerElement.classList.remove("info-scroll-true");
        } else {
          infoContainerElement.classList.add("info-scroll-true");
        }
      };
      if (remove) {
        fullHeightElement.removeEventListener("scroll", event);
      } else {
        fullHeightElement.addEventListener("scroll", event);
      }
    }
  }

  /**
   * 설    명 : 값의 앞뒤 공백을 제거하고 반환한다.
   * 인    자 : String
   * 반 환 값 : String - 좌/우측 공백이 제거된 문자열
   * 사 용 예 : alert("["+cfn_trim("    좌/우 공백 모두 제거됨     ")+"]");
   */
  static cfn_trim(a: any) {
    if (UtilService.cfn_isObject(a) && !UtilService.cfn_isArray(a) && !UtilService.cfn_isFunction(a)) {
      return a.value.replace(/(^\s*)|(\s*$)/g, "");
    } else if (UtilService.cfn_isString(a) || UtilService.cfn_isNumber(a)) {
      return a.replace(/(^\s*)|(\s*$)/g, "");
    } else {
      return "";
    }
  }

  /* 피킹확정(490)이후에는 피킹 불가  */
  static cfn_shouldNotPicking(workStat: any) {
    if (workStat > 490) return true;
    return false;
  }

  /* 출고완료(990) 이후에는 출고완료 불가   */
  static cfn_shouldNotConfirmOut(workStat: any) {
    if (workStat >= 990) return true;
    return false;
  }

  /* 컨테이너번호 유무 확인 */
  static fn_autoBestLocSaveValidation(selectedItems: any) {
    const lcId = UtilService.getCurrentLocation();
    //특정물류센터만 컨테이너 유무 확인을 함
    if (lcId == "0000001120" || lcId == "0000001202" || lcId == "0000001140" || lcId == "0000001260") {
      if (!UtilService.isEmptyObject(selectedItems.value)) {
        let tmpVal;
        for (let i = 0; i < selectedItems.value.length; i++) {
          //ASN_YN 이 Y 일 때만
          if (UtilService.cfn_trim(selectedItems.value[i]["ASN_YN"].toString()) == "Y") {
            const CNTR_NO = UtilService.cfn_trim(selectedItems.value[i]["CNTR_NO"].toString());
            /* 공백체크 */
            tmpVal = [CNTR_NO];
            for (let j = 0; j < tmpVal.length; j++) {
              if (tmpVal[j].trim() == "") {
                return false;
              }
            }
          }
        }
      }
    }
    return true;
  }

  /*출고관리*/
  /* 주문승인(190)이후에는 승인불가    */
  static cfn_shouldNotApprove(workStat: any, vrType: any) {
    if (workStat >= 190 && vrType != "N") {
      return true;
    }
    if (workStat == 190 && vrType == "N") {
      //주문승인 취소 일 경우 예외 처리
      return false;
    }
    return false;
  }

  // 윈도우 팝업에서 저장, 삭제 이벤트 이후 팝업이 닫히고, 부모창에서 그리드 검색 api호출
  static getGridDataAfterClosedPopup(popupObject: any) {
    popupObject.onbeforeunload = function () {
      window.addEventListener("message", handleMessage);
      function handleMessage(event: any) {
        if (event.data === true) {
          useSearchConditionStore().setAvailableSearchBySearchCondition(true);
        }
        // 이벤트 핸들러 제거
        window.removeEventListener("message", handleMessage);
      }
    };
  }

  // 자식창에서 부모창으로 넘겨줄 opener.postMessage셋팅(getGridDataAfterClosedPopup와 상호작용)
  static setOpenerPostMessageForGetGridData() {
    window.opener.postMessage(true, "*");
  }
  static setGridGroupSelectsChildrenProperty(gridApi: any, value: boolean) {
    gridApi.__updateProperty("groupSelectsChildren", value, false);
  }

  static setGridData(
    gridType: "client-side" | "server-side",
    gridApi: any,
    gridData: { gridDataArray: any[]; totalCount?: number } | null,
    params?: any,
    toast?: { toastStatus: "warning" | "success"; toastText: any } | null
  ) {
    if (gridType === "server-side") {
      if (gridData) {
        gridApi.__updateProperty("groupSelectsChildren", true, false);
        params.success({ rowData: gridData.gridDataArray, rowCount: gridData.totalCount });
      } else {
        gridApi.__updateProperty("groupSelectsChildren", false, false);
        params.success({ rowData: [], rowCount: 0 });
      }
    }

    // 검색 동작 이후 toast를 띄워주는 경우
    if (toast) {
      if (toast.toastStatus === "warning") {
        useToast().warning(toast.toastText);
      }
    }
  }

  static setUseSearchConditionStoreWhenResetValueIsTrue() {
    useSearchConditionStore().setResetValue(false);
    useSearchConditionStore().setAvailableSearchBySearchCondition(false);
    // useOptionDataStore().setAvailableWatchBySearchCondition(true);
  }

  // 그리드 컬럼 너비 자동맞춤 관련 유틸 (그리드 컬럼 자동맞춤에 대한 조건 다시한번 재확인후 코드 작성 필요 12.27 조성홍)
  static gridSizeColumnsToFit(type: "closeSubMenu" | "windowResize" | "onMount" | "unMount", gridApi?: any) {
    // subMenu가 닫힐때
    // window resize
    // onMount
    // unMount
  }

  // 권한관리에 맞춰 menu 데이터 재설정
  static async setMenusData() {
    const mainMenus = { value: winusMenus as IWinusMenu[] };
    const menusData = await ApiService.postApi("/menuData_rn.action", {}, {});

    const addMenus = [] as any;
    // 하드코딩된 전체 메뉴
    winusMenus.forEach((mainMenu: any) => {
      mainMenu.subMenus.forEach((sMenu: any) => {
        // 서버에서 권한별로 받아온 메뉴
        menusData.result["SS_MENU_INFO"].forEach((menu: any) => {
          // 메뉴 하위의 페이지일 경우
          if (menu["LV"] === 3 && sMenu.id.includes(menu["KEY"])) {
            // 한 페이지에 여러탭으로 나뉘어진 페이지가 있을 경우, 새개발에서는 여러페이지로(..._1, ..._2) 나뉘었기 때문에 구별.
            if (sMenu.id.length > menu["KEY"].length) {
              const addMenu = {} as any;
              addMenu.id = sMenu.id;
              addMenu.path = sMenu.path;
              addMenu.hidden = sMenu.hidden;
              addMenu.title = sMenu.title;
              addMenu["LV"] = menu["LV"];
              addMenu["KEY"] = menu["KEY"];
              addMenu["MENU_ID"] = menu["MENU_ID"];
              addMenu["DEL_AUTH"] = menu["DEL_AUTH"]; // 삭제
              addMenu["EXC_AUTH"] = menu["EXC_AUTH"]; // 엑셀, 양식다운로드
              addMenu["INS_AUTH"] = menu["INS_AUTH"]; // 입력 (엑셀 업로드, 저장, 신규 등)
              addMenu["PRT_AUTH"] = menu["PRT_AUTH"]; // 출력
              addMenu["SER_AUTH"] = menu["SER_AUTH"]; // 조회
              addMenu["UPD_AUTH"] = menu["UPD_AUTH"]; // 저장
              addMenus.push(addMenu);
            } else {
              menu.path = sMenu.path;
              menu.hidden = sMenu.hidden;
            }
          }

          if (menu["KEY"] === "WMSOM") {
            menu.id = "order";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSOP") {
            menu.id = "operation";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSST") {
            menu.id = "stock";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSTG") {
            menu.id = "statistics";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSMS") {
            menu.id = "master-data";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSDL") {
            menu.id = "delivery";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSIT") {
            menu.id = "inspection";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "TMSYS") {
            menu.id = "setting";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSAC") {
            menu.id = "cost";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSDH") {
            menu.id = "dashboard";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSPM") {
            menu.id = "packing";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSPS") {
            menu.id = "packing-2";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSPK") {
            menu.id = "packing-3";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSMO") {
            menu.id = "mobile";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSPD") {
            menu.id = "production";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSIF") {
            menu.id = "interface";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSWCS") {
            menu.id = "wcs";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          } else if (menu["KEY"] === "WMSCT") {
            menu.id = "client";
            menu.title = menu["KEY_NAME"];
            menu.tooMany = true;
          }
        });
      });
    });
    menusData.result["SS_MENU_INFO"].push(...addMenus);
    const resultMenus = menusData.result["SS_MENU_INFO"].reduce((result: any, item: any) => {
      if (item["LV"] === 2) {
        result.push({ ...item, subMenus: [] });
      } else if (item["LV"] === 3) {
        const parent = result.find((parentItem: any) => parentItem["KEY"] === item["MENU_ID"]);
        if (parent) {
          if (!item.id && !item.title) {
            item.id = item["KEY"];
            item.title = item["KEY_NAME"];
          }
          parent.subMenus.push(item);
        }
      }
      return result;
    }, []);

    mainMenus.value = [];
    for (const mainMenu of resultMenus) {
      mainMenu.subMenus = mainMenu.subMenus.filter((subMenu: any) => !subMenu.hidden && !(subMenu.hidden === undefined));
      if (mainMenu.subMenus.length > 0) {
        mainMenus.value.push(mainMenu);
      }
    }
    return mainMenus.value;
  }

  // 버튼 속성에 권한에따라 display 여부를 결정한다. ('Y' or 'N') + 페이지 권한 정보 return
  static getAuthOnButton(pageKey: string, pageId: string, menus: any, ctnButtons?: any) {
    let subMenu = null as any;
    /*
      세션 만료후 새로고침시 error페이지로 이동되고, menus가 없는데 getAuthOnButton유틸을 사용하는 인터벌이 계속 실행되어서 에러가 뜸
    */
    if (!menus) {
      return;
    }
    for (const menu of menus) {
      if (menu["KEY"] === pageKey) {
        subMenu = menu.subMenus.find((subMenu: any) => subMenu.id === pageId);
      }
    }

    if (!UtilService.isEmptyArray(ctnButtons?.value)) {
      ctnButtons?.value.forEach((item: any) => {
        if (Object.hasOwn(subMenu, item.authType)) {
          item.display = subMenu[item.authType];
        } else if (!item.authType) {
          item.display = "Y";
        }
      });
    }

    return subMenu;
  }

  static getSearchOptionInLocalStorage() {
    const userId = JSON.parse(userStore().getCurrentUser()).USER_ID;
    const tab = useTabsStore().selectedTab.value?.id;
    const searchOptionInLocalStorage = localStorage.getItem("userSearchOption" + userId + tab);

    if (searchOptionInLocalStorage) {
      return JSON.parse(searchOptionInLocalStorage);
    } else {
      return null;
    }
  }

  /**
   * '검색조건 사용자설정' 사용 시 검색조건 정보(input.ts) 변경되었을 때 해당 페이지 mount될 때 변경사항 반영
   * @param SEARCH_INPUT                : input.ts에 정의된 검색조건 정보
   * @param searchOptionInLocalStorage  : 로컬스토리지에 저장된 검색 option 정보
   * @returns                           : input.ts에 정의된 검색조건 정보에 로컬스토리지의 hidden속성 정보를 합친 검색조건 정보
   */
  static setSearchInputChanges(SEARCH_INPUT: ISearchInput[], searchOptionInLocalStorage: ISearchInput[]) {
    const userId = JSON.parse(userStore().getCurrentUser()).USER_ID;
    const tab = useTabsStore().selectedTab.value?.id;

    const searchInput = JSON.parse(JSON.stringify(SEARCH_INPUT));
    for (let i = 0; searchOptionInLocalStorage.length > i; i++) {
      //로컬스토리지와 searchInput의 hidden 속성 비교
      if (searchOptionInLocalStorage[i]["hidden"] !== searchInput[i]["hidden"]) {
        searchInput[i]["hidden"] = searchOptionInLocalStorage[i]["hidden"];
        //로컬스토리지를 통해 업데이트 된 hidden속성을 포함한 searchInput정보를 로컬스토리지에 저장
        localStorage.setItem("userSearchOption" + userId + tab, JSON.stringify(searchInput));
      }
    }
    return searchInput;
  }

  /**
   * code, name을 식별하는 key값을 반환
   * - OptionStore에 적재되는 데이터 목록의 (code, name)를 구분하는 key 명칭이 다름.
   *
   * @param optionList
   * @returns
   */
  static getOptionKey(optionList: any[], codeAlias: string[] = [], nameAlias: string[] = []) {
    codeAlias = [...codeAlias, ...["code", "CODE_CD", "value", "CODE"]];
    nameAlias = [...nameAlias, ...["codeNm", "CODE_NM", "name", "NAME"]];

    /** Default */
    if (UtilService.isEmptyArray(optionList)) {
      return {
        codeKey: "CODE_CD",
        nameKey: "CODE_NM",
      };
    }

    const codeKey: string = codeAlias[codeAlias.findIndex((dt: any) => Object.keys(optionList[optionList.length - 1]).includes(dt))];
    const nameKey: string = nameAlias[nameAlias.findIndex((dt: any) => Object.keys(optionList[optionList.length - 1]).includes(dt))];

    return {
      codeKey,
      nameKey,
    };
  }

  /**
   * 선택 목록에 바인딩될 데이터셋의 key-value 별칭을 변경한다.
   * (OptionStore에 적재되는 데이터 목록의 (code, name)를 구분하는 key 명칭이 다름.)
   *
   * TODO :: BE 서비스에서 공통 코드 형태로 반환하는 서비스 필요
   *
   * @param dataList
   * @param originKey
   * @param originName
   */
  static convertOptionDataSet(dataList: [], originCode: string, originName: string) {
    let result = new Array();

    result = dataList.map((dt: any) => {
      dt["CODE_CD"] = dt[originCode];
      dt["CODE_NM"] = dt[originName];

      return dt;
    });

    return result;
  }

  /**
   * 사용자 계정 권한 레벨 조회
   * ('MA': 시스템관리자)
   *
   * @returns
   */
  static getUserAuth() {
    const userInfo = JSON.parse(userStore().getCurrentUser());

    return userInfo["AUTH_CD"];
  }

  static setWindowSizeOption(windowOption: any, option: any) {
    windowOption.option += "width = " + option.width;
    windowOption.option += ", height = " + option.height;
    windowOption.option += ", top = " + option.top;
    windowOption.option += ", left = " + option.left;
    windowOption.option += ", location = no";
  }

  static cfn_isUndefined(a: any) {
    return typeof a == "undefined";
  }

  /**
 * 설    명 : 객체일 경우 true값을 반환한다. 문자열이나, 숫자, 배열, 함수,
								 Boolean, null 이나 undefined인 경우 false를 반환한다.
 * 인    자 : Object
 * 반 환 값 : Boolean true  - 객체일 경우
 *               Boolean false  - 문자열, 숫자, 함수, Boolean, null,
																	undefined일 경우
 * 사 용 예 : alert("배열은 객체가 아니다. : " + cfn_isObject(new Array()));
 */
  static cfn_isObject(a: any) {
    return typeof a == "object" && !!a && !UtilService.cfn_isFunction(a);
  }

  /**
   * 설    명 : Array 생성자나 [] 배열 문장으로 만든 배열인지 여부를 체크하고
   *               배열일 경우 true값을 반환한다.
   * 인    자 : Object
   * 반 환 값 : Boolean True  - 배열이면
   *               Boolean False  - 배열이 아니면
   * 사 용 예 : var arr = new Array();
   *               alert("arr은 배열이다 : " + cfn_isArray(arr));
   */
  static cfn_isArray(a: any) {
    return Object.prototype.toString.call(a) == "[object Array]";
  }

  /**
   * 설    명 : 함수인지 확인하고 함수이면 true값을 반환한다.
   *               IE 7, firefox 3.1, chrome 동작 확인
   * 인    자 : Object
   * 반 환 값 : Boolean true  - 함수이면
   *               Boolean false  - 함수가 아니면
   * 사 용 예 : alert("cfn_isEmpty()는 함수이다 " + cfn_isFunction(cfn_isEmpty));
   */
  static cfn_isFunction(a: any) {
    return typeof a == "function";
  }

  /**
   * 설    명 : 문자열일 경우 true를 반환한다.
   * 인    자 : Object
   * 반 환 값 : Boolean true  - 문자열이면
   *               Boolean false  - 문자열이이 아니면
   * 사 용 예 : alert("\"1234\"는 문자열이다 :" + cfn_isString("1234"));
   */
  static cfn_isString(a: any) {
    return typeof a == "string";
  }

  /**
* 설    명 : 유한수일 경우 true를 반환한다. NaN이나 무한수일 경우는 false를
				반환한다. 또한, 숫자로 변환 가능한 문자열이라 할지라도 false값을
				반환한다.
* 인    자 : Object
* 반 환 값 : Boolean true  - 숫자이면
*               Boolean false  - 숫자가 아니면
* 사 용 예 : alert("숫자이다 : " + cfn_isNumber(0123));
*/
  static cfn_isNumber(a: any) {
    return typeof a == "number" && isFinite(a);
  }

  /**
   * 1. 이    름 : cfn_checkString
   * 2. 설    명 : 불필요한 html태그공격에 대비하기위해서...급조(특수문자입력체크)
   * 3. 인    자 : String sInput
   * 4. 반 환 값 :	Boolean true
   * 				Boolean false
   * 5. 사 용 예 :
   * 6. 변경사항 :
   *      변경일          변경자    		변경내용
   *    --------------------------------------------------------------------------
   *      2007. 01. 21   indigo
   */
  static cfn_checkString(sInput: string) {
    const _noInputTag = "~!=#$%&*+@^`'{|}><[];:\\" + "\n" + "\r";
    if (!UtilService.checkType(sInput, _noInputTag)) {
      useToast().warning(t("common-toast.special_charater_not_allowed"));
      return false;
    }
    return true;
  }
  /**
   * 1. 이    름 : _checkType
   * 2. 설    명 :
   * 3. 인    자 :	String s
   * 				String spc
   * 4. 반 환 값 :	Boolean true
   * 				Boolean false
   * 5. 사 용 예 :
   * 6. 변경사항 :
   *      변경일          변경자    		변경내용
   *    --------------------------------------------------------------------------
   *      2007. 01. 21   indigo
   */
  static checkType(s: string, spc: string) {
    let i;
    for (i = 0; i < s.length; i++) {
      if (spc.indexOf(s.substring(i, i + 1)) > 0) {
        return false;
      }
    }
    return true;
  }

  /**
   * 1. 이    름 : getByteLength
   * 2. 설    명 :
   * 3. 인    자 :	String str
   * 4. 반 환 값 :	Number string의 byte길이
   * 5. 사 용 예 :
   */
  static getByteLength(str: string): number {
    const encoder = new TextEncoder();
    const encoded = encoder.encode(str);
    return encoded.length;
  }
}
