/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	i18n.ts
 *  Description:    다국어 기능을 제공하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
// @ts-ignore
import { createI18n } from "vue-i18n";
import messages from "@intlify/unplugin-vue-i18n/messages";

export const allLocales = [
  {name: 'korean', value: '0'},
  {name: 'english', value: '3'},
  {name: 'japanese', value: '1'},
  {name: 'chinese', value: '2'},
  {name: 'vietnam', value: '4'},
  {name: 'spanish', value: '5'}
];

export const i18n = createI18n({
  legacy: false,
  globalInjection: true,
  locale: localStorage.getItem('winusUserInfo') ? JSON.parse(JSON.parse(localStorage.getItem('winusUserInfo')!).userDetail).locale : 'korean',
  fallbackLocale: "english",
  messages: messages,
});

export async function setLocale(locale: string) {
  if (!i18n.global.availableLocales.includes(locale)) {
    const messages = await loadLocale(locale);
    if (messages === undefined) {
      return;
    }
    i18n.global.setLocaleMessage(locale, messages);
  }
  i18n.global.locale.value = locale;
}

async function loadLocale(locale: string) {
  try {
    const response = await fetch(`/src/locales/${locale}.json`);
    if (!response.ok) {
      throw new Error("Something went wrong!");
    }
    return response.json();
  } catch (error) {
    console.error(error);
  }
}
