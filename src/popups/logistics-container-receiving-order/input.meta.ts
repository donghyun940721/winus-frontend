/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	logistics-container-receiving-order/input.meta.ts
 *  Description:    입고관리 신규 팝업 메타 파일
 *  Authors:        dhkim
 *  Update History:
 *                  2023.08. : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import { orderGridMetaData, orderGridOptionsMeta } from "./input.grid";
import { ORDER_SEARCH_CONTAINER_META } from "./input.search";

export * from "./input.grid";
export * from "./input.search";

export const commonSetting = {
  authPageGroup: "WMSOP",
  authPageId: "WMSOP910_1",
  serAuthField: "SER_AUTH",
};

//#region ::  입고주문 탭

export const ORDER_SEARCH_META = {
  ...ORDER_SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const ORDER_GRID_META = {
  ...orderGridMetaData,
  ...commonSetting,
};

export const ORDER_GRID_OPTIONS_META = {
  ...orderGridOptionsMeta,
};

//#endregion
