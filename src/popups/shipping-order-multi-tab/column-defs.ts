import gridSearchButton from "@/components/renderer/grid-search-button.vue";
import gridSelectBox from "@/components/renderer/grid-select-box.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { ICellRendererSearchTextInputParams } from "@/types";
import { IColDef } from "@/types/agGrid";

export const ONLY_GRID_VIEW_COLUMN_DEFS: any = [
  {
    field: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1,
  },
  {
    field: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "POOL_NM",
    headerKey: "pool-nm",
    sortable: true,
  },
  {
    field: "POOL_GRP_NM",
    headerKey: "pool-grp-id",
    sortable: true,
  },

  {
    field: "RTI_EPC_CD",
    headerKey: "company-epc-code",
    sortable: true,
  },
  {
    field: "ITEM_QTY",
    headerKey: "product-qty",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "BOX_QTY",
    headerKey: "box-quantity",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "PLT_WGT",
    headerKey: "receiving-weight",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
];

export const SIMPLE_ORDER_MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerName: "No",
      width: 50,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      maxWidth: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "shipping-warehouse": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 50,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },

    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "delivery-destination": [
    {
      field: "No",
      headerName: "No",
      cellStyle: { textAlign: "center" },
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      maxWidth: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
};
export const SIMPLE_ORDER_GRID_COLUMN_DEFS: IColDef[] = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
  },
  {
    field: "ITEM_GRP_ID",
    headerKey: "product-group",
    headerName: "",
    headerClass: "header-center",
    width: 140,
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
    headerClass: "header-center",
    width: 140,
  },
  {
    field: "ITEM_KOR_NM",
    headerKey: "product-name",
    headerName: "",
    headerClass: "header-center",
    width: 140,
  },
  {
    field: "STOCK_QTY",
    headerKey: "current-stock",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 140,
  },
  {
    field: "IN_ORD_QTY",
    headerKey: "ord-qty",
    headerName: "",
    headerClass: "header-require",
    width: 140,
    cellStyle: { textAlign: "right" },
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "BAD_QTY",
    headerKey: "inferior-product",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 140,
  },
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    headerClass: "header-center",
    width: 140,
  },
  {
    field: "UNIT_PRICE",
    headerKey: "unit-price",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 140,
  },
  {
    field: "WH_NM",
    headerKey: "warehouse",
    headerName: "",
    headerClass: "header-center",
    width: 140,
  },
];

/**
 *
 * 입고주문 화면
 *
 */

export const ORDER_MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerName: "No",
      width: 50,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "shipping-warehouse": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 50,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "delivery-destination": [
    {
      field: "No",
      headerName: "No",
      cellStyle: { textAlign: "center" },
      width: 50,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      maxWidth: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const ORDER_TOP_GRID_COL_DEF: IColDef[] = [
  {
    field: "NO",
    headerKey: "no",
    headerName: "",
    width: 60,
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    pinned: "left",
  },
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    width: 130,
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    editable: true,
    width: 130,
    cellRenderer: gridTextInput,
    cellRendererParams: {
      autoCompleteKeys: ["CODE", "NAME", "UOM_NM", "UOM_NM", "ID", "UOM_ID", "UOM_ID"],
      searchModalInputKey: "vrSrchItemCd",
      focusOutEventKey: "vrSrchItemCd",
      srchKey: "ITEM",
      searchButtonFieldName: "ITEM",
      callModalId: "kit-product",
      defaultParamsData: {
        storeSaveKey: "owner",
        paramsKeys: ["vrSrchCustCd"],
        rowDataKeys: ["CUST_CD"],
      },
    } as ICellRendererSearchTextInputParams,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "ITEM",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 50,
    cellRenderer: gridSearchButton,
  },
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "OUT_ORD_QTY",
    headerKey: "ord-qty",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    editable: true,
    cellRenderer: gridTextInput,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    width: 130,
  },
  {
    field: "REAL_OUT_QTY",
    headerKey: "work-quantity",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "right" },
  },
  {
    field: "OUT_ORD_WEIGHT",
    headerKey: "order-weight",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    width: 130,
  },
  {
    field: "OUT_WORK_UOM_CD",
    headerKey: "uom",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "UOM",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 50,
    cellRenderer: gridSearchButton,
  },
  {
    field: "OUT_ORD_QTY",
    headerKey: "converted-qty",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "right" },
  },
  {
    field: "OUT_ORD_UOM_CD",
    headerKey: "representative-uom",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "EP_TYPE",
    headerKey: "delivery-type",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 130,
    cellRenderer: gridSelectBox,
  },
  {
    field: "UNIT_AMT",
    headerKey: "unit-price",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    width: 130,
  },
  {
    field: "REAL_BOX_QTY",
    headerKey: "box-quantity",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    width: 130,
  },
  {
    field: "REAL_PLT_QTY",
    headerKey: "plt-quantity",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    editable: true,
    cellRenderer: gridTextInput,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    width: 130,
  },
  {
    field: "CNTR_NO",
    headerKey: "container-number",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    editable: true,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "CNTR_TYPE",
    headerKey: "container-type",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridSelectBox,
  },
  {
    field: "CNTR_SEAL_NO",
    headerKey: "seal-no",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "ORD_DESC",
    headerKey: "remark-1",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    editable: true,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "ETC2",
    headerKey: "remark-2",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    editable: true,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    width: 130,
  },
];

export const ORDER_BOTTOM_GRID_COL_DEF: IColDef[] = [
  {
    field: "",
    headerKey: "no",
    headerName: "",
    width: 60,
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    pinned: "left",
  },
  {
    field: "LOC_NM",
    headerKey: "location-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "CONF_QTY",
    headerKey: "qty",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ITEM_DATE_END",
    headerKey: "product-expiration-date",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
];
