import { Format } from "@/lib/ag-grid/index";
import { UtilService } from "@/services/util-service";
import { IGridCellSearchButton, IGridCellSelectBox, IModal, ISearchInput } from "@/types";

export const SIMPLE_ORDER_CONTROL_BUTTON = [
  {
    title: "new-destinations",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    disabled: "",
  },
];

export const SIMPLE_ORDER_SEARCH_MODAL_INFO: any = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  "shipping-warehouse": {
    page: "WMSMS040",
    id: "shipping-warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_WH_CD",
        searchContainerInputId: "vrOutWhCd",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        searchContainerInputId: "vrOutWhNm",
        title: "warehouse-name",
        type: "text",
        width: "half",
      },
    ],
  },
  "delivery-destination": {
    page: "WMSCM011_2",
    id: "delivery-destination",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "2",
        S_LC_ALL: "LC_ALL",
        S_LC_ID: UtilService.getCurrentLocation(),
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "half",
        options: [{ name: "", nameKey: "alcustomerl", value: "2" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        title: "shipper-code",
        searchContainerInputId: "vrTransCustCd",
        type: "text",
        width: "half",
      },
      {
        id: "S_CUST_NM",
        title: "owner-name",
        searchContainerInputId: "vrTransCustNm",
        type: "text",
        width: "half",
      },
    ],
  },
};

export const SIMPLE_ORDER_SEARCH_INPUT: any = [
  {
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    ids: ["vrCalOutReqDt2"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "request-shipping-date",
    width: "triple",
    required: true,
    isSearch: false,
    types: ["date"],
  },
  {
    ids: ["vrOrdSubType"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "order-type",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      { value: "20", nameKey: "normal-warehousing", name: "" },
      { value: "22", nameKey: "return-receiving-defective", name: "" },
      { value: "23", nameKey: "return-receiving-normal", name: "" },
      { value: "25", nameKey: "receiving-as-stock", name: "" },
      { value: "111", nameKey: "incorrect-delivery-re-receiving", name: "" },
      { value: "137", nameKey: "collected-and-received", name: "" },
      { value: "138", nameKey: "misdelivery-receiving", name: "" },
      { value: "139", nameKey: "overdelivery-receiving", name: "" },
      { value: "158", nameKey: "online-return", name: "" },
      { value: "159", nameKey: "return-to-store", name: "" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    ids: ["vrTransCustCd", "vrTransCustNm"],
    hiddenId: "vrTransCustId",
    rowDataIds: ["CODE", "NAME"],
    rowDataHiddenId: "ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CLIENT",
    title: "delivery-destination",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    ids: ["vrOutWhCd", "vrOutWhNm"],
    hiddenId: "vrOutWhId",
    rowDataIds: ["WH_CD", "WH_NM"],
    rowDataHiddenId: "WH_ID",
    searchApiKeys: ["vrSrchWhCd", "vrSrchWhNm"],
    srchKey: "WH",
    title: "shipping-warehouse",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    ids: ["vrPayYn"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "pay-yn",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      { value: "Y", nameKey: "Y", name: "" },
      { value: "N", nameKey: "N", name: "" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 1, allowAutoSelected: true },
  },
  {
    ids: ["vrSrchItemGrp"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: ["", ""],
    srchKey: "",
    title: "product-group",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    optionsKey: "ITEMGRP",
    options: [
      {
        nameKey: "all",
        name: "",
        value: "",
      },
    ],
    types: ["select-box"],
  },
];

/**
 *
 *  출고주문 화면 정의
 *
 */

export const ORDER_SEARCH_INPUT: ISearchInput[] = [
  {
    // 화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    // 출고요청일
    ids: ["vrCalOutReqDt"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: ["", ""],
    srchKey: "",
    title: "request-shipping-date",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["date"],
  },
  {
    // 주문구분
    ids: ["vrOrdSubType"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "order-type-2",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchOrderPhase",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },

  {
    // 배송처
    ids: ["vrTransCustCd", "vrTransCustNm"],
    hiddenId: "vrTransCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    defaultSearchApiKeys: {
      custId: "OK",
      vrOrdType: "02",
    },
    title: "delivery-destination",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    // 출고창고
    ids: ["vrOutWhCd", "vrOutWhNm"],
    hiddenId: "vrOutWhId",
    rowDataIds: ["WH_CD", "WH_NM"],
    rowDataHiddenId: "WH_ID",
    searchApiKeys: ["vrSrchWhCd", "vrSrchWhNm"],
    srchKey: "WH",
    title: "shipping-warehouse",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    // 원주문번호
    ids: ["vrOrgOrdId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "original-order-number",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["vrPayYn"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "pay-yn",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsAutoSelected: { autoSelectedKeyIndex: 1, allowAutoSelected: true },
    options: [
      { name: "", nameKey: "Y", value: "Y" },
      { name: "", nameKey: "N", value: "N" },
    ],
  },
  {
    ids: ["vrKinOutYn"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "emergency-shipping-yn",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsAutoSelected: { autoSelectedKeyIndex: 1, allowAutoSelected: true },
    options: [
      { name: "", nameKey: "Y", value: "Y" },
      { name: "", nameKey: "N", value: "N" },
    ],
  },
  {
    ids: ["vrCalInReqDt"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "scheduled-receiving-date",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 컨테이너번호
    ids: ["vrCntrNoM"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "container-number",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // BL 번호
    ids: ["vrBlNo"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "BL-number",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["vrCntrSealNoM"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "seal-no",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["etaDt"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "eta",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date"],
  },
  {
    ids: ["etdDt"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "etd",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date"],
  },
  {
    ids: [""],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    isUnused: true,
    types: ["text"],
  },
];

export const ORDER_SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  "shipping-warehouse": {
    page: "WMSMS040",
    id: "shipping-warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040E3",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_WH_CD",
        searchContainerInputId: "vrOutWhCd",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        searchContainerInputId: "vrOutWhNm",
        title: "warehouse-name",
        type: "text",
        width: "half",
      },
    ],
  },
  "delivery-destination": {
    page: "WMSCM085",
    id: "delivery-destination",
    title: "search-customer",
    gridTitle: "customer-list",
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "2",
        S_LC_ALL: "LC_ALL",
        S_LC_ID: UtilService.getCurrentLocation(),
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },

    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["S_CUST_ID"],
      rowDataKeys: ["CUST_ID"],
    },

    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "", nameKey: "customer", value: "2" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrTransCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrTransCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  ITEM: {
    fieldList: ["ITEM_CODE", "RITEM_NM", "OUT_WORK_UOM_CD", "OUT_ORD_UOM_CD", "RITEM_ID", "UOM_ID", "OUT_ORD_UOM_ID"],
    rowDataKeys: ["ITEM_CODE", "ITEM_KOR_NM", "UOM_NM", "UOM_NM", "RITEM_ID", "UOM_ID", "UOM_ID"],
    callApiCondition: {
      dataType: "modal",
      key: "owner",
    },
    modalData: {
      page: "WMSCM091",
      id: "kit-product",
      title: "search-product",
      gridTitle: "product-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달

      defaultParamsData: {
        //해당 모달의 title
        storeSaveKey: "owner",
        paramsKeys: ["vrSrchCustCd"],
        rowDataKeys: ["CUST_CD"],
      },
      apis: {
        url: "/WMSCM091/listGrid_rn.action",
        params: {
          func: "fn_setWMSCM092E3",
          vrSrchCustCd: "",
          vrSrchWhId: "",
          vrViewSetItem: "",
          vrViewAll: "viewAll",
          vrColCustLotNo: "",
          vrSrchItemCd: "",
          vrSrchItemNm: "",
          vrSrchItemGrp: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchItemCd",
          title: "product-code",
          searchContainerInputId: "vrSrchItemCd",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemNm",
          searchContainerInputId: "vrSrchItemNm",
          title: "product-name",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemGrp",
          title: "product-group",
          type: "select",
          width: "half",
          optionsKey: "ITEMGRP",
          options: [{ name: "all", nameKey: "all", value: "" }],
        },
        {
          id: "vrSrchSetItemYn",
          title: "repacking",
          type: "select",
          width: "half",
          options: [
            { name: "", nameKey: "all", value: "" },
            { name: "useProductY", nameKey: "useProductY", value: "Y" },
            { name: "useProductN", nameKey: "useProductN", value: "N" },
          ],
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "CUST_LOT_NO",
        headerKey: "LOT_NO",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_CODE",
        headerKey: "product-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_KOR_NM",
        headerKey: "product-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "STOCK_QTY",
        headerKey: "current-stock",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "BAD_QTY",
        headerKey: "inferior-product",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "UOM_NM",
        headerKey: "uom",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "CONV_BOX_QTY",
        headerKey: "BOX-converted-quantity",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "CONV_PLT_QTY",
        headerKey: "plt-conversion-quantity",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "BOX_QTY",
        headerKey: "box-quantity",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "PLT_QTY",
        headerKey: "plt-quantity",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "UNIT_PRICE",
        headerKey: "unit-price",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
        valueFormatter: Format.NumberPrice,
      },
      {
        field: "ITEM_GRP_ID",
        headerKey: "product-group",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "WH_NM",
        headerKey: "warehouse-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "LOT_USE_YN",
        sortable: true,
        hide: true,
      },
      {
        field: "RITEM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "ITEM_ENG_NM",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "CUST_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_NM",
        sortable: true,
        hide: true,
      },
      {
        field: "TOTAL_STOCK_QTY",
        sortable: true,
        hide: true,
      },
    ],
  },
  UOM: {
    fieldList: ["IN_WORK_UOM_NM"],
    rowDataKeys: ["UOM_NM"],
    modalData: {
      page: "WMSCM100",
      id: "representative-uom",
      title: "search-uom",
      gridTitle: "uom-list",
      isCellRenderer: true,
      apis: {
        url: "/WMSCM100/list_rn.action",
        params: {
          func: "fn_setWMSCM100",
          UOM_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          vrSrchUomId: "",
          vrSrchUomNm: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchUomId",
          title: "uom-code",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchUomNm",
          title: "uom-name",
          type: "text",
          width: "half",
        },
      ],
    },
    colDef: [
      {
        field: "No",
        headerKey: "no",
        headerName: "",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "UOM_CD",
        headerKey: "uom-code",
        headerClass: "header-center",
      },
      {
        field: "UOM_NM",
        headerKey: "uom-name",
        headerClass: "header-center",
      },
      {
        field: "UOM_ID",
        hide: true,
      },
      {
        field: "UPD_NO",
        hide: true,
      },
      {
        field: "REG_NO",
        hide: true,
      },
      {
        field: "LC_ID",
        hide: true,
      },
    ],
  },
};

export const ORDER_CONTROL_BUTTON = [
  {
    title: "logistics-container-list",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    disabled: "",
  },
  {
    title: "new-destinations",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    disabled: "",
  },
  {
    title: "reset",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },

  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
  },
];

export const GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  EP_TYPE: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "select", value: "" },
      { name: "", nameKey: "10-ft", value: "01" },
      { name: "", nameKey: "20-ft", value: "02" },
      { name: "", nameKey: "40-ft", value: "03" },
      { name: "", nameKey: "40-fthq", value: "04" },
      { name: "", nameKey: "rf", value: "05" },
      { name: "", nameKey: "wing-body", value: "06" },
    ],
  },
  CNTR_TYPE: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "sortation", value: "SORTATION" },
      { name: "", nameKey: "hyper-flow", value: "HYPER_FLOW" },
      { name: "", nameKey: "stock", value: "STOCK" },
      { name: "", nameKey: "asn", value: "ASN" },
      { name: "", nameKey: "promotion-sorter", value: "PROMOTION_SORTER" },
      { name: "", nameKey: "promotion", value: "PROMOTION" },
      { name: "", nameKey: "dc", value: "dc" },
      { name: "", nameKey: "tc", value: "tc" },
      { name: "", nameKey: "sorter", value: "sorter" },
      { name: "", nameKey: "non-sorter", value: "sorter" },
      { name: "", nameKey: "dps", value: "DPS" },
    ],
  },
};
