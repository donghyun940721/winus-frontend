/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	logistics-container-receiving-order/input.search.ts
 *  Description:    입고관리 신규 팝업 메타 파일
 *  Authors:        dhkim
 *  Update History:
 *                  2023.08. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import type { ISearchInput } from "@/types";
import { ORDER_MODAL_COLUMN_DEFS } from "./column-defs.ts";

//#region ::  입고주문 탭

export const ORDER_SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    ids: ["calInDt"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "receiving-date",
    width: "triple",
    required: true,
    isSearch: false,
    isModal: false,
    types: ["date"],
  },
  {
    ids: ["vrBlNo"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "BL-number",
    width: "triple",
    required: false,
    isSearch: false,
    isModal: false,
    types: ["text"],
  },
  {
    ids: ["vrSrchOrderPhase"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "order-type-2",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["select-box"],
    options: [
      { value: "20", nameKey: "normal-warehousing", name: "" },
      { value: "22", nameKey: "return-receiving-defective", name: "" },
      { value: "23", nameKey: "return-receiving-normal", name: "" },
      { value: "25", nameKey: "receiving-as-stock", name: "" },
      { value: "111", nameKey: "incorrect-delivery-re-receiving", name: "" },
      { value: "137", nameKey: "collected-and-received", name: "" },
      { value: "138", nameKey: "misdelivery-receiving", name: "" },
      { value: "139", nameKey: "overdelivery-receiving", name: "" },
      { value: "158", nameKey: "online-return", name: "" },
      { value: "159", nameKey: "return-to-store", name: "" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    ids: ["vrInCustCd", "vrInCustNm"],
    hiddenId: "vrInCustId",
    rowDataIds: ["CODE", "NAME"],
    rowDataHiddenId: "ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    modalOpenCondition: "owner",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["srchSubCode5"],
      rowDataKeys: ["CUST_ID"],
    },
    defaultSearchApiKeys: {
      srchSubCode3: "108",
      custType: "1",
    },
    srchKey: "CLIENT",
    title: "receiving-address",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchWhCd", "vrSrchWhNm"],
    hiddenId: "vrSrchWhId",
    rowDataIds: ["WH_CD", "WH_NM"],
    rowDataHiddenId: "WH_ID",
    searchApiKeys: ["vrSrchWhCd", "vrSrchWhNm"],
    srchKey: "WH",
    modalOpenCondition: "owner",
    title: "receiving-warehouse",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    ids: ["vrInCustAddr"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "supplier-address",
    width: "entire",
    required: false,
    isSearch: false,
    isModal: false,
    types: ["text"],
  },
  {
    ids: ["vrInCustEmpNm"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "supplier-manager",
    width: "half",
    required: false,
    isSearch: false,
    isModal: false,
    types: ["text"],
  },
  {
    ids: ["vrInCustTel"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "tel",
    width: "half",
    required: false,
    isSearch: false,
    isModal: false,
    types: ["text"],
  },
];

export const ORDER_SEARCH_MODAL_INFO: any = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  "receiving-warehouse": {
    page: "WMSMS040",
    id: "receiving-warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",
    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_WH_CD",
        searchContainerInputId: "vrSrchWhCd",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        searchContainerInputId: "vrSrchWhNm",
        title: "warehouse-name",
        type: "text",
        width: "half",
      },
    ],
  },
  "receiving-address": {
    page: "WMSCM085",
    id: "receiving-address",
    title: "search-customer",
    gridTitle: "customer-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["trCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    defaultSearchApiKeys: {
      srchSubCode3: "108",
      custType: "1",
    },
    apis: {
      url: "/TMSCM010/list_rn.action",
      params: {
        vrSrchKey: "CLIENT",
        txtName: "",
        cmbCustType: "108",
        vrSrchSubCode4: "",
        srchSubCode1: "",
        trCustId: "",
        custType: "1",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "txtName: ",
        searchContainerInputId: "vrInCustCd",
        title: "customer-name",
        type: "text",
        width: "half",
      },
    ],
  },
};

export const ORDER_SEARCH_CONTAINER_META = {
  serAuth: "N",
  useSearch: false,
  useMore: true,
  unUsedRefreshButton: false,
  modalColumnDefs: ORDER_MODAL_COLUMN_DEFS,
  searchInput: ORDER_SEARCH_INPUT,
  searchModalInfo: ORDER_SEARCH_MODAL_INFO,
  pageInfo: "null",
};

//#endregion
