/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	logistics-container-receiving-order/input.grid.ts
 *  Description:    입고관리 신규 팝업 메타 파일
 *  Authors:        dhkim
 *  Update History:
 *                  2023.08. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { IControlBtn, IGridCellDateInput, IGridCellSearchButton, IGridCellSelectBox } from "@/types";
import { GridOptions } from "ag-grid-community";
import { ORDER_GRID_COLUMN_DEFS } from "./column-defs.ts";

const { t } = i18n.global;

//#region ::  입고주문 탭

export const ORDER_CONTROL_BUTTON: IControlBtn[] = [
  // {
  //   title: "reset",
  //   colorStyle: "primary",
  //   paddingStyle: "bold",
  //   authType: "INS_AUTH",
  //   image: "",
  // },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    authType: "INS_AUTH",
    image: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    authType: "DEL_AUTH",
    image: "",
  },
];

export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  ITEM: {
    //["ITEM_CODE", "RITEM_NM", "OUT_WORK_UOM_CD", "OUT_ORD_UOM_CD", "RITEM_ID", "UOM_ID", "IN_WORK_UOM_NM", "U_UOM_ID"],
    fieldList: ["ITEM_CODE", "RITEM_NM", "POOL_NM", "STOCK_QTY", "BAD_QTY", "UNIT_PRICE", "WH_NM"],
    rowDataKeys: ["POOL_CODE", "POOL_NM", "POOL_NM", "STOCK_QTY", "BAD_QTY", "UNIT_PRICE", "WH_NM"],
    callApiCondition: {
      dataType: "modal",
      key: "owner",
    },
    modalData: {
      page: "WMSCM091",
      id: "kit-product",
      title: "search-product",
      gridTitle: "product-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달

      defaultParamsData: {
        //해당 모달의 title
        storeSaveKey: "owner",
        paramsKeys: ["vrSrchCustId"],
        rowDataKeys: ["CUST_ID"],
      },
      apis: {
        url: "/WMSCM162/list_rn.action",
        params: {
          func: "fn_setWMSCM162",
          vrSrchCustId: "",
          vrSrchPoolCd: "",
          vrSrchPoolNm: "",
          vrSrchPoolGrp: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchPoolCd",
          title: "logistics-container-code",
          searchContainerInputId: "vrSrchItemCd",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchPoolNm",
          title: "logistics-container-name",
          searchContainerInputId: "vrSrchItemNm",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchPoolGrp",
          title: "logistics-container-group",
          type: "select",
          width: "half",
          optionsKey: "POOLGRP",
          options: [
            {
              nameKey: "all",
              name: "",
              value: "",
            },
          ],
        },
      ],
    },
    colDef: [
      {
        field: "No",
        headerKey: "no",
        headerName: "",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        maxWidth: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "POOL_GRP_ID",
        headerKey: "pool-grp-id",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "POOL_CODE",
        headerKey: "pool-grp-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "POOL_NM",
        headerKey: "pool-nm",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "POOL_SIZE",
        headerKey: "pool-size",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "UNIT_PRICE",
        headerKey: "unit-price",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "IN_WH_CD",
        headerKey: "warehouse",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "IN_ZONE_NM",
        headerKey: "receiving-zone",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "IN_WH_ID",
        headerKey: "in-zone-nm",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
      {
        field: "IN_ZONE_ID",
        headerKey: "in-zone-nm",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
      {
        field: "POOL_ID",
        headerKey: "in-zone-nm",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
      {
        field: "RITEM_ID",
        headerKey: "in-zone-nm",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
    ],
  },
  UOM: {
    fieldList: ["IN_WORK_UOM_CD", "IN_WORK_UOM_ID"],
    rowDataKeys: ["UOM_CD", "UOM_ID"],
    modalData: {
      page: "WMSCM100",
      id: "representative-uom",
      title: "search-uom",
      gridTitle: "uom-list",
      isCellRenderer: true,
      apis: {
        url: "/WMSCM100/list_rn.action",
        params: {
          func: "fn_setWMSCM100",
          UOM_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          vrSrchUomId: "",
          vrSrchUomNm: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchUomId",
          title: "uom-code",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchUomNm",
          title: "uom-name",
          type: "text",
          width: "half",
        },
      ],
    },
    colDef: [
      {
        field: "No",
        headerKey: "no",
        headerName: "",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "UOM_CD",
        headerKey: "uom-code",
        headerClass: "header-center",
      },
      {
        field: "UOM_NM",
        headerKey: "uom-name",
        headerClass: "header-center",
      },
      {
        field: "UOM_ID",
        hide: true,
      },
      {
        field: "U_UOM_ID",
        hide: true,
      },
      {
        field: "UPD_NO",
        hide: true,
      },
      {
        field: "REG_NO",
        hide: true,
      },
      {
        field: "LC_ID",
        hide: true,
      },
    ],
  },
  LOCATION: {
    fieldList: ["LOC_CD", "LOC_ID"],
    rowDataKeys: ["LOC_CD", "LOC_ID"],
    modalData: {
      page: "WMSMS080",
      id: "location",
      title: "search-location",
      gridTitle: "location-list",
      isCellRenderer: true,
      apis: {
        url: "/WMSCM080/list_rn.action",
        params: {
          func: "fn_setWMSMS080",
          LOC_ID: "",
          LOC_CD: "",
          AVAILABLE_QTY: "",
          OUT_EXP_QTY: "",
          STOCK_ID: "",
          SUB_LOT_ID: "",
          vrViewOnlyLoc: "",
          STOCK_WEIGHT: "",
          vrViewSubLotId: "",
          vrRitemId: "",
          ITEM_BEST_DATE_END: "",
          UOM_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          vrViewStockQty: "",
          vrSrchLocCd: "",
          vrSrchLocId: "",
          vrSrchCustLotNo: "",
          vrSrchLocStat: "",
          S_WH_CD: "",
          vrWhId: "",
          S_WH_NM: "",
          vrSrchLocDel: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchLocCd",
          searchContainerInputId: "txtSrchLocCd",
          title: "location-code",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchCustLotNo",
          title: "LOT-number",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchLocStat",
          title: "location-status",
          type: "select",
          width: "half",
          options: [
            { nameKey: "all", name: "", value: "300" },
            { nameKey: "use-location", name: "", value: "200" },
            { nameKey: "unused-location", name: "", value: "100" },
          ],
          optionsAutoSelected: { autoSelectedKeyIndex: 0 },
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "LOC_CD",
        headerKey: "location",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "",
        headerKey: "product",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        children: [
          { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

          { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
        ],
      },
      {
        field: "AVAILABLE_QTY",
        headerKey: "stock-quantity",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "UOM_NM",
        headerKey: "uom",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "OUT_EXP_QTY",
        headerKey: "schedule-quantity",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "REAL_PLT_QTY",
        headerKey: "plt-quantity",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "STOCK_WEIGHT",
        headerKey: "weight",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "CUST_LOT_NO",
        headerKey: "lot-number",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_BEST_DATE_END",
        headerKey: "expiration-date",
        headerName: "",
        cellStyle: { textAlign: "center" },
        headerClass: "header-center",
        sortable: true,
      },
    ],
  },
};

export const ORDER_GRID_CELL_DATE_INPUT_INFO: IGridCellDateInput = {
  gridPk: "RNUM",
};

export const ORDER_GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  CNTR_TYPE: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "select", value: "" },
      { name: "", nameKey: "10-ft", value: "01" },
      { name: "", nameKey: "20-ft", value: "02" },
      { name: "", nameKey: "40-ft", value: "03" },
      { name: "", nameKey: "40-fthq", value: "04" },
      { name: "", nameKey: "rf", value: "05" },
      { name: "", nameKey: "wing-body", value: "06" },
    ],
  },
};

export const orderGridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSOP910_1"),

  //페이징옵션
  pagingSizeList: [100, 500, 1000, 2000],
};

export const orderGridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: ORDER_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  editType: undefined,
  singleClickEdit: true,
  allowContextMenuWithControlKey: true,
  enableRangeSelection: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  context: { ...GRID_CELL_SEARCH_BUTTON, ...ORDER_GRID_CELL_DATE_INPUT_INFO, ...ORDER_GRID_CELL_SELECT_BOX },
};

//#endregion
