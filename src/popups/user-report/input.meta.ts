/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	{ 상위폴더 } / { 파일명 }
 *  Description:    { 내용 }
 *  Authors:        { 작성자 }
 *  Update History:
 *                  { 날짜 (년.월.) } : Created by {작성자}
 *
-------------------------------------------------------------------------------*/


import { SEARCH_CONTAINER_META } from "./input.search";

export * from "./input.search";

export const commonSetting = {
  authPageGroup: "ETC",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};