/// <reference types="vite/client" />
interface ImportMetaEnv {
  readonly VITE_TARGET_URL: string;
  readonly VITE_OZ_URL: string;
  readonly VITE_FILE_URL: string;
  readonly VITE_LOGIN_URL: string;
  readonly VITE_GRID_KEY: string;
  readonly VITE_URL_PREFIX: string;
}

interface ImportMeta {
  readonly env: ImportMetaEnv;
}
