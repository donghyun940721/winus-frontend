/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	ag-grid/formatter.ts
 *  Description:    AG Grid 열 포맷 유형 정의를 위한 스크립트
 *  Authors:        dhkim
 *  Update History:
 *                  2024.03. : Created by dhkim
 *
------------------------------------------------------------------------------*/
import { UtilService } from "@/services/util-service";
import { useOptionDataStore } from "@/store";
import { ValueGetterParams, type ValueFormatterParams } from "ag-grid-community";

export class Format {
  /**
   * 시간 포맷터
   *
   * @param columnParams    : valueFormatter 인자
   * @param separator1      : 시/분/초 구분자
   * @param hasMilliSeconds : MilliSeconds 표기 여부
   * @returns
   */
  static Time(columnParams: ValueFormatterParams, separator1: string = ":", hasMilliSeconds: boolean = false) {
    if (UtilService.isEmptyString(columnParams.value)) {
      return "";
    }

    const date = new Date(columnParams.value);
    const hours = String(date.getHours()).padStart(2, "0");
    const minutes = String(date.getMinutes()).padStart(2, "0");
    const seconds = String(date.getSeconds()).padStart(2, "0");
    const milliseconds = date.getMilliseconds();

    const formattedDate = hasMilliSeconds ? `${hours}${separator1}${minutes}${separator1}${seconds}.${milliseconds}` : `${hours}${separator1}${minutes}${separator1}${seconds}`;

    return formattedDate;
  }
  /**
   * 날짜 포맷터
   *
   * @param columnParams    : valueFormatter 인자
   * @param separator1      : 년/월/일 구분자
   * @param separator2      : 시/분/초 구분자
   * @param hasMilliSeconds : MilliSeconds 표기 여부
   * @returns
   */
  static Date(columnParams: ValueFormatterParams, separator1: string = "-", separator2: string = ":", hasMilliSeconds: boolean = false) {
    if (UtilService.isEmptyString(columnParams.value)) {
      return "";
    }

    const date = new Date(columnParams.value);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");
    const hours = String(date.getHours()).padStart(2, "0");
    const minutes = String(date.getMinutes()).padStart(2, "0");
    const seconds = String(date.getSeconds()).padStart(2, "0");
    const milliseconds = date.getMilliseconds();

    const formattedDate = hasMilliSeconds
      ? `${year}${separator1}${month}${separator1}${day} ${hours}${separator2}${minutes}${separator2}${seconds}.${milliseconds}`
      : `${year}${separator1}${month}${separator1}${day} ${hours}${separator2}${minutes}${separator2}${seconds}`;

    return formattedDate;
  }

  /**
   * 날짜 포맷터 (년-월-일)
   *
   * @param columnParams    : valueFormatter 인자
   * @returns
   */
  static YearMonthDay(columnParams: ValueFormatterParams) {
    if (UtilService.isEmptyString(columnParams.value)) {
      return "";
    }
    const separator = "-";
    const date = new Date(columnParams.value);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");

    const formattedDate = `${year}${separator}${month}${separator}${day}`;
    return formattedDate;
  }

  static NumberCountEmpty(columnParams: ValueFormatterParams) {
    if (!String(columnParams.value) || String(columnParams.value).trim().length === 0) {
      return "";
    }
    return Format.NumberCount(columnParams);
  }

  static NumberCount(columnParams: ValueFormatterParams) {
    return Number.isNaN(Number(columnParams.value)) ? "0" : Number(columnParams.value).toLocaleString();
  }

  static NumberPrice(columnParams: ValueFormatterParams) {
    return columnParams.value === "" || columnParams.value == null ? "0" : Math.floor(columnParams.value).toLocaleString();
  }
}

export class Getter {
  /**
   * 날짜 포맷터
   *
   * @param columnParams    : valueFormatter 인자
   * @param separator1      : 년/월/일 구분자
   * @param separator2      : 시/분/초 구분자
   * @param hasMilliSeconds : MilliSeconds 표기 여부
   * @returns
   */
  static Date(columnParams: ValueGetterParams, separator1: string = "-", separator2: string = ":", hasMilliSeconds: boolean = false) {
    let inputValue = undefined;

    if (!columnParams.colDef.field || !columnParams.data) {
      return "";
    }

    inputValue = columnParams.data[columnParams.colDef.field];

    if (UtilService.isEmptyString(inputValue)) {
      return "";
    }

    const date = new Date(inputValue);
    const year = date.getFullYear();
    const month = String(date.getMonth() + 1).padStart(2, "0");
    const day = String(date.getDate()).padStart(2, "0");
    const hours = String(date.getHours()).padStart(2, "0");
    const minutes = String(date.getMinutes()).padStart(2, "0");
    const seconds = String(date.getSeconds()).padStart(2, "0");
    const milliseconds = date.getMilliseconds();

    const formattedDate = hasMilliSeconds
      ? `${year}${separator1}${month}${separator1}${day} ${hours}${separator2}${minutes}${separator2}${seconds}.${milliseconds}`
      : `${year}${separator1}${month}${separator1}${day} ${hours}${separator2}${minutes}${separator2}${seconds}`;

    return formattedDate;
  }

  /**
   * 열의 값(code)을 획득한 기준코드의 Value(name)로 변환하여 반환
   *
   * @param optionKey : Option Store에 저장해둔 Key 값
   * @param params : valueGetter 또는 formatValue 의 전달 인자
   */
  static convetCodeToNameByOptionData(optionKey: string, params: any) {
    let code: string = "";
    let optionList = new Array();
    const optionStoreData = useOptionDataStore().getOptionData();

    if (UtilService.isEmptyObject(optionStoreData) && !Object.hasOwn(optionStoreData, optionKey)) {
      return;
    }

    // valueGetter에 적용하는 경우
    if (params instanceof Object && isValueGetterParams(params)) {
      if (UtilService.isEmptyObject(params.data) || !Object.hasOwn(params.data, params.column.getColId())) {
        return;
      }
      code = params.data[params.column.getColId()];
    } else {
      code = params;
    }

    optionList = optionStoreData[optionKey];

    if (UtilService.isEmptyArray(optionList)) {
      return code;
    }

    /** 사용된 Key 명칭 탐색 */
    const { codeKey, nameKey } = UtilService.getOptionKey(optionList);

    const targetNameIndex = optionList.findIndex((dt: any) => {
      return dt[codeKey] === code;
    });

    return targetNameIndex < 0 ? code : optionList[targetNameIndex][nameKey];
  }

  /**
   * 공백 문자열 처리
   * - 컬럼 타입과 맞지 않는 공백이 있는 경우 기본적으로 Invalid Value로 처리됨.
   *
   * @param params
   * @returns
   */
  static emptyString(params: ValueGetterParams) {
    const colField: string = params.colDef.field ?? "";

    if (UtilService.isEmptyObject(params.data) || !Object.hasOwn(params.data, colField)) {
      return "";
    }

    return params.data[colField];
  }

  /**
   * "No" 컬럼 인덱싱 공통 규칙 정의
   *
   * @param columnParams
   */
  static commonRowIndex(columnParams: ValueGetterParams) {
    return typeof columnParams.data == "object" && Object.hasOwn(columnParams.data, "RNUM") ? columnParams.data["RNUM"] : columnParams.node!.rowIndex! + 1;
  }

  /**
   * 문자열 Trim 처리
   *
   * @param params
   * @returns
   */
  static trim(params: ValueGetterParams) {
    const colField: string = params.colDef.field ?? "";

    if (UtilService.isEmptyObject(params.data) || !Object.hasOwn(params.data, colField)) {
      return "";
    }

    return String(params.data[colField]).trim();
  }
}

/**
 * ValueGetterParams 타입 식별
 *
 * @param params
 * @returns
 */
function isValueGetterParams(params: any): params is ValueGetterParams {
  return "data" in params && "column" in params;
}
