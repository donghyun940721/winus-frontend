/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	ag-grid/utils.ts
 *  Description:    AG Grid 유틸 모음
 *  Authors:        dhkim
 *  Update History:
 *                  2024.04. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import { UtilService } from "@/services/util-service";
import { useOptionDataStore, userStore } from "@/store";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
import type { ColumnState, EditableCallbackParams, GridApi, IServerSideGetRowsParams, SortModelItem } from "ag-grid-enterprise";
import { set } from "lodash";

export class GridUtils {
  
//#region :: Rows

  /**
   * 선택 행의 데이터를 반환
   * 
   * @param gridApi 
   * @returns 
   */
  static getSelectedRowData(gridApi: GridApi) {
    
    const rowModelType = gridApi.getModel().getType();
    let selectedItems = new Array;
    let selectedNodeIds:any;

    switch(rowModelType){
      case "clientSide" : 
        selectedItems = gridApi.getSelectedRows();
        break;

      case "infinite" :
      case "viewport" :
      case "serverSide" :
        // TODO :: getSelectedRows 등의 API 사용이 제약됨.
        selectedNodeIds = gridApi.getSelectedNodes().filter((dt:any) => !dt.group).map((dt:any) => dt.id);    

        selectedNodeIds.forEach((nodeId:any) => {
          selectedItems.push(gridApi.getRowNode(nodeId)!.data);
        });
        break;
    }

    return selectedItems;
  }

  /**
   * 행의 그룹 데이터 반환
   * [Server-Side] Option :: Row Group
   * 
   * @param params 
   * @param rowData 
   * @returns 
   */
  static getGroupDataSource(params: IServerSideGetRowsParams, rowData:Array<any>){

    const groupMap = new Map();         // 그룹 맵 정보
    const dataSource = new Map();       // 그룹 행 데이터
    const rowGroupCols = params.request.rowGroupCols;
    const valueCols = params.request.valueCols;
    const displayColId = ([...rowGroupCols, ...valueCols]).map(dt => dt.id);

    // TODO :: 정렬의 경우, 순차적으로 앞/뒤 정렬 키 값을 기준으로 그룹 단위를 생성 해야함. (현재 미구현)
    rowData.forEach( dt => {      
      const groupColumnData:any = {};
      const key = dt[rowGroupCols[0].id];

      displayColId.forEach(id => {
        set(groupColumnData, id, dt[id]);
      })

      dataSource.set(key, groupColumnData);

      if(groupMap.has(key)){
        const children:Array<any> = groupMap.get(key);
        children.push(dt);
        groupMap.set(key, children);
      }else{
        groupMap.set(key, [dt]);
      }
    });

    return {
      dataSource: [...dataSource.values()],
      groupMap: groupMap,
    };
  }

//#endregion

//#region :: Columns

  /**
   * 엑셀 추출 대상 열의 필드(field)명 집합을 반환
   * 
   * @param formColumnDef : 컬럼 정의
   * @returns 
   */
  static getExportColumns (formColumnDef: (IColDef | IColGroupDef)[] | null) : string[]{
    let columnKeys = [] as string[];
  
    if(formColumnDef == null){
      return columnKeys;
    };

    for (let i = 0; formColumnDef.length > i; i++) {
  
      const columnDef = formColumnDef[i];
      
      if (Object.hasOwn(columnDef, "export")) {
        columnKeys.push(String(columnDef.field));
      }
  
      // children 컬럼
      if (Object.hasOwn(columnDef, "children")) {
        columnKeys = columnKeys.concat(
          columnDef.children
            .filter((dt:any)=> Object.hasOwn(dt, "export"))
            .map((dt:any) => dt.field)
        );
      }
    }

    return columnKeys;
  }

  /**
   * select box 셀에 대한 값(Code) 목록을 반환
   * - async values를 사용하는 경우
   * 
   * @param params : Option Store에 저장해둔 Key 값
   * @returns 
   */
  static getOptionsCode (optionKey: any): string[] {
    const optionData = useOptionDataStore().getOptionData();
    let optionList = new Array();
    let optionValues = new Array();

    if(UtilService.isEmptyObject(optionData) && !Object.hasOwn(optionData, optionKey)){
      return [];
    }

    optionList = optionData[optionKey];

    if(UtilService.isEmptyArray(optionList)){
      return [];
    }

    /** 사용된 Key 명칭 탐색 */
    const { codeKey } = UtilService.getOptionKey(optionList);
    
    optionValues = optionList
      .filter((dt:any) => {
        return !UtilService.isEmptyString(dt[codeKey]);
      })
      .map((dt:any) => dt[codeKey]);

    return UtilService.isEmptyArray(optionValues) ? [] : optionValues;
  }

  /**
   * 행 선택(selected) 상태에 따라 편집 상태 변경
   * 
   * @param params
   */
  static editableSelcetedRow(params: EditableCallbackParams) {
    return params.node.isSelected() ?? false;
  }

//#endregion

//#region :: Function, Options

  /**
   * ServerSide Parameter 유형을 Winus 인자 유형으로 변경함.
   * 
   * @param {IServerSideGetRowsParams} params      : ServerSide Params
   * @param pagingSize  : 페이지 사이즈
   * @param currentPage : 현재 페이지
   * @returns 
   */
  static getBodyData(params: any, pagingSize:Number, currentPage:Number) {

    let sortDataInfo:SortModelItem[];
    let coldId:string|null = null;
    let sort:string = "asc";

    //! [Server-Side]
    if(!UtilService.isEmptyObject(params) && Object.hasOwn(params, "request")){
      sortDataInfo = params!.request.sortModel;
      coldId = sortDataInfo[0]?.colId ?? null;      // 정렬 대상 컬럼
      sort = sortDataInfo[0]?.sort ?? "asc";       // 정렬 기준
    }
    else{
      if(Object.hasOwn(params, "sort")) {
        sort = params.sort;
      }
      if(Object.hasOwn(params, "colId")) {
        coldId = params.colId;
      }
    }

    return {
      _search: false,
      nd: Date.now(),
      rows: String(pagingSize ?? ""),
      page: String(currentPage),
      sidx: coldId,
      sord: sort,
    }
  }

  /**
   * 특정 Grid의 컬럼 상태 값(Move, Hidden)을 로컬 스토리지에 저장
   * (사용자, 센터 별 관리)
   * 
   * TODO :: Row Group, Column Group이 존재하는 경우 추가 정보가 필요함.
   * 
   * @param columnState 
   * @param gridId 
   */
  static saveColumnState(columnState: ColumnState[], gridId: string) {
    const CONSTANTS_KEY = "WINUS_GRID_SETTINGS";
    const winusOptions = JSON.parse(localStorage.getItem(CONSTANTS_KEY) ?? "{}");
    const userInfo = JSON.parse(userStore().getCurrentUser());
    const userNo = userInfo["USER_NO"];
    const lcId = userInfo["location"]["LC_ID"];

    /** Init */
    winusOptions[userNo] = winusOptions[userNo] ?? {}       // User Level
    winusOptions[userNo][lcId] = winusOptions[userNo][lcId] ?? {}   // LC(Center) Level 
    winusOptions[userNo][lcId][gridId] = winusOptions[userNo][lcId][gridId] ?? {} // Grid Level

    /** Assign */
    winusOptions[userNo][lcId][gridId] = 
        columnState.map((dt:any) => {
          dt.sort = null;
          // dt.sortIndex = null;
          return dt;
        });

    localStorage.setItem(CONSTANTS_KEY, JSON.stringify(winusOptions));
  }

  /**
   * 특정 Grid의 컬럼 상태 값(Move, Hidden)을 반환
   * 
   * TODO :: Row Group, Column Group이 존재하는 경우 추가 정보가 필요함.
   * 
   * @param gridId 
   * @returns 
   */
  static getColumnState(gridId: string): ColumnState[] {
    const CONSTANTS_KEY = "WINUS_GRID_SETTINGS";
    const winusOptions = JSON.parse(localStorage.getItem(CONSTANTS_KEY) ?? "{}");
    const userInfo = JSON.parse(userStore().getCurrentUser());
    const userNo = userInfo["USER_NO"];
    const lcId = userInfo["location"]["LC_ID"];

    if(!Object.hasOwn(winusOptions, userNo)
      ||!Object.hasOwn(winusOptions[userNo], lcId)
      ||!Object.hasOwn(winusOptions[userNo][lcId], gridId)){
      return [];
    }


    return winusOptions[userNo][lcId][gridId];
  }

  /**
   * 현재 그리드의 공백 상태 여부 반환
   * 
   * @param gridApi 
   * @returns 
   */
  static isGridEmtpy (gridApi: GridApi): Boolean {
    return gridApi.getModel().isEmpty();
  }

//#endregion

}
