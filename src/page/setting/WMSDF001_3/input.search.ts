/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSDF001_3/input.search.ts
 *  Description:    시스템관리/택배단가관리 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.06. : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import type { IModal, ISearchInput } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchCustCdE3", "vrSrchCustNmE3"],
    hiddenId: "vrSrchCustIdE3",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchParcelComCdE3"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "delivery-classification",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchParcelComCdE3",
    disabled: "disabled",
    optionsAutoSelected: {
      autoSelectedKeyIndex: 0,
      allowAutoSelected: true,
    },
  },
  {
    ids: ["vrSrchParcelSeqE3"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "delivery-seq",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchParcelSeqE3",
    disabled: "disabled",
    optionsAutoSelected: {
      autoSelectedKeyIndex: 0,
      allowAutoSelected: false,
    },
  },
];

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    saveSession: true,
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCdE3",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNmE3",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  unUsedRefreshButton: true,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchInput: SEARCH_INPUT,
  pageInfo: "null",
};
