/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSDF001_3/column-defs.ts
 *  Description:    설정관리/택배기본정보관리 - 택배 단가 관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import { i18n } from "@/i18n";
import { Format } from "@/lib/ag-grid";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
    pinned: "left",
  },
  {
    field: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    field: "PARCEL_TYPE",
    headerName: t("grid-column-name.general/return"),
    width: 110,
    cellStyle: { textAlign: "center" },
    sortable: true,
    pinned: "left",
  },
  {
    field: "BOX_SIZE",
    headerName: t("grid-column-name.box-size"),
    width: 130,
    cellStyle: { textAlign: "left" },
    sortable: true,
  },
  {
    field: "BOX_PRICE",
    headerName: t("grid-column-name.unit-price"),
    width: 150,
    sortable: true,
    editable: true,
    headerClass: "header-require",
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberPrice,
  },
  {
    field: "USE_YN",
    headerName: t("grid-column-name.use-Y/N"),
    width: 100,
    editable: true,
    singleClickEdit: true,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    cellEditor: "agRichSelectCellEditor",
    cellEditorParams: {
      values: ["Y", "N"],
      cellRenderer: GridCircleIconForYn,
      cellEditorPopup: false,
    },
    sortable: true,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.reg-dt"),
    width: 180,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.Date,
    sortable: true,
  },
  {
    field: "UPD_DT",
    headerName: t("grid-column-name.date-modified"),
    width: 180,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.Date,
    sortable: true,
  },
];
