/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS030_1/input.meta.ts
 *  Description:    시스템관리/사용자관리목록 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import type { info } from "@/types/index";
import { gridMetaData, gridOptionsMeta } from "./input.grid.ts";
import { SEARCH_CONTAINER_META } from "./input.search.ts";

export * from "./input.grid.ts";
export * from "./input.search.ts";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "TMSY030",
  pk: "RNUM",
};

export const commonSetting = {
  authPageGroup: "TMSYS",
  authPageId: "TMSYS030_1",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};
