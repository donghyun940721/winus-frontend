/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS030_1/column-defs.ts
 *  Description:    시스템관리/사용자관리 - 사용자관리목록 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import { i18n } from "@/i18n";
import type { IModalColDef } from "@/types";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: IModalColDef = {
  "customer-owner": [
    {
      field: "",
      cellStyle: { textAlign: "center" },
      headerName: "No",
      width: 70,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 70,
    },
    {
      field: "ETC1",
      headerName: t("grid-column-name.customer-type"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CODE",
      headerName: t("grid-column-name.customer-code(owner)"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "NAME",
      headerName: t("grid-column-name.customer-name"),
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    field: "CLIENT_NM",
    headerName: t("grid-column-name.company-name"),
    sortable: true,
  },
  {
    field: "USER_GB_NM",
    headerName: t("grid-column-name.user-type"),
    sortable: true,
    width: 130,
  },
  {
    field: "AUTH_NM",
    headerName: t("grid-column-name.business"),
    sortable: true,
    minWidth: 200,
    flex: 1,
  },
  {
    field: "USER_ID",
    headerName: t("grid-column-name.user-id"),
    sortable: true,
  },
  {
    field: "USER_NM",
    headerName: t("grid-column-name.user-name"),
    sortable: true,
  },
  {
    field: "TEL_NO",
    headerName: t("grid-column-name.tel"),
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "MOBILE_NO",
    headerName: t("grid-column-name.cell-phone"),
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "MULTI_USE_GB",
    headerName: t("grid-column-name.multi-access-Y/N"),
    sortable: true,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    width: 150,
  },
  {
    field: "LC_CHANGE_YN",
    headerName: t("grid-column-name.change-logistics-center-Y/N"),
    sortable: true,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    cellStyle: { textAlign: "center" },
    width: 150,
  },
  {
    field: "LANG",
    headerName: t("grid-column-name.LANG"),
    sortable: true,
    cellStyle: { textAlign: "center" },
    width: 90,
  },
  {
    field: "CONN_LC_ID",
    headerName: t("grid-column-name.CONN_LC_ID"),
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 130,
  },
  {
    field: "EMAIL_ADDR",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "USER_NO",
    headerName: "",
    sortable: true,
    hide: true,
  },
];
