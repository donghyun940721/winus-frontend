/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS030_1/input.meta.ts
 *  Description:    시스템관리/사용자관리목록 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import type { IModal, ISearchInput } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const SEARCH_MODAL_INFO: IModal = {
  "customer-owner": {
    page: "WMSCM091",
    id: "customer-owner",
    title: "search-customer",
    gridTitle: "customer-owner-list",

    apis: {
      url: "/TMSCM010/list_rn.action",
      params: {
        vrSrchKey: "CLIENT",
        txtName: "",
        cmbCustType: "",
        vrSrchSubCode4: "",
        trCustId: "",
        custType: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "txtName",
        searchContainerInputId: "CUST_NAME",
        title: "customer-name",
        type: "text",
        width: "half",
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["CUST_CD", "CUST_NAME"],
    hiddenId: "CUST_ID",
    rowDataIds: ["CODE", "NAME"],
    rowDataHiddenId: "ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CLIENT",
    title: "customer-owner",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["S_USER_GB"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["S_USER_GB"],
    srchKey: "",
    title: "user-type",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "UserGB",
    options: [{ nameKey: "all", name: "", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    ids: ["AUTH_CD_E1"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["AUTH_CD_E1"],
    srchKey: "",
    title: "business",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "UserAuth",
    options: [{ nameKey: "all", name: "", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    ids: ["S_USER_ID"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["S_USER_ID"],
    srchKey: "",
    title: "user-id",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["S_USER_NAME"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["S_USER_NAME"],
    srchKey: "",
    title: "user-name",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },

  {
    ids: ["PROC_AUTH"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["PROC_AUTH"],
    srchKey: "",
    title: "storage-processing-rights-(Delivery)",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "PROC_AUTH",
    options: [{ nameKey: "all", name: "", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: "null",
};
