/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSDF001_1/column-defs.ts
 *  Description:    설정관리/택배기본정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import gridSelect from "@/components/renderer/grid-select.vue";
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import { GridUtils } from "@/lib/ag-grid/utils";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
import type { EditableCallbackParams, ICellEditorParams } from "ag-grid-community";

const { t } = i18n.global;
const isNewRow = (params: EditableCallbackParams) => {
  return params.data && Object.hasOwn(params.data, "ST_GUBUN") && params.data.ST_GUBUN === "INSERT";
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      // 신규 추가인 경우
      if (isNewRow(params)) {
        return "";
      }
      return params.node.rowIndex + 1;
    },
    pinned: "left",
  },
  {
    field: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    field: "PARCEL_COM_NM",
    headerName: t("grid-column-name.delivery-company"),
    headerClass: "header-require",
    width: 120,
    cellStyle: { textAlign: "center" },
    editable: isNewRow,
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("parcelComNmList"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("parcelComNmList", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("parcelComNmList", params.value);
    },
    sortable: true,
    pinned: "left",
  },
  {
    field: "LC_NM",
    headerName: t("grid-column-name.logistics-center"),
    headerClass: "header-require",
    width: 180,
    cellStyle: { textAlign: "left" },
    editable: isNewRow,
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("LCLIST"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("LCLIST", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("LCLIST", params.value);
    },
    sortable: true,
    pinned: "left",
  },
  {
    // 화주
    field: "CUST_NM",
    headerName: t("grid-column-name.owner"),
    headerClass: "header-require",
    width: 160,
    editable: isNewRow,
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode(params.data["LC_NM"]),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData(params.data["LC_NM"], value);
        },
      };
    },
    cellRenderer: function (params: any) {
      if (params.data && Object.hasOwn(params.data, "LC_NM")) {
        return Getter.convetCodeToNameByOptionData(params.data["LC_NM"], params.value);
      }
      return "";
    },
    sortable: true,
    pinned: "left",
  },
  {
    // 순번
    field: "PARCEL_COM_TY_SEQ",
    headerName: t("grid-column-name.sequence"),
    width: 50,
    cellStyle: { textAlign: "center" },
    sortable: true,
    pinned: "left",
  },
  {
    // 거래처코드
    field: "CUSTOMER_KEY_01",
    headerName: t("grid-column-name.customer-code(owner)"),
    headerClass: "header-require",
    width: 120,
    cellStyle: { textAlign: "right" },
    editable: isNewRow,
    sortable: true,
    pinned: "left",
  },
  {
    // 잔여송장
    field: "ABLE_INVC_CNT",
    headerName: t("grid-column-name.remaining invoice"),
    sortable: true,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    width: 130,
  },
  {
    // 사용송장
    field: "USE_INVC_CNT",
    headerName: t("grid-column-name.invoice-used"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    valueFormatter: Format.NumberCount,
    width: 130,
  },
  {
    // 전체송장
    field: "TOTAL_INVC_CNT",
    headerName: t("grid-column-name.full-invoice"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    valueFormatter: Format.NumberCount,
    width: 130,
  },
  {
    // 표시명칭
    field: "SHORT_DESC",
    headerName: t("grid-column-name.display-name2"),
    sortable: true,
    editable: true,
    headerClass: "header-require",
  },
  {
    field: "CUSTOMER_KEY_02",
    headerName: t("grid-column-name.courier-reference-code-2"),
    sortable: true,
    editable: true,
    headerClass: "header-require",
    cellStyle: { textAlign: "left" },
  },
  {
    field: "CUSTOMER_KEY_03",
    headerName: t("grid-column-name.courier-reference-code-3"),
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "left" },
  },
  {
    field: "SENDR_NM",
    headerName: t("grid-column-name.shipper-name"),
    sortable: true,
    editable: true,
    headerClass: "header-require",
  },
  {
    field: "SENDR_TEL_NO1",
    headerName: t("grid-column-name.tel-1"),
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "center" },
    headerClass: "header-require",
    width: 90,
  },
  {
    field: "SENDR_TEL_NO2",
    headerName: t("grid-column-name.tel-2"),
    sortable: true,
    editable: true,
    headerClass: "header-require",
    cellStyle: { textAlign: "center" },
    width: 90,
  },
  {
    field: "SENDR_TEL_NO3",
    headerName: t("grid-column-name.tel-3"),
    sortable: true,
    editable: true,
    headerClass: "header-require",
    cellStyle: { textAlign: "center" },
    width: 90,
  },
  {
    field: "SENDR_ZIP_NO",
    headerName: t("grid-column-name.shipper-postal-code"),
    sortable: true,
    editable: true,
    headerClass: "header-require",
    cellStyle: { textAlign: "center" },
    width: 150,
  },
  {
    field: "SENDR_ADDR1",
    headerName: t("grid-column-name.shipper-address"),
    sortable: true,
    editable: true,
    headerClass: "header-require",
    width: 200,
  },
  {
    field: "SENDR_ADDR2",
    headerName: t("grid-column-name.shipper-detail-address"),
    sortable: true,
    editable: true,
    width: 500,
    headerClass: "header-require",
  },
  {
    field: "SENDR_ADDR3",
    headerName: t("grid-column-name.shipper-address(etc)"),
    sortable: true,
    editable: true,
  },
  {
    field: "COLLECTION_AGENCY",
    headerName: t("grid-column-name.pick-up-agent/branch-name"),
    sortable: true,
    editable: true,
  },
  {
    field: "COLLECTION_AGENCY_TEL",
    headerName: t("grid-column-name.pick-up-agent/branch-tel"),
    sortable: true,
    editable: true,
  },
  {
    field: "COLLECTION_AGENCY_CD",
    headerName: t("grid-column-name.pick-up-agent/branch-code"),
    sortable: true,
    editable: true,
  },
  {
    field: "COLLECTION_AGENCY_ETC",
    headerName: t("grid-column-name.pick-up-agent/branch(etc)"),
    sortable: true,
    editable: true,
  },
  {
    field: "DLV_PRICE1",
    headerName: t("grid-column-name.surcharge-freight-cost-1"),
    sortable: true,
    editable: true,
  },
  {
    field: "DLV_PRICE2",
    headerName: t("grid-column-name.surcharge-freight-cost-2"),
    sortable: true,
    editable: true,
  },
  {
    field: "OPT_1",
    headerName: t("grid-column-name.option-1"),
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "center" },
    width: 90,
  },
  {
    field: "OPT_2",
    headerName: t("grid-column-name.option-2"),
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "center" },
    width: 90,
  },
  {
    field: "SORT_1",
    headerName: t("grid-column-name.seq-1"),
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "center" },
    width: 90,
  },
  {
    // 상품전체노출여부
    field: "ITEM_ALL_YN",
    headerName: t("grid-column-name.product-overall-exposure-Y/N"),
    sortable: true,
    singleClickEdit: true,
    editable: true,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    cellEditor: "agRichSelectCellEditor",
    cellEditorParams: {
      values: ["Y", "N"],
      cellRenderer: GridCircleIconForYn,
      cellEditorPopup: false,
    },
    width: 150,
  },
  {
    field: "REP_PRINT_TYPE",
    headerName: t("grid-column-name.representative-invoice-output-form"),
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "center" },
    width: 150,
  },
  {
    // 반품사용여부
    field: "RETURN_USE_YN",
    headerName: t("grid-column-name.use-return-Y/N"),
    sortable: true,
    singleClickEdit: true,
    editable: true,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    cellEditor: "agRichSelectCellEditor",
    cellEditorParams: {
      values: ["Y", "N"],
      cellRenderer: GridCircleIconForYn,
      cellEditorPopup: false,
    },
    width: 140,
  },
  {
    field: "REAL_OPEN_CD",
    headerName: t("grid-column-name.operational-open-code"),
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "center" },
    width: 120,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.reg-dt"),
    sortable: true,
    cellStyle: { textAlign: "center" },
    width: 170,
  },
  {
    field: "REG_NO",
    headerName: t("grid-column-name.registrar"),
    sortable: true,
    width: 130,
  },
  {
    field: "UPD_DT",
    headerName: t("grid-column-name.date-modified"),
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 170,
  },
  {
    field: "UPD_NO",
    headerName: t("grid-column-name.modifier"),
    sortable: true,
    width: 130,
  },
];
