/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSDF001_1/input.search.ts
 *  Description:    시스템관리/택배기본정보관리 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import type { ISearchInput } from "@/types";

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchParcelComCd"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "delivery-classification",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      { name: "", nameKey: "all", value: "" },
      { nameKey: "post-office", name: "", value: "01" },
      { nameKey: "cj-logistics", name: "", value: "04" },
      { nameKey: "hanjin-express", name: "", value: "05" },
      { nameKey: "logen", name: "", value: "06" },
      { nameKey: "lotte-express", name: "", value: "08" },
      { nameKey: "ilyanglogis", name: "", value: "11" },
      { nameKey: "ems", name: "", value: "12" },
      { nameKey: "dhl", name: "", value: "13" },
      { nameKey: "ups", name: "", value: "14" },
      { nameKey: "daesin", name: "", value: "22" },
      { nameKey: "kyoungdong-express", name: "", value: "23" },
      { nameKey: "poolathome", name: "", value: "85" },
      { nameKey: "q-express", name: "", value: "95" },
      { nameKey: "todaypickup", name: "", value: "94" },
      { nameKey: "yongma-logis", name: "", value: "500" },
      { nameKey: "woorigh", name: "", value: "47" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0 },
  },
  {
    ids: ["vrSrchCustomerKey1"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "customer-code",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["vrSrchLcId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "logistics-center",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "LCLIST",
    options: [{ name: "", nameKey: "all", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: false },
  },
  {
    ids: ["vrSrchCustId"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "owner",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchCustId",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  unUsedRefreshButton: true,
  modalColumnDefs: [],
  searchModalInfo: [],
  searchInput: SEARCH_INPUT,
  pageInfo: "null",
};
