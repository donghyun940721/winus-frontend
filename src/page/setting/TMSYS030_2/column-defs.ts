/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS030_2/column-defs.ts
 *  Description:    설정관리/사용자관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import gridSearchButton from "@/components/renderer/grid-search-button.vue";
import gridSelectBox from "@/components/renderer/grid-select-box.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";

export const MODAL_COLUMN_DEFS: any = {
  agency: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.WH_ID).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
  },
  {
    field: "LC_NM",
    headerKey: "logistics-center-name",
    headerName: "",
    sortable: true,
  },
  {
    field: "CONNECT_YN",
    headerKey: "initial-connect-ion",
    headerName: "",
    sortable: true,
    cellRenderer: gridSelectBox,
    width: 110,
  },
  {
    field: "USER_LC_SEQ",
    headerKey: "seq",
    headerName: "",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "ITEM_GRP_NM",
    headerKey: "product-group",
    headerName: "",
    sortable: true,
  },
  {
    field: "ITEM",
    headerKey: "",
    headerName: "",
    maxWidth: 70,
    cellRenderer: gridSearchButton,
  },
];
