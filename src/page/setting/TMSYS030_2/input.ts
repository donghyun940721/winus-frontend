/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS030_2/input.ts
 *  Description:    설정관리/사용자관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import type { IModal, IInfoInputContainer, info, IGridCellSelectBox, IGridCellSearchButton } from "@/types";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSMS040",
  pk: "WH_ID",
};

export const GRID_SELECT_BOX_INFO: IGridCellSelectBox = {
  CONNECT_YN: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "useProductY", value: "Y" },
      { name: "", nameKey: "useProductN", value: "N" },
    ],
  },
};

export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  ITEM: {
    fieldList: ["ITEM_GRP_ID", "ITEM_GRP_NM", ""],
    rowDataKeys: ["ITEM_GRP_CD", "ITEM_GRP_NM", ""],
    setParamsFromGridRowData: {
      rowDataKeys: ["LC_ID"],
      paramsKeys: ["vrSrchLcId"],
    },
    modalData: {
      page: "WMSCM091",
      id: "kit-product",
      title: "search-product-group",
      gridTitle: "product-group-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달
      apis: {
        url: "/WMSCM094/listGrp_rn.action",
        params: {
          func: "fn_setWMSCM094Q1",
          vrSrchLcId: "",
          ITEM_GRP_CD: "",
          ITEM_GRP_NM: "",
          ITEM_GRP_ID: "",
          vrSrchItemCd: "",
          vrSrchItemNm: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchItemCd",
          title: "product-group-code",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemNm",
          title: "product-group-name",
          type: "text",
          width: "half",
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "ITEM_GRP_CD",
        headerKey: "product-group-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_GRP_NM",
        headerKey: "product-group-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "TEM_GRP_ID",
        headerKey: "product-group-ID",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
    ],
  },
};

export const SEARCH_MODAL_INFO: IModal = {
  agency: {
    page: "WMSCM011",
    id: "agency",
    title: "search-owner",
    gridTitle: "owner-list",
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "", nameKey: "all", value: "12" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

export const INFO_INPUT: IInfoInputContainer[] = [
  {
    title: "user-information",
    default: "expand",
    inputs: [
      {
        ids: ["CLIENT_CD", "CLIENT_NM"],
        hiddenId: "CLIENT_ID",
        rowDataIds: ["CUST_CD", "CUST_NM"],
        rowDataHiddenId: "CUST_ID",
        title: "agency",
        type: "search",
        width: "full",
        size: "double",
        required: true,
      },
      {
        ids: ["USER_GB"],
        rowDataIds: ["USER_GB"],
        title: "user-type",
        type: "select",
        width: "full",
        size: "single",
        required: true,
        optionsKey: "UserGB",
        optionsAutoSelected: { allowAutoSelected: true, autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["ITEM_FIX_YN"],
        rowDataIds: ["ITEM_FIX_YN"],
        title: "specific-product-inquiry-rights",
        type: "select",
        width: "full",
        size: "single",
        required: false,
        optionsKey: "ITEM_FIX_YN",
        disabled: true,
        optionsAutoSelected: { allowAutoSelected: true, autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["AUTH_CD"],
        title: "business",
        type: "select",
        width: "full",
        required: true,
        size: "single",
        optionsKey: "UserAuth",
        optionsAutoSelected: { allowAutoSelected: true, autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["USER_ID"],
        rowDataIds: ["USER_ID"],
        title: "user-ID",
        type: "text",
        width: "full",
        required: true,
        size: "single",
      },
      {
        ids: ["USER_NM"],
        rowDataIds: ["USER_NM"],
        title: "user-name",
        type: "text",
        width: "full",
        size: "single",
        required: true,
      },
      {
        ids: ["PSWD"],
        rowDataIds: ["PSWD"],
        title: "password",
        type: "text",
        width: "full",
        size: "single",
        required: true,
      },
      {
        ids: ["DEPT_CD"],
        rowDataIds: ["DEPT_CD"],
        title: "department",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["EMAIL_ADDR"],
        rowDataIds: ["EMAIL_ADDR"],
        title: "e-mail",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["MOBILE_NO"],
        rowDataIds: ["MOBILE_NO"],
        title: "phone-number",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["TEL_NO"],
        rowDataIds: ["TEL_NO"],
        title: "tel",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["SUB_PASSWORD"],
        rowDataIds: ["SUB_PASSWORD"],
        title: "CRM-password",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["MULTI_USE_GB"],
        rowDataIds: ["MULTI_USE_GB"],
        title: "multi-login-Y/N",
        width: "full",
        size: "single",
        required: false,
        type: "select",
        optionsKey: "MULTI_USE_GB",
        optionsAutoSelected: { allowAutoSelected: true, autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["USER_GRID_NUM"],
        rowDataIds: ["USER_GRID_NUM"],
        title: "grid-search-settings",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["LANG"],
        rowDataIds: ["LANG"],
        title: "language-used",
        width: "full",
        size: "single",
        required: false,
        type: "select",
        optionsKey: "LANG",
        optionsAutoSelected: { allowAutoSelected: true, autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["LC_CHANGE_YN"],
        rowDataIds: ["LC_CHANGE_YN"],
        title: "logistics-centers-change-status",
        width: "full",
        size: "single",
        required: false,
        type: "select",
        optionsKey: "LC_CHANGE_YN",
        optionsAutoSelected: { allowAutoSelected: true, autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["PROC_AUTH"],
        rowDataIds: ["PROC_AUTH"],
        title: "storage-processing-rights",
        width: "full",
        size: "single",
        required: false,
        type: "select",
        optionsKey: "PROC_AUTH",
        optionsAutoSelected: { allowAutoSelected: true, autoSelectedKeyIndex: 0 },
      },
    ],
  },
  {
    title: "driver/equipment-registration-Y/N",
    default: "expand",
    inputs: [
      {
        ids: ["vrSrchDriverNm"],
        rowDataIds: ["vrSrchDriverNm"],
        title: "driver-name",
        type: "text",
        width: "full",
        size: "single",
        required: false,
        disabled: true,
      },
      {
        ids: ["phone-number"],
        rowDataIds: ["phone-number"],
        title: "(automatic-input)equipment-number",
        type: "text",
        width: "full",
        size: "single",
        required: false,
        isReadonly: true,
        disabled: true,
      },
      {
        ids: ["vrSrchLcNm"],
        rowDataIds: ["vrSrchLcNm"],
        title: "(automatic-input)logistics-centers",
        type: "text",
        width: "full",
        size: "single",
        required: false,
        isReadonly: true,
        disabled: true,
      },
    ],
  },
];

export const CONTROL_BTN = [
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
];
