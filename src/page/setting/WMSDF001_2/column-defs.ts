/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSDF001_2/column-defs.ts
 *  Description:    설정관리/택배기본정보관리 - 송장설정(상품명칭) 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import gridChangeIndexButton from "@/components/renderer/grid-change-index-button.vue";
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import { i18n } from "@/i18n";
import { IGridCellChangeIndexColDef } from "@/types";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
    pinned: "left",
  },
  {
    field: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 50,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.shipper"),
    sortable: true,
    pinned: "left",
    width: 200,
  },
  {
    field: "INVC_ITEMNM_GB",
    headerName: t("grid-column-name.division"),
    width: 120,
    sortable: true,
  },

  {
    field: "",
    headerName: t("grid-column-name.category-sequence"),
    sortable: true,
    children: [
      {
        field: "SORT_NUM",
        headerName: t("grid-column-name.seq"),
        cellStyle: { textAlign: "center" },
        width: 80,
      },
      {
        field: "order-up",
        headerName: "",
        cellClass: "renderer-cell",
        cellRenderer: gridChangeIndexButton,
        width: 30,
      } as IGridCellChangeIndexColDef,
      {
        field: "order-down",
        headerName: "",
        cellClass: "renderer-cell",
        cellRenderer: gridChangeIndexButton,
        width: 30,
      } as IGridCellChangeIndexColDef,
    ],
  },
  {
    field: "USE_YN",
    headerName: t("grid-column-name.use-Y/N"),
    width: 100,
    editable: true,
    singleClickEdit: true,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    cellEditor: "agRichSelectCellEditor",
    cellEditorParams: {
      values: ["Y", "N"],
      cellRenderer: GridCircleIconForYn,
      cellEditorPopup: false,
    },
    sortable: true,
  },
  {
    field: "PARCEL_COM_TY",
    headerName: t("grid-column-name.delivery-company"),
    width: 180,
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
  {
    field: "PARCEL_COM_TY_SEQ",
    headerName: t("grid-column-name.delivery-sequence"),
    width: 110,
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
];
