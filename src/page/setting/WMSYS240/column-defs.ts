/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSYS240/column-defs.ts
 *  Description:    설정관리/신규그리드관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

export const FORM_COLUMN_DEFS = [
  {
    field: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    },
  },
  {
    field: "WORK_SEQ",
    headerKey: "work-order",
    cellStyle: { textAlign: "right" },
    headerName: "",
  },
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    sortable: true,
  },
  {
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    sortable: true,
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerName: "",
    sortable: true,
  },
  {
    field: "OLD_LOT_TYPE",
    headerKey: "LOT-properties(old)",
    headerName: "",
    sortable: true,
  },
  {
    field: "NEW_LOT_TYPE",
    headerKey: "LOT-properties(new)",
    headerName: "",
    sortable: true,
  },
  {
    field: "OLD_OWNER_CD",
    headerKey: "owners(old)",
    headerName: "",
    sortable: true,
  },
  {
    field: "NEW_OWNER_CD",
    headerKey: "owners(new)",
    headerName: "",
    sortable: true,
  },
  {
    field: "SUB_LOT_ID",
    headerKey: "lot-id(old)",
    headerName: "",
    sortable: true,
  },
  {
    field: "NEW_SUB_LOT_ID",
    headerKey: "lot-id(new)",
    headerName: "",
    sortable: true,
  },
  {
    field: "WORK_DT",
    headerKey: "work-date",
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
  {
    field: "CHANGE_MEMO",
    headerKey: "change-history",
    headerName: "",
    sortable: true,
  },
  {
    field: "REG_DT",
    headerKey: "registration-date",
    cellStyle: { textAlign: "center" },
    headerName: "",
    sortable: true,
  },
  {
    field: "REG_NO",
    headerKey: "registrar",
    headerName: "",
    sortable: true,
  },
];
