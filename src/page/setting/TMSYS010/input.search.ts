/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS010/input.ts
 *  Description:    시스템관리/기준관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { ISearchInput, info } from "@/types/index";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "",
  pk: "RNUM",
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["CODE_TYPE_CD"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "type-code",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["CODE_TYPE_NM"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "type-name",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["USER_GB"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "user-type",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      {
        name: "",
        nameKey: "all",
        value: "",
      },
    ],
    optionsKey: "USER_GB",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];
// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: false,
  unUsedRefreshButton: false,
  modalColumnDefs: [],
  searchInput: SEARCH_INPUT,
  searchModalInfo: [],
  searchComponentControlBtn: [],
  pageInfo: INFO,
};
