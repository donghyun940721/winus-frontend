/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS010/column-defs.ts
 *  Description:    시스템관리/기준관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridSelectBox from "@/components/renderer/grid-select-box.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "USER_GB",
    headerKey: "user-type",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    editable: true,
    cellRenderer: GridSelectBox,

    width: 150,
  },
  {
    field: "CODE_TYPE_CD",
    headerKey: "type-code",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    minWidth: 110,
    flex: 1,
  },
  {
    field: "CODE_TYPE_NM",
    headerKey: "type-name",
    headerName: "",
    headerClass: "header-center",
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    minWidth: 200,
    flex: 1,
  },
  {
    field: "CODE_LEVEL",
    headerKey: "code-level",
    headerName: "",
    editable: true,
    headerClass: "header-center",
    sortable: true,
    cellRenderer: GridSelectBox,
    valueGetter: (params: any) => {
      if (params.node.data.CODE_LEVEL === "2") {
        return "삭제불가";
      } else if (params.node.data.CODE_LEVEL === "0") {
        return "삭제가능";
      } else {
        return "";
      }
    },
    minWidth: 100,
    flex: 1,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "BA_CODE_CD",
    headerKey: "base-code",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "BA_CODE_NM",
    headerKey: "base-code-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "CODE_VALUE",
    headerKey: "value",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "VIEW_SEQ",
    headerKey: "seq",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    cellStyle: { textAlign: "right" },
  },
];
