import type { info } from "@/types/index";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";
import { gridMetaData, gridOptionsMeta } from "./input.grid";
import { SEARCH_CONTAINER_META } from "./input.search";

export * from "./input.grid";
export * from "./input.search";

export const commonSetting = {
  authPageGroup: "TMSYS",
  authPageId: "TMSYS010",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const MASTER_GRID_OPTIONS_META = {
  ...gridOptionsMeta,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
};

export const DETAIL_GRID_OPTIONS_META = {
  ...gridOptionsMeta,
  columnDefs: DETAIL_GRID_COLUMN_DEFS,
};

export const INFO: info = {
  autoModal: true,
  autoModalPage: "",
  pk: "RNUM",
};
