/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS010/input.ts
 *  Description:    시스템관리/기준관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { IControlBtn, IGridCellSelectBox } from "@/types";
import { GridOptions } from "ag-grid-community";
import { MASTER_GRID_COLUMN_DEFS } from "./column-defs";

export const MASTER_CELL_RENDERER_INFO: IGridCellSelectBox = {
  CODE_LEVEL: {
    pk: "RNUM",
    optionsKey: "",
    options: [
      { name: "", nameKey: "can-be-delete", value: "0" },
      { name: "", nameKey: "master-can-be-delete", value: "1" },
      { name: "", nameKey: "can-not-delete", value: "2" },
    ],
  },
  USER_GB: {
    pk: "RNUM",
    optionsKey: "USER_GB",
    // options: [{ name: "all", nameKey: "all", value: "" }],
  },
};

export const MASTER_GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  USER_GB: {
    pk: "RNUM",
    optionsKey: "USER_GB",
  },
  CODE_LEVEL: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "can-be-delete", value: "0" },
      { name: "", nameKey: "master-can-be-delete", value: "1" },
      { name: "", nameKey: "can-not-delete", value: "2" },
    ],
  },
};

export const MASTER_CONTROL_BTN: IControlBtn[] = [
  {
    title: "save-master",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "delete-master",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
    disabled: "",
  },
  {
    title: "new-master",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
];

export const DETAIL_CONTROL_BTN: IControlBtn[] = [
  {
    title: "save-detail",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "delete-detail",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
    disabled: "",
  },
  {
    title: "new-detail",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "",
    authType: "EXC_AUTH",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [100, 200, 300, 500, 1000],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  allowContextMenuWithControlKey: true,
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
};
