/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSYS020/column-defs.ts
 *  Description:    시스템관리/다국어관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import gridTextInput from "@/components/renderer/grid-text-input.vue";

export const FORM_COLUMN_DEFS: any = [
  {
    field: "NO",
    headerKey: "no",
    headerName: "",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.PROP_KEY).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerName: "",
    width: 50,
    headerCheckboxSelection: true,
    checkboxSelection: true,
    // cellClass: "check-box-renderer",
    // headerComponent: gridHeaderCheckBoxVue,
  },
  {
    field: "CHK_BOX",
    headerName: "",
    hide: true,
  },
  {
    field: "FLAG",
    headerName: "",
    hide: true,
  },
  {
    field: "ST_GUBUN",
    headerName: "",
    hide: true,
  },

  {
    field: "PROP_KEY",
    headerKey: "key-value",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    // width: 550,
  },
  {
    field: "PROP_VAL_KR",
    headerKey: "korean",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,

    // width: 550,
  },
  {
    field: "PROP_VAL_JA",
    headerKey: "japanese",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    // width: 540,
  },
  {
    field: "PROP_VAL_CZ",
    headerKey: "chinese",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    // width: 540,
  },
  {
    field: "PROP_VAL_US",
    headerKey: "english",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    // width: 540,
  },
  {
    field: "PROP_VAL_VN",
    headerKey: "vietnamese",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    // width: 540,
  },
  {
    field: "PROP_VAL_ES",
    headerKey: "spainish",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    // width: 540,
  },
];
