/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS100/input.ts
 *  Description:    기준정보/UOM정보관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { ISearchInput } from "@/types";

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchUomId"],
    rowDataIds: ["UOM_CD"],
    searchApiKeys: ["vrSrchUomId"],
    rowDataHiddenId: "",
    hiddenId: "",
    srchKey: "UOM",
    title: "uom-code",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

export const CONTROL_BTN = [
  {
    title: "manual",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
  },
  {
    title: "enter-excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    cellStyle: {},
  },
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    cellStyle: {},
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    cellStyle: {},
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    cellStyle: {},
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    cellStyle: {},
  },
];
