/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS020/input.ts
 *  Description:    시스템관리/권한관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { UtilService } from "@/services/util-service";
import type { IGridCellCheckBox, IGridCellSelectBox, IModal, ISearchInput, info } from "@/types/index";

// done
export const INFO: info = {
  autoModal: true,
  autoModalPage: "",
  pk: "RNUM",
};

export const GRID_FILTER_INPUT: ISearchInput[] = [
  {
    ids: ["BA_CODE_CD"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "business-type-2",
    width: "entire",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "BA_CODE_CD",
    options: [{ name: "", nameKey: "all", value: "" }],
    disabled: "",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    ids: ["VIEW_MENU_CD"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "menu",
    width: "entire",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "VIEW_MENU_CD",
    disabled: "",
    options: [{ name: "", nameKey: "all", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

// done
export const SEARCH_MODAL_INFO: IModal = {
  customer: {
    page: "WMSCM085",
    id: "customer",
    title: "search-customer",
    gridTitle: "customer-list",

    apis: {
      url: "/TMSCM010/list_rn.action",
      params: {
        vrSrchKey: "CLIENT",
        txtName: "",
        cmbCustType: "104",
        vrSrchSubCode4: "",
        srchSubCode1: "",
        trCustId: "",
        custType: "",
      },
      data: {
        _search: false,
        nd: UtilService.getTimeStamp(),
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "txtName",
        searchContainerInputId: "txtCustCd",
        title: "customer-name",
        type: "text",
        width: "half",
      },
    ],
  },
};
// done
export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["txtCustCd", "txtCustNm"],
    hiddenId: "hdnCustCd",
    rowDataIds: ["CODE", "NAME"],
    rowDataHiddenId: "ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CLIENT",
    title: "customer",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["cmbAuthCd"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "authority",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "cmbAuthCd",
    disabled: "disabled",
  },
  {
    ids: ["txtAuthNm"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "name-of-authority",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

export const AUTHORITY_MANAGEMENT_SELECT_BOX_INFO: IGridCellSelectBox = {
  VIEW_MENU_CD: {
    pk: "RNUM",
    optionsKey: "VIEW_MENU_CD",
  },
};
export const AUTHORITY_MANAGEMENT_CHECKBOX_INFO: IGridCellCheckBox = {
  INS_AUTH: {
    pk: "RNUM",
    checkedValue: "1",
    unCheckedValue: "0",
  },
  DEL_AUTH: {
    pk: "RNUM",
    checkedValue: "1",
    unCheckedValue: "0",
  },
  SER_AUTH: {
    pk: "RNUM",
    checkedValue: "1",
    unCheckedValue: "0",
  },
  PRT_AUTH: {
    pk: "RNUM",
    checkedValue: "1",
    unCheckedValue: "0",
  },
  EXC_AUTH: {
    pk: "RNUM",
    checkedValue: "1",
    unCheckedValue: "0",
  },
};

export const AUTHORITY_LIST_CONTROL_BTN = [
  {
    title: "save",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
    disabled: "",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
];

export const MANAGEMENT_AND_PROGRAM_CONTROL_BTN = [
  {
    title: "save",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
    disabled: "",
  },
];
