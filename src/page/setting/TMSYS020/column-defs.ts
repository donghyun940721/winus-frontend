/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS020/column-defs.ts
 *  Description:    시스템관리/기준관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCheckBox from "@/components/renderer/grid-check-box.vue";
import GridSelectBox from "@/components/renderer/grid-select-box.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";

export const MODAL_COLUMN_DEFS: any = {
  customer: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "ETC1",
      headerKey: "customer-type",
      headerName: "",
      cellStyle: { textAlign: "left" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CODE",
      headerKey: "customer-code(owner)",
      headerName: "",
      cellStyle: { textAlign: "left" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "NAME",
      headerKey: "customer-name",
      headerName: "",
      cellStyle: { textAlign: "left" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const AUTHORITY_LIST_COLUMN_DEFS: any = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "AUTH_CD",
    headerKey: "authority-code",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    flex: 1,
  },
  {
    field: "AUTH_NM",
    headerKey: "authority-name",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    flex: 1,
  },
];

export const PROGRAM_MENU_COLUMN_DEFS: any = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "BA_CODE_NM",
    headerKey: "menu",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    flex: 1,
  },
  {
    field: "VIEW_SEQ",
    headerKey: "seq",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    cellStyle: { textAlign: "left" },
    flex: 1,
  },
];

export const AUTHORITY_MANAGEMENT_COLUMN_DEFS: any = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "PROGRAM_ID",
    headerKey: "PROGRAM_ID",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "PROGRAM_NM",
    headerKey: "display-name",
    headerName: "",
    headerClass: "header-center",
    width: 180,
  },
  {
    field: "VIEW_MENU_CD",
    headerKey: "menu-type",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    width: 110,
    cellRenderer: GridSelectBox,
  },
  {
    field: "",
    headerKey: "authority",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    children: [
      { field: "INS_AUTH", columnGroupShow: "open", headerKey: "registration", headerName: "", width: 80, cellRenderer: GridCheckBox },
      { field: "DEL_AUTH", columnGroupShow: "open", headerKey: "del", headerName: "", width: 80, cellRenderer: GridCheckBox },
      { field: "SER_AUTH", columnGroupShow: "open", headerKey: "search-2", headerName: "", width: 80, cellRenderer: GridCheckBox },
      { field: "PRT_AUTH", columnGroupShow: "open", headerKey: "printing", headerName: "", width: 80, cellRenderer: GridCheckBox },
      { field: "EXC_AUTH", columnGroupShow: "open", headerKey: "excel", headerName: "", width: 80, cellRenderer: GridCheckBox },
      {
        field: "VIEW_SEQ",
        columnGroupShow: "open",
        headerKey: "seq",
        headerName: "",
        cellRenderer: gridTextInput,
        editable: true,
        suppressKeyboardEvent: (params: any) => {
          if (params.event.key === "Enter" || params.event.key === "Backspace") {
            return true;
          } else {
            return false;
          }
        },
        width: 80,
        headerClass: "header-require",
        cellStyle: { textAlign: "left" },
      },
    ],
  },
];
