/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS040/input.grid.ts
 *  Description:    시스템관리/프로그램관리 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
------------------------------------------------------------------------------*/
import { IControlBtn, IGridCellSelectBox } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

const MAIN_CELL_RENDERER_INFO: IGridCellSelectBox = {
  TOP_MENU_CD: {
    pk: "RNUM",
    optionsKey: "vrTopMenuCd",
  },
};

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [500, 700, 900],
};

export const gridOptionsMeta = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
    lockVisible: true,
    lockPosition: true,
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  enableCellEditingOnBackspace: false,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  context: MAIN_CELL_RENDERER_INFO,
  statusBar: true,
};
