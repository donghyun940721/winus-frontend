/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS040/column-defs.ts
 *  Description:    설정관리/프로그램관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import gridSelect from "@/components/renderer/grid-select.vue";
import { i18n } from "@/i18n";
import { Getter } from "@/lib/ag-grid";
import { GridUtils } from "@/lib/ag-grid/utils";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
import type { EditableCallbackParams, ICellEditorParams, ValueGetterParams } from "ag-grid-community";

const { t } = i18n.global;

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: ValueGetterParams) => {
      if (params.data && Object.hasOwn(params.data, "RNUM")) {
        return params.data["RNUM"];
      } else {
        return "";
      }
    },
  },
  {
    field: "",
    headerName: "",
    width: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "TOP_MENU_CD",
    headerName: t("grid-column-name.display-type"),
    headerClass: "header-require",
    width: 150,
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("vrTopMenuCd"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("vrTopMenuCd", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("vrTopMenuCd", params.value);
    },
    editable: true,
    sortable: true,
  },
  {
    field: "PROGRAM_ID",
    headerName: t("grid-column-name.display-id"),
    headerClass: "header-require",
    sortable: true,
    editable: (params: EditableCallbackParams) => {
      return params.data && Object.hasOwn(params.data, "ST_GUBUN") && params.data.ST_GUBUN === "INSERT";
    },
    width: 200,
  },
  {
    field: "PROGRAM_NM",
    headerName: t("grid-column-name.display-name"),
    headerClass: "header-require",
    sortable: true,
    editable: true,
    width: 250,
  },
  {
    field: "VIEW_NM",
    headerName: t("grid-column-name.display-view-name"),
    headerClass: "header-center",
    sortable: true,
    editable: true,
    width: 250,
  },
  {
    field: "URL",
    headerName: t("grid-column-name.url"),
    headerClass: "header-require",
    sortable: true,
    editable: true,
    width: 300,
  },
  {
    field: "REMARK",
    headerName: t("grid-column-name.remark"),
    headerClass: "header-center",
    sortable: true,
    editable: true,
    width: 350,
    flex: 1,
  },
];
