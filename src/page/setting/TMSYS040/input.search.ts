/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	TMSYS040/input.search.ts
 *  Description:    시스템관리/프로그램관리 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import type { ISearchInput } from "@/types";

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrTopMenuCd"],
    rowDataIds: ["TOP_MENU_CD"],
    searchApiKeys: ["vrTopMenuCd"],
    rowDataHiddenId: "TOP_MENU_CD",
    hiddenId: "vrTopMenuCd",
    srchKey: "MENU",
    title: "display-type",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [{ name: "all", nameKey: "all", value: "" }],
    optionsKey: "vrTopMenuCd",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    ids: ["vrProgramId"],
    rowDataIds: ["PROGRAM_ID"],
    searchApiKeys: ["vrProgramId"],
    rowDataHiddenId: "",
    hiddenId: "",
    srchKey: "PROGRAM",
    title: "display-id",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["vrProgramNm"],
    rowDataIds: ["PROGRAM_NM"],
    searchApiKeys: ["vrProgramNm"],
    rowDataHiddenId: "",
    hiddenId: "",
    srchKey: "PROGRAM",
    title: "display-name",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  unUsedRefreshButton: true,
  modalColumnDefs: [],
  searchModalInfo: [],
  searchInput: SEARCH_INPUT,
  pageInfo: "null",
};
