/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSYS030/input.search.ts
 *  Description:    시스템관리/템플릿관리 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import type { IModal, ISearchInput } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 타입
    ids: ["vrTempleteType"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["vrTempleteType"],
    srchKey: "",
    title: "type",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrTempleteType",
    // optionsAutoSelected: { code: "UI", codeNm: "입고", sortSeq: 1, useYn: "Y"},
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 상세타입
    ids: ["vrCustSeq"],
    hiddenId: "",
    rowDataIds: ["vrCustSeq"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrCustSeq"],
    srchKey: "",
    title: "type-detail",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrCustSeq",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    disabled: "disabled",
  },
];

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    templateId: "",
    saveSession: true,
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  unUsedRefreshButton: true,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchInput: SEARCH_INPUT,
  pageInfo: "null",
};
