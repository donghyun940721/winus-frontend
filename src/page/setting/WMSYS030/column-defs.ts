/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSYS030/column-defs.ts
 *  Description:    시스템관리/템플릿관리 - 페이지 내 그리드 컬럼 정의 영역
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------->*/

import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import { i18n } from "@/i18n";
import { Getter } from "@/lib/ag-grid";
import { UtilService } from "@/services/util-service";
import type { EditableCallbackParams } from "ag-grid-community";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (typeof params.data == "object" && Object.hasOwn(params.data, "RNUM")) {
        return params.node.rowIndex + 1;
      } else {
        return "";
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    minWidth: 80,
    width: 80,
    pinned: "left",
  },
  {
    field: "",
    headerName: "",
    maxWidth: 50,
    headerCheckboxSelection: true,
    checkboxSelection: true,
    suppressColumnsToolPanel: true,
    pinned: "left",
  },
  {
    field: "H_FORMAT_COL_SEQ",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    hide: true,
    suppressColumnsToolPanel: true,
    width: 110,
  },
  {
    field: "FORMAT_COL_SEQ",
    headerName: t("grid-column-name.column-seq"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 110,
  },
  {
    field: "V_FORMAT_TEMPLATE_ID",
    headerName: t("grid-column-name.id"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 150,
  },
  {
    field: "V_FORMAT_CUST_SEQ",
    headerName: t("grid-column-name.SEQ"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 110,
    valueGetter: Getter.emptyString,
  },
  {
    field: "FORMAT_TEMPLATE_ID",
    headerName: t("grid-column-name."),
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "FORMAT_CUST_SEQ",
    headerName: t("grid-column-name."),
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "FORMAT_COL_NM",
    headerName: t("grid-column-name.Item-Name"),
    headerClass: "header-center",
    editable: (params: EditableCallbackParams) => {
      // 마스터 권한만 편집
      if (UtilService.getUserAuth() == "MA") {
        return true;
      }
      return false;
    },
  },
  {
    field: "FORMAT_USE_SEQ",
    headerName: t("grid-column-name.repacking-sequence"),
    headerClass: "header-center",
    width: 110,
    editable: (params: EditableCallbackParams) => {
      // 마스터 권한만 편집
      if (UtilService.getUserAuth() == "MA") {
        return true;
      }
      return false;
    },
  },
  {
    field: "FORMAT_BIND_COL",
    headerName: t("grid-column-name.Item-Code"),
    headerClass: "header-center",
    editable: (params: EditableCallbackParams) => {
      // 마스터 권한만 편집
      if (UtilService.getUserAuth() == "MA") {
        return true;
      }
      return false;
    },
  },
  {
    field: "FORMAT_MUST_YN",
    headerName: t("grid-column-name.necessary-y/n"),
    headerClass: "header-center",
    width: 100,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    cellEditor: "agRichSelectCellEditor",
    cellEditorParams: {
      values: ["Y", "N"],
      cellRenderer: GridCircleIconForYn,
      cellEditorPopup: false,
    },
    editable: (params: EditableCallbackParams) => {
      // 마스터 권한만 편집
      if (UtilService.getUserAuth() == "MA") {
        return true;
      }
      return false;
    },
  },
  {
    field: "FORMAT_DEFAULT_VALUE",
    headerName: t("grid-column-name.default-value"),
    headerClass: "header-center",
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "FORMAT_TEMPLATE_INFO",
    headerName: t("grid-column-name.template-information"),
    headerClass: "header-center",
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    width: 300,
  },
  {
    field: "FORMAT_COL_WIDTH",
    headerName: t("grid-column-name.column-size"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 130,
    valueGetter: Getter.emptyString,
    editable: (params: EditableCallbackParams) => {
      // Update 조건
      if (params.data && !Object.hasOwn(params.data, "ST_GUBUN") && params.data.ST_GUBUN !== "INSERT") {
        return true;
      }
      return false;
    },
  },
  {
    field: "FORMAT_USE_YN",
    headerName: t("grid-column-name.use-y/n"),
    headerClass: "header-center",
    width: 90,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "FORMAT_VIEW_SEQ",
    headerName: t("grid-column-name.view-seq"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 90,
    valueGetter: Getter.emptyString,
    editable: (params: EditableCallbackParams) => {
      // Update 조건
      if (params.data && !Object.hasOwn(params.data, "ST_GUBUN") && params.data.ST_GUBUN !== "INSERT") {
        return true;
      }
      return false;
    },
  },
  {
    field: "FORMAT_COL_TYPE",
    headerName: t("grid-column-name.data-type"),
    headerClass: "header-center",
    width: 130,
  },
  {
    field: "FORMAT_COL_FORMAT",
    headerName: t("grid-column-name.disply-format"),
    headerClass: "header-center",
    width: 130,
  },
  {
    field: "FORMAT_SAM_VALUE",
    headerName: t("grid-column-name.sample-value"),
    headerClass: "header-center",
    editable: true,
    cellStyle: { textAlign: "right" },
  },
];
