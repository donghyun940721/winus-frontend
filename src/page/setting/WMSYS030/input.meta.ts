/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSYS030/input.meta.ts
 *  Description:    시스템관리/템플릿관리 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import type { ITemplatePromptModalInfo, info } from "@/types/index";
import { gridMetaData, gridOptionsMeta } from "./input.grid.ts";
import { SEARCH_CONTAINER_META } from "./input.search.ts";

export * from "./input.grid.ts";
export * from "./input.search.ts";

export const commonSetting = {
  authPageGroup: "TMSYS",
  authPageId: "WMSYS030",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSCM011",
  pk: "RNUM",
};

/** 템플릿 명칭 변경, 신규템플릿생성 팝업 정보 */
export const TEMPLATE_PROMPT_MODAL_INFO: ITemplatePromptModalInfo = {
  "change-template-name": { header: "변경", content: "변경할 템플릿 명칭을 입력하세요.", title: "change-template-name" },
  "create-new-template": { header: "생성", content: "생성할 템플릿 명칭을 입력하세요..", title: "create-new-template" },
};
