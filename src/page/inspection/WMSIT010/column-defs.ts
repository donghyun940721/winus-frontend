/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSIT020/column-defs.ts
 *  Description:    기준관리/상품별 원주자재(부품/용기관리) 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MODAL_COLUMN_DEFS: any = {
  //화주
  owner: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  //상품
  product: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      valueFormatter: Format.NumberPrice,
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerKey: "no",
    headerName: "no",
    export: true,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 20,
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  //상품코드
  {
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    export: true,

    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  //상품명
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    export: true,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  //출고주문량
  {
    field: "OUT_ORD_QTY",
    headerKey: "shipping-order-qty",
    headerName: "",
    export: true,
    cellStyle: { textAlign: "center" },

    headerClass: "header-center",
    sortable: true,
    width: 90,
  },
  //검수량
  {
    field: "CHK_QTY",
    headerKey: "inspect-qty",
    headerName: "",
    export: true,
    cellStyle: { textAlign: "center", backgroundColor: "#FFFFDD" },

    headerClass: "header-center",
    sortable: true,
    width: 80,
  },

  //잔여량
  {
    field: "ORD_QTY",
    headerKey: "remaining-qty",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center", backgroundColor: "#FFDDDD" },

    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    width: 80,
  },
  //단위
  {
    field: "UOM_CD",
    headerKey: "unit-box",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    width: 50,
  },
  //주문SEQ
  {
    field: "ORD_SEQ",
    headerKey: "order-SEQ",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    width: 90,
  },
  //검수여부
  {
    field: "PICKING_YN",
    headerKey: "inspect-Y/N",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    width: 90,
  },

  {
    field: "OUT_REQ_DT",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },

  {
    field: "RITEM_ID",
    headerKey: "",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ITEM_BAR_CD",
    headerKey: "",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "OUT_ORD_QTY",
    headerKey: "",
    headerName: "",
    sortable: true,
    hide: true,
  },

  {
    field: "UOM_ID",
    headerKey: "",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "CUST_ID",
    headerKey: "",
    headerName: "",
    sortable: true,
    hide: true,
  },

  {
    field: "ORD_TYPE",
    headerKey: "",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "RNUM",
    hide: true,
  },
  {
    field: "ORD_TYPE_NM",
    headerKey: "",
    headerName: "",
    sortable: true,
    hide: true,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerKey: "no",
    headerName: "no",
    export: true,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 20,
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },

  //주문SEQ
  {
    field: "ORD_SEQ",
    headerKey: "order-SEQ",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    width: 90,
  },

  //상품코드
  {
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    export: true,

    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },

  //상품명
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    export: true,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },

  //주문량
  {
    field: "OUT_ORD_QTY",
    headerKey: "order-qty",
    headerName: "",
    export: true,
    cellStyle: { textAlign: "center" },

    headerClass: "header-center",
    sortable: true,
    width: 90,
  },

  //검수량
  {
    field: "CHK_QTY",
    headerKey: "inspect-qty",
    headerName: "",
    export: true,
    cellStyle: { textAlign: "center", backgroundColor: "#FFFFDD" },

    headerClass: "header-center",
    sortable: true,
    width: 80,
  },

  //잔여량
  {
    field: "ORD_QTY",
    headerKey: "remaining-qty",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },

    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    width: 80,
  },

  //단위
  {
    field: "UOM_CD",
    headerKey: "unit-box",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    width: 50,
  },
  //   //박스No
  {
    field: "CHK_BOX_NO",
    headerKey: "box-No",
    headerName: "",
    width: 100,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "RNUM",
    hide: true,
  },
];

// export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
//   {
//     field: "RNUM",
//     headerKey: "no",
//     headerName: "no",
//     export: true,
//     cellStyle: { textAlign: "left" },
//     headerClass: "header-center",
//     sortable: true,
//     width: 20,
//   },

//   //주문SEQ
//   {
//     field: "BOX_ORD_SEQ",
//     headerKey: "order-SEQ",
//     headerName: "",
//     headerClass: "header-center",
//     cellStyle: { textAlign: "center" },
//     sortable: true,
//     export: true,
//     // cellRenderer: gridTextInput,
//     width: 60,
//   },
//   //상품코드
//   {
//     field: "BOX_RITEM_CD",
//     headerKey: "product-code",
//     headerName: "",
//     cellStyle: { textAlign: "left" },
//     headerClass: "header-center",
//     sortable: true,
//     width: 100,
//   },
//   //상품명
//   {
//     field: "BOX_RITEM_NM",
//     headerKey: "product-name",
//     headerName: "",
//     cellStyle: { textAlign: "center" },
//     headerClass: "header-center",
//     sortable: true,
//     width: 200,
//   },
//   //주문량
//   {
//     field: "BOX_OUT_ORD_QTY",
//     headerKey: "order-qty",
//     headerName: "",
//     headerClass: "header-center",
//     sortable: true,
//     width: 70,
//   },
//   //검수량
//   {
//     field: "BOX_CHK_QTY",
//     headerKey: "inspect-qty",
//     headerName: "",
//     headerClass: "header-center",
//     sortable: true,
//     width: 70,
//   },

//   //잔여량
//   {
//     field: "BOX_ORD_QTY",
//     headerKey: "remaining-qty",
//     headerName: "",
//     headerClass: "header-center",
//     cellStyle: { textAlign: "center" },
//     sortable: true,
//     export: true,
//     // cellRenderer: gridTextInput,
//     width: 100,
//   },
//   //단위
//   {
//     field: "BOX_UOM_CD",
//     headerKey: "unit-box",
//     headerName: "",
//     headerClass: "header-center",
//     cellStyle: { textAlign: "center" },
//     sortable: true,
//     export: true,
//     // cellRenderer: gridTextInput,
//     width: 50,
//   },
//   //박스No
//   {
//     field: "BOX_CHK_BOX_NO",
//     headerKey: "box-No",
//     headerName: "",
//     width: 100,
//     cellStyle: { textAlign: "center" },
//   },

//   {
//     field: "BOX_RITEM_ID",
//     headerKey: "",
//     headerName: "",
//     sortable: true,
//     hide: true,
//   },

//   {
//     field: "BOX_OUT_ORD_UOM_ID",
//     headerKey: "",
//     headerName: "",
//     sortable: true,
//     hide: true,
//   },
//   {
//     field: "BOX_WORK_STAT",
//     headerKey: "",
//     headerName: "",
//     sortable: true,
//     hide: true,
//   },
//   {
//     field: "BOX_WORK_STAT_NM",
//     headerKey: "",
//     headerName: "",
//     sortable: true,
//     hide: true,
//   },
//   {
//     field: "BOX_PICKING_YN",
//     headerKey: "",
//     headerName: "",
//     sortable: true,
//     hide: true,
//   },
// ];
