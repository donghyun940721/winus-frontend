/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS093/input.ts
 *  Description:    상품별자동출고주문관리 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { IControlBtn, IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSCM011",
  pk: "CUST_ID",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    saveSession: true,
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "12" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        title: "shipper-code",
        type: "text",
        width: "triple",
        searchContainerInputId: "S_CUST_CD",
      },
      {
        id: "S_CUST_NM",
        title: "owner-name",
        type: "text",
        width: "triple",
        searchContainerInputId: "S_CUST_NM",
      },
    ],
  },
};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 운송장번호(오더번호)
    ids: ["ORD_ID"],
    hiddenId: "TEMP_ORD_ID",
    rowDataIds: ["ORD_ID"],
    rowDataHiddenId: "ORD_ID",
    searchApiKeys: ["ORD_ID"],
    srchKey: "ORD_ID",
    title: "transport-document-number(order-number)",
    width: "entire",
    isModal: false,
    required: false,
    isSearch: false,
    placeholder: ["blank"],
    types: ["text"],
  },

  {
    // 상품 바코드 스캔
    ids: ["ITEM_BAR_CD"],
    hiddenId: "",
    rowDataIds: ["ITEM_BAR_CD"],
    rowDataHiddenId: "ITEM_BAR_CD",
    searchApiKeys: ["ITEM_BAR_CD"],
    srchKey: "ITEM_BAR_CD",
    title: "product-barcode-scan",
    width: "entire",
    isModal: false,
    required: false,
    isSearch: false,
    placeholder: ["blank"],
    types: ["text"],
  },
  {
    // 박스 No
    ids: ["CHK_BOX_NO"],
    hiddenId: "",
    rowDataIds: ["CHK_BOX_NO"],
    rowDataHiddenId: "CHK_BOX_NO",
    searchApiKeys: ["CHK_BOX_NO"],
    title: "box-No",
    width: "entire",
    isModal: false,
    required: false,
    isSearch: false,
    types: [""],
    options: [{ name: "1", nameKey: "1", value: "1" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

// defaultParams확인 필요
export const MASTER_SEARCH_INPUT: ISearchInput[] = [
  //input 3

  //택배구분
  {
    ids: [""],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "delivery-classification",
    width: "half",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "DS_TP",
    options: [
      { name: "all", nameKey: "all", value: "" },
      // { name: "single-piece-packaging", nameKey: "single-piece-packaging", value: "N" },
      // { name: "combined-packaging", nameKey: "combined-packaging", value: "Y" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  //보내는이
  {
    ids: ["vrSrchOrderGb"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "sender",
    width: "half",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "DS_TP",
    options: [{ name: "", nameKey: "", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];
export const SEARCH_COMPONENT_CONTROL_BTN: IControlBtn[] = [
  //검수완료
  {
    title: "inspection-complete",
    colorStyle: "danger",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  //납품명세표출력
  {
    title: "delivery-statement-print",
    colorStyle: "danger",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  //엑셀다운
  {
    title: "excel-download",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];
// defaultParams확인 필요
export const DETAIL_SEARCH_INPUT: ISearchInput[] = [];

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useMore: false,
  useSetting: false, // TODO :: 위치 조정 이 후, 재설정 (TRUE)
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  // mainSearchInput: MASTER_SEARCH_INPUT,
  searchComponentControlBtn: SEARCH_COMPONENT_CONTROL_BTN,
  detailSearchInput: DETAIL_SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  searchConditionInitUrl: "",
};
