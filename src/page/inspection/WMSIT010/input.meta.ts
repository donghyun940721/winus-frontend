import type { info } from "@/types/index";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";
import { DETAIL_GRID_STATISTICS_INFO, DetailGridOptions, MASTER_GRID_STATISTICS_INFO, gridMetaData, gridOptionsMeta } from "./input.grid";

import { SEARCH_CONTAINER_META } from "./input.search";
export * from "./input.grid";
export * from "./input.search";

export const commonSetting = {
  authPageGroup: "WMSIT",
  authPageId: "WMSIT010",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
  uniqueKey: "master",
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const MASTER_GRID_OPTIONS_META = {
  ...gridOptionsMeta,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
  staticInfo: MASTER_GRID_STATISTICS_INFO,
};

export const DETAIL_GRID_OPTIONS_META = {
  ...DetailGridOptions,
  columnDefs: DETAIL_GRID_COLUMN_DEFS,
  staticInfo: DETAIL_GRID_STATISTICS_INFO,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};
export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSCM011",
  pk: "CUST_ID",
};
