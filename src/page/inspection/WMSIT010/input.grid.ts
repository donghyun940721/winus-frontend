/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS093/input.grid.ts
 *  Description:    상품별자동출고주문관리 grid meta 정보
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.06. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/

/**
 ********************* Grid Area *********************/
import { i18n } from "@/i18n";
import { IControlBtn, IGridCellSelectBox, IGridStatisticsInfo } from "@/types";
import { GridOptions } from "ag-grid-community";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const GRID_SELECT_BOX_INFO: IGridCellSelectBox = {};
export const MASTER_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [
  //전체 SEQ
  {
    id: "TOTAL_SEQ",
    title: "total-SEQ",
  },
  //전체 주문량
  {
    id: "totalSpdList",
    title: "total-order-qty",
  },
  //검수량
  {
    id: "totalSumSpdList",
    title: "inspect-qty",
  },
  //잔여량
  {
    id: "chkSumSpdList",
    title: "remain-qty",
  },
];
export const DETAIL_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [
  //전체 SEQ
  {
    id: "TOTAL_SEQ_BOX",
    title: "total-SEQ",
  },
  //전체 주문량
  {
    id: "totalSpdList_BOX",
    title: "total-order-qty",
  },
  //검수량
  {
    id: "totalSumSpdList_BOX",
    title: "inspect-qty",
  },
  //잔여량
  {
    id: "chkSumSpdList_BOX",
    title: "remain-qty",
  },
];
// done
export const MASTER_CONTROL_BTN: IControlBtn[] = [];
export const DETAIL_CONTROL_BTN: IControlBtn[] = [];

export const MasterGridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSIT010_master"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [],
};

export const DetailGridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSIT010_detail"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "",

  //페이징옵션
  pagingSizeList: [],
};
export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  masterColumnDefs: MASTER_GRID_COLUMN_DEFS,
  detailColumnDefs: DETAIL_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: true,
  statusBar: false,
  getRowId: (data) => {
    return data.data.RNUM;
  },
};

export const DetailGridOptions: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: DETAIL_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  pagination: true,
  suppressRowClickSelection: true,
  suppressClickEdit: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
};
