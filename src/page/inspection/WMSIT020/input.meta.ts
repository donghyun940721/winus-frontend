import type { info } from "@/types/index";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";
import { DetailGridMetaData, DetailGridOptions, MasterGridMetaData, MasterGridOptions, gridOptionsMeta } from "./input.grid";
import { INFO_INPUT, INFO_MODAL_INFO } from "./input.info.ts";
import { SEARCH_CONTAINER_META } from "./input.search";

export * from "./input.grid";
export * from "./input.info.ts";
export * from "./input.search";

export const commonSetting = {
  authPageGroup: "WMSMS",
  authPageId: "WMSMS081",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...commonSetting,
};

export const DETAIL_GRID_META = {
  ...commonSetting,
  ...DetailGridMetaData,
};

export const MASTER_GRID_META = {
  ...commonSetting,
  ...MasterGridMetaData,
};

export const INFO_META = {
  infoInput: INFO_INPUT,
  modalColumnDefs: SEARCH_META.modalColumnDefs,
  searchModalInfo: INFO_MODAL_INFO,
  ...commonSetting,
};

export const MASTER_GRID_OPTIONS_META = {
  ...MasterGridOptions,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
};

export const DETAIL_GRID_OPTIONS_META = {
  ...DetailGridOptions,
  columnDefs: DETAIL_GRID_COLUMN_DEFS,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};
export const INFO: info = {
  autoModal: false,
  autoModalPage: "",
  pk: "",
};
