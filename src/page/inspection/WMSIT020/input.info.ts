/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS011/input.info.ts
 *  Description:    기준관리/매출거래처정보관리 Meta data
 *  Authors:        dhkim
 *  Update History:
 *                  2024.07 : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import { UtilService } from "@/services/util-service";
import type { IControlBtn, IInfoInputContainer, IModal } from "@/types";

export const INFO_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  "in/out-zone": {
    page: "WMSMS011",
    id: "in/out-zone",
    title: "search-in/out-zone",
    gridTitle: "zone-list",
    isNewBtn: "WMSCM083",
    apis: {
      url: "/WMSCM081/list_rn.action",
      params: {
        func: "fn_setWMSCM081",
        ZONE_ID: "",
        ZONE_NM: "",
        txtSrchZoneNm: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "txtSrchZoneNm",
        title: "name",
        type: "text",
        width: "triple",
      },
    ],
  },

  "asn-receiving-logistics-center-id": {
    page: "WMSMS011Q1",
    id: "asn-receiving-logistics-center-id",
    title: "search-logistics-center",
    gridTitle: "list-of-logistics-centers",

    modalOpenCondition: { rowDataKey: "ASN_YN", rowDataValue: "Y" },
    apis: {
      url: "/WMSMS011Q1/list_rn.action",
      params: {
        S_LC_CD: "",
        S_LC_NM: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_LC_CD",
        title: "logistics-center-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_LC_NM",
        title: "logistics-center-name",
        type: "text",
        width: "half",
      },
      {
        id: "S_LC_ID",
        title: "logistics-center-id",
        type: "text",
        width: "half",
      },
    ],
  },
  "asn-receiving-client-id": {
    page: "WMSMS011",
    id: "asn-receiving-client-id",
    title: "search-owner",
    gridTitle: "owner-list",

    modalOpenCondition: { rowDataKey: "ASN_IN_LC_ID", rowDataValue: "" },
    infoDefaultParamsData: {
      paramsKeys: ["S_LC_ID"],
      rowDataKeys: ["ASN_IN_LC_ID"],
    },
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "ALL",
        S_LC_ID: UtilService.getCurrentLocation(),
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  "asn-receiving-customer-id": {
    page: "WMSMS011",
    id: "asn-receiving-customer-id",
    title: "search-owner",
    gridTitle: "owner-list",
    modalOpenCondition: { rowDataKey: "ASN_IN_TRUST_CUST_ID", rowDataValue: "" },
    infoDefaultParamsData: {
      paramsKeys: ["S_CUST_ID"],
      rowDataKeys: ["ASN_IN_TRUST_CUST_ID"],
    },

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

export const INFO_INPUT: IInfoInputContainer[] = [
  {
    title: "sender-info",
    default: "expand",
    inputs: [
      //택배구분
      {
        title: "delivery-classification",
        ids: ["PARCEL_COM_TY"],
        type: "select",
        width: "half",
        size: "triple",
        optionsKey: "SWEET_DCD",
        required: true,
        options: [{ nameKey: "select", name: "", value: "" }],
        // optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
      },
      //접수구분
      {
        title: "application-category",
        ids: ["PARCEL_ORD_TY"],
        type: "select",
        width: "half",
        size: "triple",
        optionsKey: "PARCEL_ORD",
        required: true,
        disabled: true,
        options: [{ nameKey: "select", name: "", value: "" }],
      },
      //보내는이
      {
        title: "sender",
        ids: ["PARCEL_COM_TY_SEQ"],
        type: "select",
        width: "half",
        size: "triple",
        optionsKey: "PARCEL_COM_TY_SEQ",
        required: true,
        options: [{ nameKey: "select", name: "", value: "" }],
      },

      //운임구분
      {
        title: "fare-classification",
        ids: ["PARCEL_PAY_TY"],
        rowDataIds: ["PARCEL_PAY"],
        type: "select",
        width: "half",
        size: "triple",
        optionsKey: "PARCEL_PAY",
        required: true,
        disabled: true,
        options: [{ nameKey: "select", name: "", value: "" }],
      },
      //주소
      {
        title: "address",
        ids: ["dlvSenderInfoInput"],
        rowDataIds: ["dlvSenderInfoInput"],
        type: "text",
        width: "full",
        size: "triple",
        required: true,
        disabled: true,
      },
      //박스구분
      {
        title: "box-classification",
        ids: ["PARCEL_BOX_TY"],
        type: "select",
        width: "half",
        size: "triple",
        optionsKey: "PARCEL_BOX",
        required: true,
        disabled: true,
        options: [{ nameKey: "select", name: "", value: "" }],
      },
      //운송구분
      {
        title: "transportation-category",
        ids: ["PARCEL_ETC_TY"],
        type: "select",
        width: "half",
        size: "double",
        optionsKey: "PARCEL_ETC",
        required: true,
        disabled: true,
        options: [{ nameKey: "select", name: "", value: "" }],
      },
    ],
  },
  {
    title: "inspection-info",
    default: "expand",
    inputs: [
      {
        // 화주
        ids: ["vrSrchCustId", "vrSrchCustCd", "vrSrchCustNm"],
        hiddenId: "vrSrchCustId",
        rowDataIds: ["CUST_ID", "CUST_CD", "CUST_NM"],
        rowDataHiddenId: "CUST_ID",
        title: "owner",
        width: "full",
        required: true,
        type: "triple-search",
      },
      //단포/합포
      {
        ids: ["vrSrchMutiPackingYn"],
        hiddenId: "",
        rowDataIds: ["vrSrchMutiPackingYn"],
        rowDataHiddenId: "",
        title: "single-packaging/combined-packaging",
        width: "full",
        required: true,
        type: "select",
        optionsKey: "packing-type",
        size: "triple",
        options: [],
        optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
      },
      //웨이브 차수
      {
        ids: ["vrSrchOrdDegreeHddn"],
        hiddenId: "",
        rowDataIds: ["vrSrchOrdDegreeHddn"],
        rowDataHiddenId: "",
        title: "",
        required: false,
        type: "text",
        width: "full",
        size: "double",
        hidden: true,
      },
      //웨이브No //scannerOrdDgree
      {
        ids: ["vrSrchOrdDegree"],
        hiddenId: "",
        rowDataIds: ["vrSrchOrdDegree"],
        rowDataHiddenId: "",
        title: "wave-no",
        required: false,
        type: "text",
        width: "full",
        size: "double",
      },
      //Order Hddn
      {
        ids: ["vrSrchOrdIdHddn"],
        hiddenId: "",
        rowDataIds: ["vrSrchOrdIdHddn"],
        rowDataHiddenId: "",
        title: "",
        required: false,
        type: "text",
        width: "full",
        size: "double",
        hidden: true,
      },
      //주문 //scannerOrdId
      {
        ids: ["vrSrchOrdId"],
        hiddenId: "",
        rowDataIds: ["vrSrchOrdId"],
        rowDataHiddenId: "",
        title: "order",
        required: false,
        type: "text",
        width: "full",
        size: "double",
        hidden: true,
      },
      //상품 바코드 스캔
      {
        ids: ["vrSrchItemBarcode"],
        hiddenId: "",
        rowDataIds: [""],
        rowDataHiddenId: "",
        title: "상품바코드",
        width: "full",
        required: true,
        type: "text",
        size: "triple",
      },
      {
        // 박스 추가
        ids: ["SC_CHK_BOX_NO"],
        hiddenId: "",
        rowDataIds: ["SC_CHK_BOX_NO"],
        rowDataHiddenId: "SC_CHK_BOX_NO",
        title: "box-No",
        width: "full",
        required: false,
        type: "select",
        optionsKey: "box-number",
        // options: [{ name: "1", nameKey: "1", value: "1" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
        optionControlName: "add-box",
      },
      //검수수량/총수량
      {
        ids: ["pickingExecTotalCnt"],
        hiddenId: "",
        rowDataIds: ["pickingExecTotalCnt"],
        rowDataHiddenId: "",
        title: "inspection-qty/total-qty",
        width: "full",
        required: false,
        type: "text",
        size: "triple",
        disabled: true,
      },
      //고객오더번호
      {
        ids: ["vrSrchOrgOrdId"],
        hiddenId: "",
        rowDataIds: ["vrSrchOrgOrdId"],
        rowDataHiddenId: "",
        title: "customer-order-no",
        width: "full",
        required: false,
        type: "text",
        size: "triple",
        disabled: true,
      },
      //운송장번호
      {
        ids: ["vrSrchInvcNo"],
        hiddenId: "",
        rowDataIds: ["vrSrchInvcNo"],
        rowDataHiddenId: "",
        title: "transport-document-number",
        width: "full",
        required: false,
        type: "text",
        size: "triple",
        disabled: true,
      },
      //배차번호
      {
        ids: ["vrCarNo"],
        hiddenId: "",
        rowDataIds: ["vrCarNo"],
        rowDataHiddenId: "",
        title: "dispatch-number",
        width: "full",
        required: false,
        type: "text",
        size: "triple",
        disabled: true,
      },
      //고객명
      {
        ids: ["vrSrchRcvrNm"],
        hiddenId: "",
        rowDataIds: ["vrSrchRcvrNm"],
        rowDataHiddenId: "",
        title: "customer-nm",
        width: "full",
        required: false,
        type: "text",
        size: "triple",
        disabled: true,
      },
      //주소
      {
        ids: ["vrSrchRcvrAddress"],
        hiddenId: "",
        rowDataIds: ["vrSrchRcvrAddress"],
        rowDataHiddenId: "",
        title: "address",
        width: "full",
        required: false,
        type: "text",
        size: "triple",
        disabled: true,
      },
    ],
  },
];

export const INFO_CONTROL_BTN: IControlBtn[] = [
  {
    title: "reset",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "invoice-reprint",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "invoice-receive",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
];
