/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS093/input.ts
 *  Description:    상품별자동출고주문관리 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { IModal, ISearchInput, info } from "@/types/index";
import { ref } from "vue";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSCM011",
  pk: "CUST_ID",
};

export const isColumnHidden = ref(true);
export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "CUST_CD",
        title: "shipper-code",
        searchContainerInputId: "vrSrchCustCd",
        type: "text",
        width: "triple",
      },
      {
        id: "CUST_NM",
        title: "owner-name",
        searchContainerInputId: "vrSrchCustNm",
        type: "text",
        width: "triple",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product-code",
    title: "search-product",
    gridTitle: "product-list",

    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },

    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "viewAll",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        title: "product-code",
        searchContainerInputId: "vrSrchItemCd",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        title: "product-name",
        searchContainerInputId: "vrSrchItemNm",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        optionsKey: "S_SET_ITEM_YN",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
    ],
  },
};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  // {
  //   // 화주
  //   ids: ["vrSrchCustCd", "vrSrchCustNm"],
  //   hiddenId: "vrSrchCustId",
  //   rowDataIds: ["CUST_CD", "CUST_NM"],
  //   rowDataHiddenId: "CUST_ID",
  //   searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
  //   srchKey: "CUST",
  //   title: "owner",
  //   width: "triple",
  //   isModal: false,
  //   required: false,
  //   isSearch: true,
  //   placeholder: ["code", "name"],
  //   types: ["text", "text"],
  // },
  // //단포/합포
  // {
  //   ids: ["vrSrchOrderGb"],
  //   hiddenId: "",
  //   rowDataIds: [""],
  //   rowDataHiddenId: "",
  //   searchApiKeys: [""],
  //   title: "single-packaging/combined-packaging",
  //   width: "triple",
  //   isModal: false,
  //   required: false,
  //   isSearch: false,
  //   types: ["select-box"],
  //   optionsKey: "DS_TP",
  //   options: [
  //     { name: "single-piece-packaging", nameKey: "single-piece-packaging", value: "N" },
  //     { name: "combined-packaging", nameKey: "combined-packaging", value: "Y" },
  //   ],
  //   optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  // },
  // //웨이브No
  // {
  //   ids: ["vrSrchOrdDegree"],
  //   hiddenId: "vrSrchOrdDegreeHddn",
  //   rowDataIds: [""],
  //   rowDataHiddenId: "",
  //   searchApiKeys: [""],
  //   title: "wave-no",
  //   width: "triple",
  //   isModal: false,
  //   required: false,
  //   isSearch: false,
  //   types: ["text"],
  // },
];

// defaultParams확인 필요
export const MASTER_SEARCH_INPUT: ISearchInput[] = [
  //input 3

  //택배구분
  {
    ids: [""],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "delivery-classification",
    width: "half",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "DS_TP",
    options: [
      { name: "all", nameKey: "all", value: "" },
      // { name: "single-piece-packaging", nameKey: "single-piece-packaging", value: "N" },
      // { name: "combined-packaging", nameKey: "combined-packaging", value: "Y" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  //보내는이
  {
    ids: ["vrSrchOrderGb"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "sender",
    width: "half",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "DS_TP",
    options: [{ name: "", nameKey: "", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

// defaultParams확인 필요
export const DETAIL_SEARCH_INPUT: ISearchInput[] = [];

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useMore: false,
  useSetting: false, // TODO :: 위치 조정 이 후, 재설정 (TRUE)
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  masterSearchInput: MASTER_SEARCH_INPUT,
  detailSearchInput: DETAIL_SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  searchConditionInitUrl: "",
};
