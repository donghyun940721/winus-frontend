/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSIT020/column-defs.ts
 *  Description:    기준관리/상품별 원주자재(부품/용기관리) 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { MODAL_COLUMN_DEFS } from "@/page/common-page/meta/common-modal-column-defs";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
export { MODAL_COLUMN_DEFS };

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  //(체크박스)
  {
    field: "CHK_BOX",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: false,
    checkboxSelection: true,
    width: 35,
  },
  //바코드
  {
    field: "ITEM_BAR_CD",
    headerKey: "barcode",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 140,
  },
  //상품코드
  {
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 140,
  },
  //상품명
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 240,
  },
  //출고주문량
  {
    field: "OUT_LOC_QTY",
    headerKey: "shipping-order-qty",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  //박스No
  {
    field: "BOX_NO",
    headerKey: "box-No",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  //검수량
  {
    field: "CHK_QTY",
    headerKey: "inspect-qty",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 100,
  },

  //잔여량
  {
    field: "REST_QTY",
    headerKey: "remaining-qty",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 100,
  },
  //단위
  {
    field: "UOM_CD",
    headerKey: "unit-box",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 50,
  },
  //피킹여부
  {
    field: "PICKING_YN",
    headerKey: "picking-Y/N",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 120,
  },
  //출력수
  {
    field: "PRINT_CNT",
    headerKey: "print-qty",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 120,
  },
  //택배사
  {
    field: "DLV_CD_NM",
    headerKey: "delivery-company",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 120,
  },
  //운송장번호
  {
    field: "INVC_NO",
    headerKey: "transport-document-number",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 140,
  },
  //출고예정일
  {
    field: "OUT_REQ_DT",
    headerKey: "expected-delivery-date",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 140,
  },
  //주문번호
  {
    field: "ORD_ID",
    headerKey: "order-id",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 120,
  },
  //주문SEQ
  {
    field: "ORD_SEQ",
    headerKey: "order-SEQ",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 140,
  },
  //채널
  {
    field: "DATA_SENDER_NM",
    headerKey: "channel",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 120,
  },
  //원주문번호
  {
    field: "ORG_ORD_ID",
    headerKey: "original-order-number",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 180,
  },
  //원SEQ
  {
    field: "CUST_ORD_SEQ",
    headerKey: "ori-SEQ",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 100,
  },
  //차수
  {
    field: "ORD_DEGREE",
    headerKey: "degree",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 100,
  },
  //차수명
  {
    field: "ORD_DEGREE_NM",
    headerKey: "degree-name",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 120,
  },
  //개체여부
  {
    field: "TYPE_S_YN",
    headerKey: "individual-Y/N",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 120,
  },
  //고객명
  {
    field: "D_CUST_NM",
    headerKey: "customer-nm",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 120,
  },
  //주소
  {
    field: "D_ADDRESS",
    headerKey: "address",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 200,
  },
  //상세주소
  {
    field: "D_ADDRESS2",
    headerKey: "detail-address",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 250,
  },
  //전화번호
  {
    field: "D_TEL",
    headerKey: "tel",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 120,
  },
  //우편번호
  {
    field: "D_ZIP",
    headerKey: "zip-code",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 140,
  },
  //작업순번
  {
    field: "CHK_WORK_SEQ",
    headerKey: "work-order",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 120,
  },
  //작업시간
  {
    field: "CHK_WORK_DT",
    headerKey: "work-time",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 150,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  //박스No
  {
    field: "BOX_NO",
    headerKey: "box-No",
    headerName: "",
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  //잔여출고량
  {
    field: "BOX_REST_QTY",
    headerKey: "rest-qty",
    headerName: "",
    cellStyle: { textAlign: "center" },

    headerClass: "header-center",
    sortable: true,
    width: 140,
  },
  //작업량
  {
    field: "BOX_REAL_CHK_QTY",
    headerKey: "work-qty",
    headerName: "",
    cellStyle: { textAlign: "left" },

    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  //주문번호
  {
    field: "BOX_ORD_ID",
    headerKey: "order-id",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 140,
  },
  //주문SEQ
  {
    field: "BOX_ORD_SEQ",
    headerKey: "order-seq-2",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  //상품코드
  {
    field: "BOX_RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  //상품명
  {
    field: "BOX_RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 200,
  },
  //단위
  {
    field: "BOX_UOM_CD",
    headerKey: "unit-box",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 100,
  },
];
