/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM100_3/column-defs.ts
 *  Description:    임가공 상품관리 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import GridSearchButton from "@/components/renderer/grid-search-button.vue";
import gridSelect from "@/components/renderer/grid-select.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { Getter } from "@/lib/ag-grid";
import { Format } from "@/lib/ag-grid/index";
import { GridUtils } from "@/lib/ag-grid/utils";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
import type { ICellEditorParams } from "ag-grid-community";

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  "kit-product": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
      valueFormatter: Format.NumberPrice,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "RITEM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "ITEM_ENG_NM",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "CUST_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_CD",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_CD",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_NM",
      sortable: true,
      hide: true,
    },
  ],
};

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // 세트상품ID
    field: "SET_RITEM_ID",
    sortable: true,
    hide: true,
  },
  {
    // 세트상품코드
    field: "SET_RITEM_CD",
    headerKey: "set-product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "center" },
  },
  {
    // 세트상품명
    field: "SET_RITEM_NM",
    headerKey: "set-product-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 500,
    cellStyle: { textAlign: "left" },
  },
  {
    field: "SET_RITEM_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 500,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 수량
    field: "QTY",
    headerKey: "quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "right", backgroundColor: "#FFFFDD" },
  },
  {
    // 생산처
    field: "MAKER_NM",
    headerKey: "order-maker",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "left" },
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "",
    headerKey: "no",
    maxWidth: 80,
    valueGetter: (params: any) => {
      if (String(params.node.data.PART_RITEM_ID).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    cellStyle: { textAlign: "center", backgroundColor: "#FFFFDD" },
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
    cellStyle: { textAlign: "center", backgroundColor: "#FFFFDD" },
  },
  {
    // 상품코드
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center", backgroundColor: "#FFFFDD" },
  },
  {
    field: "ITEM",
    headerName: "",
    maxWidth: 60,
    cellRenderer: GridSearchButton,
    cellClass: "renderer-cell",
    cellStyle: { textAlign: "center", backgroundColor: "#FFFFDD" },
  },
  {
    field: "PART_RITEM_ID",
    sortable: true,
    hide: true,
  },
  {
    // 상품명
    field: "PART_RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 500,
    cellStyle: { textAlign: "center", backgroundColor: "#FFFFDD" },
  },
  {
    // 수량
    field: "QTY",
    headerKey: "qty",
    headerName: "",
    sortable: true,
    headerClass: "header-require",
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      console.log(params.event.key);
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    export: true,
    width: 110,
    cellStyle: { textAlign: "right", backgroundColor: "#FFFFDD" },
  },
  {
    // UOM
    field: "UOM",
    headerKey: "uom",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center", backgroundColor: "#FFFFDD" },

    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("UOM"), // UOM 코드만 가져오기
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("UOM", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("UOM", params.value);
    },
    editable: true,
  },
];
