/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM100_3/input.search.ts
 *  Description:    임가공 상품관리 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

import type { IModal, ISearchInput, info } from "@/types";
import { IControlBtn } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSMS011",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  "kit-product": {
    page: "WMSCM091",
    id: "kit-product",
    title: "search-product",
    gridTitle: "product-list",

    defaultParamsData: {
      //해당 모달의 title
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        func: "fn_setWMSCM091",
        vrSrchCustId: "",
        vrSrchWhId: "",
        vrViewSetItem: "viewSetItem",
        vrViewAll: "viewAll",
        vrItemType: "",
        RITEM_ID: "",
        ITEM_CODE: "",
        ITEM_KOR_NM: "",
        UOM_ID: "",
        TIME_PERIOD_DAY: "",
        STOCK_QTY: "",
        CUST_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        UNIT_PRICE: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        title: "product-code",
        searchContainerInputId: "vrSrchItemCd",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchItemNm",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [{ name: "", nameKey: "all", value: "" }],
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        //임가공 select-box값
        options: [
          { name: "", nameKey: "all", value: "" },
          { name: "useProductY", nameKey: "useProductY", value: "Y" },
          { name: "useProductN", nameKey: "useProductN", value: "N" },
        ],
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: true,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 상품코드
    ids: ["vrSrchItemCd"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "product-code",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 상품명
    ids: ["vrSrchItemNm"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "item-name",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 생산처
    ids: ["vrSrchMaker"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "maker",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

export const SEARCH_COMPONENT_CONTROL_BTN: IControlBtn[] = [];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: false,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchComponentControlBtn: SEARCH_COMPONENT_CONTROL_BTN,
  pageInfo: INFO,
};
