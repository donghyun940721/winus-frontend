/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM100_3/input.grid.ts
 *  Description:    임가공 상품관리 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";
import { IControlBtn, IGridCellSearchButton, IGridCellSelectBox } from "@/types";
import { GridOptions } from "ag-grid-community";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";

export const DETAIL_GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  UOM: {
    pk: "RNUM",
    optionsKey: "UOM",
  },
};

export const DETAIL_GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  ITEM: {
    fieldList: ["PART_RITEM_ID", "ITEM_CODE", "PART_RITEM_NM"],
    rowDataKeys: ["RITEM_ID", "ITEM_CODE", "ITEM_KOR_NM"],
    modalData: {
      page: "WMSCM091",
      id: "kit-product",
      title: "search-product",
      gridTitle: "product-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달

      defaultParamsData: {
        //해당 모달의 title
        storeSaveKey: "owner",
        paramsKeys: ["vrSrchCustId"],
        rowDataKeys: ["CUST_ID"],
      },
      apis: {
        url: "/WMSCM091/list_rn.action",
        params: {
          // func: "fn_setWMSCM091_subE6",
          vrSrchCustId: "",
          vrSrchWhId: "",
          vrViewSetItem: "",
          vrViewAll: "viewAll",
          vrItemType: "",
          RITEM_ID: "",
          ITEM_CODE: "",
          ITEM_KOR_NM: "",
          UOM_ID: "",
          TIME_PERIOD_DAY: "",
          STOCK_QTY: "",
          CUST_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          UNIT_PRICE: "",
          vrSrchItemCd: "",
          vrSrchItemNm: "",
          vrSrchItemGrp: "",
          vrSrchSetItemYn: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchItemCd",
          title: "product-code",
          searchContainerInputId: "vrSrchItemCd",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemNm",
          searchContainerInputId: "vrSrchItemNm",
          title: "product-name",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemGrp",
          title: "product-group",
          type: "select",
          width: "half",
          optionsKey: "ITEMGRP",
          options: [{ name: "all", nameKey: "all", value: "" }],
        },
        {
          id: "vrSrchSetItemYn",
          title: "repacking",
          type: "select",
          width: "half",
          options: [
            { name: "", nameKey: "all", value: "" },
            { name: "useProductY", nameKey: "useProductY", value: "Y" },
            { name: "useProductN", nameKey: "useProductN", value: "N" },
          ],
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "CUST_NM",
        headerKey: "owner",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_GRP_NAME",
        headerKey: "product-group",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_CODE",
        headerKey: "product-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_KOR_NM",
        headerKey: "product-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "BOX_BAR_CD",
        headerKey: "box-barcode",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "MAKER_NM",
        headerKey: "company-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "STOCK_QTY",
        headerKey: "current-stock",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "BAD_QTY",
        headerKey: "inferior-product",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "UOM_NM",
        headerKey: "uom",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "UNIT_PRICE",
        headerKey: "unit-price",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
        valueFormatter: Format.NumberPrice,
      },
      {
        field: "WH_NM",
        headerKey: "warehouse",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "RITEM_ID",
        headerKey: "product-code",
        hide: true,
      },
      {
        field: "ITEM_ENG_NM",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "CUST_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_NM",
        sortable: true,
        hide: true,
      },
    ],
  },
};

export const MASTER_CONTROL_BTN: IControlBtn[] = [
  {
    title: "delete-product-master",
    colorStyle: "danger",
    paddingStyle: "normal",
    disabled: "",
    image: "",
    authType: "DEL_AUTH",
  },
];

export const DETAIL_CONTROL_BTN: IControlBtn[] = [
  {
    title: "edit-detail",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "new-product-detail",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "delete-product-detail",
    colorStyle: "danger",
    paddingStyle: "normal",
    disabled: "",
    image: "",
    authType: "DEL_AUTH",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [200, 300, 400],
};

export const MasterGridMetaData: any = {
  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [],
};

export const DetailGridMetaData: any = {
  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "",

  //페이징옵션
  pagingSizeList: [],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
    lockVisible: true,
    lockPosition: true,
  },
  headerHeight: 32,
  rowHeight: 32,
  mainColumnDefs: MASTER_GRID_COLUMN_DEFS,
  subColumnDefs: DETAIL_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  // getRowId: (data) => {
  //   return data.data.RNUM;
  // },
};
