/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM100_1/column-defs.ts
 *  Description:    임가공주문관리 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // SEQ
    field: "REG_SEQ",
    headerKey: "reg-seq",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // WMS물류센터(WMS ID 참조)
    field: "REG_LC_ID",
    headerKey: "reg-lc-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // WMS화주ID(WMS ID 참조)
    field: "REG_CUST_ID",
    headerKey: "reg-cust-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 고객원주문번호(WMS사용)
    field: "ORG_ORD_ID",
    headerKey: "original-order-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 고객원주문SEQ(WMS사용)
    field: "ORG_ORD_SEQ",
    headerKey: "customer-original-order-seq",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 주문일자
    field: "ORD_DT",
    headerKey: "order-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 배송일자
    field: "ORD_DLV_DT",
    headerKey: "delivery-date-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 주문종류(WMS)
    field: "ORD_TYPE",
    headerKey: "order-type-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 주문상세타입(WMS)
    field: "ORD_SUB_TYPE",
    headerKey: "order-sub-type",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 고객Biz타입(B2B,B2C,B2G,C2C 등)
    field: "ORD_BIZ_TYPE",
    headerKey: "order-biz-type",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // BL번호
    field: "ORD_BL_NO",
    headerKey: "order-bl-no",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 생산처
    field: "ORD_MAKER",
    headerKey: "order-maker",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품코드
    field: "ORD_ITEM_CD",
    headerKey: "product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품명
    field: "ORD_ITEM_NM",
    headerKey: "product-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품명(옵션)
    field: "ORD_ITEM_OP_NM",
    headerKey: "order-item-option-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 주문수량
    field: "ORD_QTY",
    headerKey: "make-qty",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // UOM
    field: "ORD_UOM",
    headerKey: "uom",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 사이즈
    field: "ORD_SIZE",
    headerKey: "size",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 색상
    field: "ORD_COLOR",
    headerKey: "color",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // LOT번호
    field: "ORD_CUST_LOT_NO",
    headerKey: "lot-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 출고 원주문번호 순번
    field: "ORD_LEGACY_ORD_DETAIL_NO",
    headerKey: "order-legacy-order-detail-no",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 생산일자
    field: "ORD_MAKE_DT",
    headerKey: "order-make-dt",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품유효기간
    field: "ORD_ITEM_BEST_DATE",
    headerKey: "order-item-best-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품유통기한만료일
    field: "ORD_ITEM_BEST_DATE_END",
    headerKey: "order-item-best-date-end",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 단가
    field: "ORD_UNIT_AMT",
    headerKey: "unit-price",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 부가세
    field: "ORD_VAT",
    headerKey: "surtax",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 합계
    field: "ORD_TOT",
    headerKey: "sum",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 결제여부
    field: "ORD_PAY_YN",
    headerKey: "order-pay-yn",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 주문인입구분(etc오더구분)
    field: "ORD_INCOM_ETC_TYPE",
    headerKey: "order-income-etc-type",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 배송지연구분(메시지)
    field: "ORD_DLV_DELAY_MSG",
    headerKey: "order-delivery-delay-msg",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 비고1
    field: "ORD_DESC1",
    headerKey: "remark-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 비고2
    field: "ORD_DESC2",
    headerKey: "remark-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 배송메시지1
    field: "DLV_DESC1",
    headerKey: "delivery-message-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 배송메시지2
    field: "DLV_DESC2",
    headerKey: "delivery-message-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 주문원천_고객원주문번호
    field: "LEGACY_ORG_ORD_ID",
    headerKey: "legacy-original-order-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 주문원천_고객원주문SEQ
    field: "LEGACY_ORG_ORD_SEQ",
    headerKey: "legacy-original-order-seq",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM고객코드
    field: "FROM_CUSTOMER_CD",
    headerKey: "from-customer-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM고객명
    field: "FROM_CUSTOMER_NM",
    headerKey: "from-customer-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM고객코드(예비)
    field: "FROM_CUSTOMER_CD_SUB",
    headerKey: "from-customer-code-sub",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM고객명(예비)
    field: "FROM_CUSTOMER_NM_SUB",
    headerKey: "from-customer-name-sub",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM전화번호1
    field: "FROM_TEL_1",
    headerKey: "from-tel-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM전화번호2
    field: "FROM_TEL_2",
    headerKey: "from-tel-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM우편번호
    field: "FROM_ZIP",
    headerKey: "from-zip",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM주소1
    field: "FROM_ADDRESS_1",
    headerKey: "from-address-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM주소2
    field: "FROM_ADDRESS_2",
    headerKey: "from-address-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM주소3
    field: "FROM_ADDRESS_3",
    headerKey: "from-address-3",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM국가
    field: "FROM_COUNTRY_REGION",
    headerKey: "from-country-region",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM도시
    field: "FROM_CITY",
    headerKey: "from-city",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM도_주
    field: "FROM_STATE_PROVINCE",
    headerKey: "from-state-province",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM_HS_CODE
    field: "FROM_HS_CODE",
    headerKey: "from-hs-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM카테고리번호
    field: "FROM_CATEGORY_NO",
    headerKey: "from-category-no",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM배송사
    field: "FROM_SHIP_COMPANY",
    headerKey: "from-ship-company",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM송장번호
    field: "FROM_SHIP_WAYBILL_NO",
    headerKey: "from-ship-waybill-no",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // FROM인보이스번호
    field: "FROM_SHIP_INVOICE_NO",
    headerKey: "from-ship-invoice-no",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO고객코드
    field: "TO_CUSTOMER_CD",
    headerKey: "to-customer-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO고객명
    field: "TO_CUSTOMER_NM",
    headerKey: "to-customer-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO고객코드(예비)
    field: "TO_CUSTOMER_CD_SUB",
    headerKey: "to-customer-code-sub",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO고객명(예비)
    field: "TO_CUSTOMER_NM_SUB",
    headerKey: "to-customer-name-sub",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO전화번호1
    field: "TO_TEL_1",
    headerKey: "to-tel-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO전화번호2
    field: "TO_TEL_2",
    headerKey: "to-tel-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO우편번호
    field: "TO_ZIP",
    headerKey: "to-zip",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO주소1
    field: "TO_ADDRESS_1",
    headerKey: "to-address-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO주소2
    field: "TO_ADDRESS_2",
    headerKey: "to-address-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO주소3
    field: "TO_ADDRESS_3",
    headerKey: "to-address-3",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO국가
    field: "TO_COUNTRY_REGION",
    headerKey: "to-country-region",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO도시
    field: "TO_CITY",
    headerKey: "to-city",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO도_주
    field: "TO_STATE_PROVINCE",
    headerKey: "to-state-province",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO_HS_CODE
    field: "TO_HS_CODE",
    headerKey: "to-hs-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO카테고리번호
    field: "TO_CATEGORY_NO",
    headerKey: "to-category-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO배송사
    field: "TO_SHIP_COMPANY",
    headerKey: "to-ship-company",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO운송장번호
    field: "TO_SHIP_WAYBILL_NO",
    headerKey: "to-ship-waybill-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // TO인보이스번호
    field: "TO_SHIP_INVOICE_NO",
    headerKey: "to-ship-invoice-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // IF 차수
    field: "ORD_DEGREE",
    headerKey: "order-degree",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 출고 원주문번호
    field: "ORD_LEGACY_ORD_NO",
    headerKey: "order-legacy-order-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 등록일
    field: "SYS_REG_DT",
    headerKey: "reg-dt",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 등록번호
    field: "SYS_REG_NO",
    headerKey: "registrant-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 등록자IP
    field: "SYS_REG_WORK_IP",
    headerKey: "registration-work-ip",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 수정일
    field: "SYS_UPD_DT",
    headerKey: "date-modified",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 수정자번호
    field: "SYS_UPD_NO",
    headerKey: "modifier-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 수정자IP
    field: "SYS_UPD_WORK_IP",
    headerKey: "update-work-ip",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 삭제여부
    field: "SYS_DEL_YN",
    headerKey: "delete-Y/N",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // WMS연동여부
    field: "SYS_WMS_LINK_YN",
    headerKey: "sys-wms-link-y/n",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // IF전송여부(Y,N)
    field: "IF_FLAG",
    headerKey: "if-process-success",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 결과메시지
    field: "IF_RST_MSG",
    headerKey: "result-message",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 임가공설정
    field: "SET_ITEM_BUTTON",
    headerKey: "repacking-setting",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
];
