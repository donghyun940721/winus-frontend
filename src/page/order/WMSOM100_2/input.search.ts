/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM100_2/input.search.ts
 *  Description:    임가공 상품생성 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

import type { IModal, ISearchInput, info } from "@/types";
import { IControlBtn } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSMS011",
  pk: "RNUM",
  pk2: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    //모달에서 접속할 페이지
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      //모달에서 검색했을 때 전달되는 파라미터들
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchCustCd", "vrSrchCustNm", "vrSrchCustId"],
    hiddenId: "",
    rowDataIds: ["CUST_CD", "CUST_NM", "CUST_ID"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 상품명
    ids: ["vrSrchItemNm"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "item-name",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 생산처
    ids: ["vrSrchMaker"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "maker",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

// defaultParams확인 필요
export const DETAIL_SEARCH_INPUT: ISearchInput[] = [
  {
    // 세트상품코드 SET_ITEM_CD
    ids: ["SET_ITEM_CD"],
    hiddenId: "",
    rowDataIds: ["ORD_ITEM_CD"],
    rowDataHiddenId: "",
    searchApiKeys: ["SET_ITEM_CD"],
    title: "set-product-code",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 세트상품명
    ids: ["SET_ITEM_NM"],
    hiddenId: "",
    rowDataIds: ["ORD_ITEM_NM"],
    rowDataHiddenId: "",
    searchApiKeys: ["SET_ITEM_NM"],
    title: "set-product-name",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["SET_ITEM_MAKER_NM"],
    hiddenId: "",
    rowDataIds: ["SET_ITEM_MAKER_NM"],
    rowDataHiddenId: "",
    searchApiKeys: ["SET_ITEM_MAKER_NM"],
    title: "",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
    hidden: true,
  },
];

export const SEARCH_COMPONENT_CONTROL_BTN: IControlBtn[] = [];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: false,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchComponentControlBtn: SEARCH_COMPONENT_CONTROL_BTN,
  pageInfo: INFO,
};
