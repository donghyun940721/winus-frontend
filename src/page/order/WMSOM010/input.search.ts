/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM010/input.ts
 *  Description:    거래명세표 발행 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { UtilService } from "@/services/util-service";
import type { IModal, ISearchInput, info } from "@/types";
import { IControlBtn } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSMS011",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  customer: {
    page: "WMSCM200",
    id: "customer",
    title: "search-owner",
    gridTitle: "owner-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["S_CUST_ID"],
      rowDataKeys: ["CUST_ID"],
    },

    apis: {
      url: "/WMSCM200/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "LC_ALL",
        S_LC_ID: UtilService.getCurrentLocation(),
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "txtSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "txtSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    //화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: true,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 일자
    ids: ["vrCalDlvDt", "vrSelDtType"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "date",
    width: "quarter",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["date", "select-box"],
    options: [
      { name: "based-on-delivery-date(+1 day)", nameKey: "based-on-delivery-date(+1 day)", value: "DLV_DT" },
      { name: "based-on-order-date", nameKey: "based-on-order-date", value: "ORD_DT" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 라우팅
    ids: ["vrSrchRoutDesc"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchRoutDesc"],
    srchKey: "",
    title: "routing",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchRoutDesc",
    options: [
      { nameKey: "not-selected", name: "not-selected", value: "" },
      { nameKey: "deokpyeong", name: "deokpyeong", value: "" },
      { nameKey: "chodo", name: "chodo", value: "" },
      { nameKey: "chungcheong", name: "chungcheong", value: "" },
      { nameKey: "gyeongnam", name: "gyeongnam", value: "" },
      { nameKey: "gyeongbuk", name: "gyeongbuk", value: "" },
      { nameKey: "honam", name: "honam", value: "" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 상품구분
    ids: ["productType"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["productType"],
    srchKey: "",
    title: "item-division",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "productType",
    options: [
      { nameKey: "all", name: "all", value: "" },
      { nameKey: "milk", name: "milk", value: "milk" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 거래처
    ids: ["vrSrchPtnrCd", "vrSrchPtnrNm"],
    hiddenId: "vrSrchPtnrId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "customer-owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    // 차량번호
    ids: ["vrSrchTrcNum"],
    hiddenId: "vrSrchPtnrId",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchTrcNum"],
    srchKey: "PARTNER",
    title: "vehicle-number",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    placeholder: ["vehicle-number"],
    types: ["text"],
  },
  {
    // 품온구분
    ids: ["vrSrchTempType"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchTempType"],
    srchKey: "",
    title: "temperature-type-name",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchTempType",
    options: [
      { nameKey: "all", name: "all", value: "" },
      { nameKey: "frozen", name: "frozen", value: "1" },
      { nameKey: "chilled", name: "chilled", value: "2" },
      { nameKey: "ambient", name: "ambient", value: "3" },
      { nameKey: "fresh", name: "fresh", value: "4" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

export const GRID_STATISTICS_INFO = [
  {
    id: "TOTAL_QTY",
    title: "number-of-searched-results",
  },
];

export const SEARCH_COMPONENT_CONTROL_BTN: IControlBtn[] = [];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: true,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchComponentControlBtn: SEARCH_COMPONENT_CONTROL_BTN,
  pageInfo: INFO,
};
