/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM010/column-defs.ts
 *  Description:    거래명세표 발행 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import type { IColDef, IColGroupDef } from "@/types/agGrid";
export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
};

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // 회사코드
    field: "SPDCOL_COMPANY_CD",
    headerKey: "company-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
    pinned: "left",
  },
  {
    // 주문번호
    field: "SPDCOL_ORD_NO",
    headerKey: "order-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "left" },
  },
  {
    // 주문유형
    field: "SPDCOL_ORD_TP",
    headerKey: "order-type",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 주문구분
    field: "SPDCOL_ORD_DIV",
    headerKey: "order-type-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 주문일자
    field: "SPDCOL_ORD_DT",
    headerKey: "order-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "left" },
  },
  {
    // 배송유형
    field: "SPDCOL_DLV_TP",
    headerKey: "delivery-type-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 배송일자
    field: "SPDCOL_DLV_DT",
    headerKey: "order-delivery-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "left" },
  },
  {
    // 고객사코드
    field: "SPDCOL_PARTNER_CD",
    headerKey: "customer-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "left" },
  },
  {
    // 점포명
    field: "SPDCOL_LN_PARTNER",
    headerKey: "store-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 250,
    cellStyle: { textAlign: "left" },
  },
  {
    // 라우팅명
    field: "SPDCOL_ROUTDESC",
    headerKey: "routing-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "left" },
  },
  {
    // 비고
    field: "SPDCOL_RMKS",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 배송기사명
    field: "SPDCOL_DLV_DRIVER_NM",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 배송기사 연락처
    field: "SPDCOL_DLV_DRIVER_TEL",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 등록일
    field: "SPDCOL_INSERT_DTS",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 등록자
    field: "SPDCOL_INSERT_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 수정일
    field: "SPDCOL_UPDATE_DTS",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 수정자
    field: "SPDCOL_UPDATE_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "RNUM")) {
        return params.node.rowIndex + 1;
      } else {
        return "";
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    pinned: "left",
  },
  {
    // 회사코드
    field: "SPDCOL_D_COMPANY_CD",
    headerKey: "company-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
    pinned: "left",
  },
  {
    // 주문번호
    field: "SPDCOL_D_ORD_NO",
    headerKey: "order-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "left" },
    pinned: "left",
  },
  {
    // 주문상세번호
    field: "SPDCOL_D_ORD_DETAIL_NO",
    headerKey: "order-detail-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
  },
  {
    // PLANT_CD
    field: "SPDCOL_D_PLANT_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // 상품코드
    field: "SPDCOL_D_ITEM_CD",
    headerKey: "product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
  },
  {
    // 상품명
    field: "SPDCOL_D_NM_ITEM",
    headerKey: "product-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 300,
    cellStyle: { textAlign: "left" },
  },
  {
    // 수량
    field: "SPDCOL_D_QT",
    headerKey: "quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 50,
    cellStyle: { textAlign: "left" },
  },
  {
    // 단가
    field: "SPDCOL_D_UM",
    headerKey: "unit-price",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
  },
  {
    // 공급가
    field: "SPDCOL_D_AM",
    headerKey: "a-supply-price",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
  },
  {
    // 부가세
    field: "SPDCOL_D_VAT",
    headerKey: "surtax",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
  },
  {
    // 합계
    field: "SPDCOL_D_TOT",
    headerKey: "sum",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
  },
  {
    // RMKS
    field: "SPDCOL_D_RMKS",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // LINK_YN
    field: "SPDCOL_D_LINK_YN",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // QTIO_TP
    field: "SPDCOL_D_QTIO_TP",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // CC_CD
    field: "SPDCOL_D_CC_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // WMS_LINK_YN
    field: "SPDCOL_D_WMS_LINK_YN",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // INSERT_DTS
    field: "SPDCOL_D_INSERT_DTS",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 180,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // INSERT_ID
    field: "SPDCOL_D_INSERT_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // UPDATE_DTS
    field: "SPDCOL_D_UPDATE_DTS",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // UPDATE_ID
    field: "SPDCOL_D_UPDATE_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // TRCNUM
    field: "SPDCOL_D_TRCNUM",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: false,
    width: 100,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
];
