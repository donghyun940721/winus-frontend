/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM120/column-defs.ts
 *  Description:    주문/입고주문관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import gridSearchButton from "@/components/renderer/grid-search-button.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const NEW_POPUP_FORM_COLUMN_DEFS: any = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    width: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
  },
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    width: 150,
  },
  {
    field: "ITEM",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 50,
    cellRenderer: gridSearchButton,
  },
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    width: 150,
  },
  {
    field: "ITEM_BARCODE",
    headerKey: "product-detail-code",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  {
    field: "UOM_CD",
    headerKey: "uom",
    headerName: "",
    headerClass: "header-require",
    width: 150,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
    sortable: true,
  },
  {
    field: "IN_WORK_ORD_QTY",
    headerKey: "ord-qty",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    width: 150,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
  },
  {
    field: "REAL_IN_QTY",
    headerKey: "work-quantity",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    cellRenderer: gridTextInput,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
  },
  {
    field: "ORD_WEIGHT",
    headerKey: "order-weight",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    cellRenderer: gridTextInput,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
  },
  {
    field: "UM",
    headerKey: "unit-price",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    cellRenderer: gridTextInput,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
  },
  {
    field: "AM",
    headerKey: "a-supply-price",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    cellRenderer: gridTextInput,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
  },
  {
    field: "VAT",
    headerKey: "surtax",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    cellRenderer: gridTextInput,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
  },
  {
    field: "ETC1",
    headerKey: "crop",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
  },
  {
    field: "UNIT_NO",
    headerKey: "unit-no",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
  },
];

export const NEW_POPUP_MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      cellStyle: { textAlign: "center" },
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "receiving-address": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "ETC1",
      headerKey: "customer-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CODE",
      headerKey: "customer-code(owner)",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "NAME",
      headerKey: "customer-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "receiving-warehouse": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  customer: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  product: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      cellStyle: { textAlign: "right" },
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.commonRowIndex,
    menuTabs: ["columnsMenuTab"],
    lockVisible: true,
    lockPosition: true,
    pinned: "left",
  },
  {
    field: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    pinned: "left",
    lockVisible: true,
    lockPosition: true,
  },
  {
    field: "WORK_STAT_NM",
    headerName: t("grid-column-name.registrations-state"),
    headerClass: "header-center",
    sortable: true,
    width: 110,
  },
  {
    field: "PARTNER_CD",
    headerName: t("grid-column-name.customer-code"),
    headerClass: "header-center",
    width: 130,
    lockVisible: true,
    lockPosition: true,
    sortable: true,
    pinned: "left",
  },
  {
    field: "PARTNER_NM",
    headerName: t("grid-column-name.customer-name-2"),
    headerClass: "header-center",
    width: 150,
    sortable: true,
  },
  {
    field: "ORG_ORD_NO",
    headerName: t("grid-column-name.receiving-request-number"),
    headerClass: "header-center",
    width: 200,
    sortable: true,
  },
  {
    field: "ORD_DETAIL_NO",
    headerName: t("grid-column-name.order-of-request"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 100,
  },
  {
    field: "LEGACY_ORG_ORD_NO",
    headerName: t("grid-column-name.customer-order-number"),
    headerClass: "header-center",
    sortable: true,
    width: 200,
  },
  {
    field: "ORD_TYPE_NM",
    headerName: t("grid-column-name.order-type"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 100,
  },
  {
    field: "ORD_SUBTYPE_NM",
    headerName: t("grid-column-name.order-detail-type"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 130,
  },
  {
    field: "BITEM_BAR_CODE",
    headerName: t("grid-column-name.product-barcode"),
    headerClass: "header-center",
    sortable: true,
    width: 200,
  },
  {
    field: "ITEM_CD",
    headerName: t("grid-column-name.item-code"),
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ITEM_SIZE",
    headerName: t("grid-column-name.size"),
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 110,
  },
  {
    field: "ITEM_COLOR",
    headerName: t("grid-column-name.color"),
    headerClass: "header-center",
    sortable: true,
    width: 110,
  },
  {
    field: "ITEM_NM",
    headerName: t("grid-column-name.item-name"),
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ITEM_BARCODE",
    headerName: t("grid-column-name.multi-product-barcode"),
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "CUST_LOT_NO",
    headerName: t("grid-column-name.LOT-NO"),
    headerClass: "header-center",
    sortable: true,
    width: 250,
  },
  {
    field: "ORD_QTY",
    headerName: t("grid-column-name.ord-qty"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "WMS_ORD_QTY",
    headerName: t("grid-column-name.order-registration-quantity"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "IN_QTY",
    headerName: t("grid-column-name.receiving-confirmed-quantity"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "UOM",
    headerName: t("grid-column-name.uom"),
    headerClass: "header-center",
    sortable: true,
    width: 80,
  },
  {
    field: "VALID_DT",
    headerName: t("grid-column-name.expiration-date"),
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "ORD_DT",
    headerName: t("grid-column-name.order-date"),
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "DLV_DT",
    headerName: t("grid-column-name.delivery-date"),
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "IN_DT",
    headerName: t("grid-column-name.receiving-date"),
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "UM",
    headerName: t("grid-column-name.unit-price"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  {
    field: "AM",
    headerName: t("grid-column-name.a-supply-price"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  {
    field: "VAT",
    headerName: t("grid-column-name.surtax"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  {
    field: "ETD",
    headerName: t("grid-column-name.ETD"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 130,
  },
  {
    field: "ETA",
    headerName: t("grid-column-name.ETA"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 130,
  },
  {
    field: "EXCHANGE_RATE",
    headerName: t("grid-column-name.exchange-rate"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
  },
  {
    field: "ORD_WEIGHT",
    headerName: t("grid-column-name.ord-qty"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "BL_NO",
    headerName: t("grid-column-name.BL-number"),
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  {
    field: "CNTR_NO",
    headerName: t("grid-column-name.container-number"),
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  {
    field: "REF_NO",
    headerName: t("grid-column-name.reference-number"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  {
    field: "LOT_TYPE",
    headerName: t("grid-column-name.receiving-type"),
    headerClass: "header-center",
    sortable: true,
    width: 110,
  },
  {
    field: "TOT",
    headerName: t("grid-column-name.sum"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "ORD_DESC",
    headerName: t("grid-column-name.other-1"),
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ETC2",
    headerName: t("grid-column-name.other-2"),
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "UNIT_NO",
    headerName: t("grid-column-name.unit-no"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  {
    field: "LOC_CD",
    headerName: t("grid-column-name.location-code"),
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "CAR_CD",
    headerName: t("grid-column-name.vehicle-number"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "MAKE_DATE",
    headerName: t("grid-column-name.manufacturing-date"),
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "IMG_YN",
    headerName: t("grid-column-name.image"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 90,
  },
];
