/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP010/input.search.ts
 *  Description:    입출고현황 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

import type { IModal, ISearchInput, info } from "@/types";
import { IControlBtn } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSOP010",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  warehouse: {
    page: "WMSMS040",
    id: "warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_WH_CD",
        searchContainerInputId: "S_WH_CD",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        searchContainerInputId: "S_WH_NM",
        title: "warehouse-name",
        type: "text",
        width: "half",
      },
    ],
  },
};
export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 주문상세
    ids: ["vrSrchComType"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "receiving/shipping-type",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchComType",
    optionMerge: true,
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    options: [
      { name: "receiving", nameKey: "receiving", value: "01" },
      { name: "shipping", nameKey: "shipping", value: "02" },
      { name: "receiving/shipping", nameKey: "receiving/shipping", value: "03" },
    ],
  },

  {
    // 작업구분
    ids: ["vrSrchOrderPhase"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "work-type",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchOrderPhase",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    //화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 승인여부
    ids: ["vrSrchApproveYn"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "approval y/n",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchApproveYn",
    options: [
      { name: "approval", nameKey: "approval", value: "Y" },
      { name: "unapproved", nameKey: "unapproved", value: "N" },
      { name: "approval-x", nameKey: "approval-x", value: "X" },
    ],
  },
  {
    // 창고
    ids: ["vrSrchWhCd", "vrSrchWhNm"],
    hiddenId: "",
    rowDataIds: ["WH_CD", "WH_NM"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchWhCd", "vrSrchWhNm"],
    srchKey: "WH",
    title: "warehouse",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 주문일자
    ids: ["vrSrchReqDtFrom", "vrSrchReqDtTo", "vrSrchYearChk"],
    hiddenId: "",
    required: true,
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "order-date",
    width: "triple",
    isModal: false,
    isSearch: false,
    checkboxLabelTitles: ["", "", "year-standard"],
    checkboxCheckedValue: ["", "", "on"],
    checkboxAutoChecked: ["2"],
    types: ["date", "date", "check-box"],
  },
  {
    // 주문번호
    ids: ["vrSrchOrderId"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "order-number",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

export const SEARCH_COMPONENT_CONTROL_BTN: IControlBtn[] = [
  {
    title: "change-receiving-date",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: false,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchComponentControlBtn: SEARCH_COMPONENT_CONTROL_BTN,
  pageInfo: INFO,
};
