/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP010/input.grid.ts
 *  Description:    입출고현황 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { IControlBtn, IGridStatisticsInfo } from "@/types";
import { GridOptions } from "ag-grid-community";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";

export const MASTER_CONTROL_BTN: IControlBtn[] = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    authType: "EXC_AUTH",
    image: "excel",
  },
  {
    title: "approve",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "",
    disabled: "",
  },
  {
    title: "prohibit",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "",
  },
];

export const MASTER_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [
  //입고(주문수량)
  {
    id: "ORD_IN_CNT_INIT",
    title: "order-qty-receiving",
  },
  //출고(주문수량)
  {
    id: "ORD_OUT_CNT_INIT",
    title: "order-qty-shipping",
  },
  //입고(작업중수량)
  {
    id: "WORK_IN_CNT_INIT",
    title: "working-qty-receiving",
  },
  //출고(작업중수량)
  {
    id: "WORK_OUT_CNT_INIT",
    title: "working-qty-shipping",
  },
];
export const MasterGridMetaData: any = {
  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "check-box-column",
  //페이징옵션
  pagingSizeList: [300, 400, 500],
};

export const DetailGridMetaData: any = {
  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "",

  //페이징옵션
  pagingSizeList: [300, 400, 500],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  mainColumnDefs: MASTER_GRID_COLUMN_DEFS,
  subColumnDefs: DETAIL_GRID_COLUMN_DEFS,
  rowSelection: "single",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
  onRowClicked(event) {
    return event;
  },
};
