/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP010/column-defs.ts
 *  Description:    입출고현황 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Getter } from "@/lib/ag-grid";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 50,
      width: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
    checkboxSelection: true,
  },
  {
    // 상태
    field: "APPROVE_YN",
    headerKey: "status",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: (params) => {
      let styleParams: any = { textAlign: "center" };
      //장기성재고일떄
      if (String(params.data["APPROVE_YN"]) == "N" || String(params.data["APPROVE_YN"]) == "X") {
        styleParams["color"] = "red";
        //관리대상 재고일떄
      }
      return styleParams;
    },
    valueGetter: (params: any) => {
      if (params.data["APPROVE_YN"] == "Y") {
        return "승인";
      } else if (params.data["APPROVE_YN"] == "X") {
        return "불하";
      }
      return "미승인";
    },
  },
  {
    // 주문번호
    field: "ORD_ID",
    headerKey: "order-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // 주문종류
    field: "ORD_TYPE",
    headerKey: "order-type-3",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (params.data["ORD_TYPE"] == "01") {
        return "승인";
      }
      return "미승인";
    },
  },
  {
    // 작업상태
    field: "WORK_STAT",
    headerKey: "operation-status",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return Getter.convetCodeToNameByOptionData("vrOrdRegisterType", params.data.WORK_STAT);
    },
  },
  {
    // 주문상세
    field: "ORD_SUBTYPE",
    headerKey: "order-detail",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (params.node.data.ORD_SUBTYPE === "20") {
        return "정상입고";
      } else {
        //30이면???
        return "반품입고";
      }
    },
  },
  {
    // 화주
    field: "CUST_NM",
    headerKey: "owner",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
  },
  {
    // 요청일자
    field: "REQ_DT",
    headerKey: "request-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // 작업일자
    field: "DT",
    headerKey: "work-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // 창고
    field: "WH_NM",
    headerKey: "warehouse",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 배송처
    field: "TRANS_CUST_NM",
    headerKey: "delivery-destination",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 긴급여부
    field: "KIN_OUT_YN",
    headerKey: "emergency-y/n",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
  },
  {
    // 배차확정여부
    field: "CAR_CONF_YN",
    headerKey: "dispatch-confirmation-status",
    headerName: "",
    export: true,
    sortable: true,
    width: 110,
    cellStyle: { textAlign: "center" },
  },
  {
    // PDA작업여부
    field: "PDA_FINISH_YN",
    headerKey: "pda-finish-y/n",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // PDA작업상태
    field: "PDA_STAT",
    headerKey: "pda-state",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // BL번호
    field: "BL_NO",
    headerKey: "BL-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
  },
  {
    // 입고일자
    field: "IN_DT",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 입고창고명
    field: "IN_WH_NM",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 입고창고ID
    field: "IN_WH_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 출고요청일
    field: "OUT_REQ_DT",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 출고일자
    field: "OUT_DT",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 출고창고명
    field: "OUT_WH_NM",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 출고창고ID
    field: "OUT_WH_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 고객POID
    field: "CUST_POID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 고객POSEQ
    field: "CUST_POSEQ",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 원주문ID
    field: "ORG_ORD_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 원주문SEQ
    field: "ORG_ORD_SEQ",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 운송고객ID
    field: "TRANS_CUST_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 고객ID
    field: "CUST_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 고객코드
    field: "CUST_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 작업순서
    field: "WORK_SEQ",
    headerKey: "work-sequence",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 총용량
    field: "CAPA_TOT",
    headerKey: "total-capacity",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 물류센터ID
    field: "LC_ID",
    headerKey: "logistics-center-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // TPL주문ID
    field: "TPL_ORD_ID",
    headerKey: "tpl-order-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 결제여부
    field: "PAY_YN",
    headerKey: "payment-status",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "RNUM",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    // 제품코드
    field: "ITEM_CODE",
    headerKey: "product-code-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "left" },
  },
  {
    // 제품명
    field: "RITEM_NM",
    headerKey: "product-name-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "left" },
  },
  {
    // 작업상태
    field: "WORK_STAT",
    headerKey: "operation-status",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "left" },
    valueGetter: (params: any) => {
      return Getter.convetCodeToNameByOptionData("vrOrdRegisterType", params.data.WORK_STAT);
    },
  },
  {
    // 주문UOM
    field: "ORD_UOM_NM",
    headerKey: "order-uom",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "ORD_UOM_ID",
    hide: true,
  },
  {
    // 주문수량
    field: "ORD_QTY",
    headerKey: "order-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    //formatter: 'integer', formatoptions: {thousandsSeparator:",", decimalPlaces: 0}
  },
  {
    // 작업수량
    field: "REAL_QTY",
    headerKey: "work-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "right" },
    //formatter: 'integer', formatoptions: {thousandsSeparator:",", decimalPlaces: 0}
  },
  {
    // 고객사Lot번호
    field: "CUST_LOT_NO",
    headerKey: "customer-lot-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "left" },
  },
  {
    // PDA작업상태
    field: "PDA_STAT",
    headerKey: "pda-state",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  {
    // PDA코드
    field: "PDA_CD",
    headerKey: "work-pda-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  {
    // Loc지시여부
    field: "LOC_YN",
    headerKey: "loc-instruction-y/n",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  {
    // 작업지시여부
    field: "WORK_YN",
    headerKey: "work-instruction-y/n",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  {
    // 주문ID
    field: "ORD_ID",

    hide: true,
  },
  {
    // 주문Seq
    field: "ORD_SEQ",
    hide: true,
  },
  {
    // 제품ID
    field: "RITEM_ID",
    hide: true,
  },
  {
    // 실제입고수량
    field: "REAL_IN_QTY",
    hide: true,
  },
  {
    // 실제출고수량
    field: "REAL_OUT_QTY",
    hide: true,
  },
  {
    // 제조일자
    field: "MAKE_DT",
    hide: true,
  },
  {
    // 유효기간(일)
    field: "TIME_PERIOD_DAY",

    hide: true,
  },
  {
    // 거부유형
    field: "RJ_TYPE",

    hide: true,
  },
  {
    // 실제팔레트수량
    field: "REAL_PLT_QTY",

    hide: true,
  },
  {
    // 실제박스수량
    field: "REAL_BOX_QTY",

    hide: true,
  },
  {
    // 확인여부
    field: "CONF_YN",

    hide: true,
  },
  {
    // 단가
    field: "UNIT_AMT",

    hide: true,
  },
  {
    // 총액
    field: "AMT",

    hide: true,
  },
  {
    // EA_CAPA
    field: "EA_CAPA",

    hide: true,
  },
  {
    // 박스바코드
    field: "BOX_BARCODE",

    hide: true,
  },
  {
    // 입고주문UOM ID
    field: "IN_ORD_UOM_ID",

    hide: true,
  },
  {
    // 출고주문UOM ID
    field: "OUT_ORD_UOM_ID",

    hide: true,
  },
  {
    // 입고주문UOM명
    field: "IN_ORD_UOM_NM",

    hide: true,
  },
  {
    // 출고주문UOM명
    field: "OUT_ORD_UOM_NM",

    hide: true,
  },
  {
    // 입고주문수량
    field: "IN_ORD_QTY",

    hide: true,
  },
  {
    // 출고주문수량
    field: "OUT_ORD_QTY",

    hide: true,
  },
];
