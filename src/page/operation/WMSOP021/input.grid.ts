/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP021/input.ts
 *  Description:    원부자재(부품/용기) 입고관리 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { IControlBtn } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    authType: "EXC_AUTH",
    image: "excel",
  },
  {
    title: "change-receiving-date",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
];
export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [200, 300, 400],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
};
