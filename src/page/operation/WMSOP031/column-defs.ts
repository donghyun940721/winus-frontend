/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP031/column-defs.ts
 *  Description:    원부자재(부품/용기) 출고관리 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "raw-material-(parts/containers)": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "POOL_GRP_NAME",
      headerKey: "pool-grp-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerKey: "pool-grp-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerKey: "pool-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // edittype: "select", editoptions: {$!comCode.getGridComboComCode("STS02")}
    // 작업상태
    field: "WORK_STAT",
    headerKey: "operation-status",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
  },
  {
    // 원주문번호
    field: "ORG_ORD_ID",
    headerKey: "original-order-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    hide: true,
  },
  {
    // 주문번호
    field: "ORD_ID",
    headerKey: "order-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
  },
  {
    // 주문SEQ
    field: "ORD_SEQ",
    headerKey: "order-Seq",
    headerName: "",
    export: true,
    sortable: true,
    width: 70,
  },
  {
    // 화주
    field: "CUST_NM",
    headerKey: "Owner",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
  },
  {
    // 배송처
    field: "TRANS_CUST_NM",
    headerKey: "receiving-address",
    headerName: "",
    export: true,
    sortable: true,
    width: 110,
  },
  {
    // LOT_NO
    field: "CUST_LOT_NO",
    headerKey: "lot-no",
    headerName: "",
    export: true,
    sortable: true,
    width: 110,
  },
  {
    // 출고예정일
    field: "OUT_REQ_DT",
    headerKey: "expected-delivery-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
  },
  {
    // 출고일자
    field: "OUT_DT",
    headerKey: "shipping-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품코드
    field: "RITEM_CD",
    headerKey: "item-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
  },
  {
    // 상품명
    field: "RITEM_NM",
    headerKey: "item-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
  },
  {
    //formatoptions:{thousandsSeparator:",",	decimalPlaces: 0}
    // 주문량
    field: "OUT_ORD_QTY",
    headerKey: "order-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
  },
  {
    //
    field: "OUT_ORD_UOM_NM",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    // formatoptions:{thousandsSeparator:",",	decimalPlaces: 2}
    // 주문중량
    field: "IN_ORD_WEIGHT",
    headerKey: "order-weight",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
  },
  {
    // formatoptions:{thousandsSeparator:",",	decimalPlaces: 0}
    // 출고량
    field: "REAL_OUT_QTY",
    headerKey: "real-out-qty",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
  },
  {
    //
    field: "OUT_UOM_NM",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    // formatoptions:{thousandsSeparator:",",	decimalPlaces: 2}
    // 출고중량
    field: "REAL_OUT_WEIGHT",
    headerKey: "real-out-weight",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
  },
  {
    // 송장번호
    field: "INV_NO",
    headerKey: "invoice-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
  },
  {
    // formatoptions:{thousandsSeparator:",",	decimalPlaces: 0}
    // 현재고
    field: "STOCK_QTY",
    headerKey: "current-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
  },
  {
    // formatoptions:{thousandsSeparator:",",	decimalPlaces: 0}
    // 불량재고
    field: "BAD_QTY",
    headerKey: "bad-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
  },
  {
    // 상품유효기간
    field: "ITEM_BEST_DATE",
    headerKey: "product-expiration-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "right" },
    hide: true,
  },
  {
    // 상품유효기간만료일
    field: "ITEM_BEST_DATE_END",
    headerKey: "product-expiration-end-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 소비기한만료일
    field: "ITEM_USE_DATE_END",
    headerKey: "consumption-expiration-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // PLT수량
    field: "REAL_PLT_QTY",
    headerKey: "plt-quantity",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    // BOX수량
    field: "REAL_BOX_QTY",
    headerKey: "box-quantity",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    // 창고
    field: "OUT_WH_NM",
    headerKey: "warehouse-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
  },
  {
    // 주문상세
    field: "ORD_SUBTYPE",
    headerKey: "order-detail",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
  },
  {
    // 피킹 작업 ID
    field: "PICKING_WORK_ID",
    headerKey: "picking-work-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
  },
  {
    // 출고 작업 ID
    field: "OUT_WORK_ID",
    headerKey: "outgoing-work-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
  },
  {
    // 출고 SEQ
    field: "OUT_WORK_SEQ",
    headerKey: "outgoing-seq",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    hide: true,
  },
  {
    // 비고1
    field: "ORD_DESC",
    headerKey: "remark#1",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
  },
  {
    // 비고2
    field: "ETC2",
    headerKey: "remark#2",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
  },
  {
    //
    field: "RITEM_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "OUT_ORD_UOM_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "CUST_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "TRANS_CUST_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "APPROVE_YN",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "OUT_WH_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "LC_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "OUT_QTY_UOM",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "OUT_ORD_UOM_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "CUST_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "TRANS_CUST_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "OUT_UOM_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "OUT_WH_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "OUT_UOM_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "OUT_LOC_QTY",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    //
    field: "VIEW_ORD_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
];
