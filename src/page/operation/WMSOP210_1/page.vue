<!------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP210_1/page.vue
 *  Description:    품목라벨(출력) 업무 화면
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------->

<!-------------------------------
  PAGE TEMPLATE MARKUPS 
-------------------------------->
<template>
  <div class="form-container form-container-local" :id="`${myPageId}-page`">
    <common-search :searchContainerMeta="SEARCH_META" @sessionStorageReady="sessionStorageReady">
      <template v-slot:search-header> </template>
      <template v-slot:search-input-header></template>
    </common-search>
    <common-form
      ref="commonFormRef"
      :gridId="`${myPageId}-grid`"
      :gridMeta="GRID_META"
      :gridOptionsMeta="GRID_OPTIONS_META"
      :formColumnDefs="FORM_COLUMN_DEFS"
      :controlBtn="CONTROL_BTN"
      :searchInput="SEARCH_INPUT"
      :onApiParamsChanged="onApiParamsChanged"
      :getServerData="getServerData"
      @cellClicked="cellClicked"
      :context="{}">
      <!-- :onCellDoubleClicked="onCellDoubleClicked"
    :readOnlyInputs="SEARCH_META.readOnlyInputs"
    @afterSearchData="afterSearchData"
    @onSelectionChanged="setSelectedItems" -->
      <div class="grid-header"></div>
      <control-container>
        <div v-for="btnData in gridControlBtnRef" :key="btnData.title">
          <control-button v-if="btnData['display'] === 'Y' && selectedItems.value && Object.keys(selectedItems.value).length > 0" :data="btnData" @clicked="eventHandler[btnData.title]" />
          <control-button v-else-if="btnData['display'] === 'Y'" :data="btnData" @clicked="eventHandler[btnData.title]" />
        </div>
      </control-container>
    </common-form>
  </div>
</template>

<!-------------------------------
  PAGE TYPE SCRIPT CODES
-------------------------------->
<script lang="ts">
export default { name: "WMSOP210_1" };
</script>
<script setup lang="ts">
import { onMounted, ref } from "vue";

import { ControlButton } from "@/components/button";
import { ControlContainer } from "@/components/layout";
import commonForm from "@/page/common-page/common-form.vue";
import CommonSearch from "@/page/common-page/common-search.vue";
import { FORM_COLUMN_DEFS } from "@/page/operation/WMSOP210_1/column-defs";
import { ApiService } from "@/services/api-service";
import { UtilService } from "@/services/util-service";
import { useCallModalIdStore, useOptionDataStore, useSelectedDataStore, useTabsStore } from "@/store";
import { useEventBusStore } from "@/store/event-bus/EventBus";
import { CellClickedEvent, GridApi, GridReadyEvent } from "ag-grid-community";
import { CONTROL_BTN, GRID_META, GRID_OPTIONS_META, SEARCH_INPUT, SEARCH_META } from "./input.meta";

// Variable Definitions
const eventBus = useEventBusStore();
const OptionDataStore = useOptionDataStore();
const myPageId = ref<string>();
const commonFormRef = ref<InstanceType<typeof commonForm> | null>(null);
const gridApi = ref<GridApi | undefined>();
const SelectedDataStore = useSelectedDataStore();
const selectedItems = ref<Record<string, any>>({});
const cellClicked = (e: CellClickedEvent) => {
  SelectedDataStore.selectedInfoHandler(e.data, true);
};
const gridControlBtnRef = ref(CONTROL_BTN);
const eventHandler: Record<string, Function> = {
  printing: fn_print,
  "print-all": fn_printAll,
};

function sessionStorageReady() {
  useCallModalIdStore().setCallModalId("owner");
}

// Framework Function Definitions
onMounted(async () => {
  eventBus.on("onGridReady", (params: GridReadyEvent) => {
    gridApi.value = params.api;
  });

  myPageId.value = useTabsStore().selectedTab.value?.id;
  const data = UtilService.getOptionApiData("WMSOP210");
  const res = await ApiService.postApi(`/rest/WINUS/WMSOP210.action`, {}, data);

  if (res.result.selectBoxInfos) {
    OptionDataStore.setOptionData(res.result.selectBoxInfos);
  } else {
    OptionDataStore.setOptionData(res.result);
  }
});

// User Function Definitions
// 출력
function fn_print() {
  // if(spdList.getRowCount() == 0){
  // 	alert("$!lang.getText('조회 된 데이터가 없습니다.')");
  // 	return false;
  // }
  // var selectIds = fn_checkedList(spdList);
  // //console.log(selectIds);
  // var param = "";
  // if(selectIds != ""){
  // 	if(confirm("$!lang.getText('출력하시겠습니까')")){
  // 		for(var i = 0; i < selectIds.length; i++){
  // 			//var CUST_ID = spdList.getValue(selectIds[i], SPDCOL_CUST_ID);
  // 			//var ORD_DT = spdList.getValue(selectIds[i], SPDCOL_ORD_DT);
  // 			var ORD_NO = spdList.getValue(selectIds[i], SPDCOL_ORD_NO);
  // 			var ITEM_CD = spdList.getValue(selectIds[i], SPDCOL_ITEM_CD);
  // 			var ITEM_SEQ = spdList.getValue(selectIds[i], SPDCOL_ITEM_SEQ);
  // 			//param += "'";
  // 			//param += CUST_ID;
  // 			//param += ORD_DT;
  // 			param += ORD_NO;
  // 			param += ITEM_CD;
  // 			param += ITEM_SEQ;
  // 			//param += "'";
  // 			if(i < selectIds.length - 1){
  // 				param += ":";
  // 			}
  // 		}
  // 		var width 		= "1000";	//900 MIN 1024
  // 		var height 		= "800";		//크기다시 재조정할 필요있음
  // 		var x = screen.availWidth;
  // 		var y = screen.availHeight;
  // 		x = (x - width) / 2;
  // 		y = (y - height) / 2;
  // 		var status = "toolbar=no, status=no, height="+height+",width="+width+",left="+x+",top="+y+",scrollbars=no,resizable=no,noresize";
  // 		window.open("", "fn_InPopup", status);
  // 		jQuery("#strParameter").val(param);
  // 		jQuery("#submitForm").submit();
  // 		////////////////////////////////////////////////////////////////////////////
  // 		//jsp 파라미터
  // 		var fileName = "WINUSOP210RB";
  // 		var reportName = "WINUSOP210.ozr";
  // 		var odiName = "WINUSOP210";
  // 		var title = '$!lang.getText("label")';
  // 		var arrKey = ['LC_ID','ALL_PARAMS'];
  // 		var params="";
  // 		params = "?strParameter=";
  // 		params += "&arrKey=" + arrKey;
  // 		params += "&LC_ID=" 		+ jQuery("select[name='SVC_INFO'] option:selected").val();
  // 		params += "&ALL_PARAMS=" + param;
  // 		params += "&fileName="+fileName;
  // 		params += "&reportName="+reportName;
  // 		params += "&odiName="+odiName;
  // 		params += "&title="+title;
  // 		var url     = "/common/OZreport8.action" + params;
  // 		var width 		= "1000";	//900 MIN 1024
  // 		var height 		= "800";		//크기다시 재조정할 필요있음
  // 		var asppop 		= cfn_openPop2(url, "fn_InPopup", width, height);
  // 		asppop.focus();
  // 	}
  // }
}
// 전체출력
function fn_printAll() {
  // if(spdList.getRowCount() == 0){
  // 	alert("$!lang.getText('조회 된 데이터가 없습니다.')");
  // 	return false;
  // }
  // if(confirm("$!lang.getText('출력하시겠습니까')")){
  // 	var fromDt = jQuery("#vrSrchReqDtFrom").val();
  // 	var toDt = jQuery("#vrSrchReqDtTo").val();
  // 	var itemCd = jQuery("#vrSrchItemCd").val();
  // 	var lcId =  '$!session.getAttribute("SS_SVC_NO")';
  // 	//jsp 파라미터
  // 	var fileName = "WINUSOP210R2RB";
  // 	var reportName = "WINUSOP210R2.ozr";
  // 	var odiName = "WINUSOP210R2";
  // 	var title = '$!lang.getText("label")';
  // 	var arrKey = ['LC_ID','FROM_DT','TO_DT','ITEM_CD'];
  // 	var param = "";
  // 	param += "?LC_ID=" + lcId;
  // 	param += "&FROM_DT=" + fromDt;
  // 	param += "&TO_DT=" + toDt;
  // 	param += "&ITEM_CD=" + (itemCd == undefined ? "" : itemCd);
  // 	param += "&arrKey=" + arrKey;
  // 	param += "&fileName="+fileName;
  // 	param += "&reportName="+reportName;
  // 	param += "&odiName="+odiName;
  // 	param += "&title="+title;
  // 	var url     = "/common/OZreport8.action" + param;
  // 	var width 	= "1100";
  // 	var height 	= "780";
  // 	var asppop = cfn_openPop2(url, "fn_confrimReportPopup_"+i, width, height);
  // }
}

function onApiParamsChanged(apiParams: any) {
  return apiParams;
}
async function getServerData(apiParams: any, bodyData: any) {
  return await ApiService.postApi("/rest/WMSOP210/list.action", apiParams, bodyData.value, false);
}
</script>

<!-------------------------------
  PAGE SCSS STYLES
-------------------------------->
<style lang="scss" scoped>
@include formContainerStyle;

.form-container {
  &-local {
    flex-direction: column;
    gap: 10px;
    height: 100%;
    padding: 0 0 0 0 !important;
  }
}
.search-control-container {
  // position: absolute;
  top: 0;
  right: 0;
}

.date-range-input {
  width: 100%;
}
</style>
