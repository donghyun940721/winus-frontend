/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP910_3/input.grid.ts
 *  Description:    운영관리/출고관리(B2C) META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.06 : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format } from "@/lib/ag-grid";
import { IControlBtn, IGridCellSearchButton, IGridCellSelectBox, IGridStatisticsInfo } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [
  {
    id: "TOTAL_COUNT",
    title: "Total-Number-of-Orders",
    subId: "TOTAL_ORD_ID_COUNT",
  },
  {
    id: "TOTAL_OUT_ORD_QTY",
    title: "total-order-quantity",
  },
  {
    id: "TOTAL_REAL_OUT_QTY",
    title: "total-shipment-quantity",
  },
];

// done
export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "total-integration",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "change-shipping-date",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "enter-template",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    authType: "DEL_AUTH",
    image: "",
  },
  {
    title: "excel-all",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
    disabled: "",
  },
  {
    title: "excel-select",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
    disabled: "",
  },
];

//#region ::  출고주문분석 (WMSTG732E3pop1)

export const WMSTG732E3pop1_CONTROL_BTN: any = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    disabled: "",
  },
];

//#endregion

//#region ::  신규 (wmsop910_3-new-popup)

export const NEW_CONTROL_BTN: IControlBtn[] = [
  {
    title: "new-destinations",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    disabled: "",
  },
  {
    title: "reset",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },

  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
  },
];

export const GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  EP_TYPE: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "select", value: "" },
      { name: "", nameKey: "10-ft", value: "01" },
      { name: "", nameKey: "20-ft", value: "02" },
      { name: "", nameKey: "40-ft", value: "03" },
      { name: "", nameKey: "40-fthq", value: "04" },
      { name: "", nameKey: "rf", value: "05" },
      { name: "", nameKey: "wing-body", value: "06" },
    ],
  },
  CNTR_TYPE: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "sortation", value: "SORTATION" },
      { name: "", nameKey: "hyper-flow", value: "HYPER_FLOW" },
      { name: "", nameKey: "stock", value: "STOCK" },
      { name: "", nameKey: "asn", value: "ASN" },
      { name: "", nameKey: "promotion-sorter", value: "PROMOTION_SORTER" },
      { name: "", nameKey: "promotion", value: "PROMOTION" },
      { name: "", nameKey: "dc", value: "dc" },
      { name: "", nameKey: "tc", value: "tc" },
      { name: "", nameKey: "sorter", value: "sorter" },
      { name: "", nameKey: "non-sorter", value: "sorter" },
      { name: "", nameKey: "dps", value: "DPS" },
    ],
  },
};

export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  ITEM: {
    fieldList: ["ITEM_CODE", "RITEM_NM", "OUT_WORK_UOM_CD", "OUT_ORD_UOM_CD", "RITEM_ID", "OUT_WORK_UOM_ID", "OUT_ORD_UOM_ID"],
    rowDataKeys: ["ITEM_CODE", "ITEM_KOR_NM", "UOM_NM", "UOM_NM", "RITEM_ID", "UOM_ID", "UOM_ID"],
    callApiCondition: {
      dataType: "modal",
      key: "owner",
    },
    modalData: {
      page: "WMSCM091",
      id: "kit-product",
      title: "search-product",
      gridTitle: "product-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달

      defaultParamsData: {
        //해당 모달의 title
        storeSaveKey: "owner",
        paramsKeys: ["vrSrchCustCd"],
        rowDataKeys: ["CUST_CD"],
      },
      apis: {
        url: "/WMSCM091/listGrid_rn.action",
        params: {
          func: "fn_setWMSCM092E3",
          vrSrchCustCd: "",
          vrSrchWhId: "",
          vrViewSetItem: "",
          vrViewAll: "viewAll",
          vrColCustLotNo: "",
          vrSrchItemCd: "",
          vrSrchItemNm: "",
          vrSrchItemGrp: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchItemCd",
          title: "product-code",
          searchContainerInputId: "vrSrchItemCd",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemNm",
          searchContainerInputId: "vrSrchItemNm",
          title: "product-name",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemGrp",
          title: "product-group",
          type: "select",
          width: "half",
          optionsKey: "ITEMGRP",
          options: [{ name: "all", nameKey: "all", value: "" }],
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        maxWidth: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "CUST_LOT_NO",
        headerKey: "LOT_NO",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_CODE",
        headerKey: "product-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_KOR_NM",
        headerKey: "product-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "STOCK_QTY",
        headerKey: "current-stock",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "BAD_QTY",
        headerKey: "inferior-product",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "UOM_NM",
        headerKey: "uom",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "CONV_BOX_QTY",
        headerKey: "BOX-converted-quantity",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "CONV_PLT_QTY",
        headerKey: "plt-conversion-quantity",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "BOX_QTY",
        headerKey: "box-quantity",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "PLT_QTY",
        headerKey: "plt-quantity",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "UNIT_PRICE",
        headerKey: "unit-price",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
        valueFormatter: Format.NumberPrice,
      },
      {
        field: "ITEM_GRP_ID",
        headerKey: "product-group-ID",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "WH_NM",
        headerKey: "warehouse-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "LOT_USE_YN",
        sortable: true,
        hide: true,
      },
      {
        field: "RITEM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "ITEM_ENG_NM",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "CUST_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_NM",
        sortable: true,
        hide: true,
      },
      {
        field: "TOTAL_STOCK_QTY",
        sortable: true,
        hide: true,
      },
    ],
  },
  UOM: {
    fieldList: ["OUT_WORK_UOM_CD"],
    rowDataKeys: ["UOM_NM"],
    modalData: {
      page: "WMSCM100",
      id: "representative-uom",
      title: "search-uom",
      gridTitle: "uom-list",
      isCellRenderer: true,
      apis: {
        url: "/WMSCM100/list_rn.action",
        params: {
          func: "fn_setWMSCM100",
          UOM_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          vrSrchUomId: "",
          vrSrchUomNm: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchUomId",
          title: "uom-code",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchUomNm",
          title: "uom-name",
          type: "text",
          width: "half",
        },
      ],
    },
    colDef: [
      {
        field: "No",
        headerKey: "no",
        headerName: "",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        maxWidth: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "UOM_CD",
        headerKey: "uom-code",
        headerClass: "header-center",
      },
      {
        field: "UOM_NM",
        headerKey: "uom-name",
        headerClass: "header-center",
      },
      {
        field: "UOM_ID",
        hide: true,
      },
      {
        field: "UPD_NO",
        hide: true,
      },
      {
        field: "REG_NO",
        hide: true,
      },
      {
        field: "LC_ID",
        hide: true,
      },
    ],
  },
  LOCATION: {
    fieldList: ["LOC_CD"],
    rowDataKeys: ["LOC_CD"],
    modalData: {
      page: "WMSMS080",
      id: "location",
      title: "search-location",
      gridTitle: "location-list",
      isCellRenderer: true,
      apis: {
        url: "/WMSCM080/list_rn.action",
        params: {
          func: "fn_setWMSMS080",
          LOC_ID: "",
          LOC_CD: "",
          AVAILABLE_QTY: "",
          OUT_EXP_QTY: "",
          STOCK_ID: "",
          SUB_LOT_ID: "",
          vrViewOnlyLoc: "",
          STOCK_WEIGHT: "",
          vrViewSubLotId: "",
          vrRitemId: "",
          ITEM_BEST_DATE_END: "",
          UOM_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          vrViewStockQty: "",
          vrSrchLocCd: "",
          vrSrchLocId: "",
          vrSrchCustLotNo: "",
          vrSrchLocStat: "",
          S_WH_CD: "",
          vrWhId: "",
          S_WH_NM: "",
          vrSrchLocDel: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchLocCd",
          searchContainerInputId: "txtSrchLocCd",
          title: "location-code",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchCustLotNo",
          title: "LOT-number",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchLocStat",
          title: "location-status",
          type: "select",
          width: "half",
          options: [
            { nameKey: "all", name: "", value: "300" },
            { nameKey: "use-location", name: "", value: "200" },
            { nameKey: "unused-location", name: "", value: "100" },
          ],
          optionsAutoSelected: { autoSelectedKeyIndex: 0 },
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "LOC_CD",
        headerKey: "location",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "",
        headerKey: "product",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        children: [
          { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

          { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
        ],
      },
      {
        field: "AVAILABLE_QTY",
        headerKey: "stock-quantity",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "UOM_NM",
        headerKey: "uom",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "OUT_EXP_QTY",
        headerKey: "schedule-quantity",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "REAL_PLT_QTY",
        headerKey: "plt-quantity",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "STOCK_WEIGHT",
        headerKey: "weight",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "CUST_LOT_NO",
        headerKey: "lot-number",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_BEST_DATE_END",
        headerKey: "expiration-date",
        headerName: "",
        cellStyle: { textAlign: "center" },
        headerClass: "header-center",
        sortable: true,
      },
    ],
  },
};

//#endregion

//#region ::  작업지시(WMSOP030T)

export const WMSOP030T_CONTROL_BUTTON: any = [
  {
    title: "print-ticket-kr",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    disabled: "",
  },
  {
    title: "complete",
    colorStyle: "danger",
    paddingStyle: "normal",
    image: "",
    disabled: "",
  },
  {
    title: "work-order",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "",
    disabled: "",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    disabled: "",
  },
];

//#endregion

export const gridMetaData: any = {
  //페이지 키
  // checkBoxColumn: "check-box-column",

  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSOP910_3"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: true,

  //페이징옵션
  pagingSizeList: [1000, 2000, 3000, 5000, 10000, 100000],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  // suppressPropertyNamesCheck: true,
  getRowClass: (params: any) => {
    if ((String(params.data?.WORK_STAT) === "990" && !(params.data?.OUT_ORD_QTY === params.data?.REAL_OUT_QTY)) || Number(params.data?.SUM_SIGN) < 0) {
      return "grid-cell-color-red";
    }
  },
  statusBar: true,
  suppressAggFuncInHeader: true,
  groupDefaultExpanded: -1,
  groupSelectsChildren: true,
  autoGroupColumnDef: {
    /** 그룹 하위에 주문번호 표기하지 않음 */
    // field: "VIEW_ORD_ID",
    headerName: t("grid-column-name.order-number"),
    minWidth: 150,
    // menuTabs: ["filterMenuTab"],
    // filter: "agSetColumnFilter",
    cellClass: "stringType",
    sortable: true,
    pinned: "left",
    suppressMovable: true,
  },
  getRowId: (params: any) => {
    return params.data.RNUM;
  },
};
