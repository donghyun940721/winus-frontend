/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM120/input.ts
 *  Description:    주문/입고주문관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
/**
 ********************* Grid Area *********************/
import { i18n } from "@/i18n";
import { IControlBtn, IGridStatisticsInfo } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [
  {
    id: "TOTAL_COUNT",
    title: "total-number-of-orders",
  },
  {
    id: "TOTAL_OUT_ORD_QTY",
    title: "total-orders",
  },
  {
    id: "TOTAL_REAL_OUT_QTY",
    title: "total-shipping-amount",
  },
];

// done
export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "total-integration",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "change-shipping-date",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "enter-template",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
    disabled: "",
  },
  {
    title: "excel-all",
    colorStyle: "success",
    paddingStyle: "bold",
    authType: "EXC_AUTH",
    image: "excel",
  },
  {
    title: "excel-select",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
    disabled: "",
  },
];

export const gridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSOP910_2"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: true,

  //페이지 키
  // checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [1000, 2000, 3000, 5000, 10000, 100000],
};

export const gridOptionsMeta: any = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  // suppressPropertyNamesCheck: true,
  getRowClass: (params: any) => {
    if ((String(params.data?.WORK_STAT) === "990" && !(params.data?.OUT_ORD_QTY === params.data?.REAL_OUT_QTY)) || Number(params.data?.SUM_SIGN) < 0) {
      return "grid-cell-color-red";
    }
  },
  statusBar: true,
  suppressAggFuncInHeader: true,
  groupDefaultExpanded: -1,
  groupSelectsChildren: true,
  autoGroupColumnDef: {
    /** 그룹 하위에 주문번호 표기하지 않음 */
    // field: "VIEW_ORD_ID",
    headerName: t("grid-column-name.order-number"),
    minWidth: 150,
    // menuTabs: ["filterMenuTab"],
    // filter: "agSetColumnFilter",
    cellClass: "stringType",
    sortable: true,
    pinned: "left",
    suppressMovable: true,
  },
  getRowId: (params: any) => {
    return params.data.RNUM;
  },
};
