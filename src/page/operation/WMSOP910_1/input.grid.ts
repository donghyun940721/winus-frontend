/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP910_1/input.ts
 *  Description:    운영/입출고관리(통합) 입고관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

/**
 ********************* Grid Area *********************/
import { i18n } from "@/i18n";
import { IControlBtn, IGridCellDateInput, IGridCellSearchButton, IGridCellSelectBox, IGridStatisticsInfo, ISplitButton } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

//gridSearchButton
export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  LOCATION: {
    fieldList: ["LOC_ID", "LOC_CD", "STOCK_ID", "STOCK_WEIGHT", "SUB_LOT_ID", "ITEM_BEST_DATE_END", "MAKE_DT"],
    rowDataKeys: ["LOC_ID", "LOC_CD", "STOCK_ID", "STOCK_WEIGHT", "SUB_LOT_ID", "ITEM_DATE_END", "MAKE_DT"],
    modalData: {
      page: "wmsop000q5v2",
      id: "location-designation",
      title: "search-location",
      gridTitle: "location-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달
      defaultParamsData: {
        storeSaveKey: "product",
        rowDataKeys: ["RITEM_ID"],
        paramsKeys: ["vrRitemId"],
      },
      apis: {
        url: "/WMSCM080/listGroupByLot_rn.action",
        params: {
          vrSrchLocCd: "",
          vrWhId: "",
          vrSrchLocId: "",
          vrViewStockQty: "VIEWSTOCKQTY",
          vrRitemId: "",
          vrViewSubLotId: "VIEWSUBLOT",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchLocCd",
          title: "location-code",
          searchContainerInputId: "vrSrchLocCd",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchCustLotNo",
          title: "lot-number",
          type: "text",
          width: "half",
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "LOC_CD",
        headerKey: "location-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "LOC_TYPE_NM",
        headerKey: "type",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "CUST_LOT_NO",
        headerKey: "lot-number",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "LOCK_YN",
        headerKey: "lock-y/n",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "",
        headerKey: "product",
        headerName: "",
        headerClass: "header-center",
        children: [
          { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },
          { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
        ],
        sortable: true,
      },
      {
        field: "AVAILABLE_QTY",
        headerKey: "stock-quantity",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "UOM_NM",
        headerKey: "uom",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "OUT_EXP_QTY",
        headerKey: "schedule-quantity",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "REAL_PLT_QTY",
        headerKey: "plt-quantity",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "STOCK_WEIGHT",
        headerKey: "weight",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "SUB_LOT_ID",
        headerKey: "sub-lot-id",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_BEST_DATE_END",
        headerKey: "best-expiration-date",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
      },
      {
        field: "MAKE_DT",
        headerKey: "manufacturing-date",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "UNIT_NO",
        headerKey: "unit-no",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "STOCK_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "LOC_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "LOC_TYPE",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_CD",
        sortable: true,
        hide: true,
      },
    ],
  },
};

export const GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [
  {
    id: "TOTAL_COUNT",
    title: "total-number-of-orders",
  },
  {
    id: "TOTAL_IN_ORD_QTY",
    title: "total-orders",
  },
  {
    id: "TOTAL_REAL_IN_QTY",
    title: "total-inventory",
  },
];

// done
export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "change-receiving-date",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "enter-template",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
    disabled: "",
  },
];

export const SPLIT_BTN: ISplitButton[] = [
  {
    title: t("control-button-name.total-integration"),
    colorStyle: "info",
    eventHandler: "etcFunctionHandler",
  },
  {
    title: t("control-button-name.excel-download"),
    colorStyle: "success",
    eventHandler: "excelButtonHandler",
    image: "excel",
  },
];

export const CONTROL_BTN_WMSOP000Q1_1 = [
  {
    title: "vehicle-registration",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    disabled: "",
  },
];
export const CONTROL_BTN_WMSOP000Q1_2 = [
  {
    title: "non-vehicle-dispatch",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
  },
];

export const CONTROL_BTN_WMSOP000Q3 = [
  {
    title: "check-in-approval",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
  },
  {
    title: "check-in-cancel",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
  },
];
export const CONTROL_BTN_WMSOP000Q4 = [
  {
    title: "dock-release",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
  },
];
export const CONTROL_BTN_WMSOP000Q5V2 = [
  {
    title: "location-reset",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "",
  },
  {
    title: "location-recommendation",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
];
export const GRID_CONTROL_BTN_WMSOP000Q9 = [
  {
    title: "create-barcode",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "",
  },
];
export const CONTROL_BTN_WMSOP000Q9 = [
  {
    title: "enter-excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    authType: "EXC_AUTH",
  },
  {
    title: "save",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "normal",
    image: "",
    authType: "DEL_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const WMSOP000Q1_GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  ADJUCT_CAR_TON: {
    pk: "RNUM",
    options: [
      { nameKey: "1-ton", name: "", value: "100" },
      { nameKey: "2-5-ton", name: "", value: "250" },
      { nameKey: "3-5-ton", name: "", value: "350" },
      { nameKey: "5-ton", name: "", value: "500" },
      { nameKey: "11-ton", name: "", value: "1100" },
      { nameKey: "25-ton", name: "", value: "150" },
      { nameKey: "trailer", name: "", value: "200" },
      { nameKey: "damas", name: "", value: "1000" },
      { nameKey: "labo", name: "", value: "1001" },
      { nameKey: "n-a", name: "", value: "00" },
      { nameKey: "1-4-ton", name: "", value: "140" },
      { nameKey: "center-pickup", name: "", value: "010" },
      { nameKey: "8-ton", name: "", value: "800" },
      { nameKey: "delivery", name: "", value: "020" },
      { nameKey: "q-delivery", name: "", value: "030" },
    ],
  },
};

export const WMSOP000Q1_GRID_CELL_DATE_INPUT: IGridCellDateInput = {
  gridPk: "RNUM",
};

export const gridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSOP910_1"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: true,

  //페이지 키
  checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [1000, 2000, 3000, 5000, 10000, 100000],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  statusBar: true,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  allowContextMenuWithControlKey: true,
  enableRangeSelection: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  suppressAggFuncInHeader: true, //그룹 관련 시작
  groupDefaultExpanded: -1,
  groupSelectsChildren: true,
  onSelectionChanged: (event) => {
    console.log(event);
  },
  autoGroupColumnDef: {
    /** 그룹 하위에 주문번호 표기하지 않음 */
    // field: "VIEW_ORD_ID",
    headerName: t("grid-column-name.order-number"),
    minWidth: 150,
    menuTabs: ["filterMenuTab"],
    filter: "agSetColumnFilter",
    cellClass: "stringType",
    sortable: true,
    pinned: "left",
    suppressMovable: true,
  },
};
