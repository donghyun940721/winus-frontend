/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP910_1/column-defs.ts
 *  Description:    운영/입출고관리(통합) 입고관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/formatter";
import type { IColDef } from "@/types/agGrid";

export const FORM_COLUMN_DEFS: IColDef[] = [
  {
    field: "",
    headerKey: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    pinned: "left",
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
    menuTabs: ["columnsMenuTab"],
    lockVisible: true,
    lockPosition: true,
  },
  {
    field: "check-box-column",
    headerKey: "",
    headerName: "checkBox2",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    pinned: "left",
    lockVisible: true,
    lockPosition: true,
  },
  {
    // 주문번호
    field: "VIEW_ORD_ID",
    headerKey: "order-number",
    headerName: "",
    cellClass: "stringType",
    export: true,
    sortable: true,
    rowGroup: true,
    hide: true,
    pinned: "left",
  },
  {
    // 주문SEQ
    field: "ORD_SEQ",
    headerKey: "order-seq-2",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 110,
    export: true,
    pinned: "left",
  },
  {
    // 정상/반품
    field: "ORD_SUBTYPE_NM",
    headerKey: "normal/return",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
    aggFunc: "first",
    pinned: "left",
  },
  {
    // 작업상태
    field: "WORK_STAT_EXCEL",
    headerKey: "operation-status",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    // pinned: "left",
    width: 130,
    export: true,
    // lockVisible: true,
    // lockPosition: true,
    aggFunc: "first",
    pinned: "left",
  },
  {
    // 상품코드
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    headerClass: "header-center",
    cellClass: "stringType",
    sortable: true,
    export: true,
    minWidth: 150,
  },
  {
    // 상품명
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    export: true,
    minWidth: 350,
  },
  {
    // 등급
    field: "GRADE",
    headerKey: "",
    headerName: "등급",
    headerClass: "header-center",
    sortable: true,
    width: 140,
    export: true,
  },
  {
    // 섹션
    field: "SECTION_CD",
    headerKey: "",
    headerName: "섹션",
    headerClass: "header-center",
    sortable: true,
    width: 140,
    export: true,
  },
  {
    // 로케이션 (추천)
    field: "REC_LOC_CD",
    headerKey: "",
    headerName: "로케이션(추천)",
    headerClass: "header-center",
    sortable: true,
    width: 140,
    export: true,
  },
  {
    // 로케이션
    field: "LOC_CD",
    headerKey: "",
    headerName: "로케이션",
    headerClass: "header-center",
    sortable: true,
    width: 140,
    export: true,
  },
  {
    // 주문수량
    field: "IN_ORD_QTY",
    headerKey: "order-quantity",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
  },
  {
    // 입고량
    field: "REAL_IN_QTY",
    headerKey: "receiving-qty",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
  },
  {
    // UOM
    field: "IN_ORD_UOM_CD",
    headerKey: "uom",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 90,
    export: true,
  },
  {
    // 주문중량
    field: "IN_ORD_WEIGHT",
    headerKey: "order-weight",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
    export: true,
  },
  {
    // 금액
    field: "PRICE",
    headerKey: "amount",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
  },
  {
    // 입고예정일
    field: "IN_REQ_DT",
    headerKey: "scheduled-receiving-date",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
    export: true,
  },
  {
    // 입고일
    field: "IN_DT",
    headerKey: "receiving-date",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
    export: true,
  },
  {
    // 박스 바코드
    field: "BOX_BAR_CD",
    headerKey: "box-barcode",
    headerName: "",
    headerClass: "header-center",
    cellClass: "stringType",
    cellStyle: { textAlign: "right" },
    sortable: true,
    export: true,
  },
  {
    // LOT 번호
    field: "CUST_LㅋOT_NO",
    headerKey: "LOT-NO",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    export: true,
  },
  {
    // BL 번호
    field: "BL_NO",
    headerKey: "BL-number",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    export: true,
  },
  {
    // 컨테이너 번호
    field: "CNTR_NO",
    headerKey: "container-number",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    export: true,
  },
  {
    // UNIT NO
    field: "UNIT_NO",
    headerKey: "UNIT-NO",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    export: true,
  },
  {
    field: "REAL_PLT_QTY",
    headerKey: "plt-quantity",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
  },
  {
    field: "ORD_DESC",
    headerKey: "remark#1",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    export: true,
  },
  {
    field: "ETC2",
    headerKey: "remark#2",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    export: true,
  },
  {
    field: "ITEM_BEST_DATE_END",
    headerKey: "product-expiration-date",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 170,
    export: true,
  },
  {
    field: "MAKE_DT",
    headerKey: "manufacturing-date-2",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
  },
  {
    field: "ORG_ORD_ID",
    headerKey: "original-order-number",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    export: true,
  },
  {
    field: "CUST_ORD_SEQ",
    headerKey: "original-order-seq",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
    export: true,
  },
  {
    field: "ORD_DEGREE_NM",
    headerKey: "order-sequence-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 130,
    export: true,
  },
  {
    field: "ORD_DEGREE",
    headerKey: "order-sequence",
    headerName: "",
    cellStyle: { textAlign: "right" },
    cellDataType: "text",
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
  },
  {
    field: "REG_DT",
    headerKey: "reg-dt",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    valueFormatter: Format.Date,
    sortable: true,
    width: 170,
    export: true,
  },
  {
    field: "REG_NM",
    headerKey: "reg-no",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    export: true,
  },
  {
    field: "UPD_DT",
    headerKey: "date-modified",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    valueFormatter: Format.Date,
    sortable: true,
    width: 170,
    export: true,
  },
  {
    field: "UPD_NM",
    headerKey: "modifier",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    export: true,
  },
];
