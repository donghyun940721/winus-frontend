/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST400/input.ts
 *  Description:    랙보충(신규) 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { IControlBtn } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";
const { t } = i18n.global;

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "rack-replenishment",
    colorStyle: "primary",
    paddingStyle: "normal",
    disabled: "",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    authType: "EXC_AUTH",
    image: "excel",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [200, 300, 400],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  // allowContextMenuWithControlKey: true,
  // enableRangeSelection: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  suppressAggFuncInHeader: true, //그룹 관련 시작
  groupDefaultExpanded: -1,
  groupSelectsChildren: true,
  pagination: false,
  statusBar: true,
  autoGroupColumnDef: {
    /** 그룹 하위에 주문번호 표기하지 않음 */
    headerName: t("grid-column-name.product-code"),
    minWidth: 120,
    menuTabs: ["filterMenuTab"],
    filter: "agSetColumnFilter",
    cellClass: "stringType",
    sortable: true,
    suppressMovable: true,
  },
};
