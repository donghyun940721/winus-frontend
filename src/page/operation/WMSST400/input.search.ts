/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST400/input.ts
 *  Description:    랙보충(신규) 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

import type { IModal, ISearchInput, info } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSST400",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      rowDataKeys: ["CUST_ID"],
      paramsKeys: ["vrSrchCustId"],
    },
    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "Y",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        title: "product-code",
        searchContainerInputId: "vrSrchRitemCd",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchRitemNm",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        options: [{ nameKey: "all", name: "", value: "" }],
        optionsKey: "ITEMGRP",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        options: [
          { nameKey: "all", name: "", value: "" },
          { nameKey: "useProductY", name: "", value: "Y" },
          { nameKey: "useProductN", name: "", value: "N" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    //화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 작업일자
    ids: ["vrSrchReqDtFrom", "vrSrchReqDtTo"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "work-date",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    // 로케이션구분
    ids: ["vrSrchLocTp"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "product-group",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      { name: "standard", nameKey: "standard", value: "N" },
      { name: "dps", nameKey: "dps", value: "Y" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 상품
    ids: ["vrSrchRitemCd", "vrSrchRitemNm"],
    hiddenId: "vrSrchRitemId",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "RITEM_ID",
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    srchKey: "ITEM",
    title: "product",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
    optionsKey: "ITEMGRP",
  },
  {
    // 등록주문차수
    ids: ["vrSrchOrdDegree"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "sequence-of-registered-orders",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 합포 Y/N
    ids: ["vrSrchMutiPackingYn"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "multi-package-y/n",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "multi-package", nameKey: "multi-package", value: "Y" },
      { name: "single-package", nameKey: "single-package", value: "N" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // ZONE
    ids: ["vrSrchZoneId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "zone",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "ZONEGUBUN",
    options: [{ nameKey: "all", name: "", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // B2C/B2B
    ids: ["vrSrchOrdBizType"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "B2C/B2B",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "B2C", nameKey: "B2C", value: "B2C" },
      { name: "B2B", nameKey: "B2B", value: "B2B" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

export const GRID_STATISTICS_INFO = [
  {
    id: "TOTAL_QTY",
    title: "total-quantity",
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: true,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
};
