/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP910_4/column-defs.ts
 *  Description:    운영관리/입/출고관리(통합) - 출고관리(H/D) 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import type { info, IModal, ISearchInput, IDateRange, IGridStatisticsInfo } from "@/types/index";
import { UtilService } from "@/services/util-service";
export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSCM011",
  pk: "RNUM",
};

export const INVALID_MODAL_DEF = {
  headerTitle: "notification",
  contentTitle: "notification-of-insufficient-quantity",
  contentSubTitle: "insufficient-quantity-list",
  columns: ["product-code", "product-name", "request-shipping-qty", "available-inventory-qty"],
};

// done
export const GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [
  {
    id: "TOTAL_COUNT_HEADER",
    title: "number-of-orders(header)",
  },
  {
    id: "TOTAL_COUNT_DETAIL",
    title: "total-number-of-orders(detail)",
  },
  {
    id: "TOTAL_QTY",
    title: "total-order-quantity",
  },
];

// done
export const DATE_RANGE_INFO: IDateRange = {
  dateInputIds: ["vrSrchReqDtFrom", "vrSrchReqDtTo"],
  checkBoxes: [
    {
      id: "cmbSrchType",
      value: "100",
      title: "schedule-date",
      defaultChecked: true,
    },
    {
      id: "cmbSrchType",
      value: "200",
      title: "shipping-date",
    },
  ],
};

//done
export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    saveSession: true,
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "12" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  warehouse: {
    page: "WMSMS040",
    id: "warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },

    inputs: [
      {
        id: "S_WH_CD",
        searchContainerInputId: "vrSrchWhCd",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        searchContainerInputId: "vrSrchWhNm",
        title: "warehouse-name	",
        type: "text",
        width: "half",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",

    defaultParamsData: {
      storeSaveKey: "owner",
      rowDataKeys: ["CUST_ID"],
      paramsKeys: ["vrSrchCustId"],
    },

    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "Y",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchRitemCd",
        title: "product-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchRitemNm",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        options: [
          { name: "", nameKey: "all", value: "" },
          { name: "useProductY", nameKey: "useProductY", value: "Y" },
          { name: "useProductN", nameKey: "useProductN", value: "N" },
        ],
      },
    ],
  },
  customer: {
    page: "WMSMS011",
    id: "customer",
    title: "search-owner",
    gridTitle: "owner-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["S_CUST_ID"],
      rowDataKeys: ["CUST_ID"],
    },

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "LC_ALL",
        S_LC_ID: UtilService.getCurrentLocation(),
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "txtSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "txtSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

// done
export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 작업구분
    ids: ["vrSrchOrderPhase"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "work-type",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsAutoSelected: { autoSelectedKeyIndex: 1, allowAutoSelected: true },
    optionsKey: "vrSrchOrderPhase",
  },
  {
    // 주문상세
    ids: ["vrSrchOrdSubtype"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "order-details",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [{ name: "select", nameKey: "select", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    optionsKey: "vrSrchOrdSubtype",
  },
  {
    //화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: true,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    //창고
    ids: ["vrSrchWhCd", "vrSrchWhNm"],
    hiddenId: "",
    rowDataIds: ["WH_CD", "WH_NM"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchWhCd", "vrSrchWhNm"],
    srchKey: "WH",
    title: "warehouse",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 거래처
    ids: ["txtSrchCustCd", "txtSrchCustNm"],
    hiddenId: "txtSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    defaultSearchApiKeys: { custId: "OK", vrSrchCustId: "" },
    isAllUseSearchApiKey: true,
    srchKey: "CUST",
    title: "customer",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    //상품
    ids: ["vrSrchRitemCd", "vrSrchRitemNm"],
    hiddenId: "vrSrchRitemId",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "RITEM_ID",
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm", "vrSrchCustCd"],
    srchKey: "ITEM",
    title: "product",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 상품군
    ids: ["vrSrchItemGrpId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "product-group",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "ITEMGRP",
    options: [
      { name: "", nameKey: "all", value: "" },
      { name: "", nameKey: "unspecified", value: "N/A" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 임가공상품타입
    ids: ["vrSrchSetItemType"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "kit-item-type",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [{ name: "select", nameKey: "select", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    optionsKey: "vrSrchSetItemType",
  },
  {
    // 주문번호
    ids: ["vrSrchOrderId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "order-number",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 원주문번호
    ids: ["vrSrchOrgOrdId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "original-order-number",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 등록주문차수
    ids: ["vrSrchOrdDegree"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "sequence-of-registered-orders",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 재고유무
    ids: ["vrSrchStockQtyEmpty"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "stock-Y/N",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [{ name: "select", nameKey: "select", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    optionsKey: "vrSrchStockQtyEmpty",
  },
  {
    // LOT NO
    ids: ["vrSrchCustLotNo"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "LOT-NO",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 컨테이너번호
    ids: ["vrSrchCntrNo"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "container-number",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 고객명
    ids: ["vrSrchSalesCustNm"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "customer-nm",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // ZONE코드
    ids: ["vrSrchZoneCd"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "zone-code",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 비고1
    ids: ["vrSrchEtc1"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "remark#1",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 비고2
    ids: ["vrSrchEtc2"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "remark#2",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["vrSrchInsertSource"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "input-source",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [{ name: "select", nameKey: "select", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    optionsKey: "vrSrchInsertSource",
  },
  {
    ids: ["vrSrchWgtYn"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "weight-management",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["vrSrchBestDateYn"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "management-Y/N-of-expiration-date",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["vrSrchRfidYn"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "use-rfid-Y/N",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["vrSrchOutOrderCntInit"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "order-registration-quantity",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["vrSrchOutWorkCntInit"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "working-cnt",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
];

//done
export const SEARCH_COMPONENT_CONTROL_BTN = [
  // TODO :: 삭제 - 편의성 기능으로 대체
  // {
  //   title: "initialize-search-criteria",
  //   colorStyle: "success",
  //   paddingStyle: "normal",
  //   authType: "",
  //   image: "",
  // },
  {
    title: "template-input-for-B2C",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  //( No.73 그리드/엑셀 양식에 대한 DB정보에 대해서 확인 후 회신하고자 함. (이전까지 보류))
  // {
  //   title: "template-input-for-sap",
  //   colorStyle: "primary",
  //   paddingStyle: "normal",
  //   image: "",
  //   disabled: "",
  // },
  // {
  //   title: "enter-template",
  //   colorStyle: "primary",
  //   paddingStyle: "normal",
  //   image: "",
  //   authType: "INS_AUTH",
  //   disabled: "",
  // },
];

// done
export const CONTROL_BTN = [
  {
    // 피킹리스트 엑셀
    title: "picking-list-excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "",
    authType: "EXC_AUTH",
    // disabled: "disabled",
  },
  // TODO :: 삭제예정. WINUS 미사용기능 판단.
  // {
  //   // 배차생성
  //   title: "create-batch",
  //   colorStyle: "success",
  //   paddingStyle: "normal",
  //   image: "",
  //   authType: "INS_AUTH",
  //   disabled: "disabled",
  // },
  // TODO :: 2차수 개발항목 (미구현)
  // {
  //   // 헤더 그리드 변경
  //   title: "change-header-grid",
  //   colorStyle: "primary",
  //   paddingStyle: "normal",
  //   image: "",
  //   authType: "INS_AUTH",
  //   disabled: "disabled",
  // },
  // {
  //   // 디테일 그리드 변경
  //   title: "change-detail-grid",
  //   colorStyle: "primary",
  //   paddingStyle: "normal",
  //   image: "",
  //   authType: "INS_AUTH",
  //   disabled: "disabled",
  // },
];
