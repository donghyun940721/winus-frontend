/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP910_4/input.grid.ts
 *  Description:    운영관리/입/출고관리(통합) - 출고관리(H/D) 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { IControlBtn, IGridCellSelectBox, IGridStatisticsInfo } from "@/types";
import { GridOptions } from "ag-grid-community";
import { MASTER_GRID_COLUMN_DEFS } from "./column-defs";

export const MASTER_CELL_RENDERER_INFO: IGridCellSelectBox = {
  CODE_LEVEL: {
    pk: "RNUM",
    optionsKey: "",
    options: [
      { name: "", nameKey: "can-be-delete", value: "0" },
      { name: "", nameKey: "master-can-be-delete", value: "1" },
      { name: "", nameKey: "can-not-delete", value: "2" },
    ],
  },
  USER_GB: {
    pk: "RNUM",
    optionsKey: "USER_GB",
  },
};

export const MASTER_CONTROL_BTN: IControlBtn[] = [
  {
    // 피킹리스트 엑셀
    title: "picking-list-excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "",
    authType: "EXC_AUTH",
    // disabled: "disabled",
  },
  // TODO :: 삭제예정. WINUS 미사용기능 판단.
  // {
  //   // 배차생성
  //   title: "create-batch",
  //   colorStyle: "success",
  //   paddingStyle: "normal",
  //   image: "",
  //   authType: "INS_AUTH",
  //   disabled: "disabled",
  // },
  // TODO :: 2차수 개발항목 (미구현)
  // {
  //   // 헤더 그리드 변경
  //   title: "change-header-grid",
  //   colorStyle: "primary",
  //   paddingStyle: "normal",
  //   image: "",
  //   authType: "INS_AUTH",
  //   disabled: "disabled",
  // },
  // {
  //   // 디테일 그리드 변경
  //   title: "change-detail-grid",
  //   colorStyle: "primary",
  //   paddingStyle: "normal",
  //   image: "",
  //   authType: "INS_AUTH",
  //   disabled: "disabled",
  // },
];

export const DETAIL_CONTROL_BTN: IControlBtn[] = [];

// done
export const GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [
  {
    id: "TOTAL_COUNT_HEADER",
    title: "number-of-orders(header)",
  },
  {
    id: "TOTAL_COUNT_DETAIL",
    title: "total-number-of-orders(detail)",
  },
  {
    id: "TOTAL_QTY",
    title: "total-order-quantity",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [100, 200, 300, 500, 1000],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  allowContextMenuWithControlKey: true,
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
};
