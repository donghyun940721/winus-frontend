/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST030_2/column-defs.ts
 *  Description:    재고/재고명의변경(신규)_통합재고 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridSearchButton from "@/components/renderer/grid-search-button.vue";
import gridSelect from "@/components/renderer/grid-select.vue";
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import { GridUtils } from "@/lib/ag-grid/utils";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
import type { ICellEditorParams } from "ag-grid-community";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  product: [
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  // location: [
  //   {
  //     field: "",
  //     headerKey: "no",
  //     headerName: "",
  //     width: 60,
  //     cellStyle: { textAlign: "center" },
  //     valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  //   },
  //   {
  //     field: "LOC_CD",
  //     headerName: "location",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "",
  //     headerName: "product",
  //     headerClass: "header-center",
  //     sortable: true,
  //     children: [
  //       { field: "RITEM_CD", columnGroupShow: "open", headerName: "code" },
  //       { field: "RITEM_NM", columnGroupShow: "open", headerName: "product-name" },
  //     ],
  //   },
  //   {
  //     field: "AVAILABLE_QTY",
  //     headerName: "stock-quantity",
  //     cellStyle: { textAlign: "right" },
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "UOM_NM",
  //     headerName: "uom",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "OUT_EXP_QTY",
  //     headerName: "schedule-quantity",
  //     cellStyle: { textAlign: "right" },
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "REAL_PLT_QTY",
  //     headerName: "plt-quantity",
  //     cellStyle: { textAlign: "right" },
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "STOCK_WEIGHT",
  //     headerName: "weight",
  //     cellStyle: { textAlign: "right" },
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "CUST_LOT_NO",
  //     headerName: "lot-number",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "ITEM_BEST_DATE_END",
  //     headerName: "expiration-date",
  //     cellStyle: { textAlign: "center" },
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  // ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    pinned: "left",
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.commonRowIndex,
  },
  {
    field: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    pinned: "left",
  },
  {
    field: "",
    headerName: t("grid-column-name.assignment"),
    headerClass: "header-center",
    sortable: true,
    children: [
      {
        field: "WH_NM",
        columnGroupShow: "open",
        headerName: t("grid-column-name.warehouse"),
      },
      {
        field: "SELL_ITEM_CD",
        columnGroupShow: "open",
        headerName: t("grid-column-name.product-code"),
      },
      {
        field: "SELL_RITEM_NM",
        columnGroupShow: "open",
        headerName: t("grid-column-name.product-name"),
      },
      {
        field: "OUT_EXP_QTY",
        columnGroupShow: "open",
        headerName: t("grid-column-name.scheduled-shipping-quantity"),
        cellStyle: { textAlign: "right" },
        width: 130,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      {
        field: "BUY_QTY",
        columnGroupShow: "open",
        headerName: t("grid-column-name.qty"),
        cellStyle: { textAlign: "right" },
        width: 130,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      {
        field: "SELL_ITEM_UOM",
        columnGroupShow: "open",
        headerName: t("grid-column-name.uom"),
        width: 90,
      },
    ],
  },
  {
    field: "",
    headerName: "→",
    width: 50,
    cellStyle: { textAlign: "center" },
    // TODO :: 표현 방식 변경 필요
    valueGetter: () => {
      return "→";
    },
  },
  {
    field: "",
    headerName: t("grid-column-name.amniotic-fluid"),
    headerClass: "header-center",
    sortable: true,
    children: [
      {
        field: "I_TO_RITEM_CD",
        headerClass: "header-require",
        columnGroupShow: "open",
        headerName: t("grid-column-name.product-code"),
        editable: true,
      },
      {
        field: "ITEM",
        headerName: "",
        width: 50,
        columnGroupShow: "open",
        cellClass: "renderer-cell",
        cellRenderer: GridSearchButton,
      },
      {
        field: "I_TO_RITEM_NM",
        headerClass: "header-require",
        columnGroupShow: "open",
        headerName: t("grid-column-name.product-name"),
        editable: true,
        aggFunc: "sum",
      },
      {
        field: "SELL_QTY",
        columnGroupShow: "open",
        headerName: t("grid-column-name.qty"),
        cellStyle: { textAlign: "right" },
        width: 110,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      {
        field: "UOM_CD",
        headerClass: "header-require",
        headerName: t("grid-column-name.uom"),
        width: 90,
        columnGroupShow: "open",
        // cellRenderer: GridSelectBox,
        cellEditor: gridSelect,
        cellEditorParams: (params: ICellEditorParams) => {
          return {
            values: GridUtils.getOptionsCode("UOM"),
            formatValue: (value: any) => {
              return Getter.convetCodeToNameByOptionData("UOM", value);
            },
          };
        },
        cellRenderer: function (params: any) {
          return Getter.convetCodeToNameByOptionData("UOM", params.value);
        },
        editable: true,
      },
    ],
  },
  {
    field: "I_CHANGE_WORK_MEMO",
    headerName: t("grid-column-name.reason-of-change"),
    headerClass: "header-center",
    sortable: true,
    // cellRenderer: gridTextInput,
    editable: true,
    flex: 1,
  },
];
