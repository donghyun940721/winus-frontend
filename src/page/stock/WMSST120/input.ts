/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST120/input.ts
 *  Description:    재고관리/재고조사 input정의 
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import type { IDateRange, IModal, ISearchInput, info } from "@/types";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSST120",
  pk: "RNUM",
};

export const DATE_RANGE_INFO: IDateRange = {
  dateInputIds: ["vrSrchReqDtFrom", "vrSrchReqDtTo"],
  checkBoxes: [
    {
      id: "",
      value: "",
      title: "by-period",
      defaultChecked: true,
    },
  ],
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd_T01",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm_T01",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  location: {
    page: "WMSCM080",
    id: "location",
    title: "search-location",
    gridTitle: "location-list",

    apis: {
      url: "/WMSCM080/list_rn.action",
      params: {
        func: "fn_setWMSCM080",
        LOC_ID: "",
        LOC_CD: "",
        AVAILABLE_QTY: "",
        OUT_EXP_QTY: "",
        STOCK_ID: "",
        SUB_LOT_ID: "",
        vrViewOnlyLoc: "",
        STOCK_WEIGHT: "",
        vrViewSubLotId: "",
        vrRitemId: "",
        ITEM_BEST_DATE_END: "",
        UOM_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        vrViewStockQty: "",
        vrSrchLocCd: "",
        vrSrchLocId: "",
        vrSrchCustLotNo: "",
        vrSrchLocStat: "300",
        S_WH_CD: "",
        vrWhId: "",
        S_WH_NM: "",
        vrSrchLocDel: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchLocCd",
        searchContainerInputId: "vrSrchLocCd",
        title: "location-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchCustLotNo",
        searchContainerInputId: "",
        title: "LOT-number",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchLocStat",
        title: "location-status",
        type: "select",
        width: "half",
        options: [
          { nameKey: "all", name: "", value: "300" },
          { nameKey: "use-location", name: "", value: "200" },
          { nameKey: "unused-location", name: "", value: "100" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["txtSrchCustCd", "txtSrchCustNm"],
    hiddenId: "hdnCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "half",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["txtSrchLocNm", "txtSrchLocCd"],
    hiddenId: "txtSrchLocId",
    rowDataIds: ["LOC_CD", "LOC_CD"],
    rowDataHiddenId: "LOC_ID",
    searchApiKeys: ["vrSrchLocCd", "vrSrchLocNm"],
    srchKey: "LOCATION",
    title: "location",
    width: "half",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
];
export const TOP_GRID_CONTROL_BTN = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const BOTTOM_GRID_CONTROL_BTN = [
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "printing",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "EXC_AUTH",
  },
];
