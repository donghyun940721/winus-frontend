/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST120/column-defs.ts
 *  Description:    재고조사 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Format, Getter } from "@/lib/ag-grid";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  location: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

        { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerKey: "stock-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerKey: "lot-number",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerKey: "expiration-date",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    },
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 60,
    cellStyle: { textAlign: "center" },
  },

  {
    field: "CYCL_STOCK_ID",
    headerKey: "stocktaking-number",
    headerName: "",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerName: "",
    cellStyle: { textAlign: "left" },
    sortable: true,
    export: true,
  },
  {
    field: "LOC_CD",
    headerKey: "location",
    headerName: "",
    cellStyle: { textAlign: "left" },
    sortable: true,
    export: true,
  },
  {
    field: "WORK_DT",
    headerKey: "work-date",
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
  },
  {
    field: "CYCL_STOCK_TYPE",
    headerKey: "inventory-arrangement-method",
    headerName: "",
    headerClass: "header-center",
    width: 130,
    // cellEditor: gridSelect,
    // cellEditorParams: (params: ICellEditorParams) => {
    //   return {
    //     values: GridUtils.getOptionsCode("CYCL01"),
    //     formatValue: (value: any) => {
    //       return Getter.convetCodeToNameByOptionData("CYCL01", value);
    //     },
    //   };
    // },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("CYCL_STOCK_TYPE", params.value);
    },
    editable: true,
    sortable: true,
  },
  {
    field: "TYPE_ST",
    headerKey: "total-epc",
    headerName: "",
    headerClass: "header-center",
    width: 120,
    // cellEditor: gridSelect,
    // cellEditorParams: (params: ICellEditorParams) => {
    //   return {
    //     values: GridUtils.getOptionsCode("TYPE_ST"),
    //     formatValue: (value: any) => {
    //       return Getter.convetCodeToNameByOptionData("TYPE_ST", value);
    //     },
    //   };
    // },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("TYPE_ST", params.value);
    },
    editable: true,
    sortable: true,
  },
  {
    field: "WORK_STAT",
    headerKey: "status",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 40,
    // cellEditor: gridSelect,
    // cellEditorParams: (params: ICellEditorParams) => {
    //   return {
    //     values: GridUtils.getOptionsCode("CYCL02"),
    //     formatValue: (value: any) => {
    //       return Getter.convetCodeToNameByOptionData("CYCL02", value);
    //     },
    //   };
    // },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("WORK_STAT", params.value);
    },
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    },
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 60,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerName: "",
    width: 120,
    cellStyle: {
      textAlign: "left",
    },
    sortable: true,
  },
  {
    field: "LOC_CD",
    headerKey: "location",
    headerName: "",
    width: 140,
    cellStyle: {
      textAlign: "left",
    },
    sortable: true,
  },
  {
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    width: 120,
    cellStyle: {
      textAlign: "right",
    },
    sortable: true,
  },
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    width: 200,
    cellStyle: {
      textAlign: "left",
    },
    sortable: true,
  },
  {
    field: "ITEM_EPC_CD",
    headerKey: "epc-code",
    headerName: "",
    width: 120,
    cellStyle: {
      textAlign: "left",
    },
    sortable: true,
  },
  {
    field: "EXP_QTY",
    headerKey: "current-stock-quantity",
    headerName: "",
    width: 100,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    sortable: true,
  },
  {
    field: "REAL_QTY",
    headerKey: "actual-quantity",
    headerName: "",
    width: 100,
    cellStyle: { textAlign: "right" },
    sortable: true,
    valueFormatter: Format.NumberCountEmpty,
    cellClass: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true ? "editable-cell" : "";
    },
    editable: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true;
    },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "DIFF_REASON",
    headerKey: "reason-for-difference",
    headerName: "",
    width: 200,
    cellStyle: {
      textAlign: "left",
    },
    sortable: true,
    cellClass: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true ? "editable-cell" : "";
    },
    editable: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true;
    },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "WORK_STAT",
    headerKey: "check-y-n",
    headerName: "",
    width: 100,
    cellStyle: {
      textAlign: "left",
    },
    cellRenderer: function (params: any) {
      //todo: 공통코드 ROTSTATE가 있어야 하는데, 조회시 ROTSTATE라는 공통코드가 없음
      return Getter.convetCodeToNameByOptionData("ROTSTATE", params.value);
    },
    sortable: true,
  },
  {
    field: "BEST_DATE",
    headerKey: "validity",
    headerName: "",
    width: 140,
    cellStyle: {
      textAlign: "center",
    },
    sortable: true,
  },
  {
    field: "CYCL_STOCK_ID",
    hide: true,
    headerKey: "",
    headerName: "",
    cellStyle: {
      textAlign: "",
    },
    sortable: true,
  },
  {
    field: "WORK_SEQ",
    hide: true,
    headerKey: "",
    headerName: "",
    cellStyle: { textAlign: "" },
    sortable: true,
  },
];
