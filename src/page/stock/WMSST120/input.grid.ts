/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST120/input.grid.ts
 *  Description:    재고조사 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { IControlBtn } from "@/types";
import { GridOptions } from "ag-grid-community";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";

export const MASTER_CONTROL_BTN: IControlBtn[] = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const DETAIL_CONTROL_BTN: IControlBtn[] = [
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "printing",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "EXC_AUTH",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [200, 300, 400],
};

// export const gridOptionsMeta: GridOptions = {
//  popupParent: document.body,
//   defaultColDef: {
//     resizable: true,
//     menuTabs: [],
//   },
//   headerHeight: 32,
//   rowHeight: 32,
//   columnDefs: MASTER_GRID_COLUMN_DEFS,
//   rowSelection: "multiple",
//   rowModelType: "clientSide",
//   enableRangeSelection: true,
//   // suppressRowTransform: true, // 병합 전제조건
//   suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
//   pagination: false,
//   statusBar: true,
//   getRowId: (data) => {
//     return data.data.RNUM;
//   },
// };
export const MASTER_GRID_OPTIONS_META: GridOptions = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
};

export const DETAIL_GRID_OPTIONS_META: GridOptions = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: DETAIL_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
};
