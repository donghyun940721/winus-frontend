/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST920/column-defs.ts
 *  Description:    현재고조회 컬럼 정의 스크립트
 *  Authors:        dhkim
 *  Update History:
 *                  2024.04 : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/formatter";
import type { IColDef, IColGroupDef, IHasKeyColDef } from "@/types/agGrid";

export const MODAL_COLUMN_DEFS: IHasKeyColDef = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  product: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  location: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerName: "",
      headerKey: "location",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerName: "",
      headerKey: "product",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerName: "", headerKey: "code" },
        { field: "RITEM_NM", columnGroupShow: "open", headerName: "", headerKey: "product-name" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerName: "",
      headerKey: "stock-quantity",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerName: "",
      headerKey: "uom",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerName: "",
      headerKey: "schedule-quantity",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerName: "",
      headerKey: "plt-quantity",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerName: "",
      headerKey: "weight",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerName: "",
      headerKey: "lot-number",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerName: "",
      headerKey: "expiration-date",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] = [
  {
    field: "No",
    headerKey: "no",
    headerName: "No",
    // headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    minWidth: 80,
    width: 80,
    pinned: "left",
    menuTabs: ["columnsMenuTab"],
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "RNUM")) {
        return params.node.rowIndex + 1;
      } else {
        return "";
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "Product",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    marryChildren: true,
    children: [
      {
        field: "RITEM_ID",
        columnGroupShow: "open",
        headerKey: "",
        headerName: "ID",
        width: 130,
        export: true,
        sortable: true,
        menuTabs: ["filterMenuTab"],
        filter: "agSetColumnFilter",
      },
      {
        field: "RITEM_CD",
        columnGroupShow: "open",
        headerKey: "",
        headerName: "코드",
        export: true,
        sortable: true,
        menuTabs: ["filterMenuTab"],
        filter: "agSetColumnFilter",
      },
      {
        field: "RITEM_NM",
        columnGroupShow: "open",
        headerKey: "",
        headerName: "명칭",
        width: 300,
        flex: 1,
        export: true,
        sortable: true,
        menuTabs: ["filterMenuTab"],
        filter: "agSetColumnFilter",
      },
      {
        field: "GRADE",
        columnGroupShow: "open",
        headerKey: "",
        headerName: "등급",
        width: 130,
        cellStyle: { textAlign: "center" },
        export: true,
        sortable: true,
        menuTabs: ["filterMenuTab"],
        filter: "agSetColumnFilter",
      },
    ],
  },
  {
    // 섹션
    field: "SECTION_CD",
    headerKey: "",
    headerName: "섹션",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 130,
    sortable: true,
    export: true,
    menuTabs: ["filterMenuTab"],
    filter: "agSetColumnFilter",
  },
  {
    // 로케이션
    field: "LOC_ID",
    headerKey: "",
    headerName: "로케이션",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 140,
    export: true,
    sortable: true,
    menuTabs: ["filterMenuTab"],
    filter: "agSetColumnFilter",
  },
  {
    // 주문수량
    field: "STOCK_QTY",
    headerKey: "",
    headerName: "수량",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    width: 110,
    export: true,
    sortable: true,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "REG_DT",
    headerKey: "reg-dt",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    valueFormatter: Format.Date,
    width: 170,
    sortable: true,
    export: true,
  },
  {
    field: "REG_NM",
    headerKey: "reg-no",
    headerName: "",
    headerClass: "header-center",
    width: 130,
    export: true,
  },
  {
    field: "UPD_DT",
    headerKey: "date-modified",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    valueFormatter: Format.Date,
    width: 170,
    export: true,
  },
  {
    field: "UPD_NM",
    headerKey: "modifier",
    headerName: "",
    headerClass: "header-center",
    width: 130,
    export: true,
  },
];
