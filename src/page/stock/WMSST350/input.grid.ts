/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST350/input.grid.ts
 *  Description:    임가공(재고관리) grid meta 정보
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.07. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/

/**
 ********************* Grid Area *********************/
import { i18n } from "@/i18n";
import { IControlBtn, IGridCellSelectBox, IGridStatisticsInfo } from "@/types";
import { GridOptions } from "ag-grid-community";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const GRID_SELECT_BOX_INFO: IGridCellSelectBox = {};
export const MASTER_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [
  //전체
  {
    id: "TOTAL_SEQ",
    title: "total-SEQ",
  },
];
export const DETAIL_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [];
// done
export const MASTER_CONTROL_BTN: IControlBtn[] = [
  //임가공작업 생성
  {
    title: "manufacturing-job-create",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "",
  },
  //추천임가공
  {
    title: "recommend-manufacturing-service",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "",
  },
  //임가공
  {
    title: "manufacturing-service",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "",
  },
  //임가공해체
  {
    title: "manufacturing-separate",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "",
  },
  //엑셀
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "INS_AUTH",
  },
  //원부재료엑셀
  {
    title: "raw-material(parts/containers)-excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "INS_AUTH",
  },
];
export const DETAIL_CONTROL_BTN: IControlBtn[] = [
  //엑셀
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "INS_AUTH",
  },
];

export const MasterGridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSST350_main"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [],
};

export const DetailGridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSST350_sub"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "",

  //페이징옵션
  pagingSizeList: [],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  mainColumnDefs: MASTER_GRID_COLUMN_DEFS,
  subColumnDefs: DETAIL_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: false,
  // getRowId: (data) => {
  //   return data.data.RNUM;
  // },
  getRowStyle: (params) => {
    // Check if the 'value' field is greater than a specific number (e.g., 100)
    if (params.data["D_QTY"] > params.data["D_STOCK_QTY"]) {
      return { backgroundColor: "#FFDEDE" };
    } else if (params.data["D_STOCK_QTY"] < 50) {
      return { backgroundColor: "#FAFAD2" };
    }
  },
};
