/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST350/input-search.ts
 *  Description:    임가공(재고관리) search-form 정의 스크립트
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.06. : Created by H. N. Ko
 *
----------------------------------------------------------------- -------------*/
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "",
  pk: "",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "CUST_CD",
        title: "shipper-code",
        searchContainerInputId: "vrSrchCustCd",
        type: "text",
        width: "half",
      },
      {
        id: "CUST_NM",
        title: "owner-name",
        searchContainerInputId: "vrSrchCustNm",
        type: "text",
        width: "half",
      },
    ],
  },
  "set-product-code": {
    page: "WMSCM091",
    id: "product-code",
    title: "search-product",
    gridTitle: "product-list",

    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },

    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "viewAll",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        title: "product-code",
        searchContainerInputId: "vrSrchItemCd",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        title: "product-name",
        searchContainerInputId: "vrSrchItemNm",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        optionsKey: "S_SET_ITEM_YN",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
    ],
  },
};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 출고예정일
    ids: ["vrSrchReqFromDt", "vrSrchReqToDt"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "schedule-date",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    // 화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    //세트상품코드
    ids: ["S_ITEM_CD", "S_ITEM_NM"],
    hiddenId: "S_ITEM_ID",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "ITEM_ID",
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    srchKey: "ITEM",
    title: "set-product-code",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 구분
    ids: ["vrSrchGb"],
    hiddenId: "",
    rowDataIds: ["vrSrchGb"],
    rowDataHiddenId: "vrSrchGb",
    searchApiKeys: ["vrSrchGb"],
    title: "sort",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "DS_TP",
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "shipping-order(standard)", nameKey: "shipping-order(standard)", value: "Y" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 상품군
    ids: ["vrSrchItemGrpId"],
    hiddenId: "",
    rowDataIds: ["vrSrchItemGrpId"],
    rowDataHiddenId: "vrSrchItemGrpId",
    searchApiKeys: ["vrSrchItemGrpId"],
    title: "product-group",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "ITEMGRP",
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "N/A", nameKey: "N/A", value: "N/A" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 상품사용여부
    ids: ["vrSrchUseYn"],
    hiddenId: "",
    rowDataIds: ["vrSrchUseYn"],
    rowDataHiddenId: "vrSrchUseYn",
    searchApiKeys: ["vrSrchUseYn"],
    title: "product-usage-status",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "COM09",
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "Y", nameKey: "Y", value: "Y" },
      { name: "N", nameKey: "N", value: "N" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 1, allowAutoSelected: true },
  },
  // {
  //   // ZONE
  //   ids: ["vrZoneGubunId"],
  //   hiddenId: "",
  //   rowDataIds: ["vrZoneGubunId"],
  //   rowDataHiddenId: "vrZoneGubunId",
  //   searchApiKeys: ["vrZoneGubunId"],
  //   title: "zone",
  //   width: "quarter",
  //   isModal: false,
  //   required: false,
  //   isSearch: false,
  //   types: ["select-box"],
  //   optionsKey: "ZONE_NM",
  //   options: [{ name: "all", nameKey: "all", value: "" }],
  //   optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  // },
  // {
  //   // 로케이션타입
  //   ids: ["vrLocationType"],
  //   hiddenId: "",
  //   rowDataIds: ["vrLocationType"],
  //   rowDataHiddenId: "vrLocationType",
  //   searchApiKeys: ["vrLocationType"],
  //   title: "location-type",
  //   width: "quarter",
  //   isModal: false,
  //   required: false,
  //   isSearch: false,
  //   types: ["select-box"],
  //   optionsKey: "DS_TP",
  //   options: [
  //     { name: "all", nameKey: "all", value: "" },
  //     { name: "manufacturing-service", nameKey: "manufacturing-service", value: "90" },
  //     { name: "shipping", nameKey: "shipping", value: "10" },
  //   ],
  //   optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  // },
];

export const MASTER_SEARCH_INPUT: ISearchInput[] = [];

// defaultParams확인 필요
export const DETAIL_SEARCH_INPUT: ISearchInput[] = [
  {
    // 세트상품코드
    ids: ["vrSrchItemCd"],
    hiddenId: "",
    rowDataIds: ["vrSrchItemCd"],
    rowDataHiddenId: "vrSrchCustId",
    searchApiKeys: ["vrSrchItemCd"],
    title: "set-product-code",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    // 세트상품명
    ids: ["vrSrchCustNm"],
    hiddenId: "",
    rowDataIds: ["vrSrchCustNm"],
    rowDataHiddenId: "vrSrchCustId",
    searchApiKeys: ["vrSrchCustNm"],
    title: "set-product-name",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useMore: false,
  useSetting: false, // TODO :: 위치 조정 이 후, 재설정 (TRUE)
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  masterSearchInput: MASTER_SEARCH_INPUT,
  detailSearchInput: DETAIL_SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  searchConditionInitUrl: "",
};
