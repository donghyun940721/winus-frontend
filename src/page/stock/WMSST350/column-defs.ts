import { CellClassParams, EditableCallbackParams } from "ag-grid-community";
/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST350/column-defs.ts
 *  Description:    임가공(재고관리) 컬럼 정의 스크립트
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.06. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MODAL_COLUMN_DEFS: any = {
  //화주
  owner: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  //세트상품코드
  "set-product-code": [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      valueFormatter: Format.NumberPrice,
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  //(체크박스)
  {
    field: "SPDCOL_CHK_BOX",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: false,
    checkboxSelection: true,
    width: 35,
  },
  //상품명
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 200,
  },
  //상품코드
  {
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 140,
  },
  //수량입력
  {
    field: "INS_QTY",
    headerKey: "enter-quantity",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 70,
    editable: (params: EditableCallbackParams<any>) => {
      // 'INS_QTY' 셀은 선택된 행인 경우에만 편집 가능
      return params.node.isSelected() ? params.colDef.field === "INS_QTY" : false;
    },
    cellClassRules: {
      // 선택된 행에서 'INS_QTY' 셀이면 자주색 배경 적용
      "purple-cell": (params: CellClassParams<any, any>) => {
        return params.node.isSelected() ? params.colDef.field === "INS_QTY" : false;
      },
    },
  },
  //현재고
  {
    field: "S_STOCK_QTY",
    headerKey: "current-stock",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 60,
  },
  //출고주문량
  {
    field: "REQ_SET_QTY",
    headerKey: "shipping-order-qty",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 80,
  },
  //생성가능수량
  {
    field: "REQ_SET_QTY",
    headerKey: "generate-possibility-qty",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 80,
  },
  //추천생성수량
  {
    field: "REQ_SET_QTY",
    headerKey: "recommend-generate-qty",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 80,
  },
  //판매량
  {
    field: "REQ_SET_QTY",
    headerKey: "sales-volume",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 80,
  },
];
export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  //원·부재료 명
  {
    field: "D_PART_RITEM_NM",
    headerKey: "raw·sub-material-name",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 180,
  },
  //상품코드
  {
    field: "D_PART_RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 160,
  },
  //필요수량
  {
    field: "D_QTY",
    headerKey: "required-qty",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 70,
  },
  //출고주문량
  {
    field: "D_OUT_ORD_QTY",
    headerKey: "shipping-order-qty",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 80,
  },
  //총 필요수량
  {
    field: "D_MAKE_QTY",
    headerKey: "total-required-qty",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 80,
  },
  //가용재고
  {
    field: "D_STOCK_ABLE_QTY",
    headerKey: "available-inventory",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 60,
  },
  //현재고
  {
    field: "D_STOCK_QTY",
    headerKey: "current-stock",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 60,
  },
];
