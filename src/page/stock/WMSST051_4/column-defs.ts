/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST051_4/column-defs.ts
 *  Description:    재고관리/일별재고-월별재고내역 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      cellStyle: { textAlign: "left" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],

  product: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      valueFormatter: Format.NumberPrice,
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "logistics-container": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "POOL_GRP_NAME",
      headerKey: "pool-grp-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerKey: "pool-grp-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerKey: "pool-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    menuTabs: ["columnsMenuTab"],
    lockPosition: true,
    lockVisible: true,
    valueGetter: Getter.commonRowIndex,
  },
  {
    field: "RITEM_CD",
    headerName: t("grid-column-name.item-code"),
    sortable: true,
    headerClass: "header-center",
    minWidth: 200,
    // export: true,
    flex: 1,
  },
  {
    field: "RITEM_NM",
    headerName: t("grid-column-name.item-name"),
    sortable: true,
    headerClass: "header-center",
    minWidth: 250,
    // export: true,
    flex: 2,
  },
  {
    field: "LAST_MONTH_STOCK",
    headerName: t("grid-column-name.inventory-at-the-end-of-the-previous-month"),
    sortable: true,
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 150,
    // export: true,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "IN_QTY",
    headerName: t("grid-column-name.receiving-quantity"),
    sortable: true,
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 150,
    // export: true,
    aggFunc: "sum",
  },
  {
    field: "OUT_QTY",
    headerName: t("grid-column-name.shipping-quantity"),
    sortable: true,
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 150,
    // export: true,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "LAST_DAY_STOCK",
    headerName: t("grid-column-name.last-month-inventory"),
    sortable: true,
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 150,
    // export: true,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "UOM_CD",
    headerName: t("grid-column-name.uom"),
    sortable: true,
    headerClass: "header-center",
    width: 150,
    // export: true,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "CUST_CD",
    headerName: t("grid-column-name.shipper-code"),
    sortable: true,
    headerClass: "header-center",
    width: 150,
    // export: true,
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.owner-name"),
    sortable: true,
    headerClass: "header-center",
    width: 150,
    // export: true,
  },
  {
    field: "INOUT_QTY",
    headerName: t("grid-column-name.receiving-and-shipping-quantity"),
    sortable: true,
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 150,
    // export: true,
  },
  {
    field: "UNIT_NM",
    headerName: t("grid-column-name.unit"),
    sortable: true,
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 150,
    // export: true,
  },
];
