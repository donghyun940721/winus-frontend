/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST030_1/input.ts
 *  Description:    재고/재고명의변경(신규)_부분재고 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";
import { IControlBtn, IGridCellSearchButton } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  ITEM: {
    fieldList: ["TO_RITEM_CD", "TO_RITEM_NM"],
    rowDataKeys: ["ITEM_CODE", "ITEM_KOR_NM"],
    modalData: {
      page: "WMSCM091",
      id: "kit-product",
      title: "search-product",
      gridTitle: "product-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달
      defaultParamsData: {
        //해당 모달의 title
        storeSaveKey: "owner",
        paramsKeys: ["vrSrchCustId"],
        rowDataKeys: ["CUST_ID"],
      },
      apis: {
        url: "/WMSCM091/list_rn.action",
        params: {
          vrViewAll: "Y",
          vrSrchCustId: "",
          vrSrchItemCd: "",
          vrSrchItemNm: "",
          vrViewSetItem: "",
          vrSrchWhId: "",
          vrItemType: "",
          vrSrchSetItemYn: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchItemCd",
          title: "product-code",
          searchContainerInputId: "vrSrchItemCd",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemNm",
          searchContainerInputId: "vrSrchItemNm",
          title: "product-name",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemGrp",
          title: "product-group",
          type: "select",
          width: "half",
          optionsKey: "ITEMGRP",
          options: [{ name: "전체", nameKey: "all", value: "" }],
        },
        {
          id: "vrSrchSetItemYn",
          title: "repacking",
          type: "select",
          width: "half",
          //임가공 select-box값
          options: [
            { name: "", nameKey: "all", value: "" },
            { name: "useProductY", nameKey: "useProductY", value: "Y" },
            { name: "useProductN", nameKey: "useProductN", value: "N" },
          ],
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "CUST_NM",
        headerKey: "owner",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_GRP_NAME",
        headerKey: "product-group",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_CODE",
        headerKey: "product-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_KOR_NM",
        headerKey: "product-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "BOX_BAR_CD",
        headerKey: "box-barcode",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "MAKER_NM",
        headerKey: "company-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "STOCK_QTY",
        headerKey: "current-stock",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "BAD_QTY",
        headerKey: "inferior-product",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "UOM_NM",
        headerKey: "uom",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "UNIT_PRICE",
        headerKey: "unit-price",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
        valueFormatter: Format.NumberPrice,
      },
      {
        field: "WH_NM",
        headerKey: "warehouse",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "RITEM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "ITEM_ENG_NM",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "CUST_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_NM",
        sortable: true,
        hide: true,
      },
    ],
  },
};
// done
export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "change-of-owner",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [100, 200, 300, 600],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
    lockVisible: true,
    lockPosition: true,
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  allowContextMenuWithControlKey: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: false,
};
