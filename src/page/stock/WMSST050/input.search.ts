/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM120/input.ts
 *  Description:    주문/입고주문관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { UtilService } from "@/services/util-service";
import { IControlBtn } from "@/types";
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSCM011",
  pk: "CUST_ID",
};

export const NEW_POPUP_CONTROL_BTN: IControlBtn[] = [];

export const NEW_POPUP_SEARCH_MODAL_INFO: any = {};

export const NEW_POPUP_SEARCH_INPUT: any = [];

export const INVENTORY_ADJUSTMENT_SEARCH_MODAL_INFO: IModal = {
  uom: {
    page: "WMSCM101",
    id: "uom",
    title: "search-uom",
    gridTitle: "uom-list",
    differentFunc: {
      title: "select",
      url: "",
    },
    apis: {
      url: "/WMSCM101/list_rn.action",
      params: {
        func: "fn_setWMSCM101",
        vrUom1Cd: "",
        vrStockQty: "",
        vrRitemId: "",
        UOM_ID: "",
        UOM_CD: "",
        UOM1_CODE: "",
        UOM2_CODE: "",
        CONV_QTY: "",
        vrSrchUomId: "",
        vrSrchUomNm: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchUomId",
        title: "uom-code",
        searchContainerInputId: "UOM_CD",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchUomNm",
        title: "uom-name",
        type: "text",
        width: "half",
      },
    ],
  },
};

export const INVENTORY_ADJUSTMENT_SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["WH_NM"],
    hiddenId: "WH_ID",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "WH",
    title: "warehouse",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["ITEM_BEST_DATE_END"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "ITEM",
    title: "expiration-date",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["RITEM_CD"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "RITEM",
    title: "product-code",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["RITEM_NM"],
    hiddenId: "RITEM_ID",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "RITEM",
    title: "product",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["LOC_CD"],
    hiddenId: "LOC_ID",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "LOC",
    title: "location",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["STOCK_QTY"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "stock-quantity",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["UOM_CD"],
    hiddenId: "",
    rowDataIds: ["UOM2_CODE"],
    rowDataHiddenId: "",
    searchApiKeys: ["UOM_CD", "UOM_NM"],
    srchKey: "UOM",
    title: "uom",
    width: "half",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text"],
  },
  {
    ids: ["MODIFYABLE_QTY"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "inventory-adjustable-quantity",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
    disabled: "disabled",
  },
  {
    ids: ["cmbWorkType"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "increase/decrease",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    options: [
      { name: "", nameKey: "(+)", value: "01" },
      { name: "", nameKey: "(-)", value: "02" },
      { name: "", nameKey: "change-uom", value: "03" },
    ],
    types: ["select-box"],
  },
  {
    ids: ["WORK_QTY"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "adjusted-quantity",
    width: "half",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["taReason"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "issue",
    width: "entire",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["textarea"],
  },
];

export const INVENTORY_ADJUSTMENT_CONTROL_BTN = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    saveSession: true,
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "12" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        title: "shipper-code",
        searchContainerInputId: "vrSrchCustCd",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        title: "owner-name",
        searchContainerInputId: "vrSrchCustNm",
        type: "text",
        width: "triple",
      },
    ],
  },
  warehouse: {
    page: "WMSMS040",
    id: "warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_WH_CD",
        searchContainerInputId: "vrSrchWhCd",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        searchContainerInputId: "vrSrchWhNm",
        title: "warehouse-name",
        type: "text",
        width: "half",
      },
    ],
  },
  "product-code": {
    page: "WMSCM091",
    id: "product-code",
    title: "search-product",
    gridTitle: "product-list",

    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },

    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "viewAll",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        title: "product-code",
        searchContainerInputId: "vrSrchItemCd",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        title: "product-name",
        searchContainerInputId: "vrSrchItemNm",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        optionsKey: "S_SET_ITEM_YN",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
    ],
  },
  location: {
    page: "WMSCM080",
    id: "location",
    title: "search-location",
    gridTitle: "location-list",

    apis: {
      url: "/WMSCM080/list_rn.action",
      params: {
        func: "fn_setWMSCM080",
        LOC_ID: "",
        LOC_CD: "",
        AVAILABLE_QTY: "",
        OUT_EXP_QTY: "",
        STOCK_ID: "",
        SUB_LOT_ID: "",
        vrViewOnlyLoc: "",
        STOCK_WEIGHT: "",
        vrViewSubLotId: "",
        vrRitemId: "",
        ITEM_BEST_DATE_END: "",
        UOM_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        vrViewStockQty: "",
        vrSrchLocCd: "",
        vrSrchLocId: "",
        vrSrchCustLotNo: "",
        vrSrchLocStat: "300",
        S_WH_CD: "",
        vrWhId: "",
        S_WH_NM: "",
        vrSrchLocDel: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchLocCd",
        searchContainerInputId: "vrSrchLocCd",
        title: "location-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchCustLotNo",
        searchContainerInputId: "",
        title: "LOT-number",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchLocStat",
        title: "location-status",
        type: "select",
        width: "half",
        options: [
          { nameKey: "all", name: "", value: "300" },
          { nameKey: "use-location", name: "", value: "200" },
          { nameKey: "unused-location", name: "", value: "100" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchWhCd", "vrSrchWhNm"],
    hiddenId: "vrSrchWhId",
    rowDataIds: ["WH_CD", "WH_NM"],
    rowDataHiddenId: "WH_ID",
    searchApiKeys: ["vrSrchWhCd", "vrSrchWhNm"],
    srchKey: "WH",
    title: "warehouse",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchItemCd", "vrSrchItemNm"],
    hiddenId: "vrSrchItemId",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "ITEM_ID",
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    srchKey: "ITEM",
    title: "product-code",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchLocCd", "vrSrchLocNm"],
    hiddenId: "vrSrchLocId",
    rowDataIds: ["LOC_CD", "LOC_CD"],
    rowDataHiddenId: "LOC_ID",
    searchApiKeys: ["vrSrchLocCd", "vrSrchLocNm"],
    srchKey: "LOCATION",
    title: "location",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
];

//done
export const SEARCH_COMPONENT_CONTROL_BTN: IControlBtn[] = [];

const displayInputCount = UtilService.getShowInputCount(2, SEARCH_INPUT) as number;

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useSetting: false,
  unUsedRefreshButton: true,
  useMore: displayInputCount !== UtilService.getAvailableSearchInputCount(SEARCH_INPUT),
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchComponentControlBtn: SEARCH_COMPONENT_CONTROL_BTN,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  // searchConditionInitUrl: "",
};
