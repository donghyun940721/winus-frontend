import type { info } from "@/types/index";
import { gridMetaData, gridOptionsMeta } from "./input.grid";
import { SEARCH_CONTAINER_META } from "./input.search";

export * from "./input.grid";
export * from "./input.search";

export const commonSetting = {
  authPageGroup: "WMSST",
  authPageId: "WMSST050",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};
export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};
export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSCM011",
  pk: "CUST_ID",
};
