/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM120/input.ts
 *  Description:    주문/입고주문관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
/**
 ********************* Grid Area *********************/
import { IControlBtn } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

// done
export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "inventory-adjustment",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
  {
    title: "setting",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [200, 300, 400],
};

export const gridOptionsMeta = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  enableRangeSelection: true,
  suppressContextMenu: true,
  rowSelection: "single",
  rowModelType: "clientSide",
  pagination: false,
  statusBar: true,
};
