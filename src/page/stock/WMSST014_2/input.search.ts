/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST014_2/input.ts
 *  Description:    일별재고조회(NEW) 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

import type { IModal, ISearchInput, info } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSST014_2",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      rowDataKeys: ["CUST_ID"],
      paramsKeys: ["vrSrchCustId"],
    },
    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "Y",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        title: "product-code",
        searchContainerInputId: "vrSrchRitemCd",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchRitemNm",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        options: [{ nameKey: "all", name: "", value: "" }],
        optionsKey: "ITEMGRP",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        options: [
          { nameKey: "all", name: "", value: "" },
          { nameKey: "useProductY", name: "", value: "Y" },
          { nameKey: "useProductN", name: "", value: "N" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
  "logistics-container": {
    page: "WMSCM162",
    id: "logistics-container",
    title: "search-logistics-container",
    gridTitle: "logistics-container-list",

    defaultParamsData: {
      //해당 모달의 title
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    apis: {
      url: "/WMSCM162/list_rn.action",
      params: {
        func: "fn_setWMSCM162",
        vrSrchCustId: "",
        vrSrchPoolCd: "",
        vrSrchPoolNm: "",
        vrSrchPoolGrp: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchPoolCd",
        title: "logistics-container-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchPoolNm",
        title: "logistics-container-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchPoolGrp",
        title: "logistics-container-group",
        type: "select",
        width: "half",
        optionsKey: "POOLGRP",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    //화주
    ids: ["vrSrchCustCd3", "vrSrchCustNm3"],
    hiddenId: "vrSrchCustId3",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 작업일자
    ids: ["vrSrchReqDtFrom3", "vrSrchReqDtTo3"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "work-date",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    // 상품군
    ids: ["vrSrchItemGrpId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "product-group",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "N/A", nameKey: "N/A", value: "N/A" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    optionsKey: "ITEMGRP",
  },
  {
    // 상품
    ids: ["vrSrchItemType", "vrSrchItemCd3", "vrSrchItemNm3"],
    hiddenId: "vrSrchItemId",
    rowDataIds: ["", "ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "ITEM_ID",
    rowData2ndIds: ["", "POOL_CODE", "POOL_NM"],
    rowData2ndHiddenId: "POOL_ID",
    searchApiKeys: ["", "vrSrchItemCd", "vrSrchItemNm"],
    srchKey: "ITEM",
    title: "product",
    modalTypes: ["", "product", "logistics-container"],
    width: "half",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["", "code", "name"],
    types: ["select-box", "text", "text"],
    optionsKey: "vrSrchItemType",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 임가공여부
    ids: ["S_SET_ITEM_YN", "chkEmptyStock"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "Kit_Item_Y/N",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    checkboxLabelTitles: ["", "excluding-absence-of-receipt/delivery-history"],
    checkboxCheckedValue: ["", "on"],
    types: ["select-box", "check-box"],
    options: [
      {
        nameKey: "all",
        name: "",
        value: "",
      },
      {
        nameKey: "Y",
        name: "",
        value: "Y",
      },
      {
        nameKey: "N",
        name: "",
        value: "N",
      },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

export const GRID_STATISTICS_INFO = [
  {
    id: "TOTAL_QTY",
    title: "total-quantity",
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: true,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
};
