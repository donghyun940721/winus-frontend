/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:    WMSST059_3/column-defs.ts
 *  Description:    현재고조회 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { Format } from "@/lib/ag-grid";
import { MODAL_COLUMN_DEFS } from "@/page/common-page/meta/common-modal-column-defs";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
export { MODAL_COLUMN_DEFS };

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    cellStyle: { textAlign: "center" },
    width: 80,
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    pinned: "left",
    lockPosition: true,
  },
  {
    // 화주
    field: "",
    headerKey: "owner",
    headerName: "",
    headerClass: "",
    sortable: true,
    children: [
      {
        field: "CUST_CD",
        headerKey: "code",
        headerName: "",
        headerClass: "",
        sortable: true,
        export: true,
      },
      {
        field: "CUST_NM",
        headerKey: "owner-name",
        headerName: "",
        headerClass: "",
        sortable: true,
        export: true,
      },
    ],
  },
  {
    // 상품군
    field: "ITEM_GRP_NM",
    headerKey: "product-group",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "center" },
    width: 80,
  },
  {
    // 상품
    field: "",
    headerKey: "product",
    headerClass: "",
    sortable: true,
    children: [
      {
        field: "RITEM_CD",
        headerKey: "code",
        headerName: "",
        headerClass: "",
        sortable: true,
        width: 150,
        export: true,
      },
      {
        field: "RITEM_NM",
        headerKey: "product-name",
        headerName: "",
        headerClass: "",
        sortable: true,
        width: 250,
        export: true,
      },
    ],
  },
  {
    // 전일재고
    field: "BEFORE_STOCK_QTY",
    headerKey: "the-day-before-stock",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    width: 150,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 입고
    field: "IN_QTY",
    headerKey: "receiving",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    width: 110,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 일반출고
    field: "OUT_QTY",
    headerKey: "shipping-2",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    width: 110,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 크로스도킹
    field: "CROSS_QTY",
    headerKey: "cross-qyt",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    width: 110,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 이벤트출고
    field: "EVENT_QTY",
    headerKey: "event-qty",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    width: 110,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "RETURN_QTY",
    headerKey: "return-shipping",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "STOCK_QTY",
    headerKey: "normal-stock",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
    export: true,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "BAD_QTY",
    headerKey: "bad-stock",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "STOCK_TOTAL_QTY",
    headerKey: "total-inventory",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "OUT_ABLE_QTY",
    headerKey: "available-stock",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
    export: true,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 110,
  },
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 110,
  },
  {
    field: "EA_QTY",
    headerKey: "ea-quantity",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "BOX_QTY",
    headerKey: "box-quantity",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 140,
    export: true,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "PLT_QTY",
    headerKey: "plt-quantity",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "BAG_QTY",
    headerKey: "bag-quantity",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "CBM_QTY",
    headerKey: "cbm",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "STOCK_WEIGHT",
    headerKey: "weight",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
    cellClass: "stringType",
  },
  {
    field: "AS_QTY",
    headerKey: "as-stock",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 110,
    export: true,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  //적정재고
  {
    field: "PROP_QTY",
    headerKey: "appropriate-stock",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
    export: true,
    cellClass: "stringType",
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "REMARK",
    headerKey: "remark",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    cellClass: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true ? "editable-cell" : "renderer-cell";
    },
    editable: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true;
    },
    export: true,
  },
  {
    // 특송사
    field: "PROP_CHK",
    headerKey: "special-delivery",
    headerName: "",
    export: true,
    sortable: true,
    width: 160,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    //
    field: "UOM_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 160,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    //
    field: "RITEM_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 160,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    //
    field: "WH_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 160,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    //
    field: "CUST_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 160,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    cellStyle: { textAlign: "center" },
    width: 80,
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "RNUM")) {
        return params.node.rowIndex + 1;
      } else {
        return "";
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    // 작업일자
    field: "WORK_DT",
    headerKey: "work-date",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
    export: true,
    cellClass: "stringType",
  },
  {
    // 로케이션
    field: "LOC_NM",
    headerKey: "location",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 150,
    export: true,
    cellClass: "stringType",
  },
  {
    // 로케이션 타입
    field: "LOC_TYPE",
    headerKey: "location-type",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품
    field: "",
    headerKey: "product",
    headerClass: "",
    sortable: true,
    children: [
      {
        field: "RITEM_CD",
        headerKey: "code",
        headerName: "",
        headerClass: "",
        sortable: true,
        width: 150,
        export: true,
      },
      {
        field: "RITEM_NM",
        headerKey: "product-name",
        headerName: "",
        headerClass: "",
        sortable: true,
        width: 250,
        export: true,
      },
    ],
  },
  {
    // 재고수량
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 유효기간만료일
    field: "ITEM_BEST_DATE_END",
    headerKey: "validity-expiration-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // 입수
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "center" },
    width: 110,
  },
  {
    // UOM
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "center" },
    width: 100,
  },
  {
    // 출고예정 수량
    field: "OUT_EXP_QTY",
    headerKey: "outgoing-expected-quantity",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    width: 130,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // PLT수량
    field: "REAL_PLT_QTY",
    headerKey: "plt-quantity",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    width: 110,
    valueFormatter: Format.NumberCount,
    cellClass: "stringType",
  },
  {
    // CBM
    field: "CBM",
    headerKey: "cbm",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    width: 110,
    valueFormatter: Format.NumberCount,
    cellClass: "stringType",
  },
  {
    // B/L번호
    field: "BL_NO",
    headerKey: "BL-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 110,
    cellStyle: { textAlign: "center" },
  },
  {
    // 창고
    field: "WH_NM",
    headerKey: "warehouse",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "left" },
  },
  {
    // LOT번호
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // 제조일자
    field: "MAKE_DT",
    headerKey: "manufacturing-date-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
    cellClass: "stringType",
  },
  {
    // 무게
    field: "STOCK_WEIGHT",
    headerKey: "weight-2",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    width: 110,
    cellClass: "stringType",
  },
  {
    // UNIT_NO
    field: "UNIT_NO",
    headerKey: "unit-no",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "center" },
    width: 150,
    cellClass: "stringType",
  },
  {
    // LOCK_YN
    field: "LOCK_YN",
    headerKey: "lock-yn",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // ITEM_CLASS
    field: "ITEM_CLASS",
    headerKey: "item-class",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // STOCK_ID
    field: "STOCK_ID",
    headerKey: "stock-id",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // STOCK_RANK
    field: "STOCK_RANK",
    headerKey: "stock-rank",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // UOM_ID
    field: "UOM_ID",
    headerKey: "uom-id",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // WH_ID
    field: "WH_ID",
    headerKey: "warehouse-id",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // LOC_ID
    field: "LOC_ID",
    headerKey: "location-id",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // RITEM_ID
    field: "RITEM_ID",
    headerKey: "item-id",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // CUST_NM
    field: "CUST_NM",
    headerKey: "customer-name",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // CUST_CD
    field: "CUST_CD",
    headerKey: "customer-code",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // CUST_ID
    field: "CUST_ID",
    headerKey: "customer-id",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
];
