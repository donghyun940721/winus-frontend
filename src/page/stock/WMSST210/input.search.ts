/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST210/input.ts
 *  Description:    원부자재(부품/용기) 입출고이력(개체) 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

import type { IModal, ISearchInput, info } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSST210",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  warehouse: {
    page: "WMSMS040",
    id: "warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_WH_CD",
        searchContainerInputId: "S_WH_CD",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        searchContainerInputId: "S_WH_NM",
        title: "warehouse-name",
        type: "text",
        width: "half",
      },
    ],
  },
  "product-code": {
    page: "WMSPL010",
    id: "product-code",
    title: "search-logistics-container",
    gridTitle: "product-list",

    apis: {
      url: "/WMSPL010/list_rn.action",
      params: {
        func: "fn_setWMSCM160_T3",
        POOL_CODE: "",
        POOL_ID: "",
        POOL_NM: "",
        POOL_GRP_ID: "",
        POOL_SIZE: "",
        UNIT_PRICE: "",
        IN_WH_ID: "",
        IN_ZONE_ID: "",
        RITEM_ID: "",
        S_POOL_CODE: "",
        S_POOL_NM: "",
        S_POOL_GRP_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_POOL_CODE",
        searchContainerInputId: "POOL_CODE",
        title: "logistics-container-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_POOL_NM",
        title: "logistics-container-name",
        type: "text",
        width: "half",
      },
      {
        id: "S_POOL_GRP_ID",
        title: "logistics-container-group",
        type: "select",
        width: "half",
        optionsKey: "POOLGRP",
        options: [{ name: "all", nameKey: "all", value: "" }],
      },
    ],
  },
  location: {
    page: "WMSMS080",
    id: "location",
    title: "search-location",
    gridTitle: "location-list",
    apis: {
      url: "/WMSCM080/list_rn.action",
      params: {
        func: "fn_setWMSMS080",
        LOC_ID: "",
        LOC_CD: "",
        AVAILABLE_QTY: "",
        OUT_EXP_QTY: "",
        STOCK_ID: "",
        SUB_LOT_ID: "",
        vrViewOnlyLoc: "",
        STOCK_WEIGHT: "",
        vrViewSubLotId: "",
        vrRitemId: "",
        ITEM_BEST_DATE_END: "",
        UOM_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        vrViewStockQty: "",
        vrSrchLocCd: "",
        vrSrchLocId: "",
        vrSrchCustLotNo: "",
        vrSrchLocStat: "",
        S_WH_CD: "",
        vrWhId: "",
        S_WH_NM: "",
        vrSrchLocDel: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchLocCd",
        searchContainerInputId: "txtSrchLocCd",
        title: "location-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchCustLotNo",
        title: "LOT-number",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchLocStat",
        title: "location-status",
        type: "select",
        width: "half",
        options: [
          { nameKey: "all", name: "", value: "300" },
          { nameKey: "use-location", name: "", value: "200" },
          { nameKey: "unused-location", name: "", value: "100" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    //화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    //구분
    ids: ["vrSrchOrderGb"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "division",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchOrderGb",
    options: [{ name: "", nameKey: "all", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    //상품코드
    ids: ["S_POOL_CODE", "S_POOL_NM"],
    hiddenId: "S_POOL_ID",
    rowDataIds: ["POOL_CODE", "POOL_NM"],
    rowDataHiddenId: "ID",
    searchApiKeys: ["POOL"],
    placeholder: ["code", "name"],
    title: "product-code",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    //물류센터
    ids: ["vrSrchUserByLcId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "logistics-center",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "LCCODE",
    hidden: true,
  },
  {
    //로케이션
    ids: ["txtSrchLocCd"],
    hiddenId: "txtSrchLocId",
    rowDataIds: ["LOC_CD"],
    rowDataHiddenId: "LOC_ID",
    searchApiKeys: ["vrSrchLocCd"],
    srchKey: "LOCATION",
    title: "location",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text"],
  },
  {
    // 조회일자
    ids: ["vrSrchReqDtFrom", "vrSrchReqDtTo"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "inquiry-date",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    //EPC 코드
    ids: ["vrSrchEpcCd", "vrSrchSerial", "vrSrchHexGb"],
    hiddenId: "",
    rowDataIds: ["", "", ""],
    rowDataHiddenId: "",
    searchApiKeys: ["", ""],
    srchKey: "",
    title: "epc-code",
    width: "half",
    style: "left: -6px",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box", "text", "select-box2"],
    optionsKey: "POOL",
    optionsKey2: "vrSrchHexGb",
    options: [{ nameKey: "all", name: "", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0 },
    optionsAutoSelected2: { autoSelectedKeyIndex: 0 },
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: true,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
};
