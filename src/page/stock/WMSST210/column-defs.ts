/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST210/column-defs.ts
 *  Description:    원부자재(부품/용기) 입출고이력(개체) 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Format, Getter } from "@/lib/ag-grid";

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      cellStyle: { textAlign: "center" },
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      maxWidth: 50,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "product-code": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "POOL_GRP_NAME",
      headerKey: "pool-grp-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerKey: "pool-grp-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerKey: "pool-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_SIZE",
      headerKey: "pool-size",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "IN_WH_CD",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "IN_ZONE_NM",
      headerKey: "receiving-zone",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "IN_WH_ID",
      headerKey: "in-zone-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "IN_ZONE_ID",
      headerKey: "in-zone-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "POOL_ID",
      headerKey: "in-zone-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "RITEM_ID",
      headerKey: "in-zone-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
  ],
  location: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

        { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerKey: "stock-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerKey: "lot-number",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerKey: "expiration-date",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // editoptions : {$!comCode.getGridComboComCode("ORD07")}
    // 구분
    field: "ORD_TYPE",
    headerKey: "division",
    headerName: "",
    export: true,
    sortable: true,
    width: 70,
    cellStyle: { textAlign: "center" },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("vrSrchOrderGb", params.value);
    },
  },
  {
    // 화주명
    field: "CUST_NM",
    headerKey: "owner-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "left" },
  },
  {
    // EPC 코드
    field: "EPC_CD",
    headerKey: "epc-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 300,
    cellStyle: { textAlign: "left" },
  },
  {
    // SEQ16
    field: "SEQ16",
    headerKey: "seq16",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품코드
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "left" },
  },
  {
    // 생성일자
    field: "REG_DT",
    headerKey: "creation-date-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 170,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.Date,
  },
  {
    // 로케이션
    field: "LOC_CD",
    headerKey: "location",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "left" },
  },
  {
    // 물류센터
    field: "LC_NM",
    headerKey: "logistics-center",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  {
    // CUST_ID
    field: "CUST_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    hide: true,
  },
  {
    // PARAM_EPC_CD
    field: "PARAM_EPC_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    hide: true,
  },
  {
    // PARAM_EPC_SERIAL
    field: "PARAM_EPC_SERIAL",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    hide: true,
  },
];
