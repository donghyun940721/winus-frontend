/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST030_3/column-defs.ts
 *  Description:    재고/재고명의변경(신규)_변경이력 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  product: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  location: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerName: "", headerKey: "code" },
        { field: "RITEM_NM", columnGroupShow: "open", headerName: "", headerKey: "product-name" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerName: "",
      headerKey: "stock-quantity",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerName: "",
      headerKey: "lot-number",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerName: "",
      headerKey: "expiration-date",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.commonRowIndex,
    menuTabs: ["columnsMenuTab"],
    lockPosition: true,
    lockVisible: true,
    pinned: "left",
  },
  {
    field: "CHANGE_WORK_ID",
    headerName: t("grid-column-name.work-id"),
    width: 130,
    pinned: "left",
  },
  {
    field: "",
    headerName: t("grid-column-name.assignment"),
    headerClass: "header-center",
    sortable: true,
    marryChildren: true,
    children: [
      {
        field: "SELL_CUST_NM",
        columnGroupShow: "open",
        headerName: t("grid-column-name.owner-name"),
      },
      {
        field: "SELL_LOC_NM",
        columnGroupShow: "open",
        headerName: t("grid-column-name.location"),
      },
      {
        field: "SELL_RITEM_NM",
        columnGroupShow: "open",
        headerName: t("grid-column-name.product-name"),
      },
      {
        field: "SELL_QTY",
        columnGroupShow: "open",
        cellStyle: { textAlign: "right" },
        headerName: t("grid-column-name.qty"),
        width: 110,
        valueFormatter: Format.NumberCount,
      },
      {
        field: "SELL_UOM_NM",
        columnGroupShow: "open",
        headerName: t("grid-column-name.uom"),
        width: 100,
      },
    ],
  },
  {
    field: "",
    headerName: "→",
    width: 50,
    cellStyle: { textAlign: "center" },
    // TODO :: 표현 방식 변경 필요
    valueGetter: () => {
      return "→";
    },
  },
  {
    field: "",
    headerName: t("grid-column-name.amniotic-fluid"),
    headerClass: "header-center",
    sortable: true,
    marryChildren: true,
    children: [
      {
        field: "BUY_LOC_NM",
        columnGroupShow: "open",
        headerName: t("grid-column-name.location"),
        sortable: true,
      },
      {
        // export: true,
        field: "BUY_RITEM_NM",
        columnGroupShow: "open",
        headerName: t("grid-column-name.product-name"),
      },
      {
        // export: true,

        field: "BUY_QTY",
        columnGroupShow: "open",
        cellStyle: { textAlign: "right" },
        headerName: t("grid-column-name.qty"),
        width: 110,
        valueFormatter: Format.NumberCount,
      },
      {
        // export: true,
        field: "BUY_UOM_NM",
        columnGroupShow: "open",
        headerName: t("grid-column-name.uom"),
        width: 100,
      },
    ],
  },
  {
    field: "CHANGE_WORK_MEMO",
    headerName: t("grid-column-name.reason-of-change"),
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "WORK_STAT_EXCEL",
    headerName: t("grid-column-name.operation-status"),
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "WORK_DT",
    headerName: t("grid-column-name.work-day"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 130,
  },
  {
    field: "REG_NO",
    headerName: t("grid-column-name.registrar"),
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.reg-dt"),
    width: 170,
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    valueGetter: Getter.Date,
  },
];
