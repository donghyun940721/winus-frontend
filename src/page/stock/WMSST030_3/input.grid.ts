/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST030_3/input.ts
 *  Description:    재고/재고명의변경(신규)_변경이력 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { IControlBtn } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [1000, 2000, 3000],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
};
