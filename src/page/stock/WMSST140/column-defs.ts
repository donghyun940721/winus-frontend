/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:    WMSST140/column-defs.ts
 *  Description:    원부자재(부품/용기)재고조회 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "raw-material-(parts/containers)": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "POOL_GRP_NAME",
      headerKey: "pool-grp-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerKey: "pool-grp-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerKey: "pool-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "RNUM",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
    valueGetter: (params: any) => {
      return params.data.RNUM;
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 80,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // 창고명
    field: "WH_NM",
    headerKey: "warehouse-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 물류용기군
    field: "ITEM_GRP_NM",
    headerKey: "logistics-container-group",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 물류용기
    field: "UOM_NM",
    headerKey: "logistics-container",
    headerName: "",
    sortable: true,
    marryChildren: true,
    children: [
      //코드
      {
        field: "RITEM_CD",
        columnGroupShow: "open",
        headerKey: "code",
      },
      //용기명
      {
        field: "RITEM_NM",
        columnGroupShow: "open",
        headerKey: "container-name",
      },
    ],
  },
  {
    // 전일재고
    field: "BEFORE_STOCK_QTY",
    headerKey: "the-day-before-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 입고
    field: "IN_QTY",
    headerKey: "receiving",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 일반출고
    field: "OUT_QTY",
    headerKey: "shipping-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 이벤트출고
    field: "EVENT_QTY",
    headerKey: "event-qty",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 정상재고
    field: "STOCK_QTY",
    headerKey: "normal-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 불량재고
    field: "BAD_QTY",
    headerKey: "bad-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // AS재고
    field: "AS_QTY",
    headerKey: "as-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 적정재고
    field: "PROP_QTY",
    headerKey: "appropriate-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 개체재고
    field: "EPC_STOCK_QTY",
    headerKey: "individual-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 매핑재고
    field: "MAPPING_QTY",
    headerKey: "mapped-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 매핑가용재고
    field: "EMPTY_PLT_QTY",
    headerKey: "mappped-available-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 비고
    field: "REMARK",
    headerKey: "remark",
    headerName: "",
    export: true,
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "center" },
  },

  {
    // 코드
    field: "CUST_CD",
    headerKey: "customer-code",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 화주명
    field: "CUST_NM",
    headerKey: "customer-name",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "center" },
    hide: true,
  },

  {
    // PROP_CHK
    field: "PROP_CHK",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // UOM_ID
    field: "UOM_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // RITEM_ID
    field: "RITEM_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // WH_ID
    field: "WH_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // CUST_ID
    field: "CUST_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "RNUM",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    // 작업일자
    field: "WORK_DT",
    headerKey: "work-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
  },
  {
    // 로케이션
    field: "LOC_NM",
    headerKey: "location",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품
    field: "",
    headerKey: "the-product",
    headerName: "",
    sortable: true,
    marryChildren: true,
    children: [
      {
        field: "RITEM_CD",
        columnGroupShow: "open",
        headerKey: "code",
      },
      {
        field: "RITEM_NM",
        columnGroupShow: "open",
        headerKey: "container-name",
      },
    ],
  },
  {
    // 재고수량
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
    formatoptions: { thousandsSeparator: ",", decimalPlaces: 0 },
  },
  {
    // UOM
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    // PLT수량
    field: "REAL_PLT_QTY",
    headerKey: "plt-quantity",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    // B/L번호
    field: "BL_NO",
    headerKey: "BL-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
  },
  {
    // 창고
    field: "WH_NM",
    headerKey: "warehouse",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "center" },
  },
  {
    // LOT번호
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 제품등급
    field: "ITEM_CLASS",
    headerKey: "product-grade",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    // 제조일자
    field: "MAKE_DT",
    headerKey: "manufacturing-date-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 재고 ID
    field: "STOCK_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    hide: true,
  },
];
