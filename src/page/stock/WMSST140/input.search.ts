/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST140/input.search.ts
 *  Description:    원부자재(부품/용기)재고조회 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

import type { IModal, ISearchInput, info } from "@/types";
import { IControlBtn } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSST140",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  warehouse: {
    page: "WMSMS040",
    id: "warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_WH_CD",
        searchContainerInputId: "S_WH_CD",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        searchContainerInputId: "S_WH_NM",
        title: "warehouse-name",
        type: "text",
        width: "half",
      },
    ],
  },
  "raw-material-(parts/containers)": {
    page: "WMSCM162",
    id: "logistics-container",
    title: "search-logistics-container",
    gridTitle: "logistics-container-list",

    defaultParamsData: {
      //해당 모달의 title
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    apis: {
      url: "/WMSCM162/list_rn.action",
      params: {
        func: "fn_setWMSCM162",
        vrSrchCustId: "",
        vrSrchPoolCd: "",
        vrSrchPoolNm: "",
        vrSrchPoolGrp: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchPoolCd",
        title: "logistics-container-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchPoolNm",
        title: "logistics-container-name",
        type: "text",
        width: "half",
      },
    ],
  },
};
export const SEARCH_INPUT: ISearchInput[] = [
  {
    //화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    //원부자재(부품/용기) 군
    ids: ["vrSrchPoolCd", "vrSrchPoolNm"],
    hiddenId: "",
    rowDataIds: ["POOL_CODE", "POOL_NM"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchPoolCd", "vrSrchPoolNm"],
    srchKey: "POOL",
    title: "raw-material-(parts/containers)",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 창고
    ids: ["vrSrchWhCd", "vrSrchWhNm"],
    hiddenId: "vrSrchWhId",
    rowDataIds: ["WH_CD", "WH_NM"],
    rowDataHiddenId: "WH_ID",
    searchApiKeys: ["vrSrchWhCd", "vrSrchWhNm"],
    srchKey: "WH",
    title: "warehouse",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 원부자재(부품/용기)군
    ids: ["vrSrchPoolGrpId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "raw-material-(parts/containers)-group",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    optionsKey: "POOLGRP",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    options: [{ name: "", nameKey: "all", value: "" }],
    types: ["select-box"],
  },
];

export const SEARCH_COMPONENT_CONTROL_BTN: IControlBtn[] = [];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: false,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
};
