import type { info } from "@/types/index";
import { LEFT_DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS, RIGHT_DETAIL_GRID_COLUMN_DEFS } from "./column-defs";
import { DetailGridMetaData, MasterGridMetaData, gridOptionsMeta } from "./input.grid";
import { SEARCH_CONTAINER_META } from "./input.search";

export * from "./input.grid";
export * from "./input.search";

export const commonSetting = {
  authPageGroup: "WMSST",
  authPageId: "WMSST070",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...MasterGridMetaData,
  ...DetailGridMetaData,
  ...commonSetting,
};

export const MASTER_GRID_OPTIONS_META = {
  ...gridOptionsMeta,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
};

export const LEFT_DETAIL_GRID_OPTIONS_META = {
  ...gridOptionsMeta,
  columnDefs: LEFT_DETAIL_GRID_COLUMN_DEFS,
};

export const RIGHT_DETAIL_GRID_OPTIONS_META = {
  ...gridOptionsMeta,
  columnDefs: RIGHT_DETAIL_GRID_COLUMN_DEFS,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};
export const INFO: info = {
  autoModal: false,
  autoModalPage: "",
  pk: "",
};
