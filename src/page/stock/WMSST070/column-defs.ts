/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST070/column-defs.ts
 *  Description:    재고관리/임가공 컬럼 정의 스크립트
 *  Authors:        L.H.PARK
 *  Update History:
 *                  2024.07. : Created by L.H.PARK
 *
------------------------------------------------------------------------------*/
import { Getter } from "@/lib/ag-grid";
import { MODAL_COLUMN_DEFS } from "@/page/common-page/meta/common-modal-column-defs";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
export { MODAL_COLUMN_DEFS };
export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "CHK_BOX",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: false,
    checkboxSelection: true,
    width: 35,
    hide: false,
  },
  {
    field: "ST_GUBUN",
    headerKey: "st_gubun",
    headerName: "ST_GUBUN",
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    sortable: true,
    width: 120,
    hide: true,
  },
  {
    field: "ST_GUBUN_IMG",
    headerKey: "st_gubun_img",
    headerName: "ST_GUBUN_IMG",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 40,
    hide: true,
  },
  {
    field: "FLAG",
    headerKey: "flag",
    headerName: "FLAG",
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    sortable: true,
    width: 100,
    hide: true,
  },
  // 작업구분
  {
    field: "WORK_TYPE",
    headerKey: "work-type",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    cellRenderer: (params: any) => {
      //ORD05
      return Getter.convetCodeToNameByOptionData("ORD05", params.value);
    },
    sortable: true,
    // export: true,
    width: 100,
  },
  // 작업일자
  {
    field: "WORK_DT",
    headerKey: "work-date",
    headerName: "$!lang.getText('작업일자')",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 120,
  },
  // 작업시간
  {
    field: "WORK_TM",
    headerKey: "work-time",
    headerName: "$!lang.getText('작업시간')",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 120,
  },
  // 작업
  {
    field: "WORK_STAT_NM",
    headerKey: "work",
    headerName: "$!lang.getText('작업')",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    cellRenderer: (params: any) => {
      //ORD05
      return Getter.convetCodeToNameByOptionData("COMPLETE", params.value);
    },
    width: 50,
  },
  // LOT번호
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-no",
    headerName: "$!lang.getText('LOT번호')",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 140,
  },
  // 완성품 그룹
  {
    headerKey: "finish-product", // 그룹 헤더 이름
    headerClass: "header-center",
    children: [
      // 코드
      {
        field: "RITEM_CD",
        headerKey: "item-code",
        headerName: "$!lang.getText('코드')",
        headerClass: "header-center",
        cellStyle: { textAlign: "left" },
        sortable: true,
        width: 120,
      },
      // 상품명
      {
        field: "RITEM_NM",
        headerKey: "item-name",
        headerName: "$!lang.getText('상품명')",
        headerClass: "header-center",
        cellStyle: { textAlign: "left" },
        sortable: true,
        width: 180,
      },
    ],
  },
  // 수량
  {
    field: "WORK_QTY",
    headerKey: "work-qty",
    headerName: "$!lang.getText('수량')",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 70,
  },
  // 입수
  {
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "$!lang.getText('입수')",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 40,
  },
  //  UOM
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "UOM",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 40,
  },
  {
    field: "ORD_DESC",
    headerKey: "remark",
    headerName: "$!lang.getText('비고')",
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    sortable: true,
    width: 200,
  },
  // 창고 그룹
  {
    headerKey: "warehouse", // 그룹 헤더 이름
    headerClass: "header-center",
    children: [
      {
        field: "WH_CD",
        headerKey: "warehouse-code",
        headerName: "$!lang.getText('코드')",
        headerClass: "header-center",
        cellStyle: { textAlign: "left" },
        sortable: true,
        width: 120,
      },
      {
        field: "WH_NM",
        headerKey: "warehouse-name",
        headerName: "$!lang.getText('창고명')",
        headerClass: "header-center",
        cellStyle: { textAlign: "left" },
        sortable: true,
        width: 120,
      },
    ],
  },
  {
    field: "LOC_CD",
    headerKey: "location-code",
    headerName: "$!lang.getText('로케이션')",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 140,
  },
  // 화주 그룹
  {
    headerKey: "owner", // 그룹 헤더 이름
    headerClass: "header-center",
    children: [
      {
        field: "CUST_CD",
        headerKey: "customer-code",
        headerName: "$!lang.getText('코드')",
        headerClass: "header-center",
        cellStyle: { textAlign: "left" },
        sortable: true,
        width: 120,
      },
      {
        field: "CUST_NM",
        headerKey: "customer-name",
        headerName: "$!lang.getText('화주명')",
        headerClass: "header-center",
        cellStyle: { textAlign: "left" },
        sortable: true,
        width: 120,
      },
    ],
  },
  {
    field: "WORK_ID",
    headerKey: "work-id",
    headerName: "WORK_ID",
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    field: "RITEM_ID",
    headerKey: "item-id",
    headerName: "RITEM_ID",
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    field: "CUST_ID",
    headerKey: "customer-id",
    headerName: "CUST_ID",
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    field: "UOM_ID",
    headerKey: "uom-id",
    headerName: "UOM_ID",
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    hide: true,
  },
];

export const LEFT_DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  // 체크박스
  {
    field: "CHK_BOX",
    headerKey: "chk-box",
    headerName: "CHK_BOX",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 35,
    hide: true, // hidden 처리
  },
  // ST_GUBUN 필드
  {
    field: "ST_GUBUN",
    headerKey: "st-gubun",
    headerName: "ST_GUBUN",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    hide: true, // hidden 처리
  },
  // ST_GUBUN_IMG 필드
  {
    field: "ST_GUBUN_IMG",
    headerKey: "st-gubun-img",
    headerName: "ST_GUBUN_IMG",
    width: 20,
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    hide: true, // hidden 처리
  },
  // FLAG 필드
  {
    field: "FLAG",
    headerKey: "flag",
    headerName: "FLAG",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    hide: true, // hidden 처리
  },
  // 완성품 그룹
  {
    headerKey: "part", // 그룹 헤더
    headerClass: "header-center",
    children: [
      {
        field: "RITEM_CD",
        headerKey: "product-code",
        headerName: "$!lang.getText('코드')",
        cellStyle: { textAlign: "left" },
        headerClass: "header-center",
        sortable: true,
        width: 140,
      },
      {
        field: "RITEM_NM",
        headerKey: "product-name",
        headerName: "$!lang.getText('상품명')",
        cellStyle: { textAlign: "left" },
        headerClass: "header-center",
        sortable: true,
        width: 180,
      },
    ],
  },
  // 수량 필드
  {
    field: "WORK_QTY",
    headerKey: "work-qty",
    headerName: "$!lang.getText('수량')",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 80,
    valueFormatter: (params) => new Intl.NumberFormat().format(params.value), // 포맷 설정
  },
  // 입수 필드
  {
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "$!lang.getText('입수')",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 80,
  },
  // UOM 필드
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "UOM",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 80,
  },
  // 로케이션 필드
  {
    field: "LOC_CD",
    headerKey: "location-code",
    headerName: "$!lang.getText('로케이션')",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 160,
  },
  // 창고 그룹
  {
    headerKey: "warehouse", // 그룹 헤더 이름
    headerClass: "header-center",
    children: [
      {
        field: "WH_CD",
        headerKey: "warehouse-code",
        headerName: "$!lang.getText('코드')",
        headerClass: "header-center",
        cellStyle: { textAlign: "left" },
        sortable: true,
        width: 120,
      },
      {
        field: "WH_NM",
        headerKey: "warehouse-name",
        headerName: "$!lang.getText('창고명')",
        headerClass: "header-center",
        cellStyle: { textAlign: "left" },
        sortable: true,
        width: 120,
      },
    ],
  },
];

export const RIGHT_DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  // 체크박스
  {
    field: "CHK_BOX",
    headerKey: "chk-box",
    headerName: "CHK_BOX",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 35,
    hide: true, // hidden 처리
  },
  // ST_GUBUN 필드
  {
    field: "ST_GUBUN",
    headerKey: "st-gubun",
    headerName: "ST_GUBUN",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    hide: true, // hidden 처리
  },
  // ST_GUBUN_IMG 필드
  {
    field: "ST_GUBUN_IMG",
    headerKey: "st-gubun-img",
    headerName: "ST_GUBUN_IMG",
    width: 20,
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    hide: true, // hidden 처리
  },
  // FLAG 필드
  {
    field: "FLAG",
    headerKey: "flag",
    headerName: "FLAG",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    hide: true, // hidden 처리
  },
  // 작업일자 필드
  {
    field: "OUT_DT",
    headerKey: "work-date",
    headerName: "$!lang.getText('작업일자')",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  // 상품 그룹
  {
    headerKey: "Product", // 그룹 헤더
    headerClass: "header-center",
    children: [
      // 코드 필드
      {
        field: "RITEM_CD",
        headerKey: "item-code",
        headerName: "$!lang.getText('코드')",
        cellStyle: { textAlign: "left" },
        headerClass: "header-center",
        sortable: true,
        width: 140,
      },
      // 명 필드
      {
        field: "RITEM_NM",
        headerKey: "item-name",
        headerName: "$!lang.getText('명')",
        cellStyle: { textAlign: "left" },
        headerClass: "header-center",
        sortable: true,
        width: 180,
      },
    ],
  },
  // 작업수량 필드
  {
    field: "OUT_ORD_QTY",
    headerKey: "work-qty",
    headerName: "$!lang.getText('작업수량')",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 80,
    valueFormatter: (params) => new Intl.NumberFormat().format(params.value), // 숫자 포맷 설정
  },
];
