/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST070/column-defs.ts
 *  Description:    재고관리/임가공 컬럼 정의 스크립트
 *  Authors:        L.H.PARK
 *  Update History:
 *                  2023.08. : Created by L.H.PARK
 *
------------------------------------------------------------------------------*/
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    saveSession: true,
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "", nameKey: "all", value: "12" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",

    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "Y",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchRitemCd",
        title: "product-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchRitemNm",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        options: [
          { name: "all", nameKey: "all", value: "" },
          { name: "useProductY", nameKey: "useProductY", value: "Y" },
          { name: "useProductN", nameKey: "useProductN", value: "N" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
  warehouse: {
    page: "WMSMS040",
    id: "warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_WH_CD",
        searchContainerInputId: "S_WH_CD",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        searchContainerInputId: "S_WH_NM",
        title: "warehouse-name",
        type: "text",
        width: "half",
      },
    ],
  },
  uom: {
    page: "WMSCM101",
    id: "uom",
    title: "search-uom",
    gridTitle: "uom-list",
    differentFunc: {
      title: "select",
      url: "",
    },
    apis: {
      url: "/WMSCM101/list_rn.action",
      params: {
        func: "fn_setWMSCM101",
        vrUom1Cd: "",
        vrStockQty: "",
        vrRitemId: "",
        UOM_ID: "",
        UOM_CD: "",
        UOM1_CODE: "",
        UOM2_CODE: "",
        CONV_QTY: "",
        vrSrchUomId: "",
        vrSrchUomNm: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchUomId",
        title: "uom-code",
        searchContainerInputId: "UOM_CD",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchUomNm",
        title: "uom-name",
        type: "text",
        width: "half",
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchReqDtFrom", "vrSrchReqDtTo"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "일자",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    ids: ["vrSrchItemCd", "vrSrchItemNm"],
    hiddenId: "vrSrchItemId",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "RITEM_ID",
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    srchKey: "ITEM",
    title: "product",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchWorkType"],
    hiddenId: "",
    rowDataIds: ["WORK_TYPE"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchWorkType"],
    title: "work-type",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    optionsKey: "ORD05",
    types: ["select-box"],
    options: [{ name: "", nameKey: "select", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 창고
    ids: ["vrSrchWhCd", "vrSrchWhNm"],
    hiddenId: "vrSrchWhId",
    rowDataIds: ["WH_CD", "WH_NM"],
    rowDataHiddenId: "WH_ID",
    searchApiKeys: ["vrSrchWhCd", "vrSrchWhNm"],
    srchKey: "WH",
    title: "warehouse",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchUomId", "vrSrchUomNm"],
    hiddenId: "",
    rowDataIds: ["UOM_ID", "UOM_NM"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchUomId", "vrSrchUomNm"],
    srchKey: "UOM",
    title: "uom",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchWorkStat"],
    hiddenId: "",
    rowDataIds: ["WORK_STAT"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchWorkStat"],
    title: "work-status",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      { value: "", nameKey: "select", name: "선택" },
      { value: "FF", nameKey: "FF", name: "완료" },
      { value: "UF", nameKey: "UF", name: "미완료" },
    ],
  },
];
// defaultParams확인 필요
export const MASTER_SEARCH_INPUT: ISearchInput[] = [];

// defaultParams확인 필요
export const DETAIL_SEARCH_INPUT: ISearchInput[] = [];

// export const MODAL_COLUMN_DEFS: any = {
// export const MODAL_COLUMN_DEFS: IHasKeyColDef = {};

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useMore: false,
  useSetting: false, // TODO :: 위치 조정 이 후, 재설정 (TRUE)
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  masterSearchInput: MASTER_SEARCH_INPUT,
  detailSearchInput: DETAIL_SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  searchConditionInitUrl: "",
};
