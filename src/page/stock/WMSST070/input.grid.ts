/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST070/column-defs.ts
 *  Description:    재고관리/임가공 컬럼 정의 스크립트
 *  Authors:        L.H.PARK
 *  Update History:
 *                  2024.07. : Created by L.H.PARK
 *
------------------------------------------------------------------------------*/
import { UtilService } from "@/services/util-service";
import { IControlBtn, IGridCellSearchButton, IGridCellSelectBox } from "@/types";
import { GridOptions } from "ag-grid-community";
import { MASTER_GRID_COLUMN_DEFS } from "./column-defs";

// const { t } = i18n.global;

export const GRID_SELECT_BOX_INFO: IGridCellSelectBox = {};
// export const MASTER_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [];
// export const DETAIL_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [];
export const MASTER_CONTROL_BTN: IControlBtn[] = [
  {
    title: "print-total-picking-list-new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "print-total-picking-list",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "print-work-list",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
];

export const DETAIL_CONTROL_BTN: IControlBtn[] = [];

export const MasterGridMetaData: any = {
  // 그리드 헤더
  // gridHeaderName: t("menu-header-title.WMSMS081_master"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [100, 200, 300],
};

export const DetailGridMetaData: any = {
  // 그리드 헤더
  // gridHeaderName: t("menu-header-title.WMSMS081_detail"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "",

  //페이징옵션
  pagingSizeList: [100, 200, 300],
};

//grid > context 쪽에 ...
export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  CUST: {
    fieldList: ["CUST_ID", "CUST_NM", "CUST_CD"],
    rowDataKeys: ["CUST_ID", "CUST_NM", "CUST_CD"],
    modalData: {
      page: "WMSST070",
      id: "cust",
      title: "search-owner",
      gridTitle: "owner-list",
      isCellRenderer: true,
      apis: {
        url: "/WMSCM011/list_rn.action",
        params: {
          S_CUST_CD: "",
          S_CUST_NM: "",
          S_CUST_ID: "",
          S_CUST_TYPE: "",
          S_LC_ALL: "LC_ALL",
          S_LC_ID: UtilService.getCurrentLocation(),
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "S_CUST_TYPE",
          title: "customer-type",
          type: "select",
          width: "triple",
          optionsReadOnly: true,
        },
        {
          id: "S_CUST_CD",
          title: "shipper-code",
          searchContainerInputId: "vrSrchCustCd",
          type: "text",
          width: "triple",
        },
        {
          id: "S_CUST_NM",
          title: "owner-name",
          type: "text",
          width: "triple",
        },
      ],
    },
    colDef: [
      {
        field: "No",
        headerKey: "no",
        headerName: "",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerKey: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "CUST_CD",
        headerKey: "shipper-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "CUST_NM",
        headerKey: "owner-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ADDR",
        headerKey: "address",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "EMP_NM",
        headerKey: "manager-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "CUST_EPC_CD",
        headerKey: "owner-epc-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "TEL",
        headerKey: "tel",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
      },
    ],
  },
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  mainColumnDefs: MASTER_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: false,
  // getRowId: (data) => {
  //   return data.data.RNUM;
  // },
  getRowStyle: (params) => {
    // Check if the 'value' field is greater than a specific number (e.g., 100)
    if (params.data["D_QTY"] > params.data["D_STOCK_QTY"]) {
      return { backgroundColor: "#FFDEDE" };
    } else if (params.data["D_STOCK_QTY"] < 50) {
      return { backgroundColor: "#FAFAD2" };
    }
  },
  context: { ...GRID_CELL_SEARCH_BUTTON },
};
