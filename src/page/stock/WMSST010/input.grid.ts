/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST010/input.grid.ts
 *  Description:    재고관리/현재고관리 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { IControlBtn, IGridCellDateInput, IGridCellSelectBox } from "@/types";
import { GridOptions } from "ag-grid-community";
import { MASTER_GRID_COLUMN_DEFS } from "./column-defs";

export const MAIN_GRID_PINNED_BOTTOM_INFO: any = {
  BEFORE_STOCK_QTY: 0,
  IN_QTY: 0,
  OUT_QTY: 0,
  RETURN_QTY: 0,
  STOCK_QTY: 0,
  BAD_QTY: 0,
  STOCK_TOTAL_QTY: 0,
  OUT_ABLE_QTY: 0,
  STOCK_WEIGHT: 0,
  EA_QTY: 0,
  BOX_QTY: 0,
  PLT_QTY: 0,
  BAG_QTY: 0,
  CBM_QTY: 0,
  TOTAL_PRICE: 0,
  ISO_QTY: 0,
  B_QTY: 0,
  CS_QTY: 0,
  AS_QTY: 0,
  OUT_EXP_QTY: 0,
  PICKING_QTY: 0,
  CHANGE_RDY_QTY: 0,
  PROP_QTY: 0,
};

export const GRID_DATE_INPUT_INFO: IGridCellDateInput = {
  gridPk: "RNUM",
};

export const DETAIL_CELL_RENDERER_INFO: IGridCellSelectBox = {
  LOCK_YN: {
    pk: "RNUM",
    optionsKey: "vrSrchLockYn",
  },
};

export const MASTER_CONTROL_BTN: IControlBtn[] = [
  {
    title: "in/out-order-regeneration",
    colorStyle: "danger",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const DETAIL_CONTROL_BTN: IControlBtn[] = [
  {
    // 전체엑셀
    title: "all-excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    authType: "EXC_AUTH",
    disabled: "",
  },
  {
    // 출력
    title: "printing",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "",
    authType: "EXC_AUTH",
  },
  {
    // 저장
    title: "detail-save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    // 필터
    title: "filter",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "",
    authType: "EXC_AUTH",
  },
  {
    // 엑셀
    title: "detail-excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [100, 200, 300, 500, 1000],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  allowContextMenuWithControlKey: true,
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
};
