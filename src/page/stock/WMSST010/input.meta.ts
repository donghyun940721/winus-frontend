/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST010/input.meta.ts
 *  Description:    재고관리/현재고관리 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import type { info } from "@/types/index";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";
import { gridMetaData, gridOptionsMeta } from "./input.grid";
import { FILTER_SEARCH_INPUT, SEARCH_CONTAINER_META, SEARCH_INPUT } from "./input.search";

export * from "./input.grid";
export * from "./input.search";

export const commonSetting = {
  authPageGroup: "WMSST",
  authPageId: "WMSST010",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
  searchInput: SEARCH_INPUT,
};

export const SEARCH_META_DETAIL = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
  searchInput: FILTER_SEARCH_INPUT,
  unUsedRefreshButton: true,
  uniqueKey: "detail",
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const MASTER_GRID_OPTIONS_META = {
  ...gridOptionsMeta,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
};

export const DETAIL_GRID_OPTIONS_META = {
  ...gridOptionsMeta,
  columnDefs: DETAIL_GRID_COLUMN_DEFS,
};

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSST010",
  pk: "",
};
