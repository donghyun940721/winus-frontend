/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST065_1/input.ts
 *  Description:    일별재고(신규)-일별재고내역 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { IControlBtn } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    authType: "EXC_AUTH",
    image: "excel",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [200, 300, 400],
  gridHeaderName: t("menu-header-title.WMSST065_1"),
};

export const gridOptionsMeta: GridOptions = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  // getRowId: (data) => {
  //   return data.data.RNUM;
  // },
};
