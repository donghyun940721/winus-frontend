/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST065_1/column-defs.ts
 *  Description:    일별재고(신규)-일별재고내역 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  product: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node && params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "logistics-container": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "POOL_GRP_NAME",
      headerKey: "pool-grp-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerKey: "pool-cd",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerKey: "pool-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        params.data.RNUM = params.node.rowIndex + 1;
        return params.data.RNUM;
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // 작업일자
    field: "SUBUL_DT",
    headerKey: "work-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    // 화주
    field: "CUST_NM",
    headerKey: "Owner",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품코드
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품명
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 300,
    cellStyle: { textAlign: "center" },
  },
  {
    // 입고
    field: "IN_QTY",
    headerKey: "receiving",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    aggFunc: "sum",
  },
  {
    // 출고
    field: "OUT_QTY",
    headerKey: "shipping",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    aggFunc: "sum",
  },
  {
    // 이벤트출고
    field: "EVENT_QTY",
    headerKey: "event-qty",
    headerName: "",
    export: false,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 재고량
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    aggFunc: "sum",
  },
  {
    // 입수
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "",
    export: true,
    sortable: true,
    width: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    // LOC_TYPE
    field: "LOC_TYPE",
    headerKey: "location-type",
    headerName: "",
    export: false,
    sortable: true,
    width: 60,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // UOM
    field: "UOM_NM",
    headerKey: "uom-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
  },
  {
    // 현재고
    field: "GOOD_QTY",
    headerKey: "current-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    aggFunc: "sum",
  },
  {
    // 현불량
    field: "BAD_QTY",
    headerKey: "bad-qty",
    headerName: "",
    export: false,
    sortable: true,
    width: 60,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // AS재고
    field: "AS_QTY",
    headerKey: "as-stock",
    headerName: "",
    export: false,
    sortable: true,
    width: 60,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 금액
    field: "UNIT_PRICE",
    headerKey: "amount",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    aggFunc: "sum",
  },
  {
    // 중량
    field: "STOCK_WEIGHT",
    headerKey: "weight",
    headerName: "",
    export: true,
    sortable: true,
    width: 60,
    cellStyle: { textAlign: "center" },
  },
  {
    // 입출고수량
    field: "INOUT_QTY",
    headerKey: "receiving-and-shipping-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    aggFunc: "sum",
  },
  {
    // CBM
    field: "CBM",
    headerKey: "cbm",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
  },
  {
    // RITEM_ID
    field: "RITEM_ID",
    headerKey: "",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // CUST_ID
    field: "CUST_ID",
    headerKey: "customer-id",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // CUST_CD
    field: "CUST_CD",
    headerKey: "customer-code",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // UOM_CD
    field: "UOM_CD",
    headerKey: "uom-code",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // YES_STOCK_QTY
    field: "YES_STOCK_QTY",
    headerKey: "yesterday-stock-qty",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // SURVEY_QTY
    field: "SURVEY_QTY",
    headerKey: "survey-quantity",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // LC_ID
    field: "LC_ID",
    headerKey: "location-id",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // SUBUL_GB
    field: "SUBUL_GB",
    headerKey: "subul-gb",
    headerName: "",
    export: false,
    sortable: false,
    width: 0,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
];
