/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST040/column-defs.ts
 *  Description:    재고관리 - 재고이동
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.09.27 : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridSearchButton from "@/components/renderer/grid-search-button.vue";
import GridSelectBox from "@/components/renderer/grid-select-box.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { Format } from "@/lib/ag-grid/index";
import { UtilService } from "@/services/util-service";
import { ICellRendererSearchTextInputParams } from "@/types";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    headerKey: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "NOW_USE_YN",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    headerClass: "header-center",
  },
  {
    field: "WORK_STAT",
    headerKey: "status",
    headerName: "",
    sortable: true,
    valueGetter: (params: any) => {
      return UtilService.convertDataValue("WMSST040", params.data.WORK_STAT);
    },
    headerClass: "header-center",
    width: 100,
  },
  {
    field: "WORK_ID",
    headerKey: "work-number",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 130,
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    sortable: true,
    headerClass: "header-center",
    width: 130,
  },
  {
    field: "MOVE_DT",
    headerKey: "work-date",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 110,
  },
  {
    field: "OUT_WH_NM",
    headerKey: "shipping-warehouse",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    width: 350,
  },
  {
    field: "IN_WH_NM",
    headerKey: "receiving-warehouse",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    width: 350,
  },
  {
    field: "WORK_CD",
    headerKey: "",
    hide: true,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    pinned: "left",
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    maxWidth: 50,
    pinned: "left",
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
  },
  {
    field: "WORK_STAT",
    headerKey: "status",
    width: 100,
    valueGetter: (params: any) => {
      return UtilService.convertDataValue("WMSST040", params.data.WORK_STAT);
    },
  },
  {
    field: "ITEM_WORK_TM",
    headerKey: "work-time",
    cellStyle: { textAlign: "center" },
    width: 110,
  },
  {
    field: "",
    headerKey: "location",
    headerClass: "header-center",
    children: [
      { field: "FROM_LOC_CD", columnGroupShow: "open", headerKey: "from" },
      {
        field: "",
        columnGroupShow: "open",
        headerKey: "blank",
        width: 40,
        cellStyle: { textAlign: "center" },
        // TODO :: Cell Renderer로 변경해야?..
        valueGetter: () => {
          return "→";
        },
      },
      { field: "TO_LOC_CD", columnGroupShow: "open", headerKey: "to" },
    ],
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
  },
  {
    field: "ITEM_NM",
    headerKey: "product-name",
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    width: 130,
  },
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
  },
  {
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    cellStyle: { textAlign: "right" },
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "MOVE_QTY",
    headerKey: "moving-quantity",
    width: 110,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    width: 110,
  },
  {
    field: "UOM_NM",
    headerKey: "uom",
    width: 100,
  },
  {
    field: "ITEM_WGT",
    headerKey: "product-weight",
    width: 100,
  },
  {
    field: "BL_NO",
    headerName: "BL NO",
  },
  {
    field: "REASON_CD",
    headerKey: "reason-(type)",
    valueGetter: (params: any) => {
      return UtilService.convertDataValue("WMSST040", params.data.REASON_CD);
    },
  },
  {
    field: "WORK_DESC",
    headerKey: "reason-(detail)",
  },
  {
    field: "STOCK_MOVE_ID",
    hide: true,
  },
  {
    field: "WORK_SEQ",
    hide: true,
  },
];

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "NOW_USE_YN",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "owner-epc-code",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "tel",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  location: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

        { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerKey: "stock-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerKey: "lot-number",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerKey: "expiration-date",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "from-location": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

        { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerKey: "stock-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerKey: "lot-number",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerKey: "expiration-date",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "to-location": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

        { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerKey: "stock-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerKey: "lot-number",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerKey: "expiration-date",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  product: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "logistics-container": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "POOL_GRP_NAME",
      headerKey: "pool-grp-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerKey: "pool-grp-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerKey: "pool-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const GRID_COL_DEF_WMSST040T2: any = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerClass: "header-center",
    sortable: true,
    flex: 2,
  },
  {
    field: "ORD_QTY",
    headerKey: "ord-qty",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    valueFormatter: Format.NumberCount,
    flex: 1,
  },
  {
    field: "",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    width: 40,
    cellStyle: { textAlign: "center" },
    // TODO :: Cell Renderer로 변경해야?..
    valueGetter: () => {
      return "→";
    },
  },
  {
    field: "MOVE_QTY",
    headerKey: "moving-quantity",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    valueFormatter: Format.NumberCount,
    flex: 1,
  },
];

export const GRID_COL_DEF_WMSST040T1: any = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    pinned: "left",
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    menuTabs: ["columnsMenuTab"],
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "WORK_STAT",
    headerKey: "status",
    headerClass: "header-center",
    width: 100,
    valueGetter: (params: any) => {
      return UtilService.convertDataValue("WMSST040", params.data.WORK_STAT);
    },
  },
  {
    field: "STOCK_MOVE_ID",
    headerKey: "work-number",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 130,
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ITEM_WORK_DT",
    headerKey: "work-date",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 110,
  },
  {
    field: "ITEM_WORK_TM",
    headerKey: "work-time",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 110,
  },
  {
    field: "REG_NM",
    headerKey: "worker",
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  {
    field: "",
    headerKey: "location",
    headerClass: "header-center",
    marryChildren: true,
    children: [
      {
        field: "FROM_LOC_CD",
        headerKey: "from",
        headerClass: "header-center",
        sortable: true,
        lockVisible: true,
      },
      {
        field: "",
        headerKey: "",
        headerClass: "header-center",
        sortable: true,
        width: 40,
        // TODO :: Cell Renderer로 변경해야?..
        valueGetter: () => {
          return "→";
        },
        lockVisible: true,
      },
      {
        field: "TO_LOC_CD",
        headerKey: "to",
        headerClass: "header-center",
        sortable: true,
        lockVisible: true,
      },
    ],
    sortable: true,
  },
  {
    field: "ITEM_NM",
    headerKey: "product-name",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-barcode",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 110,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "MOVE_QTY",
    headerKey: "moving-quantity",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 110,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerClass: "header-center",
    sortable: true,
    width: 100,
  },
  {
    field: "REASON_CD",
    headerKey: "reason-(type)",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "WORK_DESC",
    headerKey: "reason-(detail)",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "POOL_YN",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
];

export const GRID_COL_DEF_WMSST040E3: any = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    pinned: "left",
    cellStyle: { textAlign: "center" },
  },
  {
    field: "",
    maxWidth: 50,
    pinned: "left",
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
  },
  {
    field: "WORK_STAT",
    headerKey: "status",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    field: "WORK_STAT_CVT",
    headerKey: "status",
    headerClass: "header-center",
    width: 100,
    valueGetter: (params: any) => {
      return UtilService.convertDataValue("WMSST040", params.data.WORK_STAT);
    },
    sortable: true,
  },
  {
    field: "ITEM_WORK_TM",
    headerKey: "work-time",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 100,
  },
  {
    field: "",
    headerKey: "location",
    headerClass: "header-center",
    children: [
      {
        field: "FROM_LOC_CD",
        headerKey: "from",
        headerClass: "header-center",
        // width: 200,
        sortable: true,
      },
      {
        field: "",
        headerKey: "",
        headerClass: "header-center",
        sortable: true,
        width: 40,
        cellStyle: { textAlign: "center" },
        // TODO :: Cell Renderer로 변경해야?..
        valueGetter: () => {
          return "→";
        },
      },
      {
        field: "TO_LOC_CD",
        headerKey: "to",
        headerClass: "header-center",
        // width: 200,
        sortable: true,
        editable: true,
        cellRenderer: gridTextInput,
        cellRendererParams: {
          autoCompleteKeys: ["CODE"],
          searchModalInputKey: "vrSrchLocCd",
          focusOutEventKey: "vrSrchLocCd",
          srchKey: "LOCATION",
          searchButtonFieldName: "LOCATION",
          callModalId: "location",
        } as ICellRendererSearchTextInputParams,
        suppressKeyboardEvent: (params: any) => {
          if (params.event.key === "Enter" || params.event.key === "Backspace") {
            return true;
          } else {
            return false;
          }
        },
      },
      {
        field: "LOCATION",
        headerName: "",
        maxWidth: 50,
        cellRenderer: GridSearchButton,
        cellClass: (params: any) => {
          if (Object.hasOwn(params.data, "RNUM")) {
            return "renderer-cell";
          } else {
            return "hidden-renderer-cell";
          }
        },
      },
      {
        field: "TO_LOC_ID",
        headerKey: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
    ],
    sortable: true,
  },
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ITEM_GRP_NM",
    headerKey: "product-group",
    headerClass: "header-center",
    sortable: true,
    width: 100,
  },
  {
    field: "ITEM_BEST_DATE_END",
    headerKey: "validity",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 110,
  },
  {
    field: "ITEM_CODE",
    headerKey: "code",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ITEM_NM",
    headerKey: "product-name",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 110,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "MOVE_QTY",
    headerKey: "moving-quantity",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    cellStyle: { textAlign: "right" },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    width: 110,
    // TODO :: 포멧터 적용되지 않음.
    valueFormatter: Format.NumberCount,
  },
  {
    field: "OUT_EXP_QTY",
    headerKey: "scheduled-shipping-quantity",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 120,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "LEFT_QTY",
    headerKey: "remain-quantity",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 120,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerClass: "header-center",
    sortable: true,
    width: 100,
  },
  {
    field: "MAKE_DT",
    headerKey: "manufacturing-date",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 110,
  },
  {
    field: "REG_DT",
    headerKey: "receiving-date",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 110,
  },
  {
    field: "REASON_CD",
    headerKey: "reason-(type)",
    headerClass: "header-center",
    valueGetter: (params: any) => {
      return UtilService.convertDataValue("WMSST040", params.data.REASON_CD);
    },
    sortable: true,
    cellRenderer: GridSelectBox,
    optionsAutoSelected: { autoSelectedKeyIndex: 0 },
  },
  {
    field: "WORK_DESC",
    headerKey: "reason-(detail)",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "EPC_CD",
    headerKey: "epc-code",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "UNIT_NO",
    headerKey: "unit-no",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "UOM_ID",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "MAKE_DT",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "STOCK_ID",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "CUST_ID",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "RITEM_ID",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "SUB_LOT_ID",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "WORK_SEQ",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "PLT_QTY",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "ITEM_WORK_TM",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "MAKE_DT",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "BL_NO",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "ITEM_WORK_DT",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
];

export const GRID_COL_DEF_WMSST040E6: any = [
  {
    field: "NO",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    width: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
  },
  {
    field: "WORK_STAT",
    headerKey: "status",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "WORK_STAT_CVT",
    headerKey: "status",
    headerClass: "header-center",
    valueGetter: (params: any) => {
      return UtilService.convertDataValue("WMSST040", params.data.WORK_STAT);
    },
  },
  {
    field: "ITEM_WORK_TM",
    headerKey: "work-time",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "",
    headerKey: "location",
    headerClass: "header-center",
    children: [
      {
        field: "FROM_LOC_CD",
        headerKey: "from",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "",
        headerKey: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "TO_LOC_CD",
        headerKey: "to",
        headerClass: "header-center",
        sortable: true,
        editable: true,
        suppressKeyboardEvent: (params: any) => {
          if (params.event.key === "Enter" || params.event.key === "Backspace") {
            return true;
          } else {
            return false;
          }
        },
      },
      {
        field: "LOCATION",
        headerName: "",
        cellRenderer: GridSearchButton,
        cellClass: "renderer-cell",
      },
      {
        field: "TO_LOC_ID",
        headerKey: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
    ],
  },
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ITEM_GRP_NM",
    headerKey: "product-group",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ITEM_BEST_DATE_END",
    headerKey: "validity",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
  {
    field: "ITEM_CODE",
    headerKey: "code",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ITEM_NM",
    headerKey: "product-name",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "MOVE_QTY",
    headerKey: "moving-quantity",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    cellStyle: { textAlign: "right" },
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "OUT_EXP_QTY",
    headerKey: "scheduled-shipping-quantity",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "LEFT_QTY",
    headerKey: "remain-quantity",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "MAKE_DT",
    headerKey: "manufacturing-date",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
  {
    field: "REG_DT",
    headerKey: "receiving-date",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
  {
    field: "REASON_CD",
    headerKey: "reason-(type)",
    headerClass: "header-center",
    valueGetter: (params: any) => {
      return UtilService.convertDataValue("WMSST040", params.data.REASON_CD);
    },
    sortable: true,
    cellRenderer: GridSelectBox,
    optionsAutoSelected: { autoSelectedKeyIndex: 0 },
  },
  {
    field: "WORK_DESC",
    headerKey: "reason-(detail)",
    headerClass: "header-center",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "EPC_CD",
    headerKey: "epc-code",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "UNIT_NO",
    headerKey: "unit-no",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "UOM_ID",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "MAKE_DT",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "STOCK_ID",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "CUST_ID",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "RITEM_ID",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "SUB_LOT_ID",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "WORK_SEQ",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "PLT_QTY",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "ITEM_WORK_TM",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "MAKE_DT",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "BL_NO",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "ITEM_WORK_DT",
    headerKey: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
];
