/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM120/input.ts
 *  Description:    주문/입고주문관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
/**
 ********************* Grid Area *********************/
import { IControlBtn } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

// done
export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const gridMetaData: any = {
  //페이지 키
  // checkBoxColumn: "check-box-column",
  //페이징옵션
  pagingSizeList: [200, 300, 400],
};

export const gridOptionsMeta: any = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  enableRangeSelection: true,
  columnDefs: FORM_COLUMN_DEFS,
  suppressContextMenu: true,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  pagination: false,
  headerHeight: 32,
  rowHeight: 32,
};
