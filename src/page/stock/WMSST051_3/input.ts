/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST051_3/input.ts
 *  Description:    재고관리/일별재고-일별재고내역(물류용기) 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import type { ISearchInput, IModal, info } from "@/types";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSST051_3",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  //화주는 input의 title
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        optionsReadOnly: true,
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "" }],
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd3",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm3",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },

  "logistics-container": {
    page: "WMSCM162",
    id: "logistics-container",
    title: "logistics-container",
    gridTitle: "logistics-container-list",

    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    apis: {
      url: "/WMSCM162/list_rn.action",
      params: {
        func: "fn_setWMSCM162_3",
        vrSrchCustId: "",
        vrSrchPoolCd: "",
        vrSrchPoolNm: "",
        vrSrchPoolGrp: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchPoolCd",
        searchContainerInputId: "vrSrchItemCd3",
        title: "logistics-container-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchPoolNm",
        searchContainerInputId: "vrSrchItemNm3",
        title: "logistics-container-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchPoolGrp",
        title: "logistics-container-group",
        type: "select",
        width: "half",
        optionsKey: "POOLGRP",
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchReqDtFrom3", "vrSrchReqDtTo3"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "work-date",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    ids: ["vrSrchCustCd3", "vrSrchCustNm3"],
    hiddenId: "vrSrchCustId3",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd3", "vrSrchCustNm3"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchItemGrpId3"],
    hiddenId: "vrSrchItemGrpId3",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchItemGrpId3"],
    srchKey: "",
    title: "logistics-container-group",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "POOLGRP",
    options: [{ name: "all", nameKey: "all", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    ids: ["vrSrchItemCd3", "vrSrchItemNm3"],
    hiddenId: "vrSrchItemId3",
    rowDataIds: ["POOL_CODE", "POOL_NM"],
    rowDataHiddenId: "RITEM_ID",
    searchApiKeys: ["vrSrchItemCd3", "vrSrchItemNm3"],
    srchKey: "POOL",
    title: "logistics-container",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
];

export const CONTROL_BTN = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];
