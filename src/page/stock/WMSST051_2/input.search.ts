/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST051_2/input.ts
 *  Description:    재고관리/일별재고-일별재고상세 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

// done
export const INFO: info = {
  pageId: "WMSST051_2",
  autoModal: false,
  autoModalPage: "",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    saveSession: true,
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        optionsReadOnly: true,
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "" }],
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd2",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm2",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",
    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        func: "fn_setWMSCM091_2",
        vrSrchCustId: "",
        vrSrchWhId: "",
        vrViewSetItem: "",
        vrViewAll: "",
        vrItemType: "",
        RITEM_ID: "",
        ITEM_CODE: "",
        ITEM_KOR_NM: "",
        UOM_ID: "",
        TIME_PERIOD_DAY: "",
        STOCK_QTY: "",
        CUST_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        UNIT_PRICE: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchItemCd2",
        title: "product-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchItemNm2",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [{ name: "all", nameKey: "all", value: "" }],
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        options: [
          { name: "all", nameKey: "all", value: "" },
          { name: "useProductY", nameKey: "useProductY", value: "Y" },
          { name: "useProductN", nameKey: "useProductN", value: "N" },
        ],
      },
    ],
  },
  "logistics-container": {
    page: "WMSCM162",
    id: "logistics-container",
    title: "logistics-container",
    gridTitle: "logistics-container-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    apis: {
      url: "/WMSCM162/list_rn.action",
      params: {
        func: "fn_setWMSCM162",
        vrSrchCustId: "",
        vrSrchPoolCd: "",
        vrSrchPoolNm: "",
        vrSrchPoolGrp: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchPoolCd",
        title: "logistics-container-code",
        searchContainerInputId: "vrSrchItemCd2",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchPoolNm",
        title: "logistics-container-name",
        searchContainerInputId: "vrSrchItemNm2",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchPoolGrp",
        title: "logistics-container-group",
        type: "select",
        width: "half",
        optionsKey: "POOLGRP",
        options: [
          {
            nameKey: "all",
            name: "all",
            value: "",
          },
        ],
      },
    ],
  },
};

// defaultParams, 상품 인풋영역 재확인 필요(select값에 따라 호출하는 모달이 변경됨)
export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchCustCd2", "vrSrchCustNm2"],
    hiddenId: "vrSrchCustId2",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchReqDtFrom2", "vrSrchReqDtTo2"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "work-date",
    width: "triple",
    isModal: false,
    required: true,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    ids: ["vrSrchItemType2", "vrSrchItemCd2", "vrSrchItemNm2"],
    hiddenId: "vrSrchItemId2",
    rowDataIds: ["", "ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "RITEM_ID",
    rowData2ndIds: ["", "POOL_CODE", "POOL_NM"],
    rowData2ndHiddenId: "POOL_ID",
    searchApiKeys: ["", "vrSrchItemCd", "vrSrchItemNm"],
    srchKey: "ITEM",
    title: "product",
    modalOpenCondition: "owner",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    modalTypes: ["", "product", "logistics-container"],
    width: "triple",
    isModal: false,
    required: true,
    isSearch: true,
    placeholder: ["", "code", "name"],
    types: ["select-box", "text", "text"],
    optionsKey: "vrSrchItemType2",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: false,
  unUsedRefreshButton: true,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
};
