/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST076_2/column-defs.ts
 *  Description:    재고/재고조정(상태변경)_조정이력 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  product: [
    {
      field: "No",
      headerName: "No",
      cellStyle: { textAlign: "center" },
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "stock",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      valueFormatter: Format.NumberPrice,
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.commonRowIndex,
    menuTabs: ["columnsMenuTab"],
    lockPosition: true,
    lockVisible: true,
    pinned: "left",
  },
  {
    field: "WORK_SEQ",
    headerName: t("grid-column-name.work-order"),
    cellStyle: { textAlign: "right" },
    width: 110,
  },
  {
    field: "RITEM_NM",
    headerName: t("grid-column-name.product-name"),
    sortable: true,
  },
  {
    field: "RITEM_CD",
    headerName: t("grid-column-name.product-code"),
    sortable: true,
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.owner"),
    sortable: true,
  },
  {
    field: "OLD_LOT_TYPE",
    headerName: t("grid-column-name.LOT-properties(old)"),
    sortable: true,
    width: 140,
  },
  {
    field: "NEW_LOT_TYPE",
    headerName: t("grid-column-name.LOT-properties(new)"),
    sortable: true,
    width: 140,
  },
  {
    field: "OLD_OWNER_CD",
    headerName: t("grid-column-name.owners(old)"),
    sortable: true,
    width: 140,
  },
  {
    field: "NEW_OWNER_CD",
    headerName: t("grid-column-name.owners(new)"),
    sortable: true,
    width: 140,
  },
  {
    field: "SUB_LOT_ID",
    headerName: t("grid-column-name.lot-id(old)"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 140,
  },
  {
    field: "NEW_SUB_LOT_ID",
    headerName: t("grid-column-name.lot-id(new)"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 150,
  },
  {
    field: "WORK_DT",
    headerName: t("grid-column-name.work-date"),
    width: 170,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.Date,
    sortable: true,
  },
  {
    field: "CHANGE_MEMO",
    headerName: t("grid-column-name.change-history"),
    sortable: true,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.registration-date"),
    width: 170,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.Date,
    sortable: true,
  },
  {
    field: "REG_NO",
    headerName: t("grid-column-name.registrar"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 130,
  },
];
