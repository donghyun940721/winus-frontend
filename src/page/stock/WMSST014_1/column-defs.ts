/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST014_1/column-defs.ts
 *  Description:    현재고 조회(NEW) 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";
import { MODAL_COLUMN_DEFS } from "@/page/common-page/meta/common-modal-column-defs";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
export { MODAL_COLUMN_DEFS };

// export const MODAL_COLUMN_DEFS: any = {
//   owner: [
//     {
//       field: "No",
//       headerKey: "no",
//       headerName: "",
//       minWidth: 80,
//       width: 80,
//       pinned: "left",
//       cellStyle: { textAlign: "center" },
//       valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
//     },
//     {
//       field: "",
//       headerKey: "",
//       headerName: "",
//       maxWidth: 50,
//       pinned: "left",
//       cellStyle: { textAlign: "center" },
//       headerCheckboxSelection: true,
//       checkboxSelection: true,
//     },
//     {
//       field: "CUST_CD",
//       headerKey: "shipper-code",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "CUST_NM",
//       headerKey: "owner-name",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "ADDR",
//       headerKey: "address",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "EMP_NM",
//       headerKey: "manager-name",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "CUST_EPC_CD",
//       headerKey: "owner-epc-code",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "TEL",
//       headerKey: "tel",
//       headerName: "",
//       headerClass: "header-center",
//       cellStyle: { textAlign: "right" },
//       sortable: true,
//     },
//   ],
//   product: [
//     {
//       field: "No",
//       headerKey: "no",
//       headerName: "",
//       width: 60,
//       cellStyle: { textAlign: "center" },
//       valueGetter: (params: any) => params.node && params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
//     },
//     {
//       field: "",
//       headerKey: "",
//       headerName: "",
//       headerCheckboxSelection: true,
//       checkboxSelection: true,
//       width: 50,
//       cellStyle: { textAlign: "center" },
//     },
//     {
//       field: "CUST_NM",
//       headerKey: "owner",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "ITEM_GRP_NAME",
//       headerKey: "product-group",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "ITEM_CODE",
//       headerKey: "product-code",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "ITEM_KOR_NM",
//       headerKey: "product-name",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//       width: 250,
//     },
//     {
//       field: "BOX_BAR_CD",
//       headerKey: "box-barcode",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "MAKER_NM",
//       headerKey: "company-name",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "STOCK_QTY",
//       headerKey: "current-stock",
//       headerName: "",
//       cellStyle: { textAlign: "right" },
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "BAD_QTY",
//       headerKey: "inferior-product",
//       headerName: "",
//       cellStyle: { textAlign: "right" },
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "UOM_NM",
//       headerKey: "uom",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "UNIT_PRICE",
//       headerKey: "unit-price",
//       headerName: "",
//       valueFormatter: Format.NumberPrice,
//       cellStyle: { textAlign: "right" },
//       headerClass: "header-center",
//       sortable: true,
//     },
//     {
//       field: "WH_NM",
//       headerKey: "warehouse",
//       headerName: "",
//       headerClass: "header-center",
//       sortable: true,
//     },
//   ],
//   "customer-owner-zone": [
//     {
//       field: "",
//       headerName: "No",
//       width: 60,
//       cellStyle: { textAlign: "center" },
//       valueGetter: (params: any) => params.node.rowIndex + 1,
//     },
//     {
//       field: "",
//       headerName: "",
//       headerCheckboxSelection: true,
//       checkboxSelection: true,
//       width: 60,
//     },
//     {
//       field: "CUST_ZONE_ID",
//       hide: true,
//     },
//     {
//       field: "CUST_ZONE_CD",
//       hide: true,
//     },
//     {
//       field: "CUST_ZONE_NM",
//       headerKey: "zone-name",
//       headerClass: "header-center",
//       sortable: true,
//       width: 600,
//     },
//   ],
// };

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // 화주코드
    field: "CUST_CD",
    headerKey: "owner-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    // 화주
    field: "CUST_NM",
    headerKey: "owner",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    // 상품군
    field: "ITEM_GRP_NM",
    headerKey: "product-group",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    // 품온구분
    field: "TEMP_TYPE_NAME_E1M",
    headerKey: "temperature-type-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    // 상품코드
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    // 상품명
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    // 전일재고
    field: "BEFORE_STOCK_QTY",
    headerKey: "the-day-before-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 입고
    field: "IN_QTY",
    headerKey: "receiving",
    headerName: "",
    export: true,
    sortable: true,
    width: 65,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 일반출고
    field: "OUT_QTY",
    headerKey: "shipping-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 크로스도킹
    field: "CROSS_QTY",
    headerKey: "cross-qyt",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 이벤트출고
    field: "EVENT_QTY",
    headerKey: "event-qty",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 반품출고
    field: "RETURN_QTY",
    headerKey: "return-shipment",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 정상재고
    field: "STOCK_QTY",
    headerKey: "normal-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 불량재고
    field: "BAD_QTY",
    headerKey: "bad-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 전체재고
    field: "STOCK_TOTAL_QTY",
    headerKey: "total-inventory",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 출고가용재고
    field: "OUT_ABLE_QTY",
    headerKey: "available-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 출고예정재고
    field: "OUT_EXP_QTY",
    headerKey: "scheduled-for-outgoing",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 입수
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "",
    export: true,
    sortable: true,
    width: 65,
    cellStyle: { textAlign: "center" },
  },
  {
    // UOM
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    export: true,
    sortable: true,
    width: 65,
    cellStyle: { textAlign: "center" },
  },
  {
    // EA수량
    field: "EA_QTY",
    headerKey: "ea-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
  },
  {
    // EA낱개수량(BOX)
    field: "REMAIN_EA_QTY",
    headerKey: "remaining-ea-quantity-box",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
  },
  {
    // BOX수량
    field: "BOX_QTY",
    headerKey: "box-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
  },
  {
    // PLT수량
    field: "PLT_QTY",
    headerKey: "plt-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
  },
  {
    // BAG수량
    field: "BAG_QTY",
    headerKey: "bag-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
  },
  {
    // CBM
    field: "CBM_QTY",
    headerKey: "cbm",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
  },
  {
    // 중량
    field: "STOCK_WEIGHT",
    headerKey: "weight",
    headerName: "",
    export: true,
    sortable: true,
    width: 65,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
  },
  {
    // AS재고
    field: "AS_QTY",
    headerKey: "as-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
  },
  {
    // 적정재고
    field: "PROP_QTY",
    headerKey: "appropriate-stock",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
  },
  {
    // 비고
    field: "REMARK",
    headerKey: "remark",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "center" },
  },
  {
    // 특송사
    field: "PROP_CHK",
    headerKey: "special-delivery",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
  },
  {
    // UOM_ID
    field: "UOM_ID",
    headerKey: "uom-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 상품ID
    field: "RITEM_ID",
    headerKey: "product-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 창고ID
    field: "WH_ID",
    headerKey: "warehouse-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 화주ID
    field: "CUST_ID",
    headerKey: "customer-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 출고가용수량
    field: "SHIPPING_ABLE_QTY",
    headerKey: "shipping-available-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 제조사명
    field: "MAKER_NM",
    headerKey: "manufacturer-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 상품크기
    field: "ITEM_SIZE",
    headerKey: "product-size",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
    valueFormatter: Format.NumberCount,
  },
  {
    // 화주 상품코드
    field: "CUST_ITEM_CD",
    headerKey: "customer-product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 입고세트수량
    field: "IN_SET_QTY",
    headerKey: "received-set-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
    valueFormatter: Format.NumberCount,
  },
  {
    // 출고세트수량
    field: "OUT_SET_QTY",
    headerKey: "outgoing-set-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
    hide: true,
    valueFormatter: Format.NumberCount,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // 작업일자
    field: "WORK_DT_D",
    headerKey: "work-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
  },
  {
    // 입고일자
    field: "IN_DT_D",
    headerKey: "receiving-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
  },
  {
    // 로케이션
    field: "LOC_NM_D",
    headerKey: "location",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 로케이션타입
    field: "LOC_TYPE_NM_D",
    headerKey: "location-type-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 품온구분
    field: "TEMP_TYPE_NAME_E1D",
    headerKey: "temperature-type-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품코드
    field: "RITEM_CD_D",
    headerKey: "product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품명
    field: "RITEM_NM_D",
    headerKey: "product-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 240,
    cellStyle: { textAlign: "center" },
  },
  {
    // 재고수량
    field: "STOCK_QTY_D",
    headerKey: "stock-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    valueFormatter: Format.NumberCount,
    cellStyle: { textAlign: "center" },
  },
  {
    // 입수
    field: "UNIT_NM_D",
    headerKey: "qty-by-box",
    headerName: "",
    export: true,
    sortable: true,
    width: 65,
    cellStyle: { textAlign: "center" },
  },
  {
    // UOM
    field: "UOM_NM_D",
    headerKey: "uom",
    headerName: "",
    export: true,
    sortable: true,
    width: 65,
    cellStyle: { textAlign: "center" },
  },
  {
    // 출고예정수량
    field: "OUT_EXP_QTY_D",
    headerKey: "schedule-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    valueFormatter: Format.NumberCount,
    cellStyle: { textAlign: "center" },
  },
  {
    // PLT수량
    field: "REAL_PLT_QTY_D",
    headerKey: "plt-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    valueFormatter: Format.NumberCount,
    cellStyle: { textAlign: "center" },
  },
  {
    // CBM
    field: "CBM_D",
    headerKey: "cbm",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    valueFormatter: Format.NumberCount,
    cellStyle: { textAlign: "center" },
  },
  {
    // B/L번호
    field: "BL_NO_D",
    headerKey: "BL-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 창고
    field: "WH_NM_D",
    headerKey: "warehouse",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "center" },
  },
  {
    // LOT번호
    field: "CUST_LOT_NO_D",
    headerKey: "lot-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 제조일자
    field: "MAKE_DT_D",
    headerKey: "manufacturing-date-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
  },
  {
    // 유효기간만료일
    field: "ITEM_BEST_DATE_END",
    headerKey: "validity-expiration-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
  },
  {
    // 무게
    field: "STOCK_WEIGHT_D",
    headerKey: "weight-2",
    headerName: "",
    export: true,
    sortable: true,
    width: 65,
    valueFormatter: Format.NumberCount,
    cellStyle: { textAlign: "center" },
  },
  {
    // UNIT_NO
    field: "UNIT_NO_D",
    headerKey: "unit-no",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // LOCK_YN
    field: "LOCK_YN_D",
    headerKey: "lock-yn",
    headerName: "",
    export: true,
    sortable: true,
    width: 65,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 품목 클래스
    field: "ITEM_CLASS_D",
    headerKey: "item-class",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 재고 ID
    field: "STOCK_ID_D",
    headerKey: "stock-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 재고 등급
    field: "STOCK_RANK_D",
    headerKey: "stock-rank",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // UOM ID
    field: "UOM_ID_D",
    headerKey: "uom-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 창고 ID
    field: "WH_ID_D",
    headerKey: "warehouse-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 로케이션 ID
    field: "LOC_ID_D",
    headerKey: "location-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 상품 ID
    field: "RITEM_ID_D",
    headerKey: "product-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 고객 이름
    field: "CUST_NM_D",
    headerKey: "customer-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 고객 코드
    field: "CUST_CD_D",
    headerKey: "customer-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 고객 ID
    field: "CUST_ID_D",
    headerKey: "customer-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 로케이션 타입
    field: "LOC_TYPE_D",
    headerKey: "location-type",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
];
