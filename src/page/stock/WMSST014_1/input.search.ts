/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST014_1/input.ts
 *  Description:    현재고 조회(NEW) 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

import type { IControlBtn, IModal, ISearchInput, info } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSST014_1",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "txtSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "txtSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      rowDataKeys: ["CUST_ID"],
      paramsKeys: ["hdnCustId"],
    },
    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "Y",
        hdnCustId: "",
        txtSrchItemCd: "",
        txtSrchItemNm: "",
        vrViewSetItem: "",
        hdnWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "txtSrchItemCd",
        title: "product-code",
        searchContainerInputId: "vrSrchRitemCd",
        type: "text",
        width: "half",
      },
      {
        id: "txtSrchItemNm",
        searchContainerInputId: "vrSrchRitemNm",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        options: [{ nameKey: "all", name: "", value: "" }],
        optionsKey: "ITEMGRP",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        options: [
          { nameKey: "all", name: "", value: "" },
          { nameKey: "useProductY", name: "", value: "Y" },
          { nameKey: "useProductN", name: "", value: "N" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
  location: {
    page: "WMSMS080",
    id: "location",
    title: "search-location",
    gridTitle: "location-list",

    apis: {
      url: "/WMSCM080/list_rn.action",
      params: {
        func: "fn_setWMSMS080",
        LOC_ID: "",
        LOC_CD: "",
        AVAILABLE_QTY: "",
        OUT_EXP_QTY: "",
        STOCK_ID: "",
        SUB_LOT_ID: "",
        vrViewOnlyLoc: "",
        STOCK_WEIGHT: "",
        vrViewSubLotId: "",
        vrRitemId: "",
        ITEM_BEST_DATE_END: "",
        UOM_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        vrViewStockQty: "",
        txtSrchWhNm: "",
        txtSrchLocId: "",
        vrSrchCustLotNo: "",
        vrSrchLocStat: "",
        S_WH_CD: "",
        vrWhId: "",
        S_WH_NM: "",
        vrSrchLocDel: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "txtSrchWhNm",
        searchContainerInputId: "txtSrchLocCd",
        title: "location-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchCustLotNo",
        title: "LOT-number",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchLocStat",
        title: "location-status",
        type: "select",
        width: "half",
        options: [
          { nameKey: "all", name: "", value: "300" },
          { nameKey: "use-location", name: "", value: "200" },
          { nameKey: "unused-location", name: "", value: "100" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
  "customer-owner-zone": {
    page: "WMSCM085",
    id: "delivery-destination",
    title: "search-zone",
    gridTitle: "zone-list",

    apis: {
      url: "/WMSCM085/listCust_rn.action",
      params: {
        func: "fn_setWMSCM085",
        CUST_ZONE_ID: "",
        CUST_ZONE_NM: "",
        txtSrchZoneNm: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        sidx: "",
        sord: "asc",
      },
    },
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["trCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    inputs: [
      {
        id: "txtSrchZoneNm",
        title: "name",
        type: "text",
        width: "single",
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    //화주
    ids: ["txtSrchCustCd", "txtSrchCustNm"],
    hiddenId: "hdnCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["txtSrchCustCd", "txtSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 상품
    ids: ["vrSrchRitemCd", "vrSrchRitemNm"],
    hiddenId: "vrSrchRitemId",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "RITEM_ID",
    searchApiKeys: ["txtSrchItemCd", "txtSrchItemNm"],
    srchKey: "ITEM",
    title: "product",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
    optionsKey: "ITEMGRP",
  },
  {
    // 상품군
    ids: ["vrSrchItemGrpId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "product-group",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "ITEMGRP",
    options: [{ name: "", nameKey: "all", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 창고
    ids: ["txtSrchWhCd", "txtSrchWhNm"],
    hiddenId: "hdnWhId",
    rowDataIds: ["WH_CD", "WH_NM"],
    rowDataHiddenId: "WH_ID",
    searchApiKeys: ["txtSrchWhCd", "txtSrchWhNm"],
    srchKey: "WH",
    title: "warehouse",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 로케이션
    ids: ["txtSrchWhNm", "vrSrchLocNm"],
    hiddenId: "txtSrchLocId",
    rowDataIds: ["LOC_CD", "LOC_CD"],
    rowDataHiddenId: "LOC_ID",
    searchApiKeys: ["txtSrchCustCd", "txtSrchCustNm"],
    srchKey: "LOCATION",
    title: "location",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 미재고 표시
    ids: ["chkEmptyStock"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "option",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    checkboxLabelTitles: ["display-empty-stock"],
    checkboxCheckedValue: ["on"],
    types: ["check-box"],
  },
  {
    // 거래처ZONE
    ids: ["CUST_ZONE_NM", "CUST_ZONE", "vrSrchItemOrdGb"], //검색 시 요청으로 나가야 되는 폼 필드
    hiddenId: "CUST_ZONE",
    rowDataIds: ["CUST_ZONE_NM", "CUST_ZONE_ID"], //팝업 Grid에서 선택된 리코드의 필드
    rowDataHiddenId: "CUST_ZONE_ID",
    searchApiKeys: ["", ""],
    srchKey: "CUST_ZONE",
    title: "customer-owner-zone",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "select-box"],
    optionsKey: "vrSrchItemOrdGb", //select box
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 임가공상품여부
    ids: ["vrSrchSetItemYn"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchSetItemYn"],
    title: "repacking-product-y/n",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchSetItemYn",
    options: [{ name: "", nameKey: "all", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

export const SEARCH_COMPONENT_CONTROL_BTN: IControlBtn[] = [
  {
    title: "in/out-order-regeneration",
    colorStyle: "danger",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
];

export const FILTER_SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchUnitNo"],
    hiddenId: "",
    rowDataIds: ["UNIT_NO"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchUnitNo"],
    srchKey: "UNIT",
    title: "unit-no",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["vrSrchLockYn"],
    hiddenId: "",
    rowDataIds: ["LOCK_YN"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchLockYn"],
    srchKey: "LOCK",
    title: "lock-yn",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      { name: "", nameKey: "useProductY", value: "Y" },
      { name: "", nameKey: "useProductN", value: "N" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 1 },
  },
];

export const MASTER_GRID_STATISTICS_INFO = [
  {
    id: "TOTAL_QTY",
    title: "total-quantity",
  },
];

export const DETAIL_GRID_STATISTICS_INFO = [
  {
    id: "DETAIL_QTY",
    title: "detail-quantity",
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: true,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchComponentControlBtn: SEARCH_COMPONENT_CONTROL_BTN,
  pageInfo: INFO,
};
