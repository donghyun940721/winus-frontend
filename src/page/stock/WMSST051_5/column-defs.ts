/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST051_5/column-defs.ts
 *  Description:    재고관리/일별재고-기간별상세재고내역 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],

  product: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      valueFormatter: Format.NumberPrice,
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  customer: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_ZONE_NM",
      headerKey: "zone-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 500,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    menuTabs: ["columnsMenuTab"],
    lockPosition: true,
    lockVisible: true,
    valueGetter: Getter.commonRowIndex,
  },
  {
    field: "SUBLE_DT",
    headerName: t("grid-column-name.date"),
    sortable: true,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    width: 110,
    // export: true,
  },
  {
    field: "RITEM_CD",
    headerName: t("grid-column-name.item-code"),
    sortable: true,
    headerClass: "header-center",
    // export: true,
    flex: 200,
  },
  {
    field: "RITEM_NM",
    headerName: t("grid-column-name.item-name"),
    sortable: true,
    headerClass: "header-center",
    // export: true,
    flex: 250,
  },
  {
    field: "IN_CUST_ZONE",
    headerName: t("grid-column-name.customer-name"),
    sortable: true,
    headerClass: "header-center",
    // export: true,
    flex: 200,
  },
  {
    field: "YES_STOCK",
    headerName: t("grid-column-name.the-day-before-stock"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    headerClass: "header-center",
    // export: true,
    flex: 200,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "IN_QTY",
    headerName: t("grid-column-name.receiving-quantity"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    headerClass: "header-center",
    // export: true,
    flex: 200,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "OUT_QTY",
    headerName: t("grid-column-name.shipping-quantity"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    headerClass: "header-center",
    // export: true,
    flex: 200,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "STOCK_QTY",
    headerName: t("grid-column-name.current-stock"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    headerClass: "header-center",
    // export: true,
    flex: 200,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
];
