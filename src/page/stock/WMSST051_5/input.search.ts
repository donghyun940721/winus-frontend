/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM120/input.ts
 *  Description:    주문/입고주문관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { UtilService } from "@/services/util-service";
import { IControlBtn } from "@/types";
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSST051_5",
  ///// 데이터 조회했을 때 고유한 key값으로 추정됨
  pk: "RNUM",
};

export const NEW_POPUP_CONTROL_BTN: IControlBtn[] = [];

export const NEW_POPUP_SEARCH_MODAL_INFO: any = {};

export const NEW_POPUP_SEARCH_INPUT: any = [];

export const SEARCH_MODAL_INFO: IModal = {
  //화주는 input의 title
  owner: {
    //util에서 pagingoptions지정하는 key값
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {},
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        optionsReadOnly: true,
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "12" }],
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd5",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm5",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },

  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",

    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        func: "fn_setWMSCM091_5",
        vrSrchCustId: "",
        vrSrchWhId: "",
        vrViewSetItem: "",
        vrViewAll: "Y",
        vrItemType: "",
        RITEM_ID: "",
        ITEM_CODE: "",
        ITEM_KOR_NM: "",
        UOM_ID: "",
        TIME_PERIOD_DAY: "",
        STOCK_QTY: "",
        CUST_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        UNIT_PRICE: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrSrchSetItemYn: "",
      },
      data: {},
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchItemCd5",
        title: "product-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchItemNm5",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [{ name: "all", nameKey: "all", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        optionsKey: "S_SET_ITEM_YN",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
  customer: {
    page: "WMSCM085",
    id: "customer",
    title: "search-zone",
    gridTitle: "zone-list",

    apis: {
      url: "/WMSCM085/listCust_rn.action",
      params: {
        func: "fn_setWMSCM085",
        CUST_ZONE_ID: "",
        CUST_ZONE_NM: "",
        txtSrchZoneNm: "",
      },
      data: {},
    },
    inputs: [
      {
        id: "txtSrchZoneNm",
        searchContainerInputId: "vrCustZoneNm",
        title: "zone-name",
        type: "text",
        width: "half",
      },
    ],
  },
};
// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchCustCd5", "vrSrchCustNm5"],
    hiddenId: "vrSrchCustId5",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchReqDtFrom5", "vrSrchReqDtTo5"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "work-date",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    ids: ["vrSrchItemCd5", "vrSrchItemNm5"],
    hiddenId: "vrSrchItemId5",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "RITEM_ID",
    searchApiKeys: ["vrSrchItemCd5", "vrSrchItemNm5"],
    srchKey: "ITEM",
    title: "product",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrCustZoneNm"],
    hiddenId: "vrCustZoneId",
    rowDataIds: ["CUST_ZONE_NM"],
    rowDataHiddenId: "CUST_ZONE_ID",
    searchApiKeys: [],
    title: "customer",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text"],
  },
  {
    ids: ["S_SET_ITEM_YN_5"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "repacking-product-y/n",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "S_SET_ITEM_YN",
    options: [{ name: "all", nameKey: "all", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

//done
export const SEARCH_COMPONENT_CONTROL_BTN: IControlBtn[] = [];

const displayInputCount = UtilService.getShowInputCount(2, SEARCH_INPUT) as number;

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: displayInputCount !== UtilService.getAvailableSearchInputCount(SEARCH_INPUT),
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchComponentControlBtn: SEARCH_COMPONENT_CONTROL_BTN,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  // searchConditionInitUrl: "",
};
