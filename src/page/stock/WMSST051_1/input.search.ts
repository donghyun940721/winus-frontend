/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM120/input.ts
 *  Description:    주문/입고주문관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { IControlBtn } from "@/types";
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  pageId: "WMSST051_1",
  autoModal: false,
  autoModalPage: "WMSST051",
  pk: "WORK_ID",
};

export const NEW_POPUP_CONTROL_BTN: IControlBtn[] = [];

export const NEW_POPUP_SEARCH_MODAL_INFO: any = {};

export const NEW_POPUP_SEARCH_INPUT: any = [];

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
        options: [{ name: "all", nameKey: "all", value: "12" }],
      },
      {
        id: "S_CUST_CD",
        title: "shipper-code",
        searchContainerInputId: "vrSrchCustCd",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        title: "owner-name",
        searchContainerInputId: "vrSrchCustNm",
        type: "text",
        width: "triple",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",
    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchItemCd",
        title: "product-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchItemNm",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        optionsKey: "S_SET_ITEM_YN",
      },
    ],
  },
  "logistics-container": {
    page: "WMSCM162",
    id: "logistics-container",
    title: "search-logistics-container",
    gridTitle: "logistics-container-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    apis: {
      url: "/WMSCM162/list_rn.action",
      params: {
        func: "fn_setWMSCM162",
        vrSrchCustId: "",
        vrSrchPoolCd: "",
        vrSrchPoolNm: "",
        vrSrchPoolGrp: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchPoolCd",
        title: "logistics-container-code",
        searchContainerInputId: "vrSrchItemCd",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchPoolNm",
        searchContainerInputId: "vrSrchItemNm",
        title: "logistics-container-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchPoolGrp",
        title: "logistics-container-group",
        type: "select",
        width: "half",
        optionsKey: "POOLGRP",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
    ],
  },
};
// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 작업일자
    ids: ["vrSrchReqDtFrom", "vrSrchReqDtTo"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "work-date",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    // 화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 상품
    ids: ["vrSrchItemType", "vrSrchItemCd", "vrSrchItemNm"],
    hiddenId: "vrSrchItemId",
    rowDataIds: ["", "ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "RITEM_ID",
    rowData2ndIds: ["", "POOL_CODE", "POOL_NM"],
    rowData2ndHiddenId: "POOL_ID",
    searchApiKeys: ["", "vrSrchItemCd", "vrSrchItemNm"],
    srchKey: "ITEM",
    title: "product",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    modalTypes: ["", "product", "logistics-container"],
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["", "code", "name"],
    types: ["select-box", "text", "text"],
    optionsKey: "vrSrchItemType",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    ids: ["vrSrchItemGrpId"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: ["", ""],
    title: "product-group",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [{ nameKey: "all", name: "", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    optionsKey: "ITEMGRP",
  },
  {
    ids: ["S_SET_ITEM_YN", "chkEmptyStock"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "Kit_Item_Y/N",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    checkboxLabelTitles: ["", "excluding-absence-of-receipt/delivery-history"],
    checkboxCheckedValue: ["", "on"],
    types: ["select-box", "check-box"],
    options: [
      {
        nameKey: "all",
        name: "",
        value: "",
      },
      {
        nameKey: "Y",
        name: "",
        value: "Y",
      },
      {
        nameKey: "N",
        name: "",
        value: "N",
      },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    ids: ["vrSrchLikeItemNm"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "similar-product-name",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["vrSrchItemBarCd"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "product-barcode",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

export const GRID_STATISTICS_INFO = [
  {
    id: "TOTAL_IN_QTY",
    title: "total-receiving-volume",
  },
  {
    id: "TOTAL_OUT_QTY",
    title: "total-shipping-volume",
  },
  {
    id: "TOTAL_STOCK_QTY",
    title: "full-inventory",
  },
  {
    id: "TOTAL_GOOD_QTY",
    title: "total-current-inventory",
  },
  {
    id: "TOTAL_BAD_QTY",
    title: "total-defective-inventory",
  },
];

//done
export const SEARCH_COMPONENT_CONTROL_BTN: IControlBtn[] = [];

// const displayInputCount = UtilService.getShowInputCount(1, SEARCH_INPUT) as number;

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useSetting: false,
  // useMore: displayInputCount !== UtilService.getAvailableSearchInputCount(SEARCH_INPUT),
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchComponentControlBtn: SEARCH_COMPONENT_CONTROL_BTN,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  // searchConditionInitUrl: "",
};
