/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST051_1/column-defs.ts
 *  Description:    재고관리/일별재고-일별재고내역 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef, IHasKeyColDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: IHasKeyColDef = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  product: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "logistics-container": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "POOL_GRP_NAME",
      headerKey: "pool-grp-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerKey: "pool-cd",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerKey: "pool-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.commonRowIndex,
    menuTabs: ["columnsMenuTab"],
    lockPosition: true,
    lockVisible: true,
    suppressColumnsToolPanel: true,
    pinned: "left",
  },
  {
    field: "SUBUL_DT",
    headerName: t("grid-column-name.work-date"),
    headerClass: "header-center",
    width: 110,
    cellStyle: { textAlign: "center" },
    cellClass: "stringType",
    sortable: true,
    // export: true,
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.owner"),
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 130,
  },
  {
    field: "RITEM_CD",
    headerName: t("grid-column-name.product-code"),
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    cellDataType: "text",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 150,
  },
  {
    field: "RITEM_NM",
    headerName: t("grid-column-name.product-name"),
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    width: 250,
    sortable: true,
    // export: true,
    cellClass: "stringType",
  },
  {
    field: "ITEM_BAR_CD",
    headerName: t("grid-column-name.product-barcode"),
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    sortable: true,
    // export: true,
    cellClass: "stringType",
  },
  {
    field: "IN_QTY",
    headerName: t("grid-column-name.receiving"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "OUT_QTY",
    headerName: t("grid-column-name.shipping"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "STOCK_QTY",
    headerName: t("grid-column-name.total-inventory"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "GOOD_QTY",
    headerName: t("grid-column-name.current-stock"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "BAD_QTY",
    headerName: t("grid-column-name.bad-stock"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "UNIT_NM",
    headerName: t("grid-column-name.qty-by-box"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 100,
    valueFormatter: Format.NumberCount,
    // export: true,
    cellClass: "stringType",
    aggFunc: "sum",
  },
  {
    field: "UOM_NM",
    headerName: t("grid-column-name.uom"),
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 100,
  },

  {
    field: "UNIT_PRICE",
    headerName: t("grid-column-name.amount"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "STOCK_WEIGHT",
    headerName: t("grid-column-name.weight"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 110,
    aggFunc: "sum",
  },
  {
    field: "INOUT_QTY",
    headerName: t("grid-column-name.receiving-and-shipping-quantity"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 150,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "CBM",
    headerName: t("grid-column-name.cbm"),
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 130,
  },
];
