/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP021/column-defs.ts
 *  Description:    원부자재(부품/용기) 입고관리 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

export const MODAL_COLUMN_DEFS: any = {
  "assignment-owner": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // 작업번호
    field: "CHANGE_WORK_ID_CUT",
    headerKey: "work-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  //editoptions : {$!comCode.getGridComboComCode("STS00", true, "$!lang.getText('선택')")}}
  {
    // 완료
    field: "WORK_STAT",
    headerKey: "complete",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
  },
  {
    // 작업일
    field: "WORK_DT",
    headerKey: "work-day",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
  },
  {
    // 양도자
    field: "SELL_CUST_NM",
    headerKey: "transferor",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  {
    // 상품
    field: "SELL_RITEM_NM",
    headerKey: "Product",
    headerName: "",
    export: true,
    sortable: true,
    width: 300,
    cellStyle: { textAlign: "left" },
  },
  //formatoptions:{thousandsSeparator:",",	decimalPlaces: 0}
  {
    // 수량
    field: "SELL_QTY",
    headerKey: "Quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
  },
  {
    // UOM
    field: "SELL_UOM_NM",
    headerKey: "uom",
    headerName: "",
    export: true,
    sortable: true,
    width: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    // 양수자
    field: "BUY_CUST_NM",
    headerKey: "transferee",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  {
    // 상품
    field: "BUY_RITEM_NM",
    headerKey: "Product",
    headerName: "",
    export: true,
    sortable: true,
    width: 300,
    cellStyle: { textAlign: "left" },
  },
  //formatoptions:{thousandsSeparator:",",	decimalPlaces: 0}
  {
    // 수량
    field: "BUY_QTY",
    headerKey: "qty",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "right" },
  },
  {
    // BUY_UOM_NM
    field: "BUY_UOM_NM",
    headerKey: "uom",
    headerName: "",
    export: true,
    sortable: true,
    width: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    // 작업 시퀀스
    field: "WORK_SEQ",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
    cellStyle: { textAlign: "left" },
  },
  {
    // 변경 작업 ID
    field: "CHANGE_WORK_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
    cellStyle: { textAlign: "left" },
  },
];
