/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST401/input.ts
 *  Description:    재고/랙보충내역 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import type { IModal, ISearchInput, info } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSMS011",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  "shipping-warehouse": {
    page: "WMSMS040",
    id: "shipping-warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_WH_CD",
        searchContainerInputId: "vrSrchWhCd",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        searchContainerInputId: "vrSrchWhNm",
        title: "warehouse-name",
        type: "text",
        width: "half",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",

    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        func: "fn_setWMSCM091",
        vrSrchCustId: "",
        vrSrchWhId: "",
        vrViewSetItem: "",
        vrViewAll: "",
        vrItemType: "",
        RITEM_ID: "",
        ITEM_CODE: "",
        ITEM_KOR_NM: "",
        UOM_ID: "",
        TIME_PERIOD_DAY: "",
        STOCK_QTY: "",
        CUST_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        UNIT_PRICE: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchItemCdE5",
        title: "product-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchItemNmE5",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [{ name: "all", nameKey: "all", value: "" }],
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        options: [
          { value: "all", nameKey: "all", name: "" },
          { value: "Y", nameKey: "useProductY", name: "Y" },
          { value: "N", nameKey: "useProductN", name: "N" },
        ],
      },
    ],
  },
  "from-location": {
    page: "WMSMS080",
    id: "from-location",
    title: "search-location",
    gridTitle: "location-list",

    apis: {
      url: "/WMSCM080/list_rn.action",
      params: {
        func: "fn_setWMSCM080_FROM",
        LOC_ID: "",
        LOC_CD: "",
        AVAILABLE_QTY: "",
        OUT_EXP_QTY: "",
        STOCK_ID: "",
        SUB_LOT_ID: "",
        vrViewOnlyLoc: "",
        STOCK_WEIGHT: "",
        vrViewSubLotId: "",
        vrRitemId: "",
        ITEM_BEST_DATE_END: "",
        UOM_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        vrViewStockQty: "",
        vrSrchLocCd: "",
        vrSrchLocId: "",
        vrSrchCustLotNo: "",
        vrSrchLocStat: "300",
        S_WH_CD: "",
        vrWhId: "",
        S_WH_NM: "",
        vrSrchLocDel: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchLocCd",
        searchContainerInputId: "vrSrchLocCdFrom",
        title: "location-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchCustLotNo",
        searchContainerInputId: "",
        title: "LOT-number",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchLocStat",
        title: "location-status",
        type: "select",
        width: "half",
        options: [
          { nameKey: "all", name: "", value: "300" },
          { nameKey: "use-location", name: "", value: "200" },
          { nameKey: "unused-location", name: "", value: "100" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
  "to-location": {
    page: "WMSMS080",
    id: "to-location",
    title: "search-location",
    gridTitle: "location-list",

    apis: {
      url: "/WMSCM080/list_rn.action",
      params: {
        func: "fn_setWMSCM080_FROM",
        LOC_ID: "",
        LOC_CD: "",
        AVAILABLE_QTY: "",
        OUT_EXP_QTY: "",
        STOCK_ID: "",
        SUB_LOT_ID: "",
        vrViewOnlyLoc: "",
        STOCK_WEIGHT: "",
        vrViewSubLotId: "",
        vrRitemId: "",
        ITEM_BEST_DATE_END: "",
        UOM_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        vrViewStockQty: "",
        vrSrchLocCd: "",
        vrSrchLocId: "",
        vrSrchCustLotNo: "",
        vrSrchLocStat: "300",
        S_WH_CD: "",
        vrWhId: "",
        S_WH_NM: "",
        vrSrchLocDel: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchLocCd",
        searchContainerInputId: "vrSrchLocCdTo",
        title: "location-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchCustLotNo",
        searchContainerInputId: "",
        title: "LOT-number",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchLocStat",
        title: "location-status",
        type: "select",
        width: "half",
        options: [
          { nameKey: "all", name: "", value: "300" },
          { nameKey: "use-location", name: "", value: "200" },
          { nameKey: "unused-location", name: "", value: "100" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
};
/*
    isAllUseSearchApiKey 
    포커스아웃시 
    대부분 params는 해당 searchApiKey, searKey로 구성,
    그러나 인풋과 관계 없는 params도 존재하는 경우가 있음.
    focusOutSearchEvent함수 참고

    모달내에서 검색시 defaultParamsData 사용되는 타입
    다른 모달의 검색결과값을 참조할때, 
 */
export const SEARCH_INPUT: ISearchInput[] = [
  {
    //화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 작업일자
    ids: ["vrSrchReqDtFrom", "vrSrchReqDtTo"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "work-date",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    // 출고창고
    ids: ["vrSrchWhCd", "vrSrchWhNm"],
    hiddenId: "vrSrchWhId",
    rowDataIds: ["WH_CD", "WH_NM"],
    rowDataHiddenId: "WH_ID",
    searchApiKeys: ["vrSrchWhCd", "vrSrchWhNm"],
    srchKey: "WH",
    title: "shipping-warehouse",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 상품
    ids: ["vrSrchItemCd", "vrSrchItemNm"],
    hiddenId: "vrSrchItemId",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "RITEM_ID",
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    srchKey: "ITEM",
    title: "product",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchLocCdFrom", "vrSrchLocNmFrom"],
    hiddenId: "vrSrchLocIdFrom",
    rowDataIds: ["LOC_CD", "LOC_CD"],
    rowDataHiddenId: "LOC_ID",
    searchApiKeys: ["vrSrchLocCd", "vrSrchLocCd"],
    srchKey: "LOCATION",
    title: "from-location",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchLocCdTo", "vrSrchLocNmTo"],
    hiddenId: "vrSrchLocIdTo",
    rowDataIds: ["LOC_CD", "LOC_CD"],
    rowDataHiddenId: "LOC_ID",
    searchApiKeys: ["vrSrchLocCd", "vrSrchLocCd"],
    srchKey: "LOCATION",
    title: "to-location",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchOrdDegree"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "order-sequence",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
};
