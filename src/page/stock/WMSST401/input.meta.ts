/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST401/input.ts
 *  Description:    재고/랙보충내역 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import type { info } from "@/types/index";
import { gridMetaData, gridOptionsMeta } from "./input.grid";
import { SEARCH_CONTAINER_META } from "./input.search";

export * from "./input.grid";
export * from "./input.search";

export const commonSetting = {
  authPageGroup: "WMSST",
  authPageId: "WMSST401",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSMS011",
  pk: "RNUM",
};
