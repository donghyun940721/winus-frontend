/*------------------------------------------------------------------------------
*  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
*
*  Use of this software is controlled by the terms and conditions found in the
*  license agreement under which this software has been supplied.
*------------------------------------------------------------------------------
*
*  Source Name:    WMSST055/column-defs.ts
*  Description:    재고관리/월별재고내역 컬럼 정의 스크립트
*  Authors:        J. I. Cho
*  Update History:
*                  2023.08. : Created by J. I. Cho
*
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";
import { UtilService } from "@/services/util-service";

export const MODAL_COLUMN_DEFS: any = {
  //화주는 input의 title
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      cellStyle: { textAlign: "center" },
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      cellStyle: { textAlign: "center" },
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  product: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      cellStyle: { textAlign: "center" },
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      valueFormatter: Format.NumberPrice,
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  location: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      cellStyle: { textAlign: "center" },
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      cellStyle: { textAlign: "center" },
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "PRODUCT",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },
        { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerKey: "stock-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerKey: "lot-number",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerKey: "validity-expiration-date",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "logistics-container": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "POOL_GRP_NAME",
      headerKey: "pool-grp-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerKey: "pool-grp-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerKey: "pool-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    pinned: "left",
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    cellStyle: { textAlign: "center" },
    menuTabs: ["columnsMenuTab"],
    lockVisible: true,
    lockPosition: true,
  },
  {
    field: "SUBUL_DT",
    headerKey: "work-date",
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 110,
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 150,
  },
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 150,
  },
  {
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
  },
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    sortable: true,
    width: 250,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
  },
  {
    field: "ITEM_BAR_CD",
    headerKey: "product-barcode",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
  },
  {
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    headerName: "",
    cellStyle: { textAlign: "right" },
    sortable: true,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "NOW_MOVING_QTY",
    headerKey: "qty-in-transfer",
    headerName: "",
    cellStyle: { textAlign: "right" },
    sortable: true,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "ITEM_STAT",
    headerKey: "goods-status",
    headerName: "",
    sortable: true,
    valueGetter: (params: any) => {
      return params.data && UtilService.convertDataValue("WMSST055", params.data.ITEM_STAT);
    },
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 110,
  },
  {
    field: "LOC_CD",
    headerKey: "location",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 130,
  },
  {
    field: "LOC_TYPE",
    headerKey: "location-type",
    headerName: "",
    sortable: true,
    valueGetter: (params: any) => {
      return params.data && UtilService.convertDataValue("WMSST055", params.data.LOC_TYPE);
    },
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 120,
  },
  {
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    width: 110,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
  },
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 100,
  },
  {
    field: "STOCK_WEIGHT",
    headerKey: "inventory-weight-(kg)",
    cellStyle: { textAlign: "right" },
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 120,
  },
  {
    field: "CAL_PLT_QTY",
    headerKey: "converted-PLT-qty",
    cellStyle: { textAlign: "right" },
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    width: 120,
    aggFunc: "sum",
  },
  {
    field: "ITEM_BEST_DATE_END",
    headerKey: "validity",
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    headerClass: "header-center",
    export: true,
    cellClass: "stringType",
    width: 110,
  },
  //excel loc
  {
    field: "LOC_CD_FIR",
    headerKey: "location-precode",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "LOC_CNT",
    headerKey: "number-of-locations",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    hide: true,
  },
];
