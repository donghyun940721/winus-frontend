/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST057_1/column-defs.ts
 *  Description:    일별재고상세(NEW) 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  product: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node && params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "logistics-container": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "POOL_GRP_NAME",
      headerKey: "pool-grp-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerKey: "pool-cd",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerKey: "pool-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  location: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

        { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerKey: "stock-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerKey: "lot-number",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerKey: "expiration-date",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // 작업일자
    field: "SUBUL_DT",
    headerKey: "work-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
  },
  {
    // 화주
    field: "CUST_NM",
    headerKey: "owner",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // LOT 번호
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품코드
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품명
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 250,
    cellStyle: { textAlign: "center" },
  },
  {
    // 재고수량
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 이동중수량
    field: "NOW_MOVING_QTY",
    headerKey: "qty-in-transfer",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 제품상태
    field: "ITEM_STAT",
    headerKey: "goods-status",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
  },
  {
    // 로케이션
    field: "LOC_CD",
    headerKey: "location",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  {
    // 로케이션 유형
    field: "LOC_TYPE",
    headerKey: "location-type",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
  },
  {
    // 입수
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "",
    export: true,
    sortable: true,
    width: 60,
    cellStyle: { textAlign: "center" },
  },
  {
    // UOM
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    export: true,
    sortable: true,
    width: 60,
    cellStyle: { textAlign: "center" },
  },
  {
    // 재고중량(kg)
    field: "STOCK_WEIGHT",
    headerKey: "inventory-weight-(kg)",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 유효기간
    field: "ITEM_BEST_DATE_END",
    headerKey: "validity",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
    // valueFormatter: Format.Date,
  },
  {
    // WH_ID
    field: "WH_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // EXP_PLT_QTY
    field: "EXP_PLT_QTY",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "right" },
    hide: true,
  },
  {
    // IN_EXP_QTY
    field: "IN_EXP_QTY",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "right" },
    hide: true,
  },
  {
    // OUT_EXP_QTY
    field: "OUT_EXP_QTY",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "right" },
    hide: true,
  },
  {
    // REAL_PLT_QTY
    field: "REAL_PLT_QTY",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "right" },
    hide: true,
  },
  {
    // LC_ID
    field: "LC_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // LOC_ID
    field: "LOC_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // POOL_YN
    field: "POOL_YN",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // STOCK_ID
    field: "STOCK_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // SUB_LOT_ID
    field: "SUB_LOT_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 유효기간
    field: "ITEM_BEST_DATE_END",
    headerKey: "validity",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // RITEM_ID
    field: "RITEM_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // CUST_ID
    field: "CUST_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // CUST_CD
    field: "CUST_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  {
    // UOM_CD
    field: "UOM_CD",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
];
