/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST076_1/input.ts
 *  Description:    재고/재고조정(상태변경)_LOT속성변경 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { IControlBtn, IGridCellDateInput, IGridCellSelectBox } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

export const GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  OLD_LOT_TYPE: {
    pk: "RNUM",
    optionsKey: "vrLotAttribute",
  },
};

export const GRID_DATE_INPUT_INFO: IGridCellDateInput = {
  gridPk: "RNUM",
};

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "enter-template",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
    authType: "INS_AUTH",
  },
  {
    title: "save",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
    authType: "INS_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];
// export const CONTROL_BTN2 = [
//   {
//     title: "enter-template",
//     colorStyle: "primary",
//     paddingStyle: "bold",
//     image: "",
//     disabled: "",
//   },
// ];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [1000, 2000, 3000, 5000, 10000, 100000],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  suppressPaginationPanel: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  allowContextMenuWithControlKey: true,
  enableCellEditingOnBackspace: false,
  pagination: false,
  statusBar: false,
  getRowId: (data) => {
    return data.data.RNUM;
  },
  // onBodyScrollEnd: () => {
  //   useSelectedDataStore().changeSelectedDataList(gridOptions.api?.getSelectedRows());
  // },
};
