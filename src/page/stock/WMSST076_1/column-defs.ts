/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST076_1/column-defs.ts
 *  Description:    재고/재고조정(상태변경)_LOT속성변경 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import gridSelect from "@/components/renderer/grid-select.vue";
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import { GridUtils } from "@/lib/ag-grid/utils";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
import type { ICellEditorParams, ICellRendererParams } from "ag-grid-community";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  location: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

        { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerKey: "stock-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 100,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerKey: "lot-number",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerKey: "expiration-date",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },

    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  product: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      cellStyle: { textAlign: "right" },
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.commonRowIndex,
    menuTabs: ["columnsMenuTab"],
    lockPosition: true,
    lockVisible: true,
    suppressColumnsToolPanel: true,
    pinned: "left",
  },
  {
    field: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "OLD_LOT_TYPE",
    headerName: t("grid-column-name.LOT-property"),
    headerClass: "header-require",
    width: 110,
    cellStyle: { textAlign: "center" },
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("vrLotAttribute"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("vrLotAttribute", value);
        },
      };
    },
    cellRenderer: function (params: ICellRendererParams) {
      return Getter.convetCodeToNameByOptionData("vrLotAttribute", params.value);
    },
    editable: true,
    sortable: true,
  },
  {
    field: "LOC_CD",
    headerName: t("grid-column-name.location"),
    sortable: true,
  },
  {
    field: "LOC_TYPE",
    headerName: t("grid-column-name.location-Type"),
    sortable: true,
    width: 130,
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.owner"),
    sortable: true,
    width: 130,
  },
  {
    field: "RITEM_CD",
    headerName: t("grid-column-name.product-code"),
    sortable: true,
  },
  {
    field: "RITEM_NM",
    headerName: t("grid-column-name.product-name"),
    sortable: true,
  },
  {
    field: "STOCK_QTY",
    headerName: t("grid-column-name.qty"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 110,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "OUT_EXP_QTY",
    headerName: t("grid-column-name.schedule-quantity"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 130,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "WORK_QTY",
    headerName: t("grid-column-name.adjusted-quantity"),
    width: 110,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    editable: true,
    sortable: true,
  },
  {
    field: "OLD_ITEM_BEST_DATE_END",
    headerName: t("grid-column-name.expiration-date"),
    width: 140,
    cellStyle: { textAlign: "center" },
    cellEditor: "agDateStringCellEditor",
    cellEditorParams: {
      min: "0001-01-01",
      max: "9999-12-31",
    },
    valueFormatter: Format.YearMonthDay,
    editable: true,
    sortable: true,
  },
  {
    field: "OLD_MAKE_DT",
    headerName: t("grid-column-name.manufacturing-date-2"),
    width: 140,
    cellStyle: { textAlign: "center" },
    cellEditor: "agDateStringCellEditor",
    cellEditorParams: {
      min: "0001-01-01",
      max: "9999-12-31",
    },
    valueFormatter: Format.YearMonthDay,
    editable: true,
    sortable: true,
  },
  {
    field: "UNIT_NM",
    headerName: t("grid-column-name.qty-by-box"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 110,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "UOM_NM",
    headerName: t("grid-column-name.uom"),
    sortable: true,
    width: 100,
  },
  {
    field: "CUST_LOT_NO",
    headerName: t("grid-column-name.lot-number"),
    width: 170,
    sortable: true,
    editable: true,
  },
  {
    field: "ORD_ID_SEQ",
    headerName: t("grid-column-name.create-order-number-(order-number)"),
    width: 170,
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "CHANGE_MEMO",
    headerName: t("grid-column-name.change-history"),
    sortable: true,
    editable: true,
  },
  {
    field: "WH_NM",
    headerName: t("grid-column-name.warehouse"),
    sortable: true,
  },
  {
    field: "OLD_OWNER_CD",
    headerName: t("grid-column-name.new-owner-cd"),
    sortable: true,
    editable: true,
  },
];
