/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS096/column-defs.ts
 *  Description:    기준관리/상품별 원주자재(부품/용기관리) 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
----------------------------------------------------------------- -------------*/
import { i18n } from "@/i18n";

const { t } = i18n.global;
export const MODAL_COLUMN_DEFS: any = {
  //화주
  owner: [
    {
      field: "No",
      headerName: "No",
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerName: t("grid-column-name.shipper-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerName: t("grid-column-name.owner-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerName: t("grid-column-name.address"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerName: t("grid-column-name.company-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerName: t("grid-column-name.owner-epc-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerName: t("grid-column-name.tel"),
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  //주문번호
  {
    field: "ORD_VAL",
    headerKey: "order-number",
    width: 300,
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,

    pinned: "left",
  },
  //상품코드
  {
    field: "RITEM",
    headerKey: "product-code",
    width: 300,
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
  },
  //상품바코드
  {
    field: "ITEM_BAR",
    headerKey: "product-barcode",
    width: 400,
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
  },
  //상품명
  {
    field: "ITEMNM",
    headerKey: "product-name",
    width: 400,
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
  },
  //주문수
  {
    field: "OUT_ORD_CNT",
    headerKey: "order-count",
    width: 100,
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
  },
  //상품수
  {
    field: "OUT_ORD_QTY",
    headerKey: "product-qty",
    width: 100,
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
  },
  //로케이션
  {
    field: "LOC_ID_VAL",
    headerKey: "location",
    width: 150,
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
  },
];
