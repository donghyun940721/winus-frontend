/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS096/column-defs.ts
 *  Description:    기준관리/상품별 원주자재(부품/용기관리) 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format } from "@/lib/ag-grid/index";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  //화주
  owner: [
    {
      field: "No",
      headerName: "No",
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerName: t("grid-column-name.shipper-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerName: t("grid-column-name.owner-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerName: t("grid-column-name.address"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerName: t("grid-column-name.company-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerName: t("grid-column-name.owner-epc-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerName: t("grid-column-name.tel"),
      headerClass: "header-center",
      sortable: true,
    },
  ],
  //상품
  // product: [
  //   {
  //     field: "",
  //     headerKey: "no",
  //     headerName: "",
  //     width: 60,
  //     cellStyle: { textAlign: "center" },
  //     valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  //   },
  //   {
  //     field: "",
  //     headerKey: "",
  //     headerName: "",
  //     headerCheckboxSelection: true,
  //     checkboxSelection: true,
  //     width: 50,
  //   },
  //   {
  //     field: "CUST_NM",
  //     headerKey: "owner",
  //     headerName: "",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "ITEM_GRP_NAME",
  //     headerKey: "product-group",
  //     headerName: "",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "ITEM_CODE",
  //     headerKey: "product-code",
  //     headerName: "",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "ITEM_KOR_NM",
  //     headerKey: "product-name",
  //     headerName: "",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "BOX_BAR_CD",
  //     headerKey: "box-barcode",
  //     headerName: "",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "MAKER_NM",
  //     headerKey: "company-name",
  //     headerName: "",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "STOCK_QTY",
  //     headerKey: "current-stock",
  //     headerName: "",
  //     cellStyle: { textAlign: "right" },
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "BAD_QTY",
  //     headerKey: "inferior-product",
  //     headerName: "",
  //     cellStyle: { textAlign: "right" },
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "UOM_NM",
  //     headerKey: "uom",
  //     headerName: "",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "UNIT_PRICE",
  //     headerKey: "unit-price",
  //     headerName: "",
  //     cellStyle: { textAlign: "right" },
  //     valueFormatter: Format.NumberPrice,
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  //   {
  //     field: "WH_NM",
  //     headerKey: "warehouse",
  //     headerName: "",
  //     headerClass: "header-center",
  //     sortable: true,
  //   },
  // ],

  product: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node && params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "RITEM_ID",
      hide: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  //년도
  {
    field: "YYYY",
    headerKey: "year",
    headerName: "",
    cellStyle: { textAlign: "center" },
    width: 150,
    hide: false,
  },
  //월
  {
    field: "MM",
    headerKey: "month",
    cellStyle: { textAlign: "center" },

    headerName: "",
    width: 150,
  },
  //상품코드
  { field: "RITEM_CD", headerKey: "product-code", width: 200, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true },
  { field: "RITEM_NM", headerKey: "product-name", width: 400, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true },

  //출고량
  { field: "QTY", headerKey: "shipping-qty", width: 200, headerName: "", cellStyle: { textAlign: "right" }, sortable: true, export: true },
];
