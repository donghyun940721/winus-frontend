/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS093/input.ts
 *  Description:    상품별자동출고주문관리 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSCM011",
  pk: "CUST_ID",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "12" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd3",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm3",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product-code",
    title: "search-product",
    gridTitle: "product-list",

    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },

    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "viewAll",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        title: "product-code",
        searchContainerInputId: "vrSrchItemCd",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        title: "product-name",
        searchContainerInputId: "vrSrchItemNm",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        optionsKey: "S_SET_ITEM_YN",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
    ],
  },
};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  //기준
  {
    ids: ["vrSrchDate"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "standard",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    options: [
      { name: "year", nameKey: "year", value: "001" },
      { name: "month", nameKey: "month", value: "002" },
      { name: "quarterly", nameKey: "quarterly", value: "003" },
      { name: "by-specific-year-and-month", nameKey: "by-specific-year-and-month", value: "004" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 1,2,3 년도별 일자
    ids: ["vrSrchYearFrom", "vrSrchYearTo"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box", "select-box2"],
    hidden: false,
    options: [
      { name: "2024", nameKey: "2024", value: "2024" },
      { name: "2023", nameKey: "2023", value: "2023" },
      { name: "2022", nameKey: "2022", value: "2022" },
    ],
    options2: [
      { name: "2024", nameKey: "2024", value: "2024" },
      { name: "2023", nameKey: "2023", value: "2023" },
      { name: "2022", nameKey: "2022", value: "2022" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    optionsAutoSelected2: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },

  {
    // 2. 월별 일자
    ids: ["vrSrchMonthFrom", "vrSrchMonthTo"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box", "select-box2"],
    hidden: false,
    options: [
      { name: "1", nameKey: "january", value: "01" },
      { name: "2", nameKey: "febuary", value: "02" },
      { name: "3", nameKey: "march", value: "03" },
      { name: "4", nameKey: "april", value: "04" },
      { name: "5", nameKey: "may", value: "05" },
      { name: "6", nameKey: "june", value: "06" },
      { name: "7", nameKey: "july", value: "07" },
      { name: "8", nameKey: "agust", value: "08" },
      { name: "9", nameKey: "september", value: "09" },
      { name: "10", nameKey: "october", value: "10" },
      { name: "11", nameKey: "november", value: "11" },
      { name: "12", nameKey: "december", value: "12" },
    ],
    options2: [
      { name: "1", nameKey: "january", value: "01" },
      { name: "2", nameKey: "febuary", value: "02" },
      { name: "3", nameKey: "march", value: "03" },
      { name: "4", nameKey: "april", value: "04" },
      { name: "5", nameKey: "may", value: "05" },
      { name: "6", nameKey: "june", value: "06" },
      { name: "7", nameKey: "july", value: "07" },
      { name: "8", nameKey: "agust", value: "08" },
      { name: "9", nameKey: "september", value: "09" },
      { name: "10", nameKey: "october", value: "10" },
      { name: "11", nameKey: "november", value: "11" },
      { name: "12", nameKey: "december", value: "12" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    optionsAutoSelected2: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 3. 분기별 일자
    ids: ["vrSrchQuarter"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    hidden: false,
    options: [
      { name: "Q1", nameKey: "Q1", value: "Q1" },
      { name: "Q2", nameKey: "Q2", value: "Q2" },
      { name: "Q3", nameKey: "Q3", value: "Q3" },
      { name: "Q4", nameKey: "Q4", value: "Q4" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 특정년월별
    ids: ["vrSrchYearMonthFrom", "vrSrchYearMonthTo"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    hidden: false,
    types: ["month", "month"],
  },
  {
    // 직전 3개월 출고실적조회
    ids: ["chkOrdBf"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    checkboxLabelTitles: ["previous-three-months-shipping-search"],
    checkboxCheckedValue: ["on"],
    types: ["check-box"],
  },
  //화주
  {
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    // 상품군
    ids: ["vrSrchItemGrpId"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "product-group",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    optionsKey: "ITEMGRP",
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "N/A", nameKey: "N/A", value: "N/A" },
    ],
  },
  {
    // 상품
    ids: ["vrSrchItemCd", "vrSrchItemId"],
    hiddenId: "",
    rowDataIds: ["ITEM_CODE", "RITEM_ID"],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "ITEM",
    title: "product",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code"],
    types: ["text"],
    optionsKey: "ITEMGRP",
  },
  //조회기준
  {
    ids: ["vrSrchOrdGb"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "search-standard",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "DS_TP",
    options: [
      { name: "shipping-qty", nameKey: "shipping-qty", value: "SUM" },
      { name: "shipping-count", nameKey: "shipping-count", value: "COUNT" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useMore: true,
  useSetting: false, // TODO :: 위치 조정 이 후, 재설정 (TRUE)
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  searchConditionInitUrl: "",
};
