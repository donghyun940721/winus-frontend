/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST057_1/input.ts
 *  Description:    일별재고상세(NEW) 입력정보 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/

import type { IModal, ISearchInput, info } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSMS011",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 로케이션
    ids: ["vrSrchcmbLocList"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "location",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    options: [{ name: "all", nameKey: "all", value: "" }],
    optionsKey: "vrSrchcmbLocList",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    types: ["select-box"],
  },
  {
    // 로케이션 시작위치
    ids: ["vrSrchPoint"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "location-start-point",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    options: [{ name: "LEFT↗", nameKey: "LEFT↗", value: "" }],
    optionsKey: "vrSrchcmbLocList",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    types: ["select-box"],
  },
  // Zone마우스오버
  {
    ids: ["chkZoneOver", "chkAutoRefresh", "vrSrchAutoSec"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    checkboxLabelTitles: ["zone-mouse-over", "auto-refresh", ""],
    checkboxCheckedValue: ["checked", "checked"],
    types: ["check-box", "check-box", "select-box"],
    options: [{ name: "10sec", nameKey: "10sec", value: "10" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    // 물류용기EPC 코드
    ids: ["vrSrchEpcCd"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "logistics-container-EPC-code",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
  },
  {
    ids: ["vrSrchHexGb"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    checkboxLabelTitles: ["zone-mouse-over", "auto-refresh", ""],
    checkboxCheckedValue: ["selected", "selected"],
    types: ["text", "select-box"],
    options: [{ name: "decimal-number", nameKey: "decimal-number", value: "10" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  //전체
  {
    ids: ["vrSrchErrCntView"],
    hiddenId: "vrSrchSerialId",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "total",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  useMore: true,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
};
