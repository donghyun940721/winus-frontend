/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSST057_1/column-defs.ts
 *  Description:    일별재고상세(NEW) 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.06. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";
export const MODAL_COLUMN_DEFS: any = {};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },

    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // LOT_NO
    field: "CUST_LOT_NO",
    headerKey: "LOT_NO",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "center" },
  },
  {
    // (QR)
    field: "CUST_LOT_NO_QR",
    headerKey: "(QR)",
    headerName: "",
    export: true,
    sortable: true,
    width: 35,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
  },
  {
    // 로케이션
    field: "LOC_CD",
    headerKey: "location",
    headerName: "",
    export: true,
    sortable: true,
    width: 70,
    cellStyle: { textAlign: "center" },
  },
  {
    // EPC코드
    field: "RTI_EPC_CD",
    headerKey: "EPC-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 220,
    cellStyle: { textAlign: "center" },
  },
  {
    // HEX
    field: "RTI_EPC_CD_HEX",
    headerKey: "HEX",
    headerName: "",
    export: true,
    sortable: true,
    width: 50,
    cellStyle: { textAlign: "left" },
  },
  {
    // 창고명
    field: "WH_NM",
    headerKey: "warehouse-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "left" },
  },
  {
    // 상품코드
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
  },
  {
    // 상품명
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 180,
    cellStyle: { textAlign: "left" },
  },
  {
    // 화주명
    field: "CUST_NM",
    headerKey: "owner-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 140,
    cellStyle: { textAlign: "left" },
  },
  {
    // 화주코드
    field: "CUST_CD",
    headerKey: "owner-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "left" },
  },
  {
    // UOM
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품수량
    field: "STOCK_QTY",
    headerKey: "product-qty",
    headerName: "",
    export: true,
    sortable: true,
    width: 70,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
  },
  {
    // 적치율
    field: "LOC_PER",
    headerKey: "stacking-rate",
    headerName: "",
    export: true,
    sortable: true,
    width: 70,
    cellStyle: { textAlign: "right" },
  },
  {
    // 등록일
    field: "RECEIVING_DT",
    headerKey: "registration-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 로케이션바코드
    field: "LOC_BARCODE_CD",
    headerKey: "location-barcode",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  {
    // 창고코드
    field: "WH_CD",
    headerKey: "warehousing-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
  },
  {
    // 상품별 ZONE
    field: "IN_ZONE_NM",
    headerKey: "product-by-zone",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "left" },
  },
];
