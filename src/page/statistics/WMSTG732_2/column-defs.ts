/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS096/column-defs.ts
 *  Description:    기준관리/상품별 원주자재(부품/용기관리) 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format } from "@/lib/ag-grid/index";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  //화주
  owner: [
    {
      field: "No",
      headerName: "No",
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerName: t("grid-column-name.shipper-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerName: t("grid-column-name.owner-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerName: t("grid-column-name.address"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerName: t("grid-column-name.company-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerName: t("grid-column-name.owner-epc-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerName: t("grid-column-name.tel"),
      headerClass: "header-center",
      sortable: true,
    },
  ],
  //상품
  product: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "RITEM_ID",
      hide: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      valueFormatter: Format.NumberPrice,
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 100,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  //상품코드
  {
    field: "RITEM_CD",
    headerKey: "product-code",
    width: 250,
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    lockVisible: true,
    lockPosition: true,
  },
  //상품명
  { field: "RITEM_NM", headerKey: "product-name", cellStyle: { textAlign: "center" }, width: 350 },

  //로케이션명
  { field: "LOC_CD", headerKey: "location-name", width: 350, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true },
  //재고수량
  {
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 250,
    valueFormatter: Format.NumberPrice,
  },
];
