/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	{ 상위폴더 } / { 파일명 }
 *  Description:    { 내용 }
 *  Authors:        { 작성자 }
 *  Update History:
 *                  { 날짜 (년.월.) } : Created by {작성자}
 *
------------------------------------------------------------------------------*/
import { IControlBtn, IGridCellSelectBox } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "lot-stock-identification-label-print",
    colorStyle: "success",
    paddingStyle: "normal",
    authType: "INS_AUTH",
    image: "",
  },
  {
    title: "shipping-identification-label-print",
    colorStyle: "success",
    paddingStyle: "normal",
    authType: "INS_AUTH",
    image: "",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    authType: "EXC_AUTH",
    image: "excel",
  },
  {
    title: "registration-shipping-order",
    colorStyle: "primary",
    paddingStyle: "normal",
    authType: "INS_AUTH",
    image: "",
  },
  {
    title: "save",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
    disabled: "",
  },
  {
    title: "delete-lot",
    colorStyle: "danger",
    paddingStyle: "normal",
    image: "",
    authType: "",
    disabled: "",
  },
];

export const MAIN_CELL_RENDERER_INFO: IGridCellSelectBox = {
  LOCK_YN: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "useProductY", value: "Y" },
      { name: "", nameKey: "useProductN", value: "N" },
    ],
  },
};

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [1000, 2000, 3000, 5000, 10000, 100000],
};

export const gridOptionsMeta = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
};
