/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSTG070/column-defs.ts
 *  Description:    통계/LOT별재고현황 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import { UtilService } from "@/services/util-service";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
import type { ValueGetterParams } from "ag-grid-community";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerName: t("grid-column-name.shipper-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerName: t("grid-column-name.owner-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerName: t("grid-column-name.address"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerName: t("grid-column-name.manager-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerName: t("grid-column-name.owner-epc-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerName: t("grid-column-name.tel"),
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  location: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerName: t("grid-column-name.location"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerName: t("grid-column-name.product"),
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerName: t("grid-column-name.product-code") },

        { field: "RITEM_NM", columnGroupShow: "open", headerName: t("grid-column-name.product-name") },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerName: t("grid-column-name.stock-quantity"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerName: t("grid-column-name.uom"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerName: t("grid-column-name.schedule-quantity"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerName: t("grid-column-name.plt-quantity"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerName: t("grid-column-name.weight"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerName: t("grid-column-name.lot-number"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerName: t("grid-column-name.expiration-date"),
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerName: t("grid-column-name.warehouse-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerName: t("grid-column-name.warehouse-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerName: t("grid-column-name.warehouse-category"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerName: t("grid-column-name.warehouse-type"),
      headerClass: "header-center",
      sortable: true,
    },
  ],
  product: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerName: t("grid-column-name.owner"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerName: t("grid-column-name.product-group"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerName: t("grid-column-name.product-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerName: t("grid-column-name.product-name"),
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerName: t("grid-column-name.box-barcode"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerName: t("grid-column-name.company-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerName: t("grid-column-name.current-stock"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerName: t("grid-column-name.inferior-product"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerName: t("grid-column-name.uom"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerName: t("grid-column-name.unit-price"),
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerName: t("grid-column-name.warehouse"),
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "logistics-container": [
    {
      field: "",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "POOL_GRP_NAME",
      headerName: t("grid-column-name.pool-grp-id"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerName: t("grid-column-name.pool-cd"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerName: t("grid-column-name.pool-nm"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerName: t("grid-column-name.stock"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerName: t("grid-column-name.inferior-product"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerName: t("grid-column-name.unit-price"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerName: t("grid-column-name.warehouse"),
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    menuTabs: ["columnsMenuTab"],
    lockPosition: true,
    lockVisible: true,
    pinned: true,
    valueGetter: (params: any) => {
      if (typeof params.data == "object" && Object.hasOwn(params.data, "RNUM")) {
        return params.node.rowIndex + 1;
      } else {
        return "";
      }
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "FLAG",
    headerName: "",
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    lockPosition: true,
    lockVisible: true,
    pinned: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "CUST_LOT_NO",
    headerName: t("grid-column-name.lot-number"),
    // cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
  },
  {
    field: "LOCK_YN",
    headerName: t("grid-column-name.lock"),
    width: 80,
    sortable: true,
    editable: true,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    cellEditor: "agRichSelectCellEditor",
    cellEditorParams: {
      values: ["Y", "N"],
      cellRenderer: GridCircleIconForYn,
      cellEditorPopup: false,
    },
    // export: true,
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.owner"),
    sortable: true,
    // export: true,
  },
  {
    field: "WH_NM",
    headerName: t("grid-column-name.warehouse"),
    sortable: true,
    // export: true,
  },
  {
    field: "LOC_CD",
    headerName: t("grid-column-name.location"),
    sortable: true,
    // export: true,
  },
  {
    field: "LOC_TYPE",
    headerName: t("grid-column-name.type"),
    sortable: true,
    width: 80,
    valueGetter: (params: ValueGetterParams) => {
      return Getter.convetCodeToNameByOptionData("LocType", params);
    },
  },
  {
    field: "RITEM_CD",
    headerName: t("grid-column-name.product-code"),
    sortable: true,
    // export: true,
  },
  {
    field: "RITEM_NM",
    headerName: t("grid-column-name.product-name"),
    sortable: true,
    width: 250,
    // export: true,
  },
  {
    field: "ITEM_BAR_CD",
    headerName: t("grid-column-name.product-barcode"),
    sortable: true,
  },
  {
    field: "STOCK_QTY",
    headerName: t("grid-column-name.qty"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    // export: true,
    width: 130,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "STOCK_WEIGHT",
    headerName: t("grid-column-name.weight-2"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    // export: true,
    width: 130,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "UNIT_NM",
    headerName: t("grid-column-name.qty-by-box"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 110,
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    field: "UOM_NM",
    headerName: t("grid-column-name.uom"),
    sortable: true,
    // export: true,
    width: 90,
  },
  //엑셀 다운로드 시 컬럼명 '입고처코드' / 실제 데이터는 '납품처코드' 값
  {
    field: "OWNER_CD",
    headerName: t("grid-column-name.warehousing-destination-code"),
    sortable: true,
    // export: true,
    hide: true,
  },
  {
    field: "MAKE_DT",
    headerName: t("grid-column-name.manufacturing-date-2"),
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 110,
  },
  {
    field: "ITEM_BEST_DATE_END",
    headerName: t("grid-column-name.validity"),
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 110,
  },
  {
    field: "OWNER_CD",
    headerName: t("grid-column-name.delivery-destination-code"),
    // cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter") {
        return true;
      } else {
        return false;
      }
    },
    editable: true,
    sortable: true,
  },
  {
    field: "UNIT_NO",
    headerName: t("grid-column-name.unit-no"),
    sortable: true,
    // export: true,
  },
  {
    field: "CNTR_NO",
    headerName: t("grid-column-name.container-number"),
    sortable: true,
    // export: true,
  },
  {
    field: "BOX_BAR_CD",
    headerName: t("grid-column-name.box-barcode"),
    sortable: true,
    // export: true,
    hide: true,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.creation-date-2"),
    sortable: true,
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "RNUM")) {
        return UtilService.convertTimestampToDateFormat(params.data.REG_DT);
      } else {
        return "";
      }
    },
    cellStyle: { textAlign: "center" },
    // export: true,
  },
  {
    field: "LOC_ID",
    headerName: "",
    sortable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "UOM_ID",
    sortable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "CUST_ID",
    headerName: "",
    sortable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "WH_ID",
    headerName: "",
    sortable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "GRADE",
    headerName: "",
    sortable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "STOCK_ID",
    headerName: "",
    sortable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "SUB_LOT_ID",
    headerName: "",
    sortable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "CUST_CD",
    headerName: "",
    sortable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "WH_CD",
    headerName: "",
    sortable: true,
    hide: true,
    initialHide: true,
    suppressColumnsToolPanel: true,
  },
];

export const gridColumns = {
  FORM_COLUMN_DEFS: FORM_COLUMN_DEFS,
};
