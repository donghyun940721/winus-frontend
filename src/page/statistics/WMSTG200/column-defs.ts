/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSTG200/input.grid.ts
 *  Description:    LOT별이력추적 grid meta 정보
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.07. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  "logistics-container": [
    {
      field: "",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "POOL_GRP_NAME",
      headerName: t("grid-column-name.pool-grp-id"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerName: t("grid-column-name.pool-cd"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerName: t("grid-column-name.pool-nm"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerName: t("grid-column-name.stock"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerName: t("grid-column-name.inferior-product"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerName: t("grid-column-name.unit-price"),
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerName: t("grid-column-name.warehouse"),
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const NEW_TOP_GRID_COL_DEF = [
  {
    field: "NO",
    headerKey: "no",
    headerName: "",
    width: 60,
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 50,
  },
  {
    field: "PDA_NO",
    headerKey: "pda-number",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  {
    field: "ORD_ID",
    headerKey: "order-id",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  {
    field: "RTI_EPC_CD",
    headerKey: "pool-cd",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 280,
  },
  {
    field: "CUST_NM",
    headerKey: "owner-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 100,
  },

  {
    field: "CHILD_QTY",
    headerKey: "mapping-quantity",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "right" },
  },
  {
    field: "REG_DT",
    headerKey: "reg-dt",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 170,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.Date,
  },
  {
    field: "REG_NO",
    headerKey: "reg-no",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  {
    field: "UPD_DT",
    headerKey: "date-modified",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 170,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.Date,
  },
  {
    field: "UPD_NO",
    headerKey: "modifier",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  {
    field: "EVENT_DT",
    headerKey: "creation-date",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 170,
    valueGetter: Getter.Date,
  },
  {
    field: "RITEM_ID",
    hide: true,
  },
];
export const NEW_BOTTOM_GRID_COL_DEF = [
  {
    field: "NO",
    headerKey: "no",
    headerName: "",
    width: 60,
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 50,
  },
  {
    field: "PDA_NO",
    headerKey: "pda-number",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  {
    field: "ORD_ID",
    headerKey: "order-id",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  {
    field: "RTI_EPC_CD",
    headerKey: "pool-cd",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 280,
  },
  {
    field: "CUST_NM",
    headerKey: "owner-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 100,
  },

  {
    field: "CHILD_QTY",
    headerKey: "mapping-quantity",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 90,
    cellStyle: { textAlign: "right" },
  },
  {
    field: "REG_DT",
    headerKey: "reg-dt",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 170,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.Date,
  },
  {
    field: "REG_NO",
    headerKey: "reg-no",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  {
    field: "UPD_DT",
    headerKey: "date-modified",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 170,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.Date,
  },
  {
    field: "UPD_NO",
    headerKey: "modifier",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  {
    field: "EVENT_DT",
    headerKey: "creation-date",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 170,
    valueGetter: Getter.Date,
  },
  {
    field: "RITEM_ID",
    hide: true,
  },
];

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "STATE") && params.data.STATE === "C") {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    lockPosition: true,
  },
  //LOT번호
  {
    field: "CUST_LOT_NO",
    headerKey: "lot-number",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 150,
  },
  //상품코드
  {
    field: "CUST_NM",
    headerKey: "product-code",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  //EPC코드
  {
    field: "EPC_CD",
    headerKey: "EPC-code",
    headerName: "",
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    sortable: true,
    width: 300,
  },
  //SEQ16
  {
    field: "SEQ16",
    headerKey: "SEQ16",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
  },
  //상품코드
  {
    field: "RITEM_CD",
    headerKey: "item-code",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  //상태
  {
    field: "WORK_STAT",
    headerKey: "status",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 40,
    valueGetter: (params: any) => {
      const WORK_STAT_valueGetterArray: any[] = [{ 100: "정상" }, { 101: "매핑" }, { 102: "매핑해제" }, { 490: "피킹" }, { 900: "폐기" }, { 990: "출고" }];
      for (let i = 0; WORK_STAT_valueGetterArray.length > i; i++) {
        const key: any = Object.keys(WORK_STAT_valueGetterArray[i])[0];
        if (params.data?.WORK_STAT === key) {
          return WORK_STAT_valueGetterArray[i][key];
        }
      }
    },
  },
  //생성일자
  {
    field: "REG_DT",
    headerKey: "creation-date-2",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    width: 180,
    valueFormatter: Format.Date,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "STATE") && params.data.STATE === "C") {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    lockPosition: true,
  },
  //EPC코드
  {
    field: "",
    headerKey: "EPC-code",
    headerName: "",
    headerClass: "header-left",
    sortable: true,
    cellStyle: { textAlign: "left" },
    children: [
      { field: "TAG_COMPANY_CD", columnGroupShow: "open", headerKey: "company", headerName: "", cellStyle: { textAlign: "left" }, width: 100 },
      { field: "TAG_PRODUCT_CD", columnGroupShow: "open", headerKey: "product", headerName: "", cellStyle: { textAlign: "left" }, width: 80 },
      { field: "TAG_SERIAL_CD", columnGroupShow: "open", headerKey: "serial", headerName: "", cellStyle: { textAlign: "left" }, width: 80 },
    ],
  },
  //물류센터
  {
    field: "LC_NM",
    headerKey: "logistic-center",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    width: 200,
  },
  //입고
  {
    field: "",
    headerKey: "income",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    children: [
      { field: "IN_DATE", columnGroupShow: "open", headerKey: "income-date", headerName: "", cellStyle: { textAlign: "left" }, width: 170, valueFormatter: Format.Date },
      { field: "IN_GATE_CD", columnGroupShow: "open", headerKey: "income-GATE-code", headerName: "", cellStyle: { textAlign: "center" }, width: 130 },
    ],
  },
  //출고
  {
    field: "OUT_DATE",
    headerKey: "shipping",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    children: [
      { field: "OUT_DATE", columnGroupShow: "open", headerKey: "shipping-date", headerName: "", cellStyle: { textAlign: "left" }, width: 170, valueFormatter: Format.Date },
      { field: "OUT_GATE_CD", columnGroupShow: "open", headerKey: "shipping-GATE-code", headerName: "", cellStyle: { textAlign: "center" }, width: 130 },
    ],
  },
  //이동시간
  {
    field: "MOVE_DATE",
    headerKey: "moving-time",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 150,
  },
  //체류시간
  {
    field: "STAY_DATE",
    headerKey: "time-of-stay",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 150,
  },
  //매핑번호RCV
  {
    field: "MAPPING_WORK_SEQ_RCV",
    headerKey: "mapping-number-RCV",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 130,
  },
  //매핑번호SND
  {
    field: "MAPPING_WORK_SEQ_SND",
    headerKey: "mapping-number-SND",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 130,
  },
];
