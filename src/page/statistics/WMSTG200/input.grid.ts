/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSTG200/input.grid.ts
 *  Description:    LOT별이력추적 grid meta 정보
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.07. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { IControlBtn, IGridCellSelectBox, IGridStatisticsInfo } from "@/types";
import { GridOptions } from "ag-grid-community";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const GRID_SELECT_BOX_INFO: IGridCellSelectBox = {};
export const MAIN_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [];
export const SUB_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [];
// done
export const MASTER_CONTROL_BTN: IControlBtn[] = [
  {
    title: "save",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "INS_AUTH",
  },
];
export const DETAIL_CONTROL_BTN: IControlBtn[] = [];

export const MasterGridMetaData: any = {
  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "",

  //페이징옵션
  pagingSizeList: [],
};

export const DetailGridMetaData: any = {
  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "",

  //페이징옵션
  pagingSizeList: [],
};

export const popupGridMetaData: any = {
  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "",

  //페이징옵션
  pagingSizeList: [100, 200, 300],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  masterColumnDefs: MASTER_GRID_COLUMN_DEFS,
  detailColumnDefs: DETAIL_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  getRowId: (data) => {
    return data.data.RNUM;
  },
};
