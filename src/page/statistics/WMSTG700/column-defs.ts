/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSTG700/column-defs.ts
 *  Description:    배차정보 조회 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    // ORD_TYPE
    field: "ORD_TYPE",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 주문 유형
    field: "ORD_TYPE_NM",
    headerKey: "order-type",
    headerName: "",
    export: true,
    sortable: true,
    width: 70,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    // 주문 번호
    field: "ORD_ID",
    headerKey: "order-id",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "center" },
    pinned: "left",
  },
  {
    // 원 주문 번호
    field: "ORG_ORD_ID",
    headerKey: "original-order-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "center" },
  },
  {
    // 원 주문 번호 SEQ
    field: "ORG_ORD_SEQ",
    headerKey: "original-order-number-seq",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "center" },
  },
  {
    // 입출고일(예정일)
    field: "WORK_DT",
    headerKey: "inbound-outbound-date",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // 상품 코드
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "left" },
  },
  {
    // 상품명
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 250,
    cellStyle: { textAlign: "left" },
  },
  {
    // 주문 수량
    field: "ORD_QTY",
    headerKey: "ord-qty",
    headerName: "",
    export: true,
    sortable: true,
    width: 120,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 라우팅
    field: "DSP_ROUT",
    headerKey: "routing",
    headerName: "",
    export: true,
    sortable: true,
    width: 250,
    cellStyle: { textAlign: "center" },
  },
  {
    // 차량 톤수
    field: "CAR_TON",
    headerKey: "ton-of-vehicle",
    headerName: "",
    export: true,
    sortable: true,
    width: 70,
    cellStyle: { textAlign: "center" },
  },
  {
    // 기사명
    field: "DRV_NM",
    headerKey: "driver-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
  },
  {
    // 기사 전화번호
    field: "DRV_TEL",
    headerKey: "driver-phone-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "center" },
  },
  {
    // 차량 번호
    field: "CAR_CD",
    headerKey: "vehicle-number",
    headerName: "",
    export: true,
    sortable: true,
    width: 200,
    cellStyle: { textAlign: "center" },
  },
  {
    // 거래처명
    field: "TRANS_CUST_NM",
    headerKey: "customer-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 250,
    cellStyle: { textAlign: "center" },
  },
  {
    // 비용 지불 주체
    field: "DSP_PAY_CHARGE",
    headerKey: "payment-party",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  {
    // PLT 수량
    field: "EXP_PLT_QTY",
    headerKey: "plt-quantity",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 매입
    field: "UP_AMT",
    headerKey: "purchase-amount",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 매출
    field: "DOWN_AMT",
    headerKey: "sales-amount",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 수작업 비용
    field: "ETC_AMT",
    headerKey: "manual-operation-cost",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 경유 비용
    field: "TRANS_AMT",
    headerKey: "detour-cost",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 대기 비용
    field: "WAIT_AMT",
    headerKey: "waiting-cost",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 엘리베이터 이용
    field: "DEVICE_AMT",
    headerKey: "elevator-use",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 청구 비용
    field: "SUM_AMT",
    headerKey: "total-billed",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  {
    // 메모
    field: "DSP_MEMO",
    headerKey: "memo-1",
    headerName: "",
    export: true,
    sortable: true,
    width: 300,
    cellStyle: { textAlign: "left" },
  },
  {
    // 종료
    field: "END",
    headerKey: "end",
    headerName: "",
    export: true,
    sortable: true,
    width: 100,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
];
