/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSTG733_2/column-defs.ts
 *  Description:    통계관리/출고검수조회_시리얼조회 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  product: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.commonRowIndex,
    pinned: "left",
  },
  {
    field: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
  },
  {
    field: "ORD_ID",
    headerName: t("grid-column-name.order-id"),
    cellStyle: { textAlign: "right" },
    cellClass: "stringType",
    sortable: true,
    width: 130,
  },
  {
    field: "ORD_SEQ",
    headerName: t("grid-column-name.order-seq"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 90,
  },
  {
    field: "RITEM_NM",
    headerName: t("grid-column-name.product-name"),
    sortable: true,
    minWidth: 200,
    flex: 1,
  },
  {
    field: "UOM_NM",
    headerName: t("grid-column-name.unit-box"),
    sortable: true,
    width: 90,
  },
  {
    field: "POOL_NM",
    headerName: t("grid-column-name.delivery-box"),
    sortable: true,
  },
  {
    field: "CONF_BOX_NO",
    headerName: t("grid-column-name.box-group-number"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 130,
  },
  {
    field: "SERIAL_NO",
    headerName: t("grid-column-name.serial-number"),
    sortable: true,
  },
  {
    field: "ORD_QTY",
    headerName: t("grid-column-name.ord-qty"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 110,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "CONF_QTY",
    headerName: t("grid-column-name.recommended-quantity"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 110,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.reg-dt"),
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 170,
  },
  {
    field: "REG_NO",
    headerName: t("grid-column-name.registrar"),
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 130,
  },
  {
    field: "WORK_IP",
    headerName: t("grid-column-name.worker-IP"),
    sortable: true,
    width: 150,
  },
  {
    field: "INVC_DEL_YN",
    headerName: t("grid-column-name.delete-invoice-Y/N"),
    sortable: true,
    cellRenderer: GridCircleIconForYn,
    cellClass: "renderer-cell",
    cellStyle: { textAlign: "center" },
    width: 140,
  },
];
