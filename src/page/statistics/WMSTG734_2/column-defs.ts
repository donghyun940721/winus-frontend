/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS096/column-defs.ts
 *  Description:    기준관리/상품별 원주자재(부품/용기관리) 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";

export const MODAL_COLUMN_DEFS: any = {
  //화주
  owner: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  //상품
  "product-code": [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      valueFormatter: Format.NumberPrice,
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "",
    headerKey: "",
    headerName: "",
    width: 50,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 35,
  },
  // {
  //   field: "FLAG",
  //   headerKey: "",
  //   headerName: "",
  //   hide: true,
  // },

  //상품코드
  { field: "ITEM_CODE", headerKey: "product-code", width: 150, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true },
  //상품명
  { field: "PART_RITEM_NM", headerKey: "product-name", width: 350 },
  //UOM
  { field: "UOM_NM", headerKey: "uom", width: 50, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true },
  //주문수량
  { field: "MAKE_QTY", headerKey: "make-qty", width: 150, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true },
  //보충대상로케이션재고
  { field: "TARGET_STOCK_QTY", headerKey: "target-stock-qty", width: 180, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true },
  //재고-주문
  { field: "CVT_QTY", headerKey: "stock-order", headerName: "", cellStyle: { textAlign: "center" }, width: 150, sortable: true, export: true },
  //보충대상로케이션
  { field: "TARGET_LOC_CD", headerKey: "target-loc-cd", headerName: "", cellStyle: { textAlign: "center" }, width: 180, sortable: true, export: true },
  //UOM_ID
  { field: "UOM_ID", headerKey: "uom-id", width: 130, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true, hide: true },
  //PART_RITEM_ID
  { field: "PART_RITEM_ID", headerKey: "part-ritem-id", width: 50, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true, hide: true },
  { field: "RANKV", sortable: true, hide: true },
  //피킹로케이션재고
  { field: "STOCK_QTY", headerKey: "picking-location-stock-qty", width: 130, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true, hide: true },
  //보충대상로케이션재고
  { field: "TARGET_STOCK_QTY", headerKey: "target-stock-qty", width: 200, headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true, hide: true },
  { field: "RANKV", sortable: true, hide: true },
  //송장번호
  { field: "STOCK_QTY", headerKey: "target-stock-qty", hide: true },
  //택배사
  { field: "AF_STOCK_QTY", headerKey: "target-stock-qty", hide: true },
  //주문수량
  { field: "TARGET_CUST_LOT_NO", headerKey: "target-stock-qty", hide: true },
  { field: "TARGET_WORK_DT", headerKey: "target-work-dt", hide: true },
  { field: "FST_ROW", headerKey: "target-stock-qty", hide: true },
  { field: "LOC_SEQ", headerKey: "target-stock-qty", hide: true },
  { field: "CVT_QTY", headerKey: "target-stock-qty", hide: true },
  { field: "COL_END", headerKey: "target-stock-qty", hide: true },
  { field: "COL_END_1", headerKey: "target-stock-qty", hide: true },
  { field: "COL_END_2", headerKey: "target-stock-qty", hide: true },
];
