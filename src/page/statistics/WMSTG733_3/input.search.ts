/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM120/input.ts
 *  Description:    주문/입고주문관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { IModal, ISearchInput } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

// export const INFO: info = {
//   autoModal: true,
//   autoModalPage: "WMSMS011",
//   pk: "INVC_NO",
// };
export const NEW_POPUP_CONTROL_BTN = [];

export const NEW_POPUP_SEARCH_MODAL_INFO: any = {};

export const NEW_POPUP_SEARCH_INPUT: any = [];
export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    //모달에서 접속할 페이지
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      //모달에서 검색했을 때 전달되는 파라미터들
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
        options: [{ name: "all", nameKey: "all", value: "ALL" }],
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd_T3",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm_T3",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },

  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },

    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        func: "fn_setWMSCM091_T3",
        vrSrchCustId: "",
        vrSrchWhId: "",
        vrViewSetItem: "",
        vrViewAll: "Y",
        vrItemType: "",
        RITEM_ID: "",
        ITEM_CODE: "",
        ITEM_KOR_NM: "",
        UOM_ID: "",
        TIME_PERIOD_DAY: "",
        STOCK_QTY: "",
        CUST_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        UNIT_PRICE: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchItemCd_T3",
        title: "product-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchItemNm_T3",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [{ name: "all", nameKey: "all", value: "" }],
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        //임가공 select-box값
        options: [
          { name: "", nameKey: "all", value: "" },
          { name: "useProductY", nameKey: "useProductY", value: "Y" },
          { name: "useProductN", nameKey: "useProductN", value: "N" },
        ],
      },
    ],
  },
  "box-type": {
    page: "WMSPL010",
    id: "box-type",
    title: "search-logistics-container",
    gridTitle: "product-list",

    apis: {
      url: "/WMSPL010/list_rn.action",
      params: {
        func: "fn_setWMSCM160_T3",
        POOL_CODE: "",
        POOL_ID: "",
        POOL_NM: "",
        POOL_GRP_ID: "",
        POOL_SIZE: "",
        UNIT_PRICE: "",
        IN_WH_ID: "",
        IN_ZONE_ID: "",
        RITEM_ID: "",
        S_POOL_CODE: "",
        S_POOL_NM: "",
        S_POOL_GRP_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_POOL_CODE",
        searchContainerInputId: "POOL_CODE",
        title: "logistics-container-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_POOL_NM",
        title: "logistics-container-name",
        type: "text",
        width: "half",
      },
      {
        id: "S_POOL_GRP_ID",
        title: "logistics-container-group",
        type: "select",
        width: "half",
        optionsKey: "POOLGRP",
        options: [{ name: "all", nameKey: "all", value: "" }],
      },
    ],
  },
};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchReqDtFrom3"],
    rowDataIds: [""],
    searchApiKeys: [""],
    rowDataHiddenId: "",
    hiddenId: "",
    srchKey: "",
    title: "order-date",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date"],
  },
  {
    ids: ["vrSrchCustCd_T3", "vrSrchCustNm_T3"],
    hiddenId: "vrSrchCustId_T3",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchItemCd_T3", "vrSrchItemNm_T3"],
    hiddenId: "vrSrchItemId_T3",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    searchApiKeys: ["ITEM_KOR_NM", "ITEM_CODE"],
    rowDataHiddenId: "RITEM_ID",
    srchKey: "ITEM",
    title: "product",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["PACKING_BOX_TYPE_NM_T3"],
    rowDataIds: ["POOL_CODE"],
    searchApiKeys: ["PACKING_BOX_TYPE_NM_T3"],
    rowDataHiddenId: "POOL_ID",
    hiddenId: "PACKING_BOX_TYPE_T3",
    srchKey: "",
    title: "box-type",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text"],
  },
  {
    ids: ["vrSrchOrdId_T3"],
    rowDataIds: [""],
    searchApiKeys: [""],
    rowDataHiddenId: "",
    hiddenId: "",
    srchKey: "",
    title: "order-number",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

//done
export const SEARCH_COMPONENT_CONTROL_BTN = [];

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useSetting: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchComponentControlBtn: SEARCH_COMPONENT_CONTROL_BTN,
  readOnlyInputs: [],
  readOnlyResults: [],
};
