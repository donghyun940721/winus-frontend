/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM120/input.ts
 *  Description:    주문/입고주문관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
/**
 ********************* Grid Area *********************/
import { IControlBtn, IGridCellSearchButton } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    authType: "EXC_AUTH",
  },
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
];

export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  POOL: {
    fieldList: ["POOL_CODE", "POOL_ID"],
    rowDataKeys: ["POOL_CODE", "POOL_ID"],
    modalData: {
      page: "WMSPL010",
      id: "box-type",
      title: "search-logistics-container",
      gridTitle: "product-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달

      defaultParamsData: {
        //해당 모달의 title
        storeSaveKey: "owner",
        paramsKeys: ["vrSrchCustId"],
        rowDataKeys: ["CUST_ID"],
      },
      apis: {
        url: "/WMSPL010/list_rn.action",
        params: {
          func: "fn_setWMSCM160_T3",
          POOL_CODE: "",
          POOL_ID: "",
          POOL_NM: "",
          POOL_GRP_ID: "",
          POOL_SIZE: "",
          UNIT_PRICE: "",
          IN_WH_ID: "",
          IN_ZONE_ID: "",
          RITEM_ID: "",
          S_POOL_CODE: "",
          S_POOL_NM: "",
          S_POOL_GRP_ID: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "S_POOL_CODE",
          searchContainerInputId: "POOL_CODE",
          title: "logistics-container-code",
          type: "text",
          width: "half",
        },
        {
          id: "S_POOL_NM",
          title: "logistics-container-name",
          type: "text",
          width: "half",
        },
        {
          id: "S_POOL_GRP_ID",
          title: "logistics-container-group",
          type: "select",
          width: "half",
          optionsKey: "POOLGRP",
        },
      ],
    },
    colDef: [
      {
        field: "No",
        headerKey: "no",
        headerName: "",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "POOL_GRP_NAME",
        headerKey: "pool-grp-id",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "POOL_CODE",
        headerKey: "pool-grp-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "POOL_NM",
        headerKey: "pool-nm",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "POOL_SIZE",
        headerKey: "pool-size",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "UNIT_PRICE",
        headerKey: "unit-price",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "IN_WH_CD",
        headerKey: "warehouse",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "IN_ZONE_NM",
        headerKey: "receiving-zone",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "IN_WH_ID",
        headerKey: "receiving-zone",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
      {
        field: "IN_ZONE_ID",
        headerKey: "in-zone-nm",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
      {
        field: "POOL_ID",
        headerKey: "in-zone-nm",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
      {
        field: "RITEM_ID",
        headerKey: "in-zone-nm",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
    ],
  },
};

export const GRID_STATISTICS_INFO = [];

export const gridMetaData: any = {
  //페이지 키
  // checkBoxColumn: "check-box-column",
  //페이징옵션
  pagingSizeList: [],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  suppressRowClickSelection: true,
  enableRangeSelection: true,
  suppressContextMenu: true,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  allowContextMenuWithControlKey: true,
  suppressPaginationPanel: true,
  context: {
    ...GRID_CELL_SEARCH_BUTTON,
  },
};
