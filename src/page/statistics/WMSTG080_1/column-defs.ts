/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS096/column-defs.ts
 *  Description:    기준관리/상품별 원주자재(부품/용기관리) 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";

export const MODAL_COLUMN_DEFS: any = {
  //화주
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  //상품
  product: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      valueFormatter: Format.NumberPrice,
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "",
    headerKey: "",
    headerName: "No",
    width: 50,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  //상품코드
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
    cellStyle: { textAlign: "center" },

    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  //상품명
  {
    field: "ITEM_NM",
    headerKey: "product-name",
    headerName: "",
    cellStyle: { textAlign: "left" },

    headerClass: "header-center",
    sortable: true,
    width: 260,
  },
  //유통기한
  {
    field: "ITEM_DATE_END",
    headerKey: "expiration-date",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 130,
  },
  //입수
  {
    field: "UNIT_QTY",
    headerKey: "qty-by-box",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 40,
  },
  //UOM
  {
    field: "UNIT_NM",
    headerKey: "uom",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 40,
  },
  //(전)재고
  {
    field: "START_STOCK_QTY",
    headerKey: "before_stock",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.NumberCount,
    aggFunc: "sum",
  },
  //현재고 기준
  {
    field: "",
    headerKey: "current-stock-standard",
    headerName: "current-stock-standard",
    headerClass: "header-require",
    sortable: true,
    // export: true,
    children: [
      //입고일수
      {
        field: "PREV_IN_DT_CNT",
        headerKey: "receiving-day-count",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        // cellRenderer: gridTextInput,
        width: 90,
      },
      //입고량
      {
        field: "PREV_IN_QTY",
        headerKey: "receiving-qty",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        // cellRenderer: gridTextInput,
        width: 90,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      //출고일수
      {
        field: "PREV_OUT_DT_CNT",
        headerKey: "shipping-day-count",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        // cellRenderer: gridTextInput,
        width: 90,
      },
      //출고량
      {
        field: "PREV_OUT_QTY",
        headerKey: "real-out-qty",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        // cellRenderer: gridTextInput,
        width: 90,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      //재고
      {
        field: "END_STOCK_QTY",
        headerKey: "stock",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        width: 90,

        export: true,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      //발주신호
      {
        field: "PREV_ORDER_SIGNAL",
        headerKey: "order-signal",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        width: 90,
        // cellRenderer: gridTextInput,
      },
      //회전율
      {
        field: "PREV_TURN_OVER",
        headerKey: "turn-over-percent",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        width: 90,
        // cellRenderer: gridTextInput,
      },
      //회전5
      {
        field: "PREV_TURN_OVER_5",
        headerKey: "turn-over-5",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        width: 90,

        // cellRenderer: gridTextInput,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      //회전6
      {
        field: "PREV_TURN_OVER_6",
        headerKey: "turn-over-6",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        width: 90,

        // cellRenderer: gridTextInput,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      //회전7
      {
        field: "PREV_TURN_OVER_7",
        headerKey: "turn-over-7",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        width: 90,

        // cellRenderer: gridTextInput,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      //회전8
      {
        field: "PREV_TURN_OVER_8",
        headerKey: "turn-over-8",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        width: 90,

        // cellRenderer: gridTextInput,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      //회전9
      {
        field: "PREV_TURN_OVER_9",
        headerKey: "turn-over-9",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "center" },
        sortable: true,
        export: true,
        width: 90,

        // cellRenderer: gridTextInput,
        valueFormatter: Format.NumberCount,
        aggFunc: "sum",
      },
      {
        field: "PREV_OUT_QTY",

        hide: true,
      },
      {
        field: "CUR_IN_DT_CNT",
        hide: true,
      },
      {
        field: "RITEM_ID",
        hide: true,
      },
      {
        field: "CUR_STOCK_QTY",
        hide: true,
      },
    ],

    // width: 550,
  },
];
