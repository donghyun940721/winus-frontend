/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS093/input.ts
 *  Description:    상품별자동출고주문관리 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSCM011",
  pk: "CUST_ID",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  product: {
    page: "WMSCM091",
    id: "product",
    title: "search-product",
    gridTitle: "product-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      rowDataKeys: ["CUST_ID"],
      paramsKeys: ["vrSrchCustId"],
    },
    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "Y",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        title: "product-code",
        searchContainerInputId: "vrSrchRitemCd",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchRitemNm",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        options: [{ nameKey: "all", name: "", value: "" }],
        optionsKey: "ITEMGRP",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        options: [
          { nameKey: "all", name: "", value: "" },
          { nameKey: "useProductY", name: "", value: "Y" },
          { nameKey: "useProductN", name: "", value: "N" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 화주
    ids: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 출고예정일
    ids: ["vrSrchReqDtFrom", "vrSrchReqDtTo"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: [],
    title: "work-date",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["date", "date"],
  },
  {
    // 상품
    ids: ["vrSrchItemCd", "vrSrchItemNm"],
    hiddenId: "vrSrchItemId",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "RITEM_ID",
    rowData2ndIds: ["POOL_CODE", "POOL_NM"],
    rowData2ndHiddenId: "POOL_ID",
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    srchKey: "ITEM",
    title: "product",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "product", nameKey: "product", value: "G" },
      { name: "logistics-container", nameKey: "logistics-container", value: "P" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useMore: false,
  useSetting: false, // TODO :: 위치 조정 이 후, 재설정 (TRUE)
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  searchConditionInitUrl: "",
};
