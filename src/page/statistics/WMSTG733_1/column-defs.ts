/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSTG733_1/column-defs.ts
 *  Description:    통계관리/출고검수조회_출고박스조회 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid/index";

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  product: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      width: 250,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS = [
  {
    field: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
  },
  {
    //작업일자
    field: "WORK_DT",
    headerKey: "work-date",
    headerName: "",
    width: 130,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerName: "",
    width: 150,
    sortable: true,
  },
  {
    field: "ORD_ID",
    headerKey: "order-id",
    headerName: "",
    width: 130,
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "CONF_SEQ",
    headerKey: "recom-seq",
    headerName: "",
    width: 100,
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "ORD_SEQ",
    headerKey: "order-seq",
    headerName: "",
    width: 100,
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "RITEM_CD",
    headerKey: "product-code",
    headerName: "",
    width: 130,
    sortable: true,
  },
  {
    field: "RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    width: 180,
    flex: 1,
    sortable: true,
  },
  {
    field: "ORG_ORD_ID",
    headerKey: "original-order-number",
    headerName: "",
    width: 150,
    cellClass: "stringType",
    sortable: true,
  },
  {
    field: "D_CUST_NM",
    headerKey: "orderer",
    headerName: "",
    width: 130,
    sortable: true,
  },
  {
    field: "ADDRESS",
    headerKey: "address",
    headerName: "",
    width: 250,
    flex: 1.5,
    sortable: true,
  },
  {
    field: "INVC_NO",
    headerKey: "invoice-number",
    headerName: "",
    width: 150,
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "DLV_CD_NM",
    headerKey: "delivery-company",
    headerName: "",
    width: 150,
    sortable: true,
  },
  {
    field: "POOL_NM",
    headerKey: "delivery-box",
    headerName: "",
    width: 150,
    sortable: true,
  },
];
