/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOM120/input.ts
 *  Description:    주문/입고주문관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
/**
 ********************* Grid Area *********************/
import { i18n } from "@/i18n";
import { IControlBtn } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

// done
export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
    display: "Y",
  },
];

export const GRID_STATISTICS_INFO = [
  {
    id: "TOTAL_QTY",
    title: "overall-work-quantity",
  },
];

export const gridMetaData: any = {
  //페이지 키
  // checkBoxColumn: "check-box-column",
  //페이징옵션
  pagingSizeList: [],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  suppressRowClickSelection: true,
  enableRangeSelection: true,
  suppressContextMenu: true,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  allowContextMenuWithControlKey: true,
  suppressAggFuncInHeader: true, //그룹 관련 시작
};
