/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSTG020/column-defs.ts
 *  Description:    창고별재고현황 컬럼 정의 스크립트
 *  Authors:        S.Y.LIM
 *  Update History:
 *                  2024.07. : Created by S.Y.LIM
 *
------------------------------------------------------------------------------*/
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
export const MODAL_COLUMN_DEFS: any = {
  //화주
  owner: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "kit-product": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
      valueFormatter: Format.NumberPrice,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "RITEM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "ITEM_ENG_NM",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "CUST_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_CD",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_CD",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_NM",
      sortable: true,
      hide: true,
    },
  ],
};

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.commonRowIndex,
    lockPosition: true,
    lockVisible: true,
    pinned: true,
  },
  //(체크박스)
  {
    field: "CHK_BOX",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    // suppressColumnsToolPanel: true,
    // lockVisible: true,
    // lockPosition: true,
  },
  //창고
  {
    field: "WH_NM",
    headerKey: "warehouse",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  //화주코드
  {
    field: "CUST_CD",
    headerKey: "owner-code",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  //화주명
  {
    field: "CUST_NM",
    headerKey: "owner-name",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
  },
  //상품코드
  {
    field: "RITEM_CD",
    headerKey: "item-code",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
    width: 160,
  },
  //상품명
  {
    field: "RITEM_NM",
    headerKey: "item-name",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    width: 190,
  },
  //수량
  {
    field: "STOCK_QTY",
    headerKey: "quantity",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
    valueFormatter: Format.NumberPrice,
  },
  //입수
  {
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 50,
  },
  //UOM
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 50,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: Getter.commonRowIndex,
    lockPosition: true,
    lockVisible: true,
    pinned: true,
  },
  //로케이션
  {
    field: "LOC_NM",
    headerKey: "location",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellStyle: { textAlign: "center" },
    width: 120,
  },
  //상품코드
  {
    field: "RITEM_CD",
    headerKey: "item-code",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 160,
  },
  //상품명
  {
    field: "RITEM_NM",
    headerKey: "item-name",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    sortable: true,
    width: 190,
  },
  //재고수량
  {
    field: "STOCK_QTY",
    headerKey: "stock-quantity",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    sortable: true,
    width: 120,
    valueFormatter: Format.NumberPrice,
  },
  //입수
  {
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 50,
  },
  //UOM
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    // cellRenderer: gridTextInput,
    editable: true,
    width: 50,
  },
];
