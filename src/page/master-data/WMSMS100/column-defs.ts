/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS100/column-defs.ts
 *  Description:    기준정보/UOM정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (typeof params.data == "object" && Object.hasOwn(params.data, "RNUM")) {
        return params.node.rowIndex + 1;
      } else {
        return "";
      }
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerName: "",
    maxWidth: 50,
    headerCheckboxSelection: true,
    checkboxSelection: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "CHK_BOX",
    headerName: "",
    hide: true,
  },
  {
    field: "FLAG",
    headerName: "",
    hide: true,
  },
  {
    field: "ST_GUBUN",
    headerName: "",
    hide: true,
  },

  {
    field: "UOM_CD",
    headerName: t("grid-column-name.uom-code"),
    headerClass: "header-require",
    sortable: true,
    editable: true,
    width: 550,
    // export: true,
    flex: 2,
  },
  {
    field: "UOM_NM",
    headerName: t("grid-column-name.uom-name"),
    headerClass: "header-require",
    sortable: true,
    editable: true,
    width: 550,
    // export: true,
    flex: 2,
  },
  {
    field: "UOM_ID",
    headerName: t("grid-column-name.uom-id"),
    //UOM_ID에 'temp'를 포함하면 보여지지 않도록 설정
    // cellStyle: (params: CellClassParams) => {
    //   if (String(params.data.RNUM).includes("temp")) {
    //     const style = { textAlign: "center", fontSize: "0px" };
    //     return style;
    //   }
    //   return { textAlign: "center" };
    // },
    headerClass: "header-center",
    sortable: true,
    width: 540,
    flex: 1,
  },
  {
    field: "LC_ID",
    headerName: "",
    hide: true,
  },
  {
    field: "INHERIT_UOM_ID",
    headerName: "",
    hide: true,
  },
];
