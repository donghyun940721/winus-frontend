/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS100/page.vue
 *  Description:    기준정보/UOM정보관리 META File (Search Area)
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { ISearchInput } from "@/types";

const { t } = i18n.global;

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchUomId"],
    rowDataIds: ["UOM_CD"],
    searchApiKeys: ["vrSrchUomId"],
    rowDataHiddenId: "",
    hiddenId: "",
    srchKey: "UOM",
    title: "uom-code",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  modalColumnDefs: [],
  searchInput: SEARCH_INPUT,
  searchModalInfo: [],
  pageInfo: "null",
  unUsedRefreshButton: true,
};
