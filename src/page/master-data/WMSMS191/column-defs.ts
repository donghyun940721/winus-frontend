/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS191/column-defs.ts
 *  Description:    상품군정보관리 컬럼 정의 스크립트
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.06. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/
import gridSelect from "@/components/renderer/grid-select.vue";
import { Getter } from "@/lib/ag-grid";
import { GridUtils } from "@/lib/ag-grid/utils";
import type { IColDef } from "@/types/agGrid";

import type { ICellEditorParams } from "ag-grid-community";

export const MODAL_COLUMN_DEFS = {};

export const FORM_COLUMN_DEFS: IColDef[] = [
  {
    field: "ST_GUBUN",
    hide: true,
    export: false,
  },
  {
    field: "",
    hide: true,
    export: false,
  },

  {
    field: "FLAG",
    hide: true,
    export: false,
  },
  {
    field: "CHK_BOX",
    hide: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    width: 40,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
    export: false,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    width: 40,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    export: false,
  },

  //원부자재군코드
  {
    field: "POOL_GRP_CD",
    headerKey: "raw-subsidiary-material-group-code",
    headerName: "",
    editable: true,
    headerClass: "header-center",
    width: 250,
    cellStyle: { textAlign: "left" },
    export: true,
  },
  //원부자재군코드명
  {
    field: "POOL_GRP_NM",
    headerKey: "raw-subsidiary-material-group-name",
    headerName: "",
    editable: true,
    headerClass: "header-center",
    width: 250,
    cellStyle: { textAlign: "left" },
    export: true,
  },
  {
    field: "LC_ID",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    width: 250,
    cellStyle: { textAlign: "left" },
    hide: true,
    export: false,
  },
  //물류센터ID
  {
    field: "LC_NM",
    headerKey: "logistics-center-id",
    headerName: "",
    headerClass: "header-center",
    width: 250,
    cellStyle: { textAlign: "center" },
    export: false,
  },
  //입고존
  {
    field: "IN_ZONE_ID",
    headerKey: "receiving-zone",
    headerName: "",
    editable: true,
    headerClass: "header-center",
    width: 250,
    cellStyle: { textAlign: "left" },
    valueGetter: (params) => {
      return Getter.convetCodeToNameByOptionData("ZONE", params);
    },
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("ZONE"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("ZONE", value);
        },
      };
    },
    cellEditor: gridSelect,
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("ZONE", params.value);
    },
    flex: 2,
  },
  {
    field: "WORK_IP",
    hide: true,
    export: false,
  },
  {
    field: "REG_DT",
    hide: true,
    export: false,
  },
  {
    field: "REG_NO",
    hide: true,
    export: false,
  },
  {
    field: "UPD_DT",
    hide: true,
    export: false,
  },
  {
    field: "UPD_NO",
    hide: true,
    export: false,
  },
  {
    field: "POOL_GRP_ID",
    hide: true,
    export: false,
  },
  {
    field: "POOL_WGT_ARROW_RATE",
    hide: true,
    export: false,
  },
  {
    field: "ITEM_GRP_ID",
    hide: true,
    export: false,
  },
];
