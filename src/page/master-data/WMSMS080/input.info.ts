/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS080/input.info.ts
 *  Description:    기준관리/로케이션정보관리 Meta data
 *  Authors:        dhkim
 *  Update History:
 *                  2024.07 : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import { i18n } from "@/i18n";
import type { IControlBtn, IInfoInputContainer, IModal, ISplitButton } from "@/types";
import { SHIPMENT_CONTROL_GRID_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const INFO_INPUT: IInfoInputContainer[] = [
  {
    title: "default-info",
    default: "expand",
    inputs: [
      {
        ids: ["LOC_CD"],
        rowDataIds: ["LOC_CD"],
        title: "location-code",
        type: "text",
        width: "half",
        size: "single",
        required: true,
      },
      {
        ids: ["LC_BARCODE_CD"],
        rowDataIds: ["LC_BARCODE_CD"],
        title: "barcode",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["WH_CD", "WH_NM"],
        hiddenId: "WH_ID",
        rowDataIds: ["WH_CD", "WH_NM"],
        rowDataHiddenId: "WH_ID",
        title: "warehouse",
        type: "search",
        width: "full",
        size: "double",
        // isSearch: true,
        required: true,
      },
      {
        ids: ["CARRY_PLT_QTY"],
        rowDataIds: ["CARRY_PLT_QTY"],
        title: "load-capacity-plt",
        type: "text",
        width: "half",
        size: "single",
        required: true,
      },
      {
        ids: ["LOC_ABLE_PLT_QTY"],
        rowDataIds: ["LOC_ABLE_PLT_QTY"],
        title: "loadable-capacity-plt",
        type: "text",
        width: "half",
        size: "single",
        required: true,
      },
      {
        ids: ["LOC_TYPE"],
        rowDataIds: ["LOC_TYPE"],
        title: "loc-type",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "LocType",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["LOC_STAT"],
        rowDataIds: ["LOC_STAT"],
        title: "use-stat",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "LocStat",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["OP_QTY"],
        rowDataIds: ["OP_QTY"],
        title: "appropriate-stock-quantity",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["CARRY_TYPE"],
        rowDataIds: ["CARRY_TYPE"],
        title: "loading-division",
        type: "select",
        width: "half",
        size: "single",
        required: false,
        optionsKey: "CarryType",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["CUST_CD", "CUST_NM"],
        hiddenId: "CUST_ID",
        rowDataIds: ["CUST_CD", "CUST_NM"],
        rowDataHiddenId: "CUST_ID",
        title: "owner",
        type: "search",
        width: "full",
        size: "double",
        // isSearch: true,
      },
    ],
  },
  {
    title: "rack-information",
    default: "expand",
    inputs: [
      {
        ids: ["ROWCNT", "COL", "DAN"],
        rowDataIds: ["ROWCNT", "COL", "DAN"],
        title: "line-row-column",
        type: "text",
        width: "half",
        size: "triple",
      },
      {
        ids: ["CELL_W"],
        rowDataIds: ["CELL_W"],
        title: "horizontal-(m)",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["CELL_L"],
        rowDataIds: ["CELL_L"],
        title: "vertical-(m)",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["CELL_H"],
        rowDataIds: ["CELL_H"],
        title: "height-(m)",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["POSITION_X"],
        rowDataIds: ["POSITION_X"],
        title: "X-axis",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["POSITION_Y"],
        rowDataIds: ["POSITION_Y"],
        title: "Y-axis",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["POSITION_Z"],
        rowDataIds: ["POSITION_Z"],
        title: "Z-axis",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["RACK_DIRECTION"],
        rowDataIds: ["RACK_DIRECTION"],
        title: "rack-direction",
        type: "select",
        width: "half",
        size: "single",
        options: [
          { name: "", nameKey: "NONE", value: "N" },
          { name: "", nameKey: "LEFT", value: "L" },
          { name: "", nameKey: "RIGHT", value: "R" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["LOC_CBM"],
        rowDataIds: ["LOC_CBM"],
        title: "cbm",
        type: "text",
        width: "half",
        size: "single",
      },
    ],
  },
  {
    title: "loaded-information",
    default: "expand",
    inputs: [
      {
        ids: ["TAG_PREFIX"],
        rowDataIds: ["TAG_PREFIX"],
        title: "tag-prefix",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["LC_GLN_CD"],
        rowDataIds: ["LC_GLN_CD"],
        title: "gln-code",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["RS_QTY"],
        rowDataIds: ["RS_QTY"],
        title: "rack-supplementary-quantity",
        type: "text",
        width: "half",
        size: "single",
      },
      {
        ids: ["ITEM_GRP"],
        rowDataIds: ["ITEM_GRP"],
        title: "product-group",
        type: "select",
        width: "half",
        size: "single",
        optionsKey: "ITEMGRP",
        options: [{ nameKey: "all", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["LOC_ITEM_TYPE"],
        rowDataIds: ["LOC_ITEM_TYPE"],
        title: "product-contrainer-type",
        type: "select",
        width: "half",
        size: "single",
        // optionsKey: "LocType",
        options: [
          { nameKey: "all", name: "", value: "A" },
          { nameKey: "product", name: "", value: "G" },
          { nameKey: "logistics-container", name: "", value: "P" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
    ],
  },
];

export const INFO_MODAL_INFO: IModal = {
  warehouse: {
    page: "WMSMS080",
    id: "warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },

    inputs: [
      {
        id: "S_WH_CD",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_WH_NM",
        title: "warehouse-name	",
        type: "text",
        width: "half",
      },
    ],
  },
  owner: {
    page: "WMSMS010",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

export const INFO_CONTROL_BTN: IControlBtn[] = [
  {
    title: "barcode-output",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "",
  },
];

export const INFO_SPLIT_BTN: ISplitButton[] = [
  {
    title: t("control-button-name.enter-excel"),
    colorStyle: "success",
    eventHandler: "excelButtonHandler",
    image : "excel",
  },
];

export const INFO_GRID_BTN: IControlBtn[] = [
  {
    title: "loc-limit",
    colorStyle: "danger",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "loc-limit-cancel",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
  },
];

export const InfoGridMeta: any = {
  // 그리드 헤더
  gridHeaderName: t("grid-title.shipment-control-information"),

  useUserSetting: false,

  //페이징옵션
  pagingSizeList: [20, 40, 60],
};

export const InfoGridOptions = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
    lockVisible: true,
    lockPosition: true,
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: SHIPMENT_CONTROL_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  suppressContextMenu: true, // Context Menu 미사용
  statusBar: true,
  // getRowClass : (params: any) => {
  //   if(Object.hasOwn(params.data, "STATE") && params.data.STATE === "U") {
  //     return 'work-progress';
  //   }
  // },
};
