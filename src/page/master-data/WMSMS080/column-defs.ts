/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS080/column-defs.ts
 *  Description:    기준정보/로케이션정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
import { ValueGetterParams } from "ag-grid-community";

const { t } = i18n.global;

export const RE_LOCATION_COLUMN_DEF = [
  {
    field: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerName: "",
    width: 60,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
  },
  {
    field: "LOC_CD",
    headerKey: "location",
    headerName: "",
    sortable: true,
  },
  {
    field: "ROWCNT",
    headerKey: "line",
    headerName: "",
    cellStyle: { textAlign: "right" },
  },
  {
    field: "COL",
    headerKey: "row",
    headerName: "",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "DAN",
    headerKey: "column",
    headerName: "",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "LOC_TYPE",
    headerKey: "loc-type",
    headerName: "",
    sortable: true,
  },

  {
    field: "WH_NM",
    headerKey: "warehouse",
    headerName: "",
    sortable: true,
  },

  {
    field: "LC_BARCODE_CD",
    headerKey: "barcode",
    headerName: "",
    sortable: true,
  },
];

export const SHIPMENT_CONTROL_GRID_COLUMN_DEFS: IColDef[] = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    pinned: "left",
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.LOC_LOCK_SEQ).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "LOC_CD",
    headerKey: "location",
    headerName: "",
  },
  {
    field: "APPLY_FR_DT",
    headerKey: "application-start-date",
    headerName: "",
    sortable: true,
    width: 110,
  },
  {
    field: "APPLY_TO_DT",
    headerKey: "application-end-date",
    headerName: "",
    sortable: true,
    width: 110,
  },
  {
    field: "APPLY_TO_REASON_CDDT",
    headerKey: "issue-code",
    headerName: "",
    sortable: true,
    width: 110,
  },
  {
    field: "REMARK",
    headerKey: "remark",
    headerName: "",
    sortable: true,
    minWidth: 200,
  },
  {
    field: "REG_DT",
    headerKey: "registration-date",
    cellStyle: { textAlingn: "center" },
    headerName: "",
    sortable: true,
    valueFormatter: Format.Date,
    width: 170,
  },
  {
    field: "REG_NO",
    headerKey: "registrant-number",
    headerName: "",
    sortable: true,
    width: 130,
  },
  {
    field: "UPD_DT",
    headerKey: "modified-date",
    headerName: "",
    sortable: true,
    valueFormatter: Format.Date,
    width: 170,
  },
  {
    field: "UPD_NO",
    headerKey: "modifier-number",
    headerName: "",
    sortable: true,
    width: 130,
  },
];

export const MODAL_COLUMN_DEFS: any = {
  location: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

        { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerKey: "stock-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerKey: "lot-number",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerKey: "expiration-date",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "STATE") && params.data.STATE === "C") {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "STATE",
    headerName: "",
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    field: "LOC_CD",
    headerName: t("grid-column-name.location"),
    sortable: true,
    // export: true,
    cellClass: "stringType",
  },
  {
    field: "DLV_CNTL",
    headerName: t("grid-column-name.control-Y/N"),
    cellClass: "renderer-cell stringType",
    cellRenderer: GridCircleIconForYn,
    sortable: true,
    // export: true,
    width: 90,
  },
  {
    field: "ROWCNT",
    headerName: t("grid-column-name.line"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 50,
    valueGetter: Getter.emptyString,
  },
  {
    field: "COL",
    headerName: t("grid-column-name.row"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 50,
    valueGetter: Getter.emptyString,
  },
  {
    field: "DAN",
    headerName: t("grid-column-name.column"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 50,
    valueGetter: Getter.emptyString,
  },
  {
    field: "LOC_TYPE",
    headerName: t("grid-column-name.loc-type"),
    valueGetter: (params: ValueGetterParams) => {
      return Getter.convetCodeToNameByOptionData("LocType", params);
    },
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 110,
  },
  {
    field: "LOC_STAT",
    headerName: t("grid-column-name.use-state"),
    valueGetter: (params: ValueGetterParams) => {
      return Getter.convetCodeToNameByOptionData("LocStat", params);
    },
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 110,
  },
  {
    field: "WH_NM",
    headerName: t("grid-column-name.warehouse"),
    sortable: true,
  },
  {
    field: "SHIP_ABC_NM",
    headerName: t("grid-column-name.shipping-abc"),
    sortable: true,
    width: 110,
  },
  {
    field: "ITEM_GRP",
    headerName: t("grid-column-name.product-group"),
    sortable: true,
    // export: true,
    cellClass: "stringType",
  },
  {
    field: "CELL_W",
    headerName: t("grid-column-name.horizontal"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 70,
  },
  {
    field: "CELL_H",
    headerName: t("grid-column-name.height"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 70,
  },
  {
    field: "CELL_L",
    headerName: t("grid-column-name.vertical"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 70,
  },
  {
    field: "OP_QTY",
    headerName: t("grid-column-name.op-quantity"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 110,
  },
  {
    field: "RS_QTY",
    headerName: t("grid-column-name.rack-supplementary-quantity"),
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 130,
  },
  {
    field: "CARRY_TYPE",
    headerName: t("grid-column-name.loading-division"),
    valueGetter: (params: any) => {
      return Getter.convetCodeToNameByOptionData("CarryType", params);
    },
    sortable: true,
    // export: true,
    cellClass: "stringType",
    width: 100,
  },
  {
    field: "LC_BARCODE_CD",
    headerName: t("grid-column-name.barcode"),
    sortable: true,
    // export: true,
    cellClass: "stringType",
  },
  {
    field: "LOC_ITEM_TYPE",
    headerName: t("grid-column-name.product-container-type"),
    // export: true,
    cellClass: "stringType",
    hide: true,
  },
  {
    field: "LOC_CBM",
    headerName: t("grid-column-name.cbm"),
    // export: true,
    cellClass: "stringType",
    hide: true,
  },
  {
    field: "REG_NO",
    headerName: t("grid-column-name.registrar"),
    sortable: true,
    width: 150,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.registration-date"),
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.Date,
    sortable: true,
    width: 170,
  },
  {
    field: "UPD_NO",
    headerName: t("grid-column-name.modifier"),
    sortable: true,
    width: 150,
  },
  {
    field: "UPD_DT",
    headerName: t("grid-column-name.modified-date"),
    cellStyle: { textAlign: "center" },
    valueFormatter: Format.Date,
    sortable: true,
    width: 170,
  },
  //#region :: 기본정보
  {
    // 창고(코드)
    field: "WH_CD",
    hide: true,
  },
  {
    // 창고(ID)
    field: "WH_ID",
    hide: true,
  },
  {
    // 적재량(PLT)
    field: "CARRY_PLT_QTY",
    cellDataType: "text",
    hide: true,
  },
  {
    //적재가능량(PLT)
    field: "LOC_ABLE_PLT_QTY",
    cellDataType: "text",
    hide: true,
  },
  {
    // 화주(코드)
    field: "CUST_CD",
    hide: true,
  },
  {
    // 화주(명칭)
    field: "CUST_NM",
    hide: true,
  },
  {
    // 화주(ID)
    field: "CUST_ID",
    hide: true,
  },
  //#endregion
  //#region :: 랙 정보
  {
    // X축
    field: "POSITION_X",
    hide: true,
  },
  {
    // Y축
    field: "POSITION_Y",
    hide: true,
  },
  {
    // Z축
    field: "POSITION_Z",
    hide: true,
  },
  {
    // 랙 방향
    field: "RACK_DIRECTION",
    hide: true,
  },
  //#endregion
  //#region :: 적재 정보
  {
    //
    field: "TAG_PREFIX",
    hide: true,
  },
  {
    // GLN코드
    field: "LC_GLN_CD",
    hide: true,
  },
  //#endregion
  //#region ::  그 외 히든 컬럼
  {
    //
    field: "REMARK",
    hide: true,
  },
  //#endregion
];
