/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS080/input.grid.ts
 *  Description:    기준관리/로케이션정보관리 Meta data
 *  Authors:        dhkim
 *  Update History:
 *                  2024.07 : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { IControlBtn, IModalValue } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "re-location",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    // disabled: "disabled",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    // disabled: "disabled",
    image: "",
    authType: "DEL_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const GRID_CONTROL_BTN: IControlBtn[] = [
  {
    title: "re-location",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "barcode-output",
    colorStyle: "primary",
    paddingStyle: "normal",
    image: "",
    authType: "",
  },
];

export const RELOCATION_MODAL_INFO: IModalValue = {
  page: "WMSMS080",
  id: "location",
  title: "search-location",
  gridTitle: "location-list",

  apis: {
    url: "/WMSMS080/delList_rn.action",
    params: {
      func: "fn_search",
      txtSrchLocCd: "",
      txtSrchLocId: "",
      txtSrchWhCd: "",
      txtSrchWhId: "",
      txtSrchWhNm: "",
      LOC_OP3_1: "",
      LOC_OP3_2: "",
      LOC_OP3_3: "",
      vrSrchLocStat: "",
      LOC_OP4_1: "",
      LOC_OP5_1: "",
      LOC_OP6_1: "",
      vrSrchLocType: "",
    },
    data: {
      _search: false,
      nd: "",
      rows: "100",
      page: "1",
      sidx: "",
      sord: "asc",
    },
  },
  inputs: [
    {
      id: "S_WH_CD",
      title: "warehouse-code",
      type: "text",
      width: "half",
    },
    {
      id: "S_WH_NM",
      title: "warehouse-name",
      type: "text",
      width: "half",
    },
  ],
};

export const gridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("grid-title.location-information"),

  //페이징옵션
  pagingSizeList: [100, 300, 500, 1000, 3000, 5000],
};

export const gridOptionsMeta = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
    lockVisible: true,
    lockPosition: true,
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  suppressContextMenu: true, // Context Menu 미사용
  statusBar: true,
  // getRowClass : (params: any) => {
  //   if(Object.hasOwn(params.data, "STATE") && params.data.STATE === "U") {
  //     return 'work-progress';
  //   }
  // },
};
