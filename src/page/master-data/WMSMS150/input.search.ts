/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS150/input.search.ts
 *  Description:    재고조사설정 화면 search meta 정보
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "",
  pk: "",
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "txtSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "txtSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["txtSrchCustCd", "txtSrchCustNm"],
    hiddenId: "hdnCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["txtSrchCustCd", "txtSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "half",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
];

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useMore: false,
  useSetting: false, // TODO :: 위치 조정 이 후, 재설정 (TRUE)
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  searchConditionInitUrl: "",
};
