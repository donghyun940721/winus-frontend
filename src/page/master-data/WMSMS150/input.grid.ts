/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS150/input.grid.ts
 *  Description:    재고조사설정 화면 grid meta 정보
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.06. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/

/**
 ********************* Grid Area *********************/
import { i18n } from "@/i18n";
import { IControlBtn, IGridCellSearchButton, IGridCellSelectBox } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;
// done
export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const GRID_SELECT_BOX_INFO: IGridCellSelectBox = {};

export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  CUST: {
    fieldList: ["CUST_CD", "CUST_NM", "CUST_ID"],
    rowDataKeys: ["CUST_CD", "CUST_NM", "CUST_ID"],

    modalData: {
      page: "WMSCM011",
      id: "owner",
      title: "search-owner",
      gridTitle: "owner-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달

      apis: {
        url: "/WMSCM011/list_rn.action",
        params: {
          S_CUST_CD: "",
          S_CUST_NM: "",
          S_CUST_ID: "",
          S_CUST_TYPE: "",
          S_LC_ALL: "",
          S_LC_ID: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "S_CUST_TYPE",
          title: "customer-type",
          type: "select",
          width: "triple",
          options: [{ name: "all", nameKey: "all", value: "" }],
          optionsReadOnly: true,
        },
        {
          id: "S_CUST_CD",
          searchContainerInputId: "txtSrchCustCd",
          title: "shipper-code",
          type: "text",
          width: "triple",
        },
        {
          id: "S_CUST_NM",
          searchContainerInputId: "txtSrchCustNm",
          title: "owner-name",
          type: "text",
          width: "triple",
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1,
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "CUST_CD",
        headerKey: "shipper-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "CUST_NM",
        headerKey: "owner-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ADDR",
        headerKey: "address",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "EMP_NM",
        headerKey: "manager-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "CUST_EPC_CD",
        headerKey: "owner-epc-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "TEL",
        headerKey: "tel",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
    ],
  },

  LOCATION: {
    fieldList: ["LOC_CD", "LOC_ID"],
    rowDataKeys: ["LOC_CD", "LOC_ID"],
    children: true,
    modalData: {
      page: "WMSCM080Q5",
      id: "location",
      title: "search-location",
      gridTitle: "location-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달

      apis: {
        url: "/WMSCM080Q5/list02_rn.action",
        params: {
          func: "fn_setWMSCM080",
          LOC_ID: "",
          LOC_CD: "",
          AVAILABLE_QTY: "",
          STOCK_ID: "",
          SUB_LOT_ID: "",
          vrViewOnlyLoc: "",
          STOCK_WEIGHT: "",
          vrViewSubLotId: "",
          vrRitemId: "",
          ITEM_BEST_DATE_END: "",
          UOM_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          vrViewStockQty: "",
          S_WH_CD: "",
          vrWhId: "",
          S_WH_NM: "",
          vrSrchLocCd: "",
          vrSrchLocId: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "WH_CD",
          title: "warehouse",
          type: "search",
          width: "half",
          searchRowDataIds: ["WH_CD", "WH_NM"],
          searchIds: ["S_WH_CD", "S_WH_NM"],
        },
        {
          id: "vrSrchLocCd",
          title: "location-code",
          type: "text",
          width: "half",
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "LOC_CD",
        headerKey: "location-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "LOC_ID",
        headerKey: "product-group",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
    ],

    childrenModalData: {
      page: "WMSMS040",
      id: "warehouse",
      title: "search-warehouse",
      gridTitle: "warehouse-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달
      apis: {
        url: "/WMSMS040/poplist_rn.action",
        params: {
          func: "fn_setWMSCM040",
          WH_ID: "",
          WH_CD: "",
          WH_NM: "",
          S_WH_CD: "",
          S_WH_NM: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "S_WH_CD",
          title: "warehouse-code",
          type: "text",
          width: "half",
        },
        {
          id: "S_WH_NM",
          title: "warehouse-name",
          type: "text",
          width: "half",
        },
      ],
    },
    childrenColDef: [
      {
        field: "",
        headerName: "",
        width: 60,
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerKey: "",
        headerName: "",
        headerCheckboxSelection: true,
        checkboxSelection: true,
        width: 50,
      },
      {
        field: "WH_CD",
        headerKey: "warehouse-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "WH_NM",
        headerName: "",
        headerKey: "warehouse-name",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "WH_TYPE_ORI",
        headerName: "",
        headerKey: "warehouse-category",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "WH_TYPE_ORI",
        headerName: "",
        headerKey: "warehouse-type",
        headerClass: "header-center",
        sortable: true,
      },
    ],
    childrenFieldList: ["S_WH_CD", "S_WH_NM"],
    childrenRowDataKeys: ["WH_CD", "WH_NM"],
  },

  ITEM: {
    fieldList: ["RITEM_CD", "RITEM_NM", "RITEM_ID"],
    rowDataKeys: ["ITEM_CODE", "ITEM_KOR_NM", "RITEM_ID"],
    setParamsFromGridRowData: {
      rowDataKeys: ["ITEM_TY"],
      paramsKeys: ["vrItemType"],
    },
    modalData: {
      page: "WMSMS150",
      id: "kit-product",
      title: "search-product",
      gridTitle: "product-list",
      isCellRenderer: true,
      apis: {
        url: "/rest/WMSMS150.action",
        params: {
          func: "fn_setWMSMS150",
          vrSrchCustId: "",
          vrSrchWhId: "",
          vrViewSetItem: "",
          vrViewAll: "",
          vrItemType: "",
          RITEM_ID: "",
          ITEM_CODE: "",
          ITEM_KOR_NM: "",
          vrSrchItemCd: "",
          vrSrchItemNm: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchItemCd",
          title: "product-code",
          searchContainerInputId: "vrSrchItemCd",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemNm",
          searchContainerInputId: "vrSrchItemNm",
          title: "product-name",
          type: "text",
          width: "half",
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "ITEM_CODE",
        headerKey: "product-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_KOR_NM",
        headerKey: "product-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "RITEM_ID",
        headerKey: "product-code",
        hide: true,
      },
    ],
  },
};

export const GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  ITEM_TY: {
    pk: "RNUM",
    optionsKey: "ITEM_TY",
  },
  CYCL_STOCK_TYPE: {
    pk: "RNUM",
    optionsKey: "CYCL_STOCK_TYPE",
  },
  TYPE_ST: {
    pk: "RNUM",
    optionsKey: "TYPE_ST",
  },
};

export const gridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSMS150"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [100, 200, 300],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  // getRowId: (data) => {
  //   return data.data.RNUM;
  // },
};
