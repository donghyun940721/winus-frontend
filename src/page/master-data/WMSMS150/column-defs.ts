/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS150/column-defs.ts
 *  Description:    재고조사설정 화면 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import gridSearchButton from "@/components/renderer/grid-search-button.vue";
import gridSelect from "@/components/renderer/grid-select.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { Format, Getter } from "@/lib/ag-grid";
import { GridUtils } from "@/lib/ag-grid/utils";
import type { ICellEditorParams } from "ag-grid-community";

export const FORM_COLUMN_DEFS: any = [
  {
    field: "NO",
    headerKey: "no",
    headerName: "",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "",
    headerName: "",
    width: 50,
    headerCheckboxSelection: true,
    checkboxSelection: true,
  },
  {
    field: "CHK_BOX",
    headerName: "",
    hide: true,
  },
  {
    field: "FLAG",
    headerName: "",
    hide: true,
  },
  {
    field: "ST_GUBUN",
    headerName: "",
    hide: true,
  },

  {
    field: "",
    headerKey: "owner",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    children: [
      {
        field: "CUST_CD",
        headerKey: "shipper-code",
        headerName: "",
        headerClass: "header-require",
        sortable: true,
        export: true,
        cellRenderer: gridTextInput,
        editable: true,
        width: 120,
        cellStyle: { textAlign: "center" },
      },
      {
        field: "CUST",
        headerKey: "blank",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        cellRenderer: gridSearchButton,
        width: 30,
      },
      {
        field: "CUST_NM",
        headerKey: "owner-name",
        headerName: "",
        headerClass: "header-require",
        sortable: true,
        export: true,
        cellRenderer: gridTextInput,
        editable: true,
        width: 120,
        cellStyle: { textAlign: "left" },
      },
    ],
  },
  {
    field: "CUST_ID",
    headerKey: "",
    headerName: "",
    hide: true,
  },
  {
    field: "CYCL_STOCK_TYPE",
    headerKey: "inventory-arrangement-method",
    headerName: "",
    headerClass: "header-center",
    width: 130,
    cellEditor: gridSelect,
    cellStyle: { textAlign: "center" },
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("CYCL01"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("CYCL01", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("CYCL01", params.value);
    },
    editable: true,
    sortable: true,
  },
  {
    field: "TYPE_ST",
    headerKey: "total-epc",
    headerName: "",
    headerClass: "header-center",
    width: 120,
    cellStyle: { textAlign: "center" },
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("TYPE_ST"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("TYPE_ST", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("TYPE_ST", params.value);
    },
    editable: true,
    sortable: true,
  },
  {
    field: "WORK_DAY_NUM",
    headerKey: "days-to-target",
    headerName: "",
    headerClass: "header-center",
    export: true,
    editable: true,
    width: 120,
    valueFormatter: Format.NumberCount,
    cellRenderer: gridTextInput,
    cellClass: (params: any) => {
      if (Object.hasOwn(params.data, "CYCL_STOCK_TYPE") && params.data.CYCL_STOCK_TYPE === "2") {
        return "renderer-cell";
      } else {
        return "hidden-renderer-cell";
      }
    },
    cellStyle: { textAlign: "center" },
  },
  {
    field: "LOC",
    headerKey: "location",
    headerName: "",
    headerClass: "header-center",
    // export: true,
    children: [
      {
        field: "LOC_CD",
        headerKey: "location-code",
        headerName: "",
        headerClass: "header-center",
        export: true,
        editable: true,
        width: 120,
        cellRenderer: gridTextInput,
        cellStyle: { textAlign: "center" },
      },
      {
        field: "LOCATION",
        headerKey: "blank",
        headerName: "",
        headerClass: "header-center",
        cellRenderer: gridSearchButton,
        width: 30,
      },
    ],
  },

  {
    field: "ITEM_TY",
    headerKey: "product-group",
    headerName: "",
    headerClass: "header-center",
    width: 100,
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("ITEM_TY"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("ITEM_TY", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("ITEM_TY", params.value);
    },
    editable: true,
    sortable: true,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "",
    headerKey: "goods",
    headerName: "",
    headerClass: "header-center",
    children: [
      {
        field: "RITEM_CD",
        headerKey: "product-code",
        headerName: "",
        headerClass: "header-center",
        export: true,
        editable: true,
        width: 120,
        cellRenderer: gridTextInput,
        cellStyle: { textAlign: "center" },
      },
      {
        field: "ITEM",
        headerKey: "blank",
        headerName: "",
        headerClass: "header-center",
        cellRenderer: gridSearchButton,
        width: 30,
      },
      {
        field: "RITEM_NM",
        headerKey: "product-name",
        headerName: "",
        headerClass: "header-center",
        export: true,
        editable: true,
        width: 120,
        cellRenderer: gridTextInput,
        cellStyle: { textAlign: "left" },
      },
    ],
  },
  { hide: true, field: "RITEM_ID" },
  { hide: true, field: "LOC_ID" },
  { hide: true, field: "ZONE_ID" },
  { hide: true, field: "OLD_LOC_ID" },
  { hide: true, field: "CYCL_STOCK_CONFIG_ID" },
];

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1,
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};
