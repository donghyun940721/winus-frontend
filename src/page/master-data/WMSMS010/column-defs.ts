/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS010/column-defs.ts
 *  Description:    기준정보/화주정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import { i18n } from "@/i18n";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",

      headerKey: "",

      headerName: "No",

      width: 60,

      cellStyle: { textAlign: "center" },

      valueGetter: (params: any) => params.node.rowIndex + 1,
      // 인덱스는 0이 아닌 1부터 시작
    },

    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "first-contractor-code/name": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "ID",
      headerKey: "code-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "NAME",
      headerKey: "name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CODE",
      headerKey: "sub-code",
      headerName: "",
      sortable: true,
    },
    {
      field: "",
      headerKey: "Owner",
      headerName: "",
      sortable: true,
    },
  ],
  "contract-department-code/name": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "ID",
      headerKey: "code-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "NAME",
      headerKey: "name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CODE",
      headerKey: "sub-code",
      headerName: "",
      sortable: true,
    },
    {
      field: "",
      headerKey: "Owner",
      headerName: "",
      sortable: true,
    },
  ],
  "management-person-code/name": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "ID",
      headerKey: "code-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "NAME",
      headerKey: "name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CODE",
      headerKey: "sub-code",
      headerName: "",
      sortable: true,
    },
    {
      field: "",
      headerKey: "Owner",
      headerName: "",
      sortable: true,
    },
  ],
  "collection-contact-code/name": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "ID",
      headerKey: "code-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "NAME",
      headerKey: "name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CODE",
      headerKey: "sub-code",
      headerName: "",
      sortable: true,
    },
    {
      field: "",
      headerKey: "Owner",
      headerName: "",
      sortable: true,
    },
  ],
  "recognized-sales-office-staff/person": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "ID",
      headerKey: "code-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "NAME",
      headerKey: "name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CODE",
      headerKey: "sub-code",
      headerName: "",
      sortable: true,
    },
    {
      field: "",
      headerKey: "Owner",
      headerName: "",
      sortable: true,
    },
  ],
  "group-employee/name": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "ID",
      headerKey: "code-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "NAME",
      headerKey: "name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CODE",
      headerKey: "sub-code",
      headerName: "",
      sortable: true,
    },
    {
      field: "",
      headerKey: "Owner",
      headerName: "",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "STATE") && params.data.STATE === "C") {
        return "";
      }
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "STATE",
    headerName: "",
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    field: "NOW_USE_YN",
    headerName: t("grid-column-name.use"),
    sortable: true,
    width: 50,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
  },
  {
    field: "CUST_CD",
    headerName: t("grid-column-name.code"),
    sortable: true,
    // export: true,
    minWidth: 130,
    flex: 1,
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.owner-name"),
    minWidth: 200,
    sortable: true,
    // export: true,
    flex: 2,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.creation-date"),
    width: 150,
    sortable: true,
    // export: true,
  },
  {
    field: "REG_NM",
    headerName: t("grid-column-name.writer"),
    width: 150,
    sortable: true,
    // export: true,
  },
  //#region ::  기본정보
  {
    field: "CUST_ID",
    hide: true,
  },
  {
    // 대표화주여부
    field: "TYPICAL_CUST_GB",
    hide: true,
  },
  {
    // 화주EPC코드
    field: "CUST_EPC_CD",
    hide: true,
  },
  {
    //화주TYPE
    field: "CUST_TYPE",
    hide: true,
  },
  //#endregion
  //#region ::  정산설정
  {
    //정산사용여부
    field: "USE_CAL_YN",
    hide: true,
  },
  {
    // 정산유형
    field: "CAL_TYPE",
    hide: true,
  },
  {
    // 관리등급
    field: "MANAGE_GRADE",
    hide: true,
  },
  {
    // 결제조건
    field: "SETTLE_TERM",
    hide: true,
  },
  {
    // 신용등급
    field: "CREDIT_GRADE",
    hide: true,
  },
  {
    // 계약기간(From)
    field: "APPLY_FR_DT",
    hide: true,
  },
  {
    // 계약기간(To)
    field: "APPLY_TO_DT",
    hide: true,
  },
  {
    // 결제기일
    field: "PAY_DAY",
    hide: true,
  },
  {
    // 마감일
    field: "CLOSING_DAY",
    hide: true,
  },
  {
    // 최초계약자(ID)
    field: "CONT_EMPLOYEE_ID",
    hide: true,
  },
  {
    // 최초계약자(이름)
    field: "CONT_EMPLOYEE_NM",
    hide: true,
  },
  {
    // 계약부서(ID)
    field: "CONT_DEPT_ID",
    hide: true,
  },
  {
    // 계약부서(이름)
    field: "CONT_DEPT_NM",
    hide: true,
  },
  {
    // 계약부서(코드)
    field: "CONT_DEPT_CD",
    hide: true,
  },
  {
    // 관리담당자(ID)
    field: "REAL_EMPLOYEE_ID",
    hide: true,
  },
  {
    // 관리담당자(이름)
    field: "REAL_EMPLOYEE_NM",
    hide: true,
  },
  {
    // 수금담당자(ID)
    field: "COLLECT_EMPLOYEE_ID",
    hide: true,
  },
  {
    // 수금담당자(이름)
    field: "COLLECT_EMPLOYEE_NM",
    hide: true,
  },
  {
    // 인정매출처직원(ID)
    field: "APPROVAL_EMP_ID",
    hide: true,
  },
  {
    // 인정매출처직원(이름)
    field: "APPROVAL_EMP_NM",
    hide: true,
  },
  {
    // 그룹사직원(ID)
    field: "GROUP_COM_EMP_ID",
    hide: true,
  },
  {
    // 그룹사직원(이름)
    field: "GROUP_COM_EMP_NM",
    hide: true,
  },
  {
    // 계좌번호
    field: "ACCOUNT_NO1",
    hide: true,
  },
  {
    // 은행코드
    field: "ACCOUNT_CD1",
    hide: true,
  },
  {
    field: "ACCOUNT_NO2",
    hide: true,
  },
  {
    field: "ACCOUNT_NO3",
    hide: true,
  },
  {
    field: "ACCOUNT_NO4",
    hide: true,
  },
  {
    field: "ACCOUNT_NM1",
    hide: true,
  },
  {
    field: "ACCOUNT_NM2",
    hide: true,
  },
  {
    field: "ACCOUNT_NM3",
    hide: true,
  },
  {
    field: "ACCOUNT_NM4",
    hide: true,
  },
  //#endregion
  //#region ::  작업설정
  {
    // 출고통제
    field: "DLV_CNTL",
    hide: true,
  },
  {
    // 안전재고기준타입
    field: "SAFE_STOCK_TY",
    hide: true,
  },
  {
    // 입고 LOT_NO 사용
    field: "LOT_NO_USE_YN",
    hide: true,
  },
  {
    // 거래처 필수유무
    field: "CLIENT_NEC_YN",
    hide: true,
  },
  {
    // SAP 사용유무
    field: "SAP_USE_YN",
    hide: true,
  },
  {
    // 구성품주문입력여부
    field: "SET_RELEASE_YN",
    hide: true,
  },
  {
    // 배송추적사용여부
    field: "PARCEL_TRACE_YN",
    hide: true,
  },
  {
    // 박스추천사용여부
    field: "PARCEL_BOX_RECOM_YN",
    hide: true,
  },
  {
    // key
    field: "INTERFACE_KEY",
    hide: true,
  },
  {
    // partner_key
    field: "PARTNER_KEY",
    hide: true,
  },
  //#endregion
  //#region ::  담당자정보
  {
    field: "DUTY_NM",
    hide: true,
  },
  {
    field: "DUTY_PHONE",
    hide: true,
  },
  {
    field: "DUTY_MOBILE",
    hide: true,
  },
  {
    field: "E_MAIL",
    hide: true,
  },
  //#endregion
  //#region ::  사업자정보
  {
    // 상호
    field: "TRADE_NM",
    hide: true,
  },
  {
    // 대표자성명
    field: "REP_NM",
    hide: true,
  },
  {
    // 사업자등록구분
    field: "BIZ_NO_TYPE",
    hide: true,
  },
  {
    // 사업자번호
    field: "BIZ_NO",
    hide: true,
  },
  {
    // 전화번호
    field: "TEL",
    hide: true,
  },
  {
    // 팩스번호
    field: "FAX",
    hide: true,
  },
  {
    // 우편번호
    field: "ZIP",
    hide: true,
  },
  {
    // 주소
    field: "ADDR",
    hide: true,
  },
  {
    // 홈페이지주소
    field: "HTTP",
    hide: true,
  },
  {
    // 거래형태
    field: "DEAL_TYPE",
    hide: true,
  },
  {
    // 주요제품
    field: "MAJOR_PRODUCTS",
    hide: true,
  },
  {
    // 관리종목
    field: "MANAGE_EVENT",
    hide: true,
  },
  {
    // 관리업태
    field: "MANAGE_BIZ_CONT",
    hide: true,
  },
  {
    // 업태
    field: "BIZ_COND",
    hide: true,
  },
  {
    // 업종
    field: "BIZ_TYPE",
    hide: true,
  },
  //#endregion
  // {
  //   field: "ORI_CUST_CD",
  //   hide: true,
  // },
  // {
  //   field: "MOBILE_NO",
  //   hide: true,
  // },
  // {
  //   field: "DUTY_ID",
  //   hide: true,
  // },
  // {
  //   field: "REG_NO",
  //   hide: true,
  // },
  // {
  //   field: "CRE_LIMIT_DAY",
  //   hide: true,
  // },
  // {
  //   field: "REPRESENT_CUST_CD",
  //   hide: true,
  // },
  // {
  //   field: "CURR_CD",
  //   hide: true,
  // },
  // {
  //   field: "CRE_SALE_AMT",
  //   hide: true,
  // },
  // {
  //   field: "CRE_LIMIT_AMT",
  //   hide: true,
  // },
  // {
  //   field: "ORD_AMT",
  //   hide: true,
  // },
  // {
  //   field: "UN_RECEPT_AMT",
  //   hide: true,
  // },
  // {
  //   field: "SALE_PER_NO",
  //   hide: true,
  // },
  // {
  //   field: "CUST_STAT",
  //   hide: true,
  // },
  // {
  //   field: "VAT_YN",
  //   hide: true,
  // },
  // {
  //   field: "TR_UPDOWN_YN",
  //   hide: true,
  // },
  // {
  //   field: "IN_TON",
  //   hide: true,
  // },
  // {
  //   field: "CALC_SEQ",
  //   hide: true,
  // },
  // {
  //   field: "DLV_REQ_DT",
  //   hide: true,
  // },
  // {
  //   field: "DLV_CNTL_COMMENT",
  //   hide: true,
  // },
  // {
  //   field: "REP_CUST_REP_NM",
  //   hide: true,
  // },
  // {
  //   field: "LC_POSITION_CD",
  //   hide: true,
  // },
  // {
  //   field: "RCV_RCM_TY",
  //   hide: true,
  // },
  // {
  //   field: "EVENT",
  //   hide: true,
  // },
  // {
  //   field: "CONT_ID",
  //   hide: true,
  // },
  // {
  //   field: "CONT_DETAIL_ID",
  //   hide: true,
  // },
];
