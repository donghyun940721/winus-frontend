/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:    WMSMS092_3/input.grid.ts
 *  Description:    임가공상품구성정보 그리드 메타 정보
 *  Authors:        H.N.KO
 *  Update History:
 *                  2024.07. : Created by H.N.KO
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format } from "@/lib/ag-grid/index";
import { IControlBtn, IGridCellSearchButton, IGridCellSelectBox, info } from "@/types";
import { GridOptions } from "ag-grid-community";
import { MASTER_GRID_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSCM011",
  pk: "RNUM",
  pk2: "PART_RITEM_ID",
};

export const DETAIL_GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  UOM_NM: {
    pk: "RNUM",
    optionsKey: "UOM",
  },
};

export const DETAIL_GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  ITEM: {
    fieldList: ["PART_RITEM_ID", "ITEM_CODE", "PART_RITEM_NM", "UOM_ID", "UOM_NM"],
    rowDataKeys: ["RITEM_ID", "ITEM_CODE", "ITEM_KOR_NM", "UOM_ID", "UOM_NM"],
    modalData: {
      page: "WMSCM091",
      id: "kit-product",
      title: "search-product",
      gridTitle: "product-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달

      defaultParamsData: {
        //해당 모달의 title
        storeSaveKey: "owner",
        paramsKeys: ["vrSrchCustId"],
        rowDataKeys: ["CUST_ID"],
      },
      apis: {
        url: "/WMSCM091/list_rn.action",
        params: {
          // func: "fn_setWMSCM091_subE6",
          vrSrchCustId: "",
          vrSrchWhId: "",
          vrViewSetItem: "",
          vrViewAll: "viewAll",
          vrItemType: "",
          RITEM_ID: "",
          ITEM_CODE: "",
          ITEM_KOR_NM: "",
          UOM_ID: "",
          TIME_PERIOD_DAY: "",
          STOCK_QTY: "",
          CUST_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          UNIT_PRICE: "",
          vrSrchItemCd: "",
          vrSrchItemNm: "",
          vrSrchItemGrp: "",
          vrSrchSetItemYn: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchItemCd",
          title: "product-code",
          searchContainerInputId: "vrSrchItemCd",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemNm",
          searchContainerInputId: "vrSrchItemNm",
          title: "product-name",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemGrp",
          title: "product-group",
          type: "select",
          width: "half",
          optionsKey: "ITEMGRP",
          options: [{ name: "all", nameKey: "all", value: "" }],
        },
        {
          id: "vrSrchSetItemYn",
          title: "repacking",
          type: "select",
          width: "half",
          options: [
            { name: "", nameKey: "all", value: "" },
            { name: "useProductY", nameKey: "useProductY", value: "Y" },
            { name: "useProductN", nameKey: "useProductN", value: "N" },
          ],
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "CUST_NM",
        headerKey: "owner",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_GRP_NAME",
        headerKey: "product-group",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_CODE",
        headerKey: "product-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_KOR_NM",
        headerKey: "product-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "BOX_BAR_CD",
        headerKey: "box-barcode",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "MAKER_NM",
        headerKey: "company-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "STOCK_QTY",
        headerKey: "current-stock",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "BAD_QTY",
        headerKey: "inferior-product",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "UOM_NM",
        headerKey: "uom",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "UNIT_PRICE",
        headerKey: "unit-price",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
        valueFormatter: Format.NumberPrice,
      },
      {
        field: "WH_NM",
        headerKey: "warehouse",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "RITEM_ID",
        headerKey: "product-code",
        hide: true,
      },
      {
        field: "ITEM_ENG_NM",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "CUST_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_NM",
        sortable: true,
        hide: true,
      },
    ],
  },
};

export const MASTER_CONTROL_BTN: IControlBtn[] = [];

export const DETAIL_CONTROL_BTN: IControlBtn[] = [
  {
    title: "enter-excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    authType: "INS_AUTH",
  },
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    // disabled: "disabled",
    authType: "DEL_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const MASTER_CELL_RENDERER_INFO: IGridCellSelectBox = {
  UOM_NM: {
    pk: "RNUM",
    optionsKey: "UOM",
  },
};

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [100, 200, 300, 500, 1000, 5000, 10000],
};
// export const MasterGridMetaData: any = {
//   // 그리드 헤더
//   gridHeaderName: t("menu-header-title.WMSMS092_3_master"),

//   // 그리드 사용자 설정 사용여부
//   useUserSetting: false,

//   //페이지 키
//   checkBoxColumn: "check-box-column",
//   //페이징옵션
//   pagingSizeList: [100, 200, 300, 500, 1000, 5000, 10000],
// };

// export const DetailGridMetaData: any = {
//   // 그리드 헤더
//   gridHeaderName: t("menu-header-title.WMSMS092_3_detail"),

//   // 그리드 사용자 설정 사용여부
//   useUserSetting: false,

//   //페이지 키
//   checkBoxColumn: "check-box-column",
//   //페이징옵션
//   pagingSizeList: [100, 200, 300, 500, 1000, 5000, 10000],
// };

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  allowContextMenuWithControlKey: true,
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
};

// export const MastergridOptionsMeta: GridOptions = {
// popupParent: document.body,
//   defaultColDef: {
//     resizable: true,
//     menuTabs: [],
//   },
//   context: GRID_CELL_SEARCH_BUTTON,
//   headerHeight: 32,
//   rowHeight: 32,
//   columnDefs: MASTER_GRID_COLUMN_DEFS,
//   suppressRowClickSelection: true,
//   suppressContextMenu: true,
//   suppressCellFocus: true,
//   rowSelection: "single",
//   rowModelType: "clientSide",
//   pagination: true,
//   getRowId: (data) => {
//     return data.data[INFO.pk];
//   },
// };

// export const DetailgridOptionsMeta: GridOptions = {
// popupParent: document.body,
//   defaultColDef: {
//     resizable: true,
//     menuTabs: [],
//   },
//   headerHeight: 32,
//   rowHeight: 32,
//   columnDefs: DETAIL_GRID_COLUMN_DEFS,
//   suppressRowClickSelection: true,
//   suppressContextMenu: true,
//   suppressClickEdit: true,
//   rowSelection: "multiple",
//   rowModelType: "clientSide",
//   pagination: true,
//   enableCellEditingOnBackspace: false,
//   groupSelectsChildren: true,
//   getRowId: (data) => {
//     //고유 key값
//     return data.data.RNUM;
//   },
// };
