/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:    WMSMS092_3/input.grid.ts
 *  Description:    임가공상품구성정보 컬럼 정보
 *  Authors:        H.N.KO
 *  Update History:
 *                  2024.07. : Created by H.N.KO
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import GridSearchButton from "@/components/renderer/grid-search-button.vue";
import GridSelectBox from "@/components/renderer/grid-select-box.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { Format } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "",
    headerKey: "no",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    width: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    headerClass: "header-center",
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerName: "",
    sortable: true,
    width: 200,
    headerClass: "header-center",
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
    sortable: true,
    width: 250,
    headerClass: "header-center",
  },
  {
    field: "ITEM_KOR_NM",
    headerName: "",
    headerKey: "product-name",
    sortable: true,
    width: 550,
    headerClass: "header-center",
  },
  {
    field: "UNIT_NM",
    headerName: "",
    headerKey: "qty-by-box",
    sortable: true,
    width: 350,
    headerClass: "header-center",
  },
  {
    field: "ITEM_ENG_NM",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ITEM_SHORT_NM",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "RITEM_ID",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "REP_UOM_ID",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "CUST_ID",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "CUST_CD",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ITEM_BAR_CD",
    headerName: "",
    sortable: true,
    hide: true,
  },
];

//세트상품구성 컬럼
export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerKey: "no",
    headerName: "",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RITEM_ID).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerName: "",
    width: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
  },
  {
    field: "ITEM",
    headerName: "",
    width: 30,
    cellRenderer: GridSearchButton,
    cellClass: "renderer-cell",
  },
  {
    field: "PART_RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
  },
  {
    field: "SET_ITEM_YN",
    headerKey: "repacking-product-y/n",
    headerName: "",
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    sortable: true,
  },
  {
    field: "QTY",
    headerKey: "qty",
    headerName: "",
    cellStyle: { textAlign: "right" },
    headerClass: "header-require",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    headerClass: "header-require",
    sortable: true,
    cellRenderer: GridSelectBox,
    optionsAutoSelected: { autoSelectedKeyIndex: 0 },
  },
  {
    field: "UOM_ID",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "REG_DT",
    headerKey: "creation-date",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
  {
    field: "REG_NM",
    headerKey: "writer",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "UPD_DT",
    headerKey: "date-modified",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "UPD_NM",
    headerKey: "modifier",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "SET_RITEM_ID",
    sortable: true,
    hide: true,
  },
  {
    field: "SET_RITEM_NM",
    sortable: true,
    hide: true,
  },
  {
    field: "SET_SEQ",
    sortable: true,
    hide: true,
  },
  {
    field: "PART_RITEM_ID",
    sortable: true,
    hide: true,
  },
  {
    field: "PART_RITEM_CODE",
    sortable: true,
    hide: true,
  },
];

//화주/상품 모달 컬럼
export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],

  "kit-product": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
      valueFormatter: Format.NumberPrice,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "RITEM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "ITEM_ENG_NM",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "CUST_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_CD",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_CD",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_NM",
      sortable: true,
      hide: true,
    },
  ],
};
