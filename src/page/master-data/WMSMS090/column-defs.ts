/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS090/column-defs.ts
 *  Description:    기준정보/상품정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridSearchButton from "@/components/renderer/grid-search-button.vue";
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const ONLY_GRID_VIEW_COLUMN_DEFS: any = [
  {
    field: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1,
  },
  {
    field: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    sortable: true,
    width: 500,
  },
  {
    field: "ITEM_BAR_CD",
    headerKey: "barcode",
    sortable: true,
    width: 500,
  },
];

export const MODAL_COLUMN_DEFS: any = {
  "deleted-product": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1,
    },
    {
      field: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "ST_GUBUN",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },

    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_NM",
      headerKey: "product-name",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BAR_CD",
      headerKey: "product-barcode",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_NM",
      headerKey: "unit",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UPD_DT",
      headerKey: "date-modified",
      headerClass: "header-center",
      cellStyle: { textAlign: "center" },
      sortable: true,
    },
    {
      field: "UPD_NO",
      headerKey: "modifier",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_ID",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
  ],
  owner: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",

      width: 60,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",

      headerKey: "shipper-code",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",

      headerKey: "owner-name",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",

      headerKey: "address",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",

      headerKey: "manager-name",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",

      headerKey: "owner-epc-code",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerName: "",
      headerKey: "tel",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "product-code": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NM",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      headerClass: "header-center",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "RITEM_ID",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },

    {
      field: "REP_UOM_ID",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "CUST_ID",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_CD",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_ID",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_CD",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_NM",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
  ],
  "default-receiving-zone": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 60,
    },
    {
      field: "ZONE_ID",
      hide: true,
    },
    {
      field: "ZONE_NM",
      headerKey: "zone-name",
      headerClass: "header-center",
      sortable: true,
      width: 600,
    },
  ],
  "default-shipping-zone": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1,
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 60,
    },
    {
      field: "ZONE_ID",
      hide: true,
    },
    {
      field: "ZONE_NM",
      headerKey: "zone-name",
      headerClass: "header-center",
      sortable: true,
      width: 600,
    },
  ],
  "in-transcust-zone": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1,
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 60,
    },
    {
      field: "CUST_ZONE_ID",
      hide: true,
    },
    {
      field: "CUST_ZONE_CD",
      hide: true,
    },
    {
      field: "CUST_ZONE_NM",
      headerKey: "zone-name",
      headerClass: "header-center",
      sortable: true,
      width: 600,
    },
  ],
  "out-transcust-zone": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1,
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 60,
    },
    {
      field: "CUST_ZONE_ID",
      hide: true,
    },
    {
      field: "CUST_ZONE_CD",
      hide: true,
    },
    {
      field: "CUST_ZONE_NM",
      headerKey: "zone-name",
      headerClass: "header-center",
      sortable: true,
      width: 600,
    },
  ],
  "default-fixed-location": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "LOC_CD",
      headerKey: "location",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "",
      headerKey: "product",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      children: [
        { field: "RITEM_CD", columnGroupShow: "open", headerKey: "code", headerName: "" },

        { field: "RITEM_NM", columnGroupShow: "open", headerKey: "product-name", headerName: "" },
      ],
    },
    {
      field: "AVAILABLE_QTY",
      headerKey: "stock-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "OUT_EXP_QTY",
      headerKey: "schedule-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "REAL_PLT_QTY",
      headerKey: "plt-quantity",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_WEIGHT",
      headerKey: "weight",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_LOT_NO",
      headerKey: "lot-number",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_BEST_DATE_END",
      headerKey: "expiration-date",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "packaging-box-type": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "POOL_GRP_NAME",
      headerKey: "pool-grp-id",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_CODE",
      headerKey: "pool-grp-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_NM",
      headerKey: "pool-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "POOL_SIZE",
      headerKey: "pool-size",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "IN_WH_CD",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "IN_ZONE_NM",
      headerKey: "receiving-zone",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "IN_WH_ID",
      headerKey: "in-zone-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "IN_ZONE_ID",
      headerKey: "in-zone-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "POOL_ID",
      headerKey: "in-zone-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "RITEM_ID",
      headerKey: "in-zone-nm",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
  ],
  "representative-uom": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "UOM_CD",
      headerKey: "uom-code",
      headerClass: "header-center",
    },
    {
      field: "UOM_NM",
      headerKey: "uom-name",
      headerClass: "header-center",
    },
    {
      field: "UOM_ID",
      hide: true,
    },
    {
      field: "UPD_NO",
      hide: true,
    },
    {
      field: "REG_NO",
      hide: true,
    },
    {
      field: "LC_ID",
      hide: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "STATE") && params.data.STATE === "C") {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "STATE",
    headerName: "",
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    field: "ST_GUBUN",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },

  {
    field: "CUST_CD",
    headerName: t("grid-column-name.owner"),
    headerClass: "header-center",
    width: 150,
    sortable: true,
    // export: true,
  },
  {
    field: "ITEM_GRP_NM",
    headerName: t("grid-column-name.product-group"),
    headerClass: "header-center",
    width: 120,
    sortable: true,
    // export: true,
  },
  {
    field: "ITEM_CODE",
    headerName: t("grid-column-name.product-code"),
    headerClass: "header-center",
    width: 140,
    sortable: true,
    // export: true,
  },
  {
    field: "ITEM_COLOR_VIEW",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "ITEM_KOR_NM",
    headerName: t("grid-column-name.product-name"),
    headerClass: "header-center",
    width: 200,
    sortable: true,
    // export: true,
  },
  {
    field: "UNIT_NM_DETAIL",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "UNIT_QTY",
    headerName: t("grid-column-name.qty-by-box"),
    headerClass: "header-center",
    width: 100,
    cellStyle: { textAlign: "right" },
    valueGetter: Getter.emptyString,
    sortable: true,
  },
  {
    field: "REP_UOM_CD",
    headerName: t("grid-column-name.uom"),
    headerClass: "header-center",
    width: 100,
    sortable: true,
    // export: true,
  },
  {
    field: "ITEM_ENG_NM",
    headerName: t("grid-column-name.product-name-english"),
    headerClass: "header-center",
    width: 200,
    sortable: true,
    // export: true,
  },
  {
    field: "IN_ZONE_NM",
    headerName: t("grid-column-name.receiving-zone"),
    headerClass: "header-center",
    sortable: true,
    // export: true,
  },
  {
    field: "OUT_ZONE_NM",
    headerName: t("grid-column-name.shipping-zone"),
    headerClass: "header-center",
    sortable: true,
    // export: true,
  },
  {
    field: "IN_CUST_ZONE_NM",
    headerName: t("grid-column-name.in-transcust-zone"),
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "OUT_CUST_ZONE_NM",
    headerName: t("grid-column-name.out-transcust-zone"),
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "PROP_QTY",
    headerName: t("grid-column-name.appropriate-stock"),
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    // export: true,
    width: 110,
    valueFormatter: Format.NumberCount,
  },
  {
    field: "EXPIRY_DATE",
    headerName: t("grid-column-name.expiration-date"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
    width: 110,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.registration-date"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    // valueGetter: (params: any) => {
    //   if (params.data["REG_DT"]) {
    //     return UtilService.convertTimestampToDateFormat(params.node.data.REG_DT);
    //   }
    // },
    valueGetter: Getter.Date,
    sortable: true,
    width: 170,
  },
  {
    field: "UPD_DT",
    headerName: t("grid-column-name.modified-date"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    // valueGetter: (params: any) => {
    //   if (params.data["UPD_DT"]) {
    //     return UtilService.convertTimestampToDateFormat(params.node.data.UPD_DT);
    //   }
    // },
    valueGetter: Getter.Date,
    sortable: true,
    width: 170,
  },
  {
    field: "ITEM_ID",
    headerName: t("grid-column-name.upload-id"),
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
    width: 130,
  },
  {
    field: "WORKING_TIME",
    headerName: t("grid-column-name.working-time"),
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    // Maker이름
    field: "MAKER_NM",
    headerName: t("grid-column-name.maker-nm"),
    headerClass: "header-center",
    sortable: true,
    // export: true,
  },
  {
    field: "LC_CD",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    //! 상품구분 필수값
    field: "RITEM_ID",
    hide: true,
  },
  {
    field: "STOCK_UOM_ID",
    hide: true,
  },
  {
    field: "BUY_UNIT",
    hide: true,
  },
  {
    field: "ITEM_GRP_TYPE",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    // 상품군(중)
    field: "ITEM_GRP_2ND_ID",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    // 상품군(소)
    field: "ITEM_GRP_3RD_ID",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  //#region ::  기본정보
  {
    // 화주ID
    field: "CUST_ID",
    hide: true,
  },
  {
    // 화주명칭
    field: "CUST_NM",
    hide: true,
  },
  {
    // 상품코드
    field: "ITEM_CODE",
    hide: true,
  },
  {
    // 상품명(현지어)
    field: "ITEM_KOR_NM",
    hide: true,
  },
  {
    // 상품명(영어)
    field: "ITEM_ENG_NM",
    hide: true,
  },
  {
    // 상품명(약어)
    field: "ITEM_SHORT_NM",
    hide: true,
  },
  {
    // 임가공상품여부
    field: "SET_ITEM_YN",
    headerName: t("grid-column-name.repacking-product-y/n"),
    hide: true,
    // export: true,
  },
  {
    // 대표UOM
    field: "REP_UOM_CD",
    hide: true,
  },
  {
    // 대표UOM (ID)
    field: "REP_UOM_ID",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    // 상품군(대)
    field: "ITEM_GRP_ID",
    hide: true,
  },
  {
    // 상품군(중)
    field: "ITEM_GRP_2ND_ID",
    hide: true,
  },
  {
    // 상품군(소)
    field: "ITEM_GRP_3RD_ID",
    hide: true,
  },
  {
    // 상품구분코드
    field: "ITEM_DEVISION_CD",
    hide: true,
  },
  {
    // 상품설명
    field: "ITEM_DESC",
    hide: true,
    // export: true,
  },
  {
    // 상품이미지
    field: "ORG_FILENAME",
    hide: true,
  },
  {
    // 상품관리TYPE
    field: "TYPE_ST",
    hide: true,
  },
  {
    // 사용여부
    field: "USE_YN",
    hide: true,
  },
  {
    // 상품상태
    field: "ITEM_TYPE",
    hide: true,
  },
  //#endregion
  //#region ::  부가코드 정보
  {
    // 화주상품코드
    field: "CUST_ITEM_CD",
    headerName: t("grid-column-name.owner-product-code"),
    hide: true,
  },
  {
    // 고객상품코드
    field: "CUST_LEGACY_ITEM_CD",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    // 박스바코드
    field: "BOX_BAR_CD",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    // 상품바코드
    field: "ITEM_BAR_CD",
    headerName: t("grid-column-name.product-barcode"),
    hide: true,
    // export: true,
  },
  {
    // 고객상품바코드
    field: "CUST_BARCODE",
    hide: true,
    // export: true,
  },
  {
    // ITF CODE
    field: "ITF_CD",
    hide: true,
  },
  {
    //
    field: "TAG_PREFIX",
    hide: true,
  },
  {
    //
    field: "BOX_EPC_CD",
    hide: true,
  },
  {
    //
    field: "ITEM_EPC_CD",
    hide: true,
  },
  //#endregion
  //#region ::  상세정보
  {
    // 색상
    field: "COLOR",
    hide: true,
  },
  {
    // 상품중량
    field: "ITEM_WGT",
    headerName: t("grid-column-name.product-weight"),
    hide: true,
    // export: true,
  },
  {
    // 단가
    field: "UNIT_PRICE",
    headerName: t("grid-column-name.unit-price"),
    hide: true,
    // export: true,
  },
  {
    // 판매가
    field: "SALES_PRICE",
    hide: true,
  },
  {
    // 통화명
    field: "CURRENCY_NAME",
    hide: true,
    // export: true,
  },
  {
    // 출하등급
    field: "SHIP_ABC",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    // 무게등급
    field: "WEIGHT_CLASS",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    // 단위수량(입수)
    field: "UNIT_QTY",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    //단위명
    field: "UNIT_NM",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    // 가로
    field: "SIZE_W",
    headerName: t("grid-column-name.horizontal"),
    hide: true,
    // export: true,
  },
  {
    //세로
    field: "SIZE_H",
    headerName: t("grid-column-name.vertical"),
    hide: true,
    // export: true,
  },
  {
    // 높이
    field: "SIZE_L",
    headerName: t("grid-column-name.height"),
    hide: true,
    // export: true,
  },
  {
    // 크기
    field: "ITEM_SIZE",
    hide: true,
  },
  {
    // 내수/수출구분
    field: "IN_OUT_BOUND",
    hide: true,
  },
  {
    // 적정재고일수
    field: "PROP_STOCK_DAY",
    hide: true,
    // export: true,
  },
  {
    // 팔레트관리여부
    field: "PLT_YN",
    hide: true,
  },
  {
    //REWORK 여부
    field: "REWORK_ITEM_YN",
    hide: true,
  },
  {
    // 품온
    field: "ITEM_TMP",
    hide: true,
  },
  {
    // 설비품온구분
    field: "DEV_ITEM_TMP",
    hide: true,
    // export: true,
  },
  {
    // 임가공상품타입
    field: "SET_ITEM_TYPE",
    hide: true,
    // export: true,
  },
  //#endregion
  //#region ::  기간정보
  {
    // 유통기한통제기준
    field: "BEST_DATE_TYPE",
    hide: true,
  },
  {
    // 유효기간
    field: "BEST_DATE_NUM",
    headerName: t("grid-column-name.validity"),
    hide: true,
    // export: true,
  },
  {
    // 소비기한
    field: "USE_DATE_NUM",
    hide: true,
  },
  {
    // 유효기간단위
    field: "BEST_DATE_UNIT",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    // 소비기한단위
    field: "USE_DATE_UNIT",
    hide: true,
  },
  {
    // 출하최소유효기간단위
    field: "OUT_BEST_DATE_UNIT",
    hide: true,
  },
  {
    // 입고최소유효기간단위
    field: "IN_BEST_DATE_UNIT",
    hide: true,
  },
  {
    // 출고최소유효기간
    field: "OUT_BEST_DATE_NUM",
    hide: true,
  },
  {
    // 입고최소유효기간
    field: "IN_BEST_DATE_NUM",
    hide: true,
  },
  //#endregion
  //#region ::  작업설정
  {
    // 입고추천타입
    field: "IN_LOC_REC",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    // 출고추천타입
    field: "OUT_LOC_REC",
    hide: true,
    // export: true,
  },
  {
    // 입고사용여부
    field: "LOT_USE_YN",
    hide: true,
    // export: true,
  },
  {
    // 출고자동주문사용여부
    field: "AUTO_OUT_ORD_YN",
    hide: true,
  },
  {
    // 물류용기자동계산
    field: "AUTO_CAL_PLT_YN",
    hide: true,
  },
  {
    //
    field: "LOT_PREFIX",
    hide: true,
  },
  {
    // 기본입고존
    field: "IN_ZONE",
    hide: true,
  },
  {
    // 기본출고존
    field: "OUT_ZONE",
    hide: true,
  },
  {
    // 입고거래처존
    field: "IN_CUST_ZONE",
    hide: true,
  },
  {
    // 출고거래처존
    field: "OUT_CUST_ZONE",
    hide: true,
  },
  {
    // 기본고정로케이션 (명칭)
    field: "FIX_LOC_NM",
    hide: true,
  },
  {
    // 기본고정로케이션 (코드)
    field: "FIX_LOC_CD",
    hide: true,
    // export: true,
  },
  {
    // 기본고정로케이션 (ID)
    field: "FIX_LOC_ID",
    hide: true,
  },
  //#endregion
  //#region ::  택배설정 (포장)
  {
    field: "ITEM_CAPACITY",
    hide: true,
    // export: true,
  },
  {
    field: "PARCEL_CBM",
    hide: true,
  },
  {
    field: "PARCEL_IN_QTY",
    hide: true,
  },
  // {
  //   // 포장BOX종류(명칭)
  //   field: "PACKING_BOX_TYPE_NM",
  //   hide: true,
  // },
  {
    // 포장BOX종류(코드)
    field: "PACKING_BOX_TYPE_CD",
    hide: true,
  },
  {
    // 포장BOX종류(ID)
    field: "PACKING_BOX_TYPE",
    hide: true,
  },
  //#endregion

  // {
  //   field: "TIME_PERIOD_DAY",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "ORD_CUST_ID",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "KAN_CD",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "VAT_YN",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "ITEM_CLASS",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "OP_QTY",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "RS_QTY",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "EA_YN",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "RFID",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "IN_WH_ID",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },

  // {
  //   field: "IN_WH_NM",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "OP_UOM_ID",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },

  // {
  //   field: "CUST_EPC_CD",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "INNER_BOX_BAR_CD",
  //   headerName: t("grid-column-name.box-barcode"),
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  //   // export: true,
  // },
  // {
  //   field: "CUST_ITEM_CD",
  //   headerName: t("grid-column-name.cust-item-cd"),
  //   headerClass: "header-center",
  //   sortable: true,
  // },
  // {
  //   field: "UOM_YN",
  //   headerName: "UOM_YN",
  //   hide: true,
  //   // export: true,
  // },
  // {
  //   field: "DEL_YN",
  //   headerName: "DEL_YN",
  //   hide: true,
  //   // export: true,
  // },
  // {
  //   field: "QTY",
  //   headerName: t("grid-column-name.converted-qty-number"),
  //   hide: true,
  //   // export: true,
  // },
  // {
  //   field: "UOM1_CODE",
  //   headerName: t("grid-column-name.converted-qty-unit-box"),
  //   hide: true,
  //   // export: true,
  // },
  // {
  //   field: "UOM2_CODE",
  //   headerName: t("grid-column-name.transformation-unit-plt"),
  //   hide: true,
  //   // export: true,
  // },
  // {
  //   field: "IMAGE_PATH",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "IMAGE_ID",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "ESSENTIAL",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "GET_DATE",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "TRAN_BAR_CD",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "ITEM_ETC1",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },

  // {
  //   field: "CURR_STOCK_YN",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "COUNTRY_ORIGIN",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "PARCEL_COM_TY",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "SERIAL_CHK_YN",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "PARCEL_COMP_CD",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "MACHINE_TYPE",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "FIX_LOC_ZONE_ID",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "FIX_LOC_ZONE_NM",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "MIN_PARCEL_CBM",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "SUB_MATERIALS",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "PLT_BAR_CD",
  //   headerClass: "header-center",
  //   sortable: true,
  //   hide: true,
  // },
];

// UOM환산이력 컬럼정보
export const UOM_CONVERSION_HISTORY_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "STATE") && params.data.STATE === "C") {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    field: "",
    maxWidth: 60,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
  },
  {
    field: "STATE",
    headerName: "",
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    // 수량
    field: "QTY",
    headerName: t("grid-column-name.qty"),
    width: 90,
    editable: true,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    sortable: true,
    flex: 1,
  },
  {
    // 기준 UOM
    field: "UOM1_CODE",
    headerName: t("grid-column-name.standard-uom"),
    width: 100,
    flex: 2,
  },
  {
    field: "UOM1_CD",
    hide: true,
    width: 60,
  },
  {
    // 버튼 (기준UOM)
    field: "UOM1",
    headerName: "",
    maxWidth: 50,
    cellRenderer: GridSearchButton,
    cellClass: "renderer-cell",
  },
  {
    field: "",
    headerName: "→",
    maxWidth: 60,
    cellStyle: { textAlign: "center" },
    // TODO :: 표현 방식 변경 필요
    valueGetter: () => {
      return "→";
    },
  },
  {
    // 수량
    field: "UOM2_QTY",
    headerName: t("grid-column-name.qty"),
    sortable: true,
    width: 90,
    flex: 1,
    cellStyle: { textAlign: "right" },
    valueGetter: (params: any) => 1,
  },
  {
    // 변환 UOM
    field: "UOM2_CODE",
    headerName: t("grid-column-name.convert-uom"),
    sortable: true,
    flex: 2,
  },
  {
    field: "UOM2_CD",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    // 버튼 (변환UOM)
    field: "UOM2",
    headerName: "",
    maxWidth: 50,
    cellRenderer: GridSearchButton,
    cellClass: "renderer-cell",
  },
  {
    field: "UOM_SEQ",
    sortable: true,
    hide: true,
  },
  {
    field: "RITEM_ID",
    sortable: true,
    hide: true,
  },
  {
    field: "CLOSE_DT",
    sortable: true,
    hide: true,
  },
  {
    field: "UOM1_CD",
    sortable: true,
    hide: true,
  },
  {
    field: "UOM2_CD",
    sortable: true,
    hide: true,
  },
];
