/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS090/input.meta.ts
 *  Description:    기준관리/상품정보관리 Meta data
 *  Authors:        dhkim
 *  Update History:
 *                  2024.07 : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import type { info } from "@/types";
import { UOM_CONVERSION_HISTORY_COLUMN_DEFS } from "./column-defs.ts";
import { gridMetaData, gridOptionsMeta } from "./input.grid.ts";
import { INFO_INPUT, INFO_MODAL_INFO, InfoGridMeta, InfoGridOptions } from "./input.info.ts";
import { SEARCH_CONTAINER_META } from "./input.search.ts";

export * from "./input.grid.ts";
export * from "./input.info.ts";
export * from "./input.search.ts";

export const commonSetting = {
  authPageGroup: "WMSMS",
  authPageId: "WMSMS090",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const INFO_META = {
  infoInput: INFO_INPUT,
  modalColumnDefs: SEARCH_META.modalColumnDefs,
  searchModalInfo: INFO_MODAL_INFO,
  formColumnDefs: UOM_CONVERSION_HISTORY_COLUMN_DEFS,
  gridOptionsMeta: InfoGridOptions,
  ...InfoGridMeta,
  ...commonSetting,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};

export const INFO: info = {
  autoModal: true,
  autoModalPage: "WMSCM011",
  pk: "RNUM",
};
