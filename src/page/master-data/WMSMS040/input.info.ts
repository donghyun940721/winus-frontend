/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS040/input.info.ts
 *  Description:    기준관리/창고정보관리 Meta data
 *  Authors:        dhkim
 *  Update History:
 *                  2024.07 : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import { IControlBtn, IInfoInputContainer } from "@/types";

export const INFO_INPUT: IInfoInputContainer[] = [
  {
    title: "warehouse-information",
    default: "expand",
    inputs: [
      {
        ids: ["WH_ID"],
        rowDataIds: ["WH_ID"],
        title: "warehouse-id",
        type: "text",
        width: "full",
        size: "single",
        required: false,
        isReadonly: true,
      },
      {
        ids: ["WH_CD"],
        rowDataIds: ["WH_CD"],
        title: "warehouse-code",
        type: "text",
        width: "full",
        size: "single",
        required: true,
      },
      {
        ids: ["WH_NM"],
        rowDataIds: ["WH_NM"],
        title: "warehouse-name",
        type: "text",
        width: "full",
        size: "single",
        required: true,
      },
      {
        ids: ["WH_TYPE"],
        title: "warehouse-type",
        type: "select",
        width: "full",
        required: false,
        size: "single",
        optionsKey: "WhType",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["WH_GB"],
        title: "warehouse-category",
        type: "select",
        width: "full",
        required: false,
        size: "single",
        optionsKey: "WhGB",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["MAX_LEN_W"],
        rowDataIds: ["MAX_LEN_W"],
        title: "horizontal",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["MAX_LEN_L"],
        rowDataIds: ["MAX_LEN_L"],
        title: "vertical",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
    ],
  },
];

export const INFO_CONTROL_BTN: IControlBtn[] = [
  {
    title: "enter-excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    authType: "INS_AUTH",
  },
];
