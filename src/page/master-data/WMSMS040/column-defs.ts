/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS040/column-defs.ts
 *  Description:    기준정보/창고정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  warehouse: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "STATE") && params.data.STATE === "C") {
        return "";
      }
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "STATE",
    headerName: "",
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    field: "WH_NM",
    headerName: t("grid-column-name.warehouse-name"),
    minWidth: 200,
    sortable: true,
    // export: true,
    flex: 1,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.creation-date"),
    cellStyle: { textAlign: "center" },
    sortable: true,
    // export: true,
    width: 160,
  },
  {
    field: "REG_NM",
    headerName: t("grid-column-name.writer"),
    sortable: true,
    // export: true,
    width: 150,
  },
  {
    field: "UPD_DT",
    headerName: t("grid-column-name.Modified-Date"),
    cellStyle: { textAlign: "center" },
    sortable: true,
    // export: true,
    width: 160,
  },
  {
    field: "UPD_NM",
    headerName: t("grid-column-name.modifier"),
    sortable: true,
    // export: true,
    width: 150,
  },
  {
    // 창고 ID
    field: "WH_ID",
    // cellDataType: "text",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 창고 CODE
    field: "WH_CD",
    // cellDataType: "text",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 창고 명칭
    field: "WH_CD",
    // cellDataType: "text",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 창고 유형
    field: "WH_TYPE",
    // cellDataType: "text",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 창고 구분
    field: "WH_GB",
    // cellDataType: "text",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 창고 명칭
    field: "WH_CD",
    // cellDataType: "text",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 가로
    field: "MAX_LEN_W",
    // cellDataType: "text",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 세로
    field: "MAX_LEN_L",
    // cellDataType: "text",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
];
