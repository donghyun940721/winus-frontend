/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS120/page.vue
 *  Description:    기준정보/도크장정보관리 화면
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.08. : Created by  H. N. Ko
 *
-------------------------------------------------------------------------------*/
import type { info } from "@/types";
import { gridMetaData, gridOptionsMeta } from "./input.grid.ts";
import { INFO_INPUT } from "./input.info.ts";
import { SEARCH_CONTAINER_META } from "./input.search.ts";

export * from "./input.grid.ts";
export * from "./input.info.ts";
export * from "./input.search.ts";

export const commonSetting = {
  authPageGroup: "WMSMS",
  authPageId: "WMSMS120",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const INFO_META = {
  infoInput: INFO_INPUT,
  modalColumnDefs: SEARCH_META.modalColumnDefs,
  searchModalInfo: SEARCH_META.searchModalInfo,
  ...commonSetting,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSMS120",
  pk: "RNUM",
};
