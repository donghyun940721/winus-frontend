/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS120/page.vue
 *  Description:    기준정보/도크장정보관리 화면
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.08. : Created by  H. N. Ko
 *
-------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { IControlBtn } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const CONTROL_BTN: IControlBtn[] = [];

export const gridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSMS120"),

  //페이징옵션
  pagingSizeList: [100, 200, 300],
};

export const gridOptionsMeta = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
    lockVisible: true,
    lockPosition: true,
  },
  headerHeight: 32,
  rowHeight: 32,
  // cellDataType: false,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  suppressContextMenu: true, // Context Menu 미사용
  statusBar: true,
  // getRowId: (data) => {
  //   return data.data.RNUM;
  // },
};
