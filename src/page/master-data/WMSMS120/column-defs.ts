/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS120/page.vue
 *  Description:    기준정보/도크장정보관리 화면
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.08. : Created by  H. N. Ko
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  "car-no": [
    {
      field: "No",
      headerName: "No",
      cellStyle: { textAlign: "center" },
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      maxWidth: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "TRANS_NM",
      headerKey: "transporter",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CAR_CD",
      headerKey: "vehicle-number",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CAR_ID",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
    {
      field: "TRANS_CD",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
      hide: true,
    },
  ],
  "warehouse-code": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "WH_CD",
      headerKey: "warehouse-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_ID",
      hide: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_GB",
      headerKey: "warehouse-category",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_TYPE",
      headerKey: "warehouse-type",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  dock_code: [
    {
      field: "CHK_BOX",
      headerName: "",
      cellStyle: { textAlign: "center" },
      maxWidth: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "DOCK_NO",
      headerKey: "dock-no",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "center" },

      sortable: true,
    },
    {
      field: "DOCK_NM",
      headerKey: "dock-nm",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "center" },
      sortable: true,
    },
    {
      field: "DOCK_CD",
      headerKey: "",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "center" },
      sortable: true,
      hide: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    // pinned: "left",
    // lockPosition: true,
  },

  {
    field: "WH_CD",
    editable: true,
    hide: true,
  },
  {
    field: "WH_NM",
    editable: true,
    hide: true,
  },
  {
    field: "WH_ID",
    editable: true,
    hide: true,
  },
  //
  {
    field: "DOCK_CD",
    headerName: t("grid-column-name.code"),
    minWidth: 200,
    sortable: true,
    // export: true,
    flex: 1,
    editable: true,
  },
  {
    field: "DOCK_NM",
    headerName: t("grid-column-name.dock-name"),
    minWidth: 200,
    sortable: true,
    // export: true,
    flex: 1,
    editable: true,
  },
  {
    field: "CAR_ID",
    editable: true,
    hide: true,
  },
  {
    field: "CAR_CD",
    headerName: t("grid-column-name.vehicle-number"),
    minWidth: 200,
    sortable: true,
    flex: 1,
    editable: true,
  },

  {
    field: "SEQ",
    editable: true,
    hide: true,
  },
  {
    field: "IN_TON",
    editable: true,
    hide: true,
  },

  {
    field: "REMARK",
    editable: true,
    hide: true,
  },
];
export const DOCK_FORM_COLUMN_DEFS: any = [
  {
    field: "CHECK_BOX",
    headerName: "",
    cellStyle: { textAlign: "center" },
    maxWidth: 50,
    headerCheckboxSelection: true,
    checkboxSelection: true,
    hide: true,
  },
  {
    field: "RNUM",
    headerName: "No",
    cellStyle: { textAlign: "center" },
    width: 60,
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "CAR_CD",
    headerKey: "dock",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "CAR_TON",
    headerKey: "vehicle-number",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "CAR_ID",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "WH_ID",
    editable: true,
    sortable: true,
    hide: true,
  },
  {
    field: "DOCK_NO",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
];

export const gridColumns = {
  FORM_COLUMN_DEFS: FORM_COLUMN_DEFS,
  DOCK_FORM_COLUMN_DEFS: DOCK_FORM_COLUMN_DEFS,
};
