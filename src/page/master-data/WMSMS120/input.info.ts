/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS120/page.vue
 *  Description:    기준정보/도크장정보관리 화면
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.08. : Created by  H. N. Ko
 *
-------------------------------------------------------------------------------*/

import { IControlBtn, IInfoInputContainer } from "@/types";

export const INFO_INPUT: IInfoInputContainer[] = [
  {
    title: "warehouse-information",
    default: "expand",
    inputs: [
      //창고 코드
      {
        ids: ["WH_CD", "WH_NM"],
        hiddenId: "WH_ID",
        rowDataIds: ["WH_CD", "WH_NM"],
        rowDataHiddenId: "WH_ID",
        title: "warehouse-code",
        type: "search",
        width: "full",
        size: "single",
        required: false,
      },
      //도크장 코드
      {
        ids: ["DOCK_CD"],
        rowDataIds: ["DOCK_CD"],
        title: "dock_code",
        type: "text",
        width: "full",
        size: "single",
        required: true,
      },

      //도크장명
      {
        ids: ["DOCK_NM"],
        rowDataIds: ["DOCK_NM"],
        title: "dock-name",
        type: "text",
        width: "full",
        size: "single",
        required: true,
      },

      //도크장 순번
      {
        ids: ["SEQ"],
        rowDataIds: ["SEQ"],
        title: "dock-seq",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },

      //진입가능톤수
      {
        ids: ["IN_TON"],
        title: "in_ton",
        type: "select",
        width: "full",
        required: false,
        size: "single",
        options: [
          { name: "select", nameKey: "select", value: "" },
          { name: "1-ton", nameKey: "1-ton", value: "100" },
          { name: "2-5-ton", nameKey: "2-5-ton", value: "250" },
          { name: "3-5-ton", nameKey: "3-5-ton", value: "350" },
          { name: "5-ton", nameKey: "5-ton", value: "500" },
          { name: "11-ton", nameKey: "11-ton", value: "1100" },
          { name: "25-ton", nameKey: "25-ton", value: "150" },
          { name: "trailer", nameKey: "trailer", value: "200" },
          { name: "damas", nameKey: "damas", value: "1000" },
          { name: "labo", nameKey: "labo", value: "1001" },
          { name: "n-a", nameKey: "n-a", value: "00" },
          { name: "1-4-ton", nameKey: "1-4-ton", value: "140" },
          { name: "center-pickup", nameKey: "center-pickup", value: "010" },
          { name: "8-ton", nameKey: "8-ton", value: "800" },
          { name: "delivery", nameKey: "delivery", value: "020" },
          { name: "q-delivery", nameKey: "q-delivery", value: "030" },
        ],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      //차량번호
      {
        ids: ["CAR_CD"],
        hiddenId: "CAR_ID",
        rowDataIds: ["CAR_CD"],
        rowDataHiddenId: "CAR_ID",
        title: "car-no",
        type: "search",
        width: "full",
        size: "single",
        required: false,
      },
      //비고
      {
        ids: ["REMARK"],
        rowDataIds: ["REMARK"],
        title: "remark",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
    ],
  },
];

export const INFO_CONTROL_BTN: IControlBtn[] = [
  //엑셀입력
  {
    title: "enter-excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "INS_AUTH",
  },
  //저장
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  //신규
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  //삭제
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
];
