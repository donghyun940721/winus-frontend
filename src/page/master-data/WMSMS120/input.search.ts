/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS120/page.vue
 *  Description:    기준정보/도크장정보관리 화면
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.08. : Created by  H. N. Ko
 *
------------------------------------------------------------------------------*/
import type { IModal } from "@/types";
import type { ISearchInput } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const SEARCH_MODAL_INFO: IModal = {
  "car-no": {
    page: "WMSCM050",
    id: "vehicle",
    title: "search-vehicle",
    gridTitle: "vehicle-list",

    apis: {
      url: "/rest/WMSCM050/list.action",
      params: {
        vrSrchCarCd: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchCarCd",
        title: "vehicle-number",
        type: "text",
        width: "half",
      },
    ],
  },

  "warehouse-code": {
    page: "WMSMS040",
    id: "warehouse",
    title: "search-warehouse",
    gridTitle: "warehouse-list",

    apis: {
      url: "/WMSMS040/poplist_rn.action",
      params: {
        func: "fn_setWMSMS040",
        WH_ID: "",
        WH_CD: "",
        WH_NM: "",
        S_WH_CD: "",
        S_WH_NM: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "WH_CD",
        searchContainerInputId: "WH_CD",
        title: "warehouse-code",
        type: "text",
        width: "half",
      },
      {
        id: "WH_NM",
        searchContainerInputId: "WH_NM",
        title: "warehouse-name",
        type: "text",
        width: "half",
      },
    ],
  },
  dock_code: {
    page: "WMSCM120",
    id: "DOCK",
    title: "dock-search",
    gridTitle: "dock-list",
    apis: {
      url: "/rest/WMSMS120/list.action",
      params: {
        func: "fn_setWMSMS120",
        DOCK_NO: "",
        DOCK_NM: "",
        DOCK_CD: "",
        vrSrchDockNo: "",
        vrSrchDockNm: "",
        vrSrchDockCd: "",
      },

      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      //도크장 번호
      {
        id: "DOCK_NO",
        title: "dock-no",
        searchContainerInputId: "DOCK_NO",
        type: "text",
        width: "half",
      },
      //도크장명
      {
        id: "DOCK_NM",
        title: "dock-nm",
        searchContainerInputId: "DOCK_NM",
        type: "text",
        width: "half",
      },
    ],
  },
};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  //도크장 코드
  {
    ids: ["vrSrchDockNo", "vrSrchDockNm"],
    hiddenId: "vrSrchDockCd",
    rowDataIds: ["DOCK_NO", "DOCK_NM"],
    rowDataHiddenId: "DOCK_CD",
    searchApiKeys: ["vrSrchDockNo", "vrSrchDockNm"],
    srchKey: "DOCK",
    title: "dock_code",
    width: "half",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
];

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useSetting: false,
  unUsedRefreshButton: true,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchInput: SEARCH_INPUT,
  pageInfo: null,
  searchComponentControlBtn: [],
};
