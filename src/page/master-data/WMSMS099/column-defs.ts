/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS099/column-defs.ts
 *  Description:    기준정보/상품군정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import gridTextInput from "@/components/renderer/grid-text-input.vue";

// done
export const FORM_COLUMN_DEFS: any = [
  {
    field: "NO",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    pinned: "left",
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.ITEM_GRP_ID).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    pinned: "left",
  },

  {
    field: "ITEM_GRP_CD",
    headerKey: "product-group-code",
    headerName: "",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    headerClass: "header-require",
    width: 150,
    flex: 2,
  },
  {
    field: "ITEM_GRP_NM",
    headerKey: "product-group-name",
    headerName: "",
    sortable: true,
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    headerClass: "header-require",
    width: 150,
    flex: 2,
  },
  {
    field: "ITEM_GRP_ID",
    headerKey: "product-group-ID",
    headerName: "",
    sortable: true,
    valueGetter: (params: any) => {
      if (String(params.node.data.ITEM_GRP_ID).includes("temp")) {
        return "";
      } else {
        return params.node.data.ITEM_GRP_ID;
      }
    },
    cellStyle: { textAlign: "center" },
    minWidth: 130,
    flex: 1,
  },
];
