/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS093/input.ts
 *  Description:    상품별자동출고주문관리 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSCM011",
  pk: "CUST_ID",
};

export const SEARCH_MODAL_INFO: IModal = {
  "search-user": {
    page: "TMSYS030",
    id: "user",
    title: "user-search",
    gridTitle: "user-list",

    apis: {
      url: "/TMSYS030/listQ2_rn.action",
      params: {
        vrSrchUserNo: "",
        vrSrchUserId: "",
        vrSrchUserNm: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchUserId",
        searchContainerInputId: "vrSrchUserId",
        title: "user-ID",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchUserNm",
        searchContainerInputId: "vrSrchUserNm",
        title: "user-name",
        type: "text",
        width: "half",
      },
    ],
  },
};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 사용자
    ids: ["vrSrchUserId", "vrSrchUserNm", "vrSrchUserNo"],
    hiddenId: "vrSrchUserNo",
    rowDataIds: ["USER_ID", "USER_NM", "USER_NO"],
    rowDataHiddenId: "USER_NO",
    searchApiKeys: ["vrSrchUserId", "vrSrchUserNm", "vrSrchUserNo"],
    srchKey: "USER",
    title: "search-user",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },

  {
    // 화주코드
    ids: ["vrSrchCustCdSelectBox"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchCustCdSelectBox"],
    title: "shipper-code",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["multi-select"],
    options: [
      // { name: "all", nameKey: "all", value: null },
      { name: "TC", nameKey: "TC", value: "TC" },
      { name: "DC", nameKey: "DC", value: "DC" },
    ],
  },
  {
    //  화주명
    ids: ["vrCustNm"],
    hiddenId: "",
    rowDataIds: ["vrCustNm"],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "vrCustNm",
    title: "owner-name",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

// defaultParams확인 필요
export const MAIN_SEARCH_INPUT: ISearchInput[] = [];

// defaultParams확인 필요
export const DETAIL_SEARCH_INPUT: ISearchInput[] = [];

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useMore: false,
  useSetting: false, // TODO :: 위치 조정 이 후, 재설정 (TRUE)
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  mainSearchInput: MAIN_SEARCH_INPUT,
  detailSearchInput: DETAIL_SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  searchConditionInitUrl: "",
};
