/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS011/column-defs.ts
 *  Description:    기준정보/거래처정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid";

import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MODAL_COLUMN_DEFS: any = {
  "search-user": [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      maxWidth: 50,
    },
    {
      field: "USER_ID",
      headerKey: "user-ID",
      headerName: "",

      cellStyle: { textAlign: "center" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "USER_NO",
      headerKey: "user-no",
      headerName: "",
      hide: true,
    },

    {
      field: "USER_NM",
      headerKey: "user-name",
      headerName: "",
      headerClass: "header-center",

      sortable: true,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CLIENT_NM",
      headerKey: "logistics-center-name",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",

      sortable: true,
    },
    {
      field: "AUTH_NM",
      headerKey: "authority-name",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",

      sortable: true,
    },
    {
      field: "USER_GB_NM",
      headerKey: "classification",
      headerName: "",
      cellStyle: { textAlign: "center" },
      headerClass: "header-center",

      sortable: true,
    },
  ],
};

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "RNUM",
    headerName: "No",
    maxWidth: 80,
    cellStyle: { textAlign: "center" },
    pinned: "left",
    lockPosition: true,
  },
  //사용자ID
  {
    field: "USER_ID",
    headerKey: "user-id",
    headerName: "user-id",
    maxWidth: 150,
    pinned: "left",
    lockPosition: true,
  },
  //사용자명
  {
    field: "USER_NM",
    headerKey: "user-name",
    headerName: "user-name",
    sortable: true,
    width: 150,
    headerClass: "header-center",
    flex: 1,
  },
  //등록일
  {
    field: "INSERT_DATE",
    headerKey: "reg-dt",
    headerName: "reg-dt",
    sortable: true,
    width: 150,
    headerClass: "header-center",
    valueFormatter: Format.Date,

    flex: 1,
  },
  {
    field: "USER_NO",
    headerKey: "",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    flex: 1,
    hide: true,
  },
  {
    field: "END",
    headerKey: "",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    flex: 1,
    hide: true,
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "RNUM",
    headerName: "",
    maxWidth: 30,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.CUST_ID).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    lockPosition: true,
  },
  //체크박스
  {
    field: "CHK_BOX",
    headerKey: "",
    headerName: "",
    width: 40,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
  },
  //표시여부
  {
    field: "DATA_CHK",
    headerKey: "display-Y/N",
    headerName: "",
    width: 100,
    maxWidth: 100,
    cellStyle: (params) => {
      let styleParams: any = { textAlign: "center" };
      //표시여부 N일때 빨간글씨
      if (String(params.data["DATA_CHK"]) == "N") {
        styleParams["color"] = "red";
        //관리대상 재고일떄
      } else {
        styleParams["color"] = "blue";
      }
      return styleParams;
    },
  },
  //화주코드
  {
    field: "CUST_CD",
    headerKey: "shipper-code",
    headerName: "shipper-code",
    sortable: true,
    width: 150,
    headerClass: "header-center",
    flex: 1,
  },
  //화주명
  {
    field: "CUST_NM",
    headerKey: "owner-name",
    headerName: "owner-name",
    sortable: true,
    width: 150,
    headerClass: "header-center",
    flex: 1,
  },
  //등록일
  {
    field: "REG_DT",
    headerKey: "reg-dt",
    headerName: "reg-dt",
    sortable: true,
    width: 160,
    headerClass: "header-center",
    valueFormatter: Format.Date,

    flex: 1,
  },
  {
    field: "CUST_ID",
    headerKey: "owner-name",
    headerName: "owner-id",
    sortable: true,
    width: 150,
    headerClass: "header-center",
    flex: 1,
    hide: true,
  },

  {
    field: "END",
    headerKey: "",
    headerName: "",
    sortable: true,
    headerClass: "header-center",
    flex: 1,
    hide: true,
  },
];
