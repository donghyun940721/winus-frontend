/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS093/input.grid.ts
 *  Description:    상품별자동출고주문관리 grid meta 정보
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.06. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/

/**
 ********************* Grid Area *********************/
import { i18n } from "@/i18n";
import { IControlBtn, IGridCellSearchButton, IGridCellSelectBox, IGridStatisticsInfo } from "@/types";
import { GridOptions } from "ag-grid-community";
import { FORM_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

//gridSearchButton
export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {};

export const GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [];

export const GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  WORK_STAT: {
    pk: "RNUM",
    optionsKey: "WORK_STAT",
  },
};

// done
export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "order-insert",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "start",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "",
    authType: "",
  },
  {
    title: "stop",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "",
  },
  {
    title: "end",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "",
  },
];

export const gridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("menu-header-title.WMSMS094"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [100, 200, 300],
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
};
