/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS093/input.ts
 *  Description:    상품별자동출고주문관리 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { IModal, ISearchInput, info } from "@/types/index";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSMS093",
  pk: "RNUM",
};

export const SEARCH_MODAL_INFO: IModal = {};

// defaultParams확인 필요
export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: [],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: ["timeinfo"],
    title: "current-time",
    width: "entire",
    isModal: false,
    required: false,
    isSearch: false,
    types: [],
  },
];

// common-search 에서 사용되는 Meta 정보
export const SEARCH_CONTAINER_META = {
  useMore: false,
  useSetting: false, // TODO :: 위치 조정 이 후, 재설정 (TRUE)
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: INFO,
  readOnlyInputs: [],
  readOnlyResults: [],
  searchConditionInitUrl: "",
};
