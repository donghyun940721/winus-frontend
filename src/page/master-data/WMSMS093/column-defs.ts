/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS093/column-defs.ts
 *  Description:    상품별자동출고주문관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { Format } from "@/lib/ag-grid";
import type { IColDef } from "@/types/agGrid";

export const MODAL_COLUMN_DEFS = {};

export const FORM_COLUMN_DEFS: IColDef[] = [
  {
    field: "FLAG",
    hide: true,
  },
  {
    field: "ST_GUBUN",
    hide: true,
  },

  {
    field: "SRC_LC_ID",
    hide: true,
  },
  {
    field: "SRC_CUST_ID",
    hide: true,
  },
  {
    field: "SRC_RITEM_ID",
    hide: true,
  },
  {
    field: "TAG_LC_ID",
    hide: true,
  },

  {
    field: "TAG_CUST_ID",
    hide: true,
  },
  {
    field: "TAG_RITEM_ID",
    hide: true,
  },
  {
    field: "ST_GUBUN",
    hide: true,
  },
  //(맨앞 번호)
  {
    field: "",
    headerKey: "",
    headerName: "",
    width: 30,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.LC_ID).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  //(체크박스)
  {
    field: "CHK_BOX",
    headerKey: "",
    headerName: "",
    width: 30,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
  },
  {
    field: "WORK_STAT",
    headerKey: "status",
    headerName: "",
    headerClass: "",
    // headerClass: "header-require",
    sortable: true,
    width: 80,
    valueGetter: (params: any) => {
      if (String(params.node.data.WORK_STAT) == "S") {
        return "START";
      } else if (String(params.node.data.WORK_STAT) == "E") {
        return "END";
      } else if (String(params.node.data.WORK_STAT) == "P") {
        return "PAUSE";
      } else {
        return "";
      }
    },
  },

  //상품코드
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
    cellStyle: { textAlign: "left" },
    width: 180,
    sortable: true,
    editable: false,
  },
  //상품명
  {
    field: "ITEM_NAME",
    headerKey: "product-name",
    headerName: "",
    cellStyle: { textAlign: "left" },
    width: 200,
    sortable: true,
    editable: false,
  },
  //(화주명)
  {
    field: "CUST_NAME",
    headerKey: "",
    headerName: "",
    width: 150,
    cellStyle: { textAlign: "center" },
    hide: true,
    sortable: false,
  },
  //주문간격
  {
    field: "INSERT_ORD_TIME",
    headerKey: "insert-ord-time",
    headerName: "",
    width: 90,
    sortable: true,
    editable: true,
    valueFormatter: Format.NumberCount,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    cellStyle: (params) => {
      let styleParams: any = { textAlign: "right" };
      //표시여부 N일때 빨간글씨
      styleParams["background-color"] = "rgb(235, 247, 255)";
      //관리대상 재고일떄
      return styleParams;
    },
  },
  //주문수량
  {
    field: "INSERT_ORD_QTY",
    headerKey: "insert-ord-qty",
    headerName: "",
    width: 90,
    sortable: true,
    editable: true,
    valueFormatter: Format.NumberCount,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    cellStyle: (params) => {
      let styleParams: any = { textAlign: "right" };
      //표시여부 N일때 빨간글씨
      styleParams["background-color"] = "rgb(235, 247, 255)";
      //관리대상 재고일떄
      return styleParams;
    },
  },
  //주문횟수
  {
    field: "INSERT_ORD_CNT",
    headerKey: "insert-ord-cnt",
    headerName: "",
    width: 90,
    sortable: true,
    editable: true,
    valueFormatter: Format.NumberCount,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    cellStyle: (params) => {
      let styleParams: any = { textAlign: "right" };
      //표시여부 N일때 빨간글씨
      styleParams["background-color"] = "rgb(235, 247, 255)";
      //관리대상 재고일떄
      return styleParams;
    },
  },
  //시작시간
  {
    field: "START_TIME",
    headerKey: "start-time",
    headerName: "",
    width: 200,
    sortable: true,
    editable: false,
    valueFormatter: Format.Date,
    cellStyle: (params) => {
      let styleParams: any = { textAlign: "center" };
      //표시여부 N일때 빨간글씨
      styleParams["background-color"] = "#d3d3d3";
      //관리대상 재고일떄
      return styleParams;
    },
  },
  //중단시간
  {
    field: "STOP_TIME",
    headerKey: "stop-time",
    headerName: "",
    width: 200,
    sortable: true,
    editable: false,
    valueFormatter: Format.Date,
    cellStyle: (params) => {
      let styleParams: any = { textAlign: "center" };
      //표시여부 N일때 빨간글씨
      styleParams["background-color"] = "#d3d3d3";
      //관리대상 재고일떄
      return styleParams;
    },
  },
  //종료시간
  {
    field: "END_TIME",
    headerKey: "end-time",
    headerName: "",
    width: 200,
    sortable: true,
    editable: false,
    valueFormatter: Format.Date,
    cellStyle: (params) => {
      let styleParams: any = { textAlign: "center" };
      //표시여부 N일때 빨간글씨
      styleParams["background-color"] = "#d3d3d3";
      //관리대상 재고일떄
      return styleParams;
    },
  },
  //전일종료시간
  {
    field: "BEFORE_END_TIME",
    headerKey: "before-end-time",
    headerName: "",
    width: 200,
    sortable: true,
    editable: false,
    valueFormatter: Format.Date,
    cellStyle: (params) => {
      let styleParams: any = { textAlign: "center" };
      //표시여부 N일때 빨간글씨
      styleParams["background-color"] = "#d3d3d3";
      //관리대상 재고일떄
      return styleParams;
    },
  },
  //최종처리시간
  {
    field: "FINAL_REG_DT",
    headerKey: "final-reg-dt",
    headerName: "",
    cellStyle: { textAlign: "center" },
    width: 200,
    sortable: true,
    editable: false,
    valueFormatter: Format.Date,
  },
  //중단기간
  {
    field: "START_STOP",
    headerKey: "start-stop",
    headerName: "",
    cellStyle: { textAlign: "right" },
    width: 100,
    sortable: true,
    editable: false,
    valueFormatter: Format.NumberCount,
  },
  //종료기간
  {
    field: "START_END",
    headerKey: "start-end",
    headerName: "",
    cellStyle: { textAlign: "right" },
    width: 100,
    editable: false,
    valueFormatter: Format.NumberCount,
  },
  //GROUPING_ZONE1
  {
    field: "GROUPING_ZONE1",
    headerKey: "grouping-zone1",
    headerName: "",
    cellStyle: { textAlign: "center" },
    width: 150,
    editable: true,
    // valueFormatter: Format.NumberCount,
  },
  //GROUPING_ZONE2

  {
    field: "GROUPING_ZONE2",
    headerKey: "grouping-zone2",
    headerName: "",
    cellStyle: { textAlign: "center" },
    width: 150,
    editable: true,
    // valueFormatter: Format.NumberCount,
  },
];
