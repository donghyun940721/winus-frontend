/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS081/input.ts
 *  Description:    기준관리/ZONE정보관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { IGridCellSearchButton, ISearchInput, info } from "@/types/index";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "",
  pk: "RNUM",
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["LC_NAME"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "zone-name",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
];

export const LEFT_CONTROL_BTN = [
  {
    title: "save",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
];

export const RIGHT_CONTROL_BTN = [
  {
    title: "enter-excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "",
  },
  {
    title: "save",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
];

export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  LOCATION: {
    fieldList: ["LOC_CD", "LOC_ID"],
    rowDataKeys: ["LOC_CD", "LOC_ID"],
    modalData: {
      page: "WMSMS081",
      id: "location",
      title: "search-location",
      gridTitle: "location-list",
      isCellRenderer: true,
      apis: {
        url: "/WMSCM080Q5/list02_rn.action",
        params: {
          func: "fn_setWMSCM080_row",
          LOC_ID: "",
          LOC_CD: "",
          AVAILABLE_QTY: "",
          STOCK_ID: "",
          SUB_LOT_ID: "",
          vrViewOnlyLoc: "",
          STOCK_WEIGHT: "",
          vrViewSubLotId: "",
          vrRitemId: "",
          ITEM_BEST_DATE_END: "",
          UOM_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          vrViewStockQty: "",
          S_WH_CD: "",
          vrWhId: "",
          S_WH_NM: "",
          vrSrchLocCd: "",
          vrSrchLocId: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "WH_CD",
          title: "warehouse",
          type: "search",
          width: "half",
          searchRowDataIds: ["WH_CD", "WH_NM"],
          searchIds: ["S_WH_CD", "S_WH_NM"],
          childrenColumnDefs: [],
        },
        {
          id: "vrSrchLocCd",
          title: "location-code",
          type: "text",
          width: "half",
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "LOC_CD",
        headerKey: "location-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "LOC_ID",
        headerKey: "product-group",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
    ],
  },
};
