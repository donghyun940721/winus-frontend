import type { info } from "@/types/index";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";
import { DetailGridMetaData, DetailGridOptions, MasterGridMetaData, MasterGridOptions, gridOptionsMeta } from "./input.grid";
import { FILTER_SEARCH_INPUT, SEARCH_CONTAINER_META } from "./input.search";

export * from "./input.grid";
export * from "./input.search";

export const commonSetting = {
  authPageGroup: "WMSMS",
  authPageId: "WMSMS081",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const SEARCH_META_DETAIL = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
  searchInput: FILTER_SEARCH_INPUT,
  unUsedRefreshButton: true,
  uniqueKey: "detail",
};

export const GRID_META = {
  ...MasterGridMetaData,
  ...DetailGridMetaData,
  ...commonSetting,
};

export const MASTER_GRID_OPTIONS_META = {
  ...MasterGridOptions,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
};

export const DETAIL_GRID_OPTIONS_META = {
  ...DetailGridOptions,
  columnDefs: DETAIL_GRID_COLUMN_DEFS,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};
export const INFO: info = {
  autoModal: false,
  autoModalPage: "",
  pk: "",
};
