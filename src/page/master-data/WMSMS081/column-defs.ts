/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS081/column-defs.ts
 *  Description:    기준관리/ZONE정보관리 컬럼 정의 스크립트
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.07. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/
import gridColorInput from "@/components/renderer/grid-color-input.vue";
import gridLocationSearch from "@/components/renderer/grid-location-search.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { MODAL_COLUMN_DEFS } from "@/page/common-page/meta/common-modal-column-defs";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export { MODAL_COLUMN_DEFS };

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    checkboxSelection: true,
    width: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "ZONE_NM",
    headerKey: "zone-name",
    headerName: "",
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "" },
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "COLOR",
    headerKey: "color",
    headerName: "",
    width: 70,
    cellClass: "renderer-cell",
    cellEditor: gridColorInput,
    editable: true,
    cellRenderer: (params: any) => {
      const div = document.createElement("div");
      div.style.width = "100%";
      div.style.height = "100%";
      div.style.backgroundColor = params.value || "#000000";
      return div;
    },
  },
  { field: "REG_NM", headerKey: "writer", headerName: "", sortable: true, cellStyle: { textAlign: "left" } },
  { field: "REG_DT", headerKey: "creation-date", headerName: "", sortable: true, cellStyle: { textAlign: "center" } },
  { hide: true, field: "LC_ID" },
  { hide: true, field: "ZONE_ID" },
  { hide: true, field: "COLOR" },
  { hide: true, field: "UPD_DT" },
  { hide: true, field: "UPD_NO" },
  { hide: true, field: "LC_NM" },
  { hide: true, field: "REG_NO" },
  { hide: true, field: "UPD_NM" },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 50,
    cellStyle: { textAlign: "center" },
  },

  { hide: true, field: "LOC_ID" },
  { hide: true, field: "ZONE_ID" },
  { hide: true, field: "OLD_LOC_ID" },
  {
    field: "",
    headerKey: "location",
    headerName: "",
    children: [
      {
        field: "LOC_CD",
        headerKey: "location-code",
        headerName: "",
        headerClass: "header-center",
        export: true,
        width: 120,
        cellClass: function (params) {
          // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
          return params.data.editable === true ? "editable-cell" : "renderer-cell";
        },
        editable: false,
        cellRenderer: gridTextInput,
      },
      {
        headerName: "",
        width: 100,
        cellStyle: { textAlign: "center" },
        headerClass: "header-center",
        cellRenderer: gridLocationSearch,
      },
    ],
  },
  {
    field: "WORK_SEQ",
    headerKey: "work-order",
    headerName: "",
    sortable: true,
    // editable: true,
    cellStyle: { textAlign: "" },
    cellRenderer: gridTextInput,
    cellClass: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true ? "editable-cell" : "renderer-cell";
    },
    editable: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true;
    },
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  { field: "LOC_STAT", headerKey: "status", headerName: "", sortable: true, cellStyle: { textAlign: "" } },
  { field: "WH_NM", headerKey: "warehouse", headerName: "", sortable: true, cellStyle: { textAlign: "" } },
  { field: "ROWCNT", headerKey: "line", headerName: "", sortable: true, cellStyle: { textAlign: "" } },
  { field: "COL", headerKey: "row", headerName: "", sortable: true, cellStyle: { textAlign: "" } },
  { field: "DAN", headerKey: "column", headerName: "", sortable: true, cellStyle: { textAlign: "" } },
  { field: "CARRY_PLT_QTY", headerKey: "plt-quantity", headerName: "", sortable: true, cellStyle: { textAlign: "" } },
];
