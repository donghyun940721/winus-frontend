/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS081/column-defs.ts
 *  Description:    기준관리/ZONE정보관리 컬럼 정의 스크립트
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.07. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/
import { IControlBtn, IGridCellSearchButton, IGridCellSelectBox, IGridStatisticsInfo } from "@/types";
import { GridOptions } from "ag-grid-community";
import { DETAIL_GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";

// const { t } = i18n.global;

export const GRID_SELECT_BOX_INFO: IGridCellSelectBox = {};
export const MAIN_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [];
export const SUB_GRID_STATISTICS_INFO: IGridStatisticsInfo[] = [];
// done
export const MASTER_CONTROL_BTN: IControlBtn[] = [
  {
    title: "save",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
];

export const DETAIL_CONTROL_BTN: IControlBtn[] = [
  {
    title: "search-filter",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "",
  },
  {
    title: "enter-excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "",
  },
  {
    title: "save",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    disabled: "",
  },
];

export const MasterGridMetaData: any = {
  // 그리드 헤더
  // gridHeaderName: t("menu-header-title.WMSMS081_master"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [100, 200, 300],
};

export const DetailGridMetaData: any = {
  // 그리드 헤더
  // gridHeaderName: t("menu-header-title.WMSMS081_detail"),

  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [100, 200, 300],
};

//grid > context 쪽에 ...
export const GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  LOCATION: {
    fieldList: ["LOC_CD"],
    rowDataKeys: ["LOC_CD"],
    children: true,
    modalData: {
      page: "WMSCM080Q5",
      id: "location",
      title: "search-location",
      gridTitle: "location-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달
      apis: {
        url: "/WMSCM080Q5/list02_rn.action",
        params: {
          func: "fn_setWMSCM080",
          LOC_ID: "",
          LOC_CD: "",
          AVAILABLE_QTY: "",
          STOCK_ID: "",
          SUB_LOT_ID: "",
          vrViewOnlyLoc: "",
          STOCK_WEIGHT: "",
          vrViewSubLotId: "",
          vrRitemId: "",
          ITEM_BEST_DATE_END: "",
          UOM_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          vrViewStockQty: "",
          S_WH_CD: "",
          vrWhId: "",
          S_WH_NM: "",
          vrSrchLocCd: "",
          vrSrchLocId: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "WH_CD",
          title: "warehouse",
          type: "search",
          width: "half",
          searchRowDataIds: ["WH_CD", "WH_NM"],
          searchIds: ["S_WH_CD", "S_WH_NM"],
        },
        {
          id: "vrSrchLocCd",
          title: "location-code",
          type: "text",
          width: "half",
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "LOC_CD",
        headerKey: "location-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "LOC_ID",
        headerKey: "product-group",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
    ],
  },
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  rowSelection: "single",
  rowModelType: "clientSide",
  pagination: true,
  suppressClickEdit: true,
  suppressRowClickSelection: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
  enableCellEditingOnBackspace: false,
};

export const MasterGridOptions: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
  rowSelection: "single",
  rowModelType: "clientSide",
  pagination: true,
  suppressClickEdit: true,
  // suppressRowClickSelection: true,
  getRowId: (data) => {
    return data.data.RNUM;
  },
  enableCellEditingOnBackspace: false,
};

export const DetailGridOptions: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: DETAIL_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  pagination: true,
  // suppressRowClickSelection: true,
  suppressClickEdit: true,
  // getRowId: (data) => {
  //   return data.data.RNUM;
  // },
  enableCellEditingOnBackspace: false,
  groupSelectsChildren: true,
};
