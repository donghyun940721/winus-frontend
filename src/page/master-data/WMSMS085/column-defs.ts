/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS085/column-defs.ts
 *  Description:    기준관리/거래처ZONE정보관리 컬럼 정의 스크립트
 *  Authors:        H. N. Ko
 *  Update History:
 *                  2024.07. : Created by H. N. Ko
 *
------------------------------------------------------------------------------*/
import gridColorInput from "@/components/renderer/grid-color-input.vue";
import gridSearchButton from "@/components/renderer/grid-search-button.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MODAL_COLUMN_DEFS: any = {
  "customer-owner": [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "chk",
    headerKey: "",
    headerName: "",
    checkboxSelection: true,
    width: 50,
    cellStyle: { textAlign: "center" },
  },
  { field: "LC_ID", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
  { field: "CUST_ZONE_ID", headerKey: "zone-id", headerName: "", cellStyle: { textAlign: "" }, sortable: true },
  {
    field: "CUST_ZONE_CD",
    headerKey: "zone-code",
    headerName: "",
    cellClass: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true ? "editable-cell" : "renderer-cell";
    },
    editable: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true;
    },
    sortable: true,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "CUST_ZONE_NM",
    headerKey: "zone-name",
    headerName: "",
    cellClass: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      console.log(params.data.editable === true ? "editable-cell" : "renderer-cell");
      return params.data.editable === true ? "editable-cell" : "renderer-cell";
    },
    editable: function (params) {
      // 행의 editable 상태를 확인하여 편집 가능 여부를 결정
      return params.data.editable === true;
    },
    sortable: true,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  {
    field: "COLOR",
    headerKey: "color",
    headerName: "",
    width: 70,
    cellClass: "renderer-cell",
    cellEditor: gridColorInput,
    editable: true,
    cellRenderer: (params: any) => {
      const div = document.createElement("div");
      div.style.width = "100%";
      div.style.height = "100%";
      div.style.backgroundColor = params.value || "#000000";
      return div;
    },
  },
  { field: "REG_NM", headerKey: "writer", headerName: "", cellStyle: { textAlign: "" }, sortable: true },
  { field: "REG_DT", headerKey: "creation-date", headerName: "", cellStyle: { textAlign: "" }, sortable: true },
  { field: "UPD_DT", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
  { field: "UPD_NO", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
  { field: "LC_NM", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
  { field: "REG_NO", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
  { field: "UPD_NM", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "chk",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 50,
    cellStyle: { textAlign: "center" },
  },
  {
    field: "CUST_CD",
    headerKey: "customer-code(owner)",
    headerName: "",
    cellStyle: { textAlign: "" },
    sortable: true,
    editable: true,
    cellRenderer: gridTextInput,
    export: true,
  },
  {
    field: "CUST",
    maxWidth: 50,
    headerName: "",
    cellClass: "renderer-cell",
    cellRenderer: gridSearchButton,
  },
  {
    field: "CUST_NM",
    headerKey: "customer-name",
    headerName: "",
    cellStyle: { textAlign: "" },
    sortable: true,
    export: true,
  },
  {
    field: "WORK_SEQ",
    headerKey: "work-order",
    headerName: "",
    cellStyle: { textAlign: "" },
    sortable: true,
    editable: true,
    // cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    export: true,
  },
  {
    field: "CUST_ZONE_CD",
    headerKey: "zone-code",
    headerName: "",
    hide: true,
    export: true,
  },
  {
    field: "CUST_ZONE_NM",
    headerKey: "zone-name",
    headerName: "",
    hide: true,
    export: true,
  },
  { field: "CUST_ID", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
  { field: "OLD_CUST_ID", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
  { field: "CUST_ZONE_ID", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
];
