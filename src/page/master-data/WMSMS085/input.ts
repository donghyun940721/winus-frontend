/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS085/input.ts
 *  Description:    기준관리/거래처ZONE정보관리 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { ISearchInput, info } from "@/types/index";

export const INFO: info = {
  autoModal: true,
  autoModalPage: "",
  pk: "RNUM",
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["LC_NAME"],
    hiddenId: "",
    rowDataIds: ["", ""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "cust-zone-name",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["txtSrchCustCd", "txtSrchCustNm"],
    hiddenId: "txtSrchTrustCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    defaultSearchApiKeys: {
      custId: "OK",
      vrSrchCustId: "",
    },
    title: "customer-owner",
    width: "half",
    isModal: false,
    required: false,
    isSearch: true,
    types: ["text", "text"],
  },
];
