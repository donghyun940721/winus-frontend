/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS096/column-defs.ts
 *  Description:    기준관리/상품별 원주자재(부품/용기관리) 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridSelectBox from "@/components/renderer/grid-select.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { Getter } from "@/lib/ag-grid";
import { Format } from "@/lib/ag-grid/index";
import { GridUtils } from "@/lib/ag-grid/utils";

import type { ICellEditorParams } from "ag-grid-community";
export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "product-code": [
    {
      field: "",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      valueFormatter: Format.NumberPrice,
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: any = [
  {
    field: "FLAG",
    headerKey: "",
    headerName: "",
    sortable: false,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    field: "RNUM",
    headerKey: "no",
    headerName: "",
    width: 60,
    cellStyle: { textAlign: "center" },
    // valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "CHK_BOX",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 50,
  },
  //구분
  {
    field: "GUBUN",
    headerKey: "division",
    headerName: "",
    cellStyle: { textAlign: "center" },
    width: 50,
    sortable: true,
    valueGetter: (params: any) => {
      return params.data.GUBUN == "I" ? "입고" : "출고";
    },
  },
  //상태
  {
    field: "STAT",
    headerKey: "status",
    headerName: "",
    width: 80,
    sortable: true,
    editable: true,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("STAT"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("STAT", value);
        },
      };
    },

    cellEditor: GridSelectBox,
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("STAT", params.value);
    },

    cellStyle: (params: any) => {
      let styleParams = { textAlign: "center" };
      //장기성재고일떄
      if (String(params.data?.STAT) === "Y") {
        styleParams["color"] = "blue";
        //관리대상 재고일떄
      } else {
        styleParams["color"] = "red";
      }
      return styleParams;
    },
  },
  {
    field: "ORD_TYPE",
    headerKey: "",
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    hide: true,
  },
  //상품코드
  { field: "GOODS_ITEM_CD", headerKey: "product-code", headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true },
  { field: "ITEM_KOR_NM", headerKey: "product-name", headerName: "", cellStyle: { textAlign: "center" }, sortable: true, export: true },
  //상품유형
  {
    field: "ITEM_TYPE",
    headerKey: "product-type",
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    valueGetter: (params: any) => {
      return Getter.convetCodeToNameByOptionData("ITEMGRP", params);
    },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("ITEMGRP", params.value);
    },
  },
  //물류기기명
  {
    field: "POOL_KOR_NM",
    headerKey: "logistics-tool-name",
    editable: true,
    headerName: "",
    cellStyle: { textAlign: "" },
    sortable: true,
    export: true,
    width: 300,
    valueGetter: (params: any) => {
      return Getter.convetCodeToNameByOptionData("ITEM_TYPE_NAME", params);
    },
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("ITEM_TYPE_NAME"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("ITEM_TYPE_NAME", value);
        },
      };
    },
    cellEditor: GridSelectBox,
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("ITEM_TYPE_NAME", params.value);
    },
  },
  //상품수량
  {
    field: "GOODS_QTY",
    headerKey: "product-qty",
    headerName: "",
    headerClass: "header-require",
    cellStyle: { textAlign: "right" },
    sortable: true,
    editable: true,
    export: true,
    width: 100,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  //물류기기수량
  {
    field: "POOL_QTY",
    headerKey: "logistics-equipment-quantity",
    headerName: "",
    width: 130,
    cellStyle: { textAlign: "right" },
    sortable: true,
    editable: true,
    headerClass: "header-require",
    export: true,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  //커버수량
  {
    field: "COVER_QTY",
    headerKey: "cover-quantity",
    headerName: "",
    headerClass: "header-require",
    width: 100,
    cellStyle: { textAlign: "right" },
    sortable: true,
    editable: true,
    export: true,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  //거래처
  {
    field: "TRANS_CUST_ID",
    headerKey: "customer",
    headerName: "",
    cellStyle: { textAlign: "center" },
    editable: true,
    sortable: true,
    width: 200,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("CUST_ID"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("CUST_ID", value);
        },
      };
    },
    cellEditor: GridSelectBox,
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("CUST_ID", params.value);
    },
  },
  { field: "GOODS_RITEM_ID", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
  { field: "VIEW_ORG_POOL_QTY", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
  { field: "VIEW_ORG_POOL_RITEM_ID", headerKey: "", headerName: "", cellStyle: { textAlign: "" }, sortable: true, hide: true },
  {
    field: "LOT_PREFIX",
    headerKey: "lot-prefix",
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    editable: true,
    cellRenderer: gridTextInput,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
];
