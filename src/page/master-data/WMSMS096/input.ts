/*-----------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS096/input.ts
 *  Description:    기준관리/상품별 원주자재(부품/용기관리) 인풋 정의 ts파일
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { info, IModal, ISearchInput, IGridCellSelectBox } from "@/types/index";

// done
export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSCM011",
  pk: "CUST_ID",
};

export const GRID_SELECT_BOX_INFO: IGridCellSelectBox = {
  STAT: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "useProductY", value: "Y" },
      { name: "", nameKey: "useProductN", value: "N" },
    ],
  },
  POOL_KOR_NM: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "useProductY", value: "Y" },
      { name: "", nameKey: "useProductN", value: "N" },
    ],
  },
  TRANS_CUST_ID: {
    pk: "RNUM",
    options: [
      { name: "", nameKey: "useProductY", value: "Y" },
      { name: "", nameKey: "useProductN", value: "N" },
    ],
  },
};

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "12" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        title: "shipper-code",
        searchContainerInputId: "vrSrchCustCd",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        title: "owner-name",
        searchContainerInputId: "vrSrchCustNm",
        type: "text",
        width: "triple",
      },
    ],
  },

  "product-code": {
    page: "WMSCM091",
    id: "product-code",
    title: "search-product",
    gridTitle: "product-list",

    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },

    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "viewAll",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        title: "product-code",
        searchContainerInputId: "vrSrchItemCd",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        title: "product-name",
        searchContainerInputId: "vrSrchItemNm",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        optionsKey: "S_SET_ITEM_YN",
        options: [
          {
            nameKey: "all",
            name: "",
            value: "",
          },
        ],
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["txtSrchCustCd", "txtSrchCustNm"],
    hiddenId: "hdnCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchOrderGb"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "division",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "DS_TP",
    disabled: "disabled",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
  {
    ids: ["S_ITEM_CD", "S_ITEM_NM"],
    hiddenId: "S_ITEM_ID",
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    rowDataHiddenId: "ITEM_ID",
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    srchKey: "ITEM",
    title: "product-code",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
];

// done
export const CONTROL_BTN = [
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
  },
  {
    title: "del",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
  },
];
