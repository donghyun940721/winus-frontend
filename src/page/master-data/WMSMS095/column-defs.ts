/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS095/column-defs.ts
 *  Description:    화주별거래처상품관리 정의 스크립트
 *  Authors:        S. Y. LIM
 *  Update History:
 *                  2024.09. : Created by S. Y. Lim
------------------------------------------------------------------------------*/
import GridSearchButton from "@/components/renderer/grid-search-button.vue";
import gridSelect from "@/components/renderer/grid-select.vue";
import { i18n } from "@/i18n";
import { Getter } from "@/lib/ag-grid";
import { Format } from "@/lib/ag-grid/index";
import { GridUtils } from "@/lib/ag-grid/utils";
import { useOptionDataStore } from "@/store";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
import type { ICellEditorParams } from "ag-grid-community";
const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      minWidth: 80,
      width: 80,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      maxWidth: 50,
      pinned: "left",
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  "kit-product": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
      valueFormatter: Format.NumberPrice,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "RITEM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "ITEM_ENG_NM",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "CUST_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_CD",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_CD",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_NM",
      sortable: true,
      hide: true,
    },
  ],
};

export const GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    //!화면 Migration시 반영
    valueGetter: (params: any) => {
      if (params.data && params.node && !params.node.group) {
        return params.data["RNUM"] ?? params.node.rowIndex + 1;
      } else {
        return "";
      }
    },
    menuTabs: ["columnsMenuTab"],
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
    lockVisible: true,
  },
  // ST_GUBUN
  {
    field: "ST_GUBUN",
    headerKey: "st_gubun",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  // ST_GUBUN_IMG
  {
    field: "ST_GUBUN_IMG",
    headerKey: "st_gubun_img",
    headerName: "",
    export: true,
    sortable: true,
    width: 20,
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  // 화주명
  {
    field: "TRUST_CUST_NM",
    headerKey: "owner-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 160,
    cellStyle: { textAlign: "left" },
  },
  // 거래처명
  {
    field: "CUST_NM",
    headerKey: "customer-name",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  // 거래처코드
  {
    field: "CUST_CD",
    headerKey: "customer-code(owner)",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "left" },
  },
  // 유효기간관리여부
  {
    field: "BEST_DATE_YN",
    headerKey: "validity-management-y/n",
    headerName: "",
    export: true,
    sortable: true,
    width: 150,
    cellStyle: { textAlign: "center" },
  },
  // TRUST_CUST_ID
  {
    field: "TRUST_CUST_ID",
    headerKey: "trust_cust_id",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
  // CUST_ID
  {
    field: "CUST_ID",
    headerKey: "cust_id",
    headerName: "",
    export: true,
    sortable: true,
    width: 80,
    cellStyle: { textAlign: "left" },
    hide: true,
  },
];

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "chk",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 50,
    cellStyle: { textAlign: "center" },
  },
  // 상품
  {
    field: "ST_GUBUN",
    headerKey: "Product",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
    cellStyle: { textAlign: "left" },
    marryChildren: true,
    children: [
      // 상품군코드
      {
        field: "ITEM_GRP_CD",
        headerKey: "product-group-code",
        headerName: "",
        export: true,
        sortable: true,
        editable: true,
        cellStyle: { textAlign: "left" },
        width: 130,
      },
      // 상품 팝업
      {
        field: "GRP",
        headerKey: "blank",
        headerName: "",
        maxWidth: 60,
        cellRenderer: GridSearchButton,
        cellClass: "renderer-cell",
      },
      // 상품군명
      {
        field: "ITEM_GRP_NM",
        headerKey: "product-group-name",
        headerName: "",
        export: true,
        sortable: true,
        editable: true,
        cellStyle: { textAlign: "left" },
        width: 130,
      },
    ],
  },
  // 무게오차허용률
  {
    field: "WGT_ARROW_RATE",
    headerKey: "weight-allowable-error-rate",
    headerName: "",
    export: true,
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "right" },
    width: 150,
    valueFormatter: Format.NumberCount,
  },
  // 입출하최소유효기간
  {
    field: "BEST_DATE_NUM",
    headerKey: "min-validity-period",
    headerName: "",
    export: true,
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "right" },
    width: 150,
    valueFormatter: Format.NumberCount,
  },
  // 입출하최소유효기간단위
  {
    field: "BEST_DATE_UNIT",
    headerKey: "min-validity-period-unit",
    headerName: "",
    export: true,
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "center" },
    cellEditor: gridSelect,
    width: 150,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("YMD"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("YMD", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      const optionData = useOptionDataStore().getOptionData();
      const data = optionData["YMD"];
      const selectedData = data.find((item: any) => item.CODE === params.value);

      if (selectedData) {
        params.data["BEST_DATE_UNIT"] = selectedData.CODE_CD;
      }

      return Getter.convetCodeToNameByOptionData("YMD", params.value);
    },
  },
  // TRUST_CUST_ID
  {
    field: "TRUST_CUST_ID",
    headerKey: "trust_cust_id",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
    cellStyle: { textAlign: "left" },
  },
  // CUST_ID
  {
    field: "CUST_ID",
    headerKey: "customer-id",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
    cellStyle: { textAlign: "left" },
  },
  // ITEM_GRP_ID
  {
    field: "ITEM_GRP_ID",
    headerKey: "product-group-ID",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
    cellStyle: { textAlign: "left" },
  },
  // ITEM_GRP_ID_C
  {
    field: "ITEM_GRP_ID_C",
    headerKey: "product-group-ID",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
    cellStyle: { textAlign: "left" },
  },
];

export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerKey: "",
    headerName: "No",
    width: 60,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    },
  },
  {
    field: "chk",
    headerKey: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 50,
    cellStyle: { textAlign: "center" },
  },
  // 상품
  {
    field: "ST_GUBUN",
    headerKey: "Product",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
    cellStyle: { textAlign: "left" },
    marryChildren: true,
    children: [
      // 상품코드
      {
        field: "ITEM_CODE",
        headerKey: "product-code",
        headerName: "",
        export: true,
        sortable: true,
        editable: true,
        cellStyle: { textAlign: "left" },
        width: 130,
      },
      // 상품 ICON
      {
        field: "ITEM",
        headerKey: "blank",
        headerName: "",
        maxWidth: 60,
        cellRenderer: GridSearchButton,
        cellClass: "renderer-cell",
      },
      // 상품명
      {
        field: "ITEM_KOR_NM",
        headerKey: "product-name",
        headerName: "",
        export: true,
        sortable: true,
        editable: true,
        cellStyle: { textAlign: "left" },
        width: 130,
      },
    ],
  },
  {
    // 무게 오차 우선순위
    field: "WGT_PRIORITY",
    headerKey: "weight-error-priority",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "center" },
    editable: true,
    width: 150,
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      const weightErrorOptions = {
        WGT_PRIORITY: [
          { value: "1", label: t("select-options.weight-allowable-error-rate") },
          { value: "2", label: t("select-options.weight-error-tolerance-range(kg)") },
        ],
      };

      useOptionDataStore().setOptionData(weightErrorOptions);

      return {
        values: weightErrorOptions.WGT_PRIORITY.map((option: any) => option.value),
        formatValue: (value: any) => {
          const optionData = useOptionDataStore().getOptionData();
          const selectedOption = optionData.WGT_PRIORITY.find((item: any) => item.value === value);
          return selectedOption ? selectedOption.label : value;
        },
      };
    },
    cellRenderer: function (params: any) {
      const optionData = useOptionDataStore().getOptionData();

      if (optionData && optionData.WGT_PRIORITY) {
        const selectedOption = optionData.WGT_PRIORITY.find((item: any) => item.value === params.value);

        return selectedOption ? selectedOption.label : params.value;
      }

      return params.label;
    },
    optionsAutoSelected: { autoSelectedKeyIndex: 1 },
  },
  {
    // 무게 오차 허용범위 (Kg)
    field: "ITEM_WGT_ARROW_RANGE",
    headerKey: "weight-error-tolerance-range(kg)",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    editable: true,
    width: 160,
    valueFormatter: Format.NumberCount,
  },
  {
    // 무게 오차 허용률 (%)
    field: "ITEM_WGT_ARROW_RATE",
    headerKey: "weight-allowable-error-rate",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "right" },
    editable: false,
    width: 140,
    valueFormatter: Format.NumberCount,
  },
  {
    // 입출하 최소 유효기간
    field: "BEST_DATE_NUM",
    headerKey: "min-validity-period",
    headerName: "",
    export: true,
    sortable: true,
    editable: true,
    cellStyle: { textAlign: "right" },
    width: 140,
    valueFormatter: Format.NumberCount,
  },
  {
    // 입출하 최소 유효기간 단위
    field: "BEST_DATE_UNIT",
    headerKey: "min-validity-period-unit",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "center" },
    editable: true,
    width: 160,
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("YMD"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("YMD", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      const optionData = useOptionDataStore().getOptionData();
      const data = optionData["YMD"];
      const selectedData = data.find((item: any) => item.CODE === params.value);

      if (selectedData) {
        params.data["BEST_DATE_UNIT"] = selectedData.CODE_CD;
      }

      return Getter.convetCodeToNameByOptionData("YMD", params.value);
    },
  },
  {
    // 유통기한 순차 진행 여부
    field: "VAILD_FIFO_YN",
    headerKey: "valid-fifo-y/n",
    headerName: "",
    export: true,
    sortable: true,
    cellStyle: { textAlign: "center" },
    editable: true,
    width: 150,
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("vrSrchLockYn"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("vrSrchLockYn", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      const optionData = useOptionDataStore().getOptionData();
      const data = optionData["vrSrchLockYn"];
      const selectedData = data.find((item: any) => item.CODE === params.value);

      if (selectedData) {
        params.data["VAILD_FIFO_YN"] = selectedData.CODE_CD;
      }

      return Getter.convetCodeToNameByOptionData("vrSrchLockYn", params.value);
    },
  },
  {
    // 화주 ID (Trust Customer ID)
    field: "TRUST_CUST_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    // 고객사 ID (Customer ID)
    field: "CUST_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    // 세트상품 ID
    field: "RITEM_ID",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
  {
    // 세트상품 ID (복제)
    field: "RITEM_ID_C",
    headerKey: "",
    headerName: "",
    export: true,
    sortable: true,
    hide: true,
  },
];
