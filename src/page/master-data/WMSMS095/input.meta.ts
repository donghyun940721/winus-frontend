/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS095/input.meta.ts
 *  Description:    화주별거래처상품관리 정의 스크립트
 *  Authors:        S. Y. LIM
 *  Update History:
 *                  2024.09. : Created by S. Y. Lim
------------------------------------------------------------------------------*/

import type { info } from "@/types/index";
import { DETAIL_GRID_COLUMN_DEFS, GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";
import { gridMetaData, gridOptionsMeta, gridOptionsMetaCommon } from "./input.grid";
import { SEARCH_CONTAINER_META } from "./input.search";

export * from "./input.grid";
export * from "./input.search";

export const commonSetting = {
  authPageGroup: "WMSMS",
  authPageId: "WMSMS095",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMetaCommon,
  columnDefs: GRID_COLUMN_DEFS,
};

export const MASTER_GRID_OPTIONS_META = {
  ...gridOptionsMeta,
  columnDefs: MASTER_GRID_COLUMN_DEFS,
};

export const DETAIL_GRID_OPTIONS_META = {
  ...gridOptionsMeta,
  columnDefs: DETAIL_GRID_COLUMN_DEFS,
};

export const INFO: info = {
  autoModal: false,
  autoModalPage: "",
  pk: "RNUM",
};
