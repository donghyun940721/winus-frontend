/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS095/input.grid.ts
 *  Description:    화주별거래처상품관리 정의 스크립트
 *  Authors:        S. Y. LIM
 *  Update History:
 *                  2024.09. : Created by S. Y. Lim
------------------------------------------------------------------------------*/
import { Getter } from "@/lib/ag-grid";
import { Format } from "@/lib/ag-grid/index";
import { IControlBtn, IGridCellSearchButton, IGridCellSelectBox } from "@/types";
import { GridOptions } from "ag-grid-community";
import { DETAIL_GRID_COLUMN_DEFS, GRID_COLUMN_DEFS, MASTER_GRID_COLUMN_DEFS } from "./column-defs";

export const DETAIL_GRID_CELL_SELECT_BOX: IGridCellSelectBox = {
  // BEST_DATE_UNIT: {
  //   pk: "RNUM",
  //   optionsKey: "YMD",
  // },
  // WGT_PRIORITY: {
  //   pk: "RNUM",
  //   optionsKey: "WGT_PRIORITY",
  // },
  // VAILD_FIFO_YN: {
  //   pk: "RNUM",
  //   optionsKey: "vrSrchLockYn",
  // },
};

export const DETAIL_GRID_CELL_SEARCH_BUTTON: IGridCellSearchButton = {
  GRP: {
    fieldList: ["ITEM_GRP_ID", "ITEM_GRP_NM", "ITEM_GRP_CD"],
    rowDataKeys: ["ITEM_GRP_ID", "ITEM_GRP_NM", "ITEM_GRP_CD"],
    setParamsFromGridRowData: {
      rowDataKeys: ["LC_ID"],
      paramsKeys: ["vrSrchLcId"],
    },
    modalData: {
      page: "WMSCM091",
      id: "kit-product",
      title: "search-product-group",
      gridTitle: "product-group-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달
      apis: {
        url: "/rest/WMSMS094/list.action",
        params: {
          func: "fn_setWMSMS150",
          vrSrchLcId: "",
          ITEM_GRP_TYPE: "",
          ITEM_GRP_CD: "",
          ITEM_GRP_NM: "",
          ITEM_GRP_ID: "",
          vrSrchItemCd: "",
          vrSrchItemNm: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchItemCd",
          title: "product-group-code",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemNm",
          title: "product-group-name",
          type: "text",
          width: "half",
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "ITEM_GRP_TYPE",
        headerKey: "product-group-type",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        cellRenderer: function (params: any) {
          return Getter.convetCodeToNameByOptionData("ITEM_TY", params.value);
        },
      },
      {
        field: "ITEM_GRP_CD",
        headerKey: "product-group-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_GRP_NM",
        headerKey: "product-group-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_GRP_ID",
        headerKey: "product-group-id",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
        hide: true,
      },
    ],
  },
  ITEM: {
    fieldList: ["ITEM_CODE", "ITEM_KOR_NM", "RITEM_ID"],
    rowDataKeys: ["ITEM_CODE", "ITEM_KOR_NM", "RITEM_ID"],
    modalData: {
      page: "WMSCM091",
      id: "kit-product",
      title: "search-product",
      gridTitle: "product-list",
      isCellRenderer: true, // 모달에서 선택시 기본적으로 스토어에 저장이 되지만 해당 키값을 통해 edit으로 값을 전달

      defaultParamsData: {
        //해당 모달의 title
        storeSaveKey: "owner",
        paramsKeys: ["vrSrchCustId"],
        rowDataKeys: ["CUST_ID"],
      },
      apis: {
        url: "/WMSCM091/list_rn.action",
        params: {
          func: "fn_setWMSCM091_sub",
          vrSrchCustId: "",
          vrSrchWhId: "",
          vrViewSetItem: "NOT_SET",
          vrViewAll: "viewAll",
          vrItemType: "",
          RITEM_ID: "",
          ITEM_CODE: "",
          ITEM_KOR_NM: "",
          UOM_ID: "",
          TIME_PERIOD_DAY: "",
          STOCK_QTY: "",
          CUST_ID: "",
          UOM_CD: "",
          UOM_NM: "",
          UNIT_PRICE: "",
          vrSrchItemCd: "",
          vrSrchItemNm: "",
          vrSrchItemGrp: "",
          vrSrchSetItemYn: "",
        },
        data: {
          _search: false,
          nd: "",
          rows: "100",
          page: "1",
          sidx: "",
          sord: "asc",
        },
      },
      inputs: [
        {
          id: "vrSrchItemCd",
          title: "product-code",
          searchContainerInputId: "vrSrchItemCd",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemNm",
          searchContainerInputId: "vrSrchItemNm",
          title: "product-name",
          type: "text",
          width: "half",
        },
        {
          id: "vrSrchItemGrp",
          title: "product-group",
          type: "select",
          width: "half",
          optionsKey: "ITEMGRP",
          options: [{ name: "all", nameKey: "all", value: "" }],
        },
        {
          id: "vrSrchSetItemYn",
          title: "repacking",
          type: "select",
          width: "half",
          options: [
            { name: "", nameKey: "all", value: "" },
            { name: "useProductY", nameKey: "useProductY", value: "Y" },
            { name: "useProductN", nameKey: "useProductN", value: "N" },
          ],
        },
      ],
    },
    colDef: [
      {
        field: "",
        headerName: "No",
        width: 60,
        cellStyle: { textAlign: "center" },
        valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
      },
      {
        field: "",
        headerName: "",
        width: 50,
        cellStyle: { textAlign: "center" },
        headerCheckboxSelection: true,
        checkboxSelection: true,
      },
      {
        field: "CUST_NM",
        headerKey: "owner",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_GRP_NAME",
        headerKey: "product-group",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_CODE",
        headerKey: "product-code",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "ITEM_KOR_NM",
        headerKey: "product-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "BOX_BAR_CD",
        headerKey: "box-barcode",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "MAKER_NM",
        headerKey: "company-name",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "STOCK_QTY",
        headerKey: "current-stock",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "BAD_QTY",
        headerKey: "inferior-product",
        headerName: "",
        headerClass: "header-center",
        cellStyle: { textAlign: "right" },
        sortable: true,
      },
      {
        field: "UOM_NM",
        headerKey: "uom",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "UNIT_PRICE",
        headerKey: "unit-price",
        headerName: "",
        cellStyle: { textAlign: "right" },
        headerClass: "header-center",
        sortable: true,
        valueFormatter: Format.NumberPrice,
      },
      {
        field: "WH_NM",
        headerKey: "warehouse",
        headerName: "",
        headerClass: "header-center",
        sortable: true,
      },
      {
        field: "RITEM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "ITEM_ENG_NM",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "CUST_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "UOM_ID",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_CD",
        sortable: true,
        hide: true,
      },
      {
        field: "REP_UOM_NM",
        sortable: true,
        hide: true,
      },
    ],
  },
};

export const MASTER_CONTROL_BTN: IControlBtn[] = [
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const DETAIL_CONTROL_BTN: IControlBtn[] = [
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "normal",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [100, 200, 300],
};

export const MasterGridMetaData: any = {
  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "check-box-column",

  //페이징옵션
  pagingSizeList: [],
};

export const DetailGridMetaData: any = {
  // 그리드 사용자 설정 사용여부
  useUserSetting: false,

  //페이지 키
  checkBoxColumn: "",

  //페이징옵션
  pagingSizeList: [],
};

export const gridOptionsMetaCommon: GridOptions = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
    lockVisible: true,
    lockPosition: true,
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: GRID_COLUMN_DEFS,
  mainColumnDefs: MASTER_GRID_COLUMN_DEFS,
  subColumnDefs: DETAIL_GRID_COLUMN_DEFS,
  rowSelection: "single",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  // getRowId: (data) => {
  //   return data.data.RNUM;
  // },
};

export const gridOptionsMeta: GridOptions = {
  popupParent: document.body,
  defaultColDef: {
    resizable: true,
    menuTabs: [],
    lockVisible: true,
    lockPosition: true,
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: GRID_COLUMN_DEFS,
  mainColumnDefs: MASTER_GRID_COLUMN_DEFS,
  subColumnDefs: DETAIL_GRID_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  // suppressRowTransform: true, // 병합 전제조건
  suppressRowClickSelection: false, // row클릭시 체크박스 체크방지
  pagination: false,
  statusBar: true,
  // getRowId: (data) => {
  //   return data.data.RNUM;
  // },
};
