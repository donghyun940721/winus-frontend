/**------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS020/input.ts
 *  Description:    기준관리/센터별작업정책 input정의 
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
-------------------------------------------------------------------------------**/

import { IControlBtn } from "@/types";

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "search",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
  },
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
  },
];
