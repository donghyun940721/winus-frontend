/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS011/input.search.ts
 *  Description:    기준관리/매출거래처정보관리 Meta data
 *  Authors:        dhkim
 *  Update History:
 *                  2024.07 : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import { UtilService } from "@/services/util-service";
import type { IModal, ISearchInput } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs.ts";

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSMS011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    saveSession: true,
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "",
        S_LC_ID: UtilService.getCurrentLocation(),
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "txtSrchTrustCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "txtSrchTrustCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  customer: {
    page: "WMSMS011",
    id: "customer",
    title: "search-owner",
    gridTitle: "owner-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["S_CUST_ID"],
      rowDataKeys: ["CUST_ID"],
    },

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "",
        S_LC_ALL: "LC_ALL",
        S_LC_ID: UtilService.getCurrentLocation(),
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "txtSrchCustCd",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "txtSrchCustNm",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["txtSrchTrustCustCd", "txtSrchTrustCustNm"],
    hiddenId: "txtSrchTrustCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    srchKey: "CUST",
    title: "owner",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["txtSrchCustCd", "txtSrchCustNm"],
    hiddenId: "txtSrchCustId",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm", "vrSrchCustId", "custId"],
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    isAllUseSearchApiKey: true,
    srchKey: "CUST",
    title: "customer",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchCustInOutType"],
    hiddenId: "",
    rowDataIds: [],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchCustInOutType"],
    srchKey: "",
    title: "customer-type",
    width: "triple",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "vrSrchCustInOutType",
    options: [{ nameKey: "all", name: "", value: "" }],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  unUsedRefreshButton: true,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchModalInfo: SEARCH_MODAL_INFO,
  searchInput: SEARCH_INPUT,
  pageInfo: "null",
};
