/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS011/input.grid.ts
 *  Description:    기준관리/매출거래처정보관리 Meta data
 *  Authors:        dhkim
 *  Update History:
 *                  2024.07 : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import { i18n } from "@/i18n";
import { IControlBtn } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

const { t } = i18n.global;

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const gridMetaData: any = {
  // 그리드 헤더
  gridHeaderName: t("grid-title.customer-list"),

  //페이징옵션
  pagingSizeList: [100, 500, 1000, 5000, 10000],
};

export const gridOptionsMeta = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
    lockVisible: true,
    lockPosition: true,
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  suppressContextMenu: true, // Context Menu 미사용
  statusBar: true,
  // getRowClass : (params: any) => {
  //   if(Object.hasOwn(params.data, "STATE") && params.data.STATE === "U") {
  //     return 'work-progress';
  //   }
  // },
};
