/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS011/column-defs.ts
 *  Description:    기준정보/거래처정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  customer: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],

  //입출고존 모달
  "in/out-zone": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 60,
    },
    {
      field: "ZONE_NM",
      headerKey: "zone-name",
      headerClass: "header-center",
      sortable: true,
      width: 600,
    },
  ],
  "asn-receiving-logistics-center-id": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "LC_CD",
      headerKey: "logistics-center-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "LC_NM",
      headerKey: "logistics-center-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "LC_ID",
      headerKey: "logistics-center-id",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  "asn-receiving-client-id": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  "asn-receiving-customer-id": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    maxWidth: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "STATE") && params.data.STATE === "C") {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    maxWidth: 50,
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "ST_GUBUN",
    headerName: "",
    hide: true,
  },
  {
    field: "FLAG",
    headerName: "",
    hide: true,
  },
  {
    field: "STATE",
    headerName: "",
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    field: "TRUST_CUST_CD",
    headerName: t("grid-column-name.owner"),
    width: 130,
  },
  {
    field: "CUST_CD",
    headerName: t("grid-column-name.code"),
    width: 130,
    sortable: true,
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.customer-name"),
    minWidth: 300,
    sortable: true,
    // export: true,
    flex: 1,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.first-created-date"),
    width: 170,
    sortable: true,
    // export: true,
  },
  {
    field: "REG_NM",
    headerName: t("grid-column-name.first-creator"),
    width: 150,
    sortable: true,
    // export: true,
  },
  //#region ::  기본정보
  {
    // 화주(ID)
    field: "CUST_ID",
    hide: true,
  },
  {
    // 거래처명
    field: "TRUST_CUST_NM",
    hide: true,
  },
  {
    // 거래처(ID)
    field: "TRUST_CUST_ID",
    headerName: "",
    hide: true,
  },
  {
    // 거래처TYPE
    field: "CUST_INOUT_TYPE",
    hide: true,
  },
  {
    // 거래처바코드
    field: "CUST_BAR_CD",
    hide: true,
  },
  {
    // 거래처 메모
    field: "BIZ_MEMO",
    hide: true,
  },
  {
    // 대표거래처코드
    field: "REPRESENT_CUST_CD",
    hide: true,
  },
  {
    // 거래처상태
    field: "CUST_STAT",
    hide: true,
  },
  {
    // 거래처상품타입
    field: "CUST_TYPE",
    hide: true,
  },
  //#endregion
  //#region ::  사업자정보
  {
    // 대표자 성명
    field: "REP_NM",
    hide: true,
  },
  {
    // 휴대폰번호
    field: "MOBILE_NO",
    hide: true,
    // export: true,
  },
  {
    // 사업자등록번호
    field: "BIZ_NO",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    // 사업장전화
    field: "TEL",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    // 사업장팩스번호
    field: "FAX",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    // 사업장우편번호
    field: "ZIP",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    // 주소
    field: "ADDR",
    hide: true,
    // export: true,
  },
  {
    // 홈페이지주소
    field: "HTTP",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    // 신용등급
    field: "CREDIT_GRADE",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    // 업태
    field: "BIZ_COND",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    // 업종
    field: "BIZ_TYPE",
    headerName: "",
    sortable: true,
    hide: true,
  },
  //#endregion
  //#region ::  작업설정
  {
    //
    field: "TAG_PREFIX",
    hide: true,
  },
  {
    // GLN_CD
    field: "SGLN_CD",
    hide: true,
  },
  {
    // 입출고존 (명칭)
    field: "INOUT_ZONE_NM",
    hide: true,
  },
  {
    // 입출고존 (ID)
    field: "INOUT_ZONE_ID",
    hide: true,
  },
  {
    // 입출고존 (ID)
    field: "ZONE_NM",
    hide: true,
  },
  {
    // 부가세여부
    field: "VAT_YN",
    hide: true,
  },
  {
    // 유효기간관리여부
    field: "BEST_DATE_YN",
    hide: true,
  },
  {
    // 납기소요시간
    field: "DELIVERY_TIME",
    hide: true,
  },
  //#endregion
  //#region ::  ASN 설정
  {
    field: "ASN_YN",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_MAKE_TYPE",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_IN_LC_ID",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_IN_LC_NM",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_IN_LC_CD",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_IN_TRUST_CUST_ID",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_IN_TRUST_CUST_NM",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_IN_TRUST_CUST_CD",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_IN_CUST_ID",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_IN_CUST_NM",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_IN_CUST_CD",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ASN_TYPE",
    headerName: "",
    sortable: true,
    hide: true,
  },
  //#endregion
  //#region ::  담당자 정보
  {
    field: "DUTY_NM",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "DUTY_MOBILE",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "E_MAIL",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "E_MAIL_DOMAIN",
    headerName: "",
    sortable: true,
    hide: true,
  },
  //#endregion

  // {
  //   field: "DUTY_ID",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "CRE_LIMIT_DAY",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "SETTLE_TERM_DAY",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "DLV_CNTL",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },

  // {
  //   field: "REP_CUST_REP_NM",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "CURR_CD",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "CRE_SALE_AMT",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "CRE_LIMIT_AMT",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "ORD_AMT",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "UN_RECEPT_AMT",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "SALE_PER_NO",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "TR_UPDOWN_YN",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "IN_TON",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "CALC_SEQ",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "DLV_REQ_DT",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "REG_DT",
  //   headerName: t("grid-column-name.first-created-date"),
  //   width: 170,
  //   sortable: true,
  //   // export: true,
  // },
  // {
  //   field: "REG_NM",
  //   headerName: t("grid-column-name.first-creator"),
  //   width: 150,
  //   sortable: true,
  //   // export: true,
  // },
  // {
  //   field: "REG_NO",
  //   headerName: t("grid-column-name.first-created-date"),
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "UPD_DT",
  //   headerName: t("grid-column-name.first-created-date"),
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "UPD_NM",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },

  // {
  //   field: "CUST_EPC_CD",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "INOUT_ZONE_ID",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "CUST_GRP_ID",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "BIZ_MEMO",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "IF_CUST_CD",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "STORAGE_DESC",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "S_ITEM_CD",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "S_ITEM_NM",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
  // {
  //   field: "S_ITEM_ID",
  //   headerName: "",
  //   sortable: true,
  //   hide: true,
  // },
];
