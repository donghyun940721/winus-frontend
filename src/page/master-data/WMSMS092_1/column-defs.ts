/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS092_1/column-defs.ts
 *  Description:    기준정보/임가공,세트상품정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridSearchButton from "@/components/renderer/grid-search-button.vue";
import GridSelectBox from "@/components/renderer/grid-select-box.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { Format } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

export const MASTER_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "",
    headerKey: "no",
    maxWidth: 80,
    pinned: "left",
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    headerClass: "header-center",
    maxWidth: 50,
    pinned: "left",
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerName: "",
    sortable: true,
    width: 400,
    headerClass: "header-center",
    flex: 1,
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
    sortable: true,
    width: 350,
    headerClass: "header-center",
    flex: 1,
  },
  {
    field: "ITEM_KOR_NM",
    headerName: "",
    headerKey: "product-name",
    sortable: true,
    width: 550,
    headerClass: "header-center",
    flex: 2,
  },
  {
    field: "UNIT_NM",
    headerName: "",
    headerKey: "qty-by-box",
    sortable: true,
    width: 350,
    headerClass: "header-center",
    flex: 1,
  },
  {
    field: "ITEM_ENG_NM",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ITEM_SHORT_NM",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "RITEM_ID",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "REP_UOM_ID",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "CUST_ID",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "CUST_CD",
    headerName: "",
    sortable: true,
    hide: true,
  },
  {
    field: "ITEM_BAR_CD",
    headerName: "",
    sortable: true,
    hide: true,
  },
];

//세트상품구성 컬럼
export const DETAIL_GRID_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "",
    headerKey: "no",
    maxWidth: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.PART_RITEM_ID).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    field: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
    sortable: true,
    headerClass: "header-require",
    export: true,
    minWidth: 130,
    flex: 1,
  },
  {
    field: "ITEM",
    headerName: "",
    maxWidth: 60,
    cellRenderer: GridSearchButton,
    cellClass: "renderer-cell",
  },
  {
    field: "PART_RITEM_NM",
    headerKey: "product-name",
    headerName: "",
    sortable: true,
    headerClass: "header-require",
    export: true,
    minWidth: 200,
    flex: 2,
  },
  {
    field: "QTY",
    headerKey: "qty",
    headerName: "",
    cellStyle: { textAlign: "right" },
    sortable: true,
    headerClass: "header-require",
    cellRenderer: gridTextInput,
    editable: true,
    suppressKeyboardEvent: (params: any) => {
      console.log(params.event.key);
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
    export: true,
    width: 110,
  },
  {
    field: "UNIT_NM",
    headerKey: "qty-by-box",
    headerName: "",
    sortable: true,
    width: 110,
  },
  {
    field: "UOM_NM",
    headerKey: "uom",
    headerName: "",
    sortable: true,
    cellRenderer: GridSelectBox,
    headerClass: "header-require",
    optionsAutoSelected: { autoSelectedKeyIndex: 0 },
    export: true,
    width: 90,
  },
  {
    field: "UOM_ID",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
  },
  {
    field: "REG_DT",
    headerKey: "creation-date",
    headerName: "",
    cellStyle: { textAlign: "center" },
    sortable: true,
    export: true,
    width: 150,
  },
  {
    field: "REG_NM",
    headerKey: "writer",
    headerName: "",
    sortable: true,
    export: true,
    width: 110,
  },
  {
    field: "UPD_DT",
    headerKey: "date-modified",
    headerName: "",
    sortable: true,
    cellStyle: { textAlign: "center" },
    export: true,
    width: 150,
  },
  {
    field: "UPD_NM",
    headerKey: "modifier",
    headerName: "",
    sortable: true,
    export: true,
    width: 130,
  },
  {
    field: "SET_RITEM_ID",
    sortable: true,
    hide: true,
  },
  {
    field: "SET_RITEM_NM",
    sortable: true,
    hide: true,
  },
  {
    field: "SET_SEQ",
    sortable: true,
    hide: true,
  },
  {
    field: "PART_RITEM_ID",
    sortable: true,
    hide: true,
  },
  {
    field: "PART_RITEM_CODE",
    sortable: true,
    hide: true,
  },
];

//화주/상품 모달 컬럼
export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1,
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],

  "kit-product": [
    {
      field: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
      valueFormatter: Format.NumberPrice,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "RITEM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "ITEM_ENG_NM",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "CUST_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_CD",
      sortable: true,
      hide: true,
    },
    {
      field: "UOM_ID",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_CD",
      sortable: true,
      hide: true,
    },
    {
      field: "REP_UOM_NM",
      sortable: true,
      hide: true,
    },
  ],
};
