/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS100/page.vue
 *  Description:    기준정보/임가공,세트상품구성정보조회 META File (Search Area)
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import { IModal, ISearchInput } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 화주
    ids: ["vrSrchCustCdE5", "vrSrchCustNmE5"],
    rowDataIds: ["CUST_CD", "CUST_NM"],
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustIdE5",
    rowDataHiddenId: "CUST_ID",
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 세트상품
    ids: ["vrSrchItemCdE5", "vrSrchItemNmE5"],
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    rowDataHiddenId: "RITEM_ID",
    hiddenId: "vrSrchItemIdE5",
    srchKey: "ITEM",
    title: "kit-product",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 구성품
    ids: ["vrSrchPartItemCdE5", "vrSrchPartItemNmE5"],
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    rowDataHiddenId: "RITEM_ID",
    hiddenId: "vrSrchPartItemIdE5",
    srchKey: "ITEM",
    title: "component-product",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    // 상품사용여부
    ids: ["USE_YN_E5"],
    rowDataIds: ["", ""],
    searchApiKeys: ["", ""],
    rowDataHiddenId: "",
    hiddenId: "",
    srchKey: "",
    title: "product-usage-status",
    width: "quarter",
    isModal: false,
    required: false,
    //상품사용여부 검색버튼 비활성화
    isSearch: false,
    types: ["select-box"],
    options: [
      { name: "", nameKey: "all", value: "" },
      { name: "", nameKey: "useProductY", value: "Y" },
      { name: "", nameKey: "useProductN", value: "N" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 1, allowAutoSelected: true },
  },
];

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    //모달에서 접속할 페이지
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",
    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "", nameKey: "all", value: "" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCdE5",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNmE5",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  "kit-product": {
    page: "WMSCM091",
    id: "kit-product",
    title: "search-product",
    gridTitle: "product-list",
    apis: {
      //상품코드 팝업조회
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "viewAll",
        vrViewSetItem: "viewSetItem",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "Y",
        func: "fn_setWMSCM091",
        vrSrchCustId: "",
        RITEM_ID: "",
        ITEM_CODE: "",
        ITEM_KOR_NM: "",
        UOM_ID: "",
        TIME_PERIOD_DAY: "",
        STOCK_QTY: "",
        CUST_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        UNIT_PRICE: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchItemCdE5",
        title: "product-code",
        type: "text",
        width: "triple",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchItemNmE5",
        title: "product-name",
        type: "text",
        width: "triple",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "triple",
        optionsKey: "ITEMGRP",
        options: [{ name: "", nameKey: "all", value: "" }],
      },
      // {
      //   id: "vrSrchSetItemYn",
      //   title: "repacking",
      //   type: "select",
      //   width: "half",
      //   options: [
      //     { value: "", nameKey: "all", name: "" },
      //     { value: "Y", nameKey: "useProductY", name: "" },
      //     { value: "N", nameKey: "useProductN", name: "" },
      //   ],
      // },
    ],
  },
  "component-product": {
    page: "WMSCM091",
    id: "component-product",
    title: "search-product",
    gridTitle: "product-list",
    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    apis: {
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "N",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchPartItemCdE5",
        title: "product-code",
        type: "text",
        width: "triple",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchPartItemNmE5",
        title: "product-name",
        type: "text",
        width: "triple",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "triple",
        optionsKey: "ITEMGRP",
        options: [{ name: "", nameKey: "all", value: "" }],
      },
    ],
  },
};

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: "null",
  unUsedRefreshButton: true,
};
