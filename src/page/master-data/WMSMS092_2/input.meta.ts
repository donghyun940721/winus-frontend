/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS100/page.vue
 *  Description:    기준정보/임가공,세트상품구성정보조회 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import type { info } from "@/types/index";
import { gridMetaData, gridOptionsMeta } from "./input.grid.ts";
import { SEARCH_CONTAINER_META } from "./input.search.ts";

export * from "./input.grid.ts";
export * from "./input.search.ts";

export const commonSetting = {
  authPageGroup: "WMSMS",
  authPageId: "WMSMS092_2",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};

export const INFO: info = {
  autoModal: false,
  autoModalPage: "WMSCM011",
  pk: "RNUM",
};
