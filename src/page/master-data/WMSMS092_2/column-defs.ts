/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS092_2/column-defs.ts
 *  Description:    기준정보/임가공상품구성정보관리 - 세트상품구성내역조회 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import { Format } from "@/lib/ag-grid/index";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerName: "No",
    maxWidth: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스 1부터 시작
    cellDataType: false,
  },
  {
    field: "SET_RITEM_CD",
    headerName: t("grid-column-name.kit-product-code"),
    sortable: true,
    headerClass: "header-center",
    width: 300,
    cellDataType: false,
    // export: true,
    flex: 1,
  },
  {
    field: "SET_RITEM_NM",
    headerName: t("grid-column-name.kit-product-name"),
    sortable: true,
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    width: 400,
    cellDataType: false,
    // export: true,
    flex: 2,
  },
  {
    field: "PART_RITEM_CD",
    headerName: t("grid-column-name.component-product-code"),
    sortable: true,
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    width: 300,
    cellDataType: false,
    // export: true,
    flex: 1,
  },
  {
    field: "PART_RITEM_NM",
    headerName: t("grid-column-name.component-product-name"),
    sortable: true,
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    width: 300,
    cellDataType: false,
    // export: true,
    flex: 2,
  },
  {
    field: "UNIT_NM",
    headerName: t("grid-column-name.qty-by-box"),
    sortable: true,
    cellStyle: { textAlign: "left" },
    headerClass: "header-center",
    width: 205,
    cellDataType: false,
  },
  {
    field: "QTY",
    headerName: t("grid-column-name.qty"),
    sortable: true,
    cellStyle: { textAlign: "right" },
    headerClass: "header-center",
    width: 205,
    cellDataType: false,
    // export: true,
    valueFormatter: Format.NumberCount,
  },
];

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerName: "No",
      cellStyle: { textAlign: "center" },
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      cellStyle: { textAlign: "center" },
      width: 50,
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "manager-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
  ],

  "kit-product": [
    {
      field: "No",
      headerName: "No",
      cellStyle: { textAlign: "center" },
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      valueFormatter: Format.NumberPrice,
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
  "component-product": [
    {
      field: "No",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_NAME",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "current-stock",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      valueFormatter: Format.NumberPrice,
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};
