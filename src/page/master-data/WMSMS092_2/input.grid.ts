/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS100/page.vue
 *  Description:    기준정보/임가공,세트상품구성정보조회 META File (Grid Area)
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import { IControlBtn } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [100, 200, 300, 500, 1000, 5000, 10000],
};

export const gridOptionsMeta = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  statusBar: true,
};
