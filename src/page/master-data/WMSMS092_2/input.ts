/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS092_2/input.ts
 *  Description:    기준정보/임가공,세트상품정보조회 입력정보 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import type { ISearchInput, IModal } from "@/types";

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    //모달에서 접속할 페이지
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "", nameKey: "all", value: "" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCdE5",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNmE5",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
  "kit-product": {
    page: "WMSCM091",
    id: "kit-product",
    title: "search-product",
    gridTitle: "product-list",

    apis: {
      //상품코드 팝업조회
      url: "/WMSCM091/list_rn.action",
      params: {
        vrViewAll: "viewAll",
        vrViewSetItem: "viewSetItem",
        vrSrchWhId: "",
        vrItemType: "",
        vrSrchSetItemYn: "",
        func: "fn_setWMSCM091",
        vrSrchCustId: "",
        RITEM_ID: "",
        ITEM_CODE: "",
        ITEM_KOR_NM: "",
        UOM_ID: "",
        TIME_PERIOD_DAY: "",
        STOCK_QTY: "",
        CUST_ID: "",
        UOM_CD: "",
        UOM_NM: "",
        UNIT_PRICE: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrSrchItemGrp: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchItemCdE5",
        title: "product-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchItemNmE5",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [{ name: "", nameKey: "all", value: "" }],
      },
      {
        id: "vrSrchSetItemYn",
        title: "repacking",
        type: "select",
        width: "half",
        options: [
          { value: "", nameKey: "all", name: "" },
          { value: "Y", nameKey: "useProductY", name: "" },
          { value: "N", nameKey: "useProductN", name: "" },
        ],
      },
    ],
  },
  "component-product": {
    page: "WMSCM091",
    id: "component-product",
    title: "search-product",
    gridTitle: "product-list",

    defaultParamsData: {
      storeSaveKey: "owner",
      paramsKeys: ["vrSrchCustId"],
      rowDataKeys: ["CUST_ID"],
    },
    apis: {
      url: "/WMSCM091/listSetYn_rn.action",
      params: {
        vrViewAll: "",
        vrSrchCustId: "",
        vrSrchItemCd: "",
        vrSrchItemNm: "",
        vrViewSetItem: "",
        vrSrchWhId: "",
        vrItemType: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "vrSrchItemCd",
        searchContainerInputId: "vrSrchPartItemCdE5",
        title: "product-code",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemNm",
        searchContainerInputId: "vrSrchPartItemNmE5",
        title: "product-name",
        type: "text",
        width: "half",
      },
      {
        id: "vrSrchItemGrp",
        title: "product-group",
        type: "select",
        width: "half",
        optionsKey: "ITEMGRP",
        options: [{ name: "", nameKey: "all", value: "" }],
      },
    ],
  },
};

/*

    ids: ['S_CUST_CD', 'S_CUST_NM'],
      1. 모달에서 검색결과를 스토어에 저장시 ** id[0]:선택한 로우 데이터 **   형태로 저장됨
      2. 검색결과를 searchinput에 넣어줄때 ** id[0].rowDataIds[i] **
          - 해당되는 모달.해당인풋에 해당되는 그리드 데이터의 key값
      3. 아이디의 0번째 값으로 모달에서 선택된 데이터를 스토어에 저장
          - S_CUST_CD:{key:value, key:value}
    
    rowDataId: 해당 인풋에 해당되는 그리드 데이터의 key값 ,
    keyDownId: focusout시 사용되는 해당 인풋의 key값,
    srchKey: focusout시 해당되는 인풋의 srchKey키값의 value,

*/

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["vrSrchCustCdE5", "vrSrchCustNmE5"],
    rowDataIds: ["CUST_CD", "CUST_NM"],
    searchApiKeys: ["vrSrchCustCd", "vrSrchCustNm"],
    hiddenId: "vrSrchCustIdE5",
    rowDataHiddenId: "CUST_ID",
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchItemCdE5", "vrSrchItemNmE5"],
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    rowDataHiddenId: "RITEM_ID",
    hiddenId: "vrSrchItemIdE5",
    srchKey: "ITEM",
    title: "kit-product",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["vrSrchPartItemCdE5", "vrSrchPartItemNmE5"],
    rowDataIds: ["ITEM_CODE", "ITEM_KOR_NM"],
    searchApiKeys: ["vrSrchItemCd", "vrSrchItemNm"],
    rowDataHiddenId: "RITEM_ID",
    hiddenId: "vrSrchPartItemIdE5",
    srchKey: "ITEM",
    title: "component-product",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: true,
    placeholder: ["code", "name"],
    types: ["text", "text"],
  },
  {
    ids: ["USE_YN_E5"],
    rowDataIds: ["", ""],
    searchApiKeys: ["", ""],
    rowDataHiddenId: "",
    hiddenId: "",
    srchKey: "",
    title: "product-usage-status",
    width: "quarter",
    isModal: false,
    required: false,
    //상품사용여부 검색버튼 비활성화
    isSearch: false,
    types: ["select-box"],
    options: [
      { name: "", nameKey: "all", value: "" },
      { name: "", nameKey: "useProductY", value: "Y" },
      { name: "", nameKey: "useProductN", value: "N" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 1, allowAutoSelected: true },
  },
];

export const CONTROL_BTN = [
  {
    title: "excel",
    colorStyle: "success",
    paddingStyle: "bold",
    image: "excel",
    authType: "EXC_AUTH",
  },
];
