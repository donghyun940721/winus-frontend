/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS094/column-defs.ts
 *  Description:    기준정보/상품군정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import gridSelect from "@/components/renderer/grid-select.vue";
import gridTextInput from "@/components/renderer/grid-text-input.vue";
import { Getter } from "@/lib/ag-grid";
import { GridUtils } from "@/lib/ag-grid/utils";
import type { IColDef } from "@/types/agGrid";
import type { ICellEditorParams } from "ag-grid-community";

export const MODAL_COLUMN_DEFS = {};

export const FORM_COLUMN_DEFS: IColDef[] = [
  {
    field: "",
    headerKey: "",
    headerName: "",
    width: 5,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.LC_ID).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerKey: "",
    headerName: "",
    width: 5,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
  },

  //상품군코드
  {
    field: "ITEM_GRP_CD",
    headerKey: "product-group-code",
    headerName: "",
    sortable: true,
    width: 250,
    cellRenderer: gridTextInput,
    cellStyle: { textAlign: "center" },

    editable: true,
    suppressKeyboardEvent: (params: any) => {
      if (params.event.key === "Enter" || params.event.key === "Backspace") {
        return true;
      } else {
        return false;
      }
    },
  },
  //상품군명
  {
    field: "ITEM_GRP_NM",
    headerKey: "product-group-name",
    cellStyle: { textAlign: "center" },
    width: 250,
    editable: true,
    headerName: "",
  },
  //입고존
  {
    field: "IN_ZONE_ID",
    headerKey: "receiving-zone",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    editable: true,
    headerName: "",
    valueGetter(params) {
      return Getter.convetCodeToNameByOptionData("ZONE", params);
    },
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("ZONE"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("ZONE", value);
        },
      };
    },
    cellEditor: gridSelect,
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("ZONE", params.value);
    },
    flex: 2,
  },
  //업로드ID
  {
    field: "ITEM_GRP_ID",
    headerKey: "upload-id",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },

    headerName: "",
  },
  {
    field: "LC_ID",
    headerKey: "",
    headerClass: "",
    hide: true,
  },
  {
    field: "ST_GUBUN",
    headerKey: "",
    headerClass: "",
    hide: true,
  },
  {
    field: "ITEM_GRP_TYPE",
    headerKey: "",
    headerClass: "",
    hide: true,
  },
];
