/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS010/input.info.ts
 *  Description:    기준관리/물류센터정보관리 Meta data
 *  Authors:        dhkim
 *  Update History:
 *                  2024.07 : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import { IInfoInputContainer, IModal } from "@/types";

export const INFO_MODAL_INFO: IModal = {
  "receiving-logistics-center": {
    page: "WMSMS011Q1",
    id: "receiving-logistics-center",
    title: "search-logistics-center",
    gridTitle: "list-of-logistics-centers",

    apis: {
      url: "/WMSMS011Q1/list_rn.action",
      params: {
        func: "fn_setRECEIVING_LC_ID",
        S_LC_CD: "",
        S_LC_NM: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_LC_CD",
        title: "logistics-center-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_LC_NM",
        title: "logistics-center-name",
        type: "text",
        width: "half",
      },
      {
        id: "S_LC_ID",
        title: "logistics-center-id",
        type: "text",
        width: "half",
      },
    ],
  },
  "shipping-logistics-center": {
    page: "WMSMS011Q1",
    id: "receiving-logistics-center",
    title: "search-logistics-center",
    gridTitle: "list-of-logistics-centers",

    apis: {
      url: "/WMSMS011Q1/list_rn.action",
      params: {
        func: "fn_setRECEIVING_LC_ID",
        S_LC_CD: "",
        S_LC_NM: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: null,
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_LC_CD",
        title: "logistics-center-code",
        type: "text",
        width: "half",
      },
      {
        id: "S_LC_NM",
        title: "logistics-center-name",
        type: "text",
        width: "half",
      },
      {
        id: "S_LC_ID",
        title: "logistics-center-id",
        type: "text",
        width: "half",
      },
    ],
  },
};

export const INFO_INPUT: IInfoInputContainer[] = [
  {
    title: "default-info",
    default: "expand",
    inputs: [
      {
        ids: ["LC_ID"],
        rowDataIds: ["LC_ID"],
        title: "logistics-center-id",
        type: "text",
        width: "full",
        size: "single",
        isReadonly: true,
      },
      {
        ids: ["LC_CD"],
        rowDataIds: ["LC_CD"],
        title: "logistics-center-code",
        type: "text",
        width: "full",
        size: "single",
        required: true,
      },
      {
        ids: ["LC_NM"],
        rowDataIds: ["LC_NM"],
        title: "logistics-center-name",
        type: "text",
        width: "full",
        size: "single",
        required: true,
      },
      {
        ids: ["LC_ENG_NM"],
        rowDataIds: ["LC_ENG_NM"],
        title: "logistics-center-name-english",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["LC_TIME_ZONE"],
        rowDataIds: ["LC_TIME_ZONE"],
        title: "lc-time-zone",
        type: "select",
        width: "half",
        required: true,
        size: "single",
        optionsKey: "LC_TIME_ZONE",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["TAG_PREFIX"],
        rowDataIds: ["TAG_PREFIX"],
        title: "tag-prefix",
        type: "text",
        width: "half",
        size: "single",
        required: false,
      },
      {
        ids: ["GLN_CD"],
        rowDataIds: ["GLN_CD"],
        title: "gln-code",
        type: "text",
        width: "half",
        size: "single",
        required: false,
      },
      {
        ids: ["LATITUDE"],
        rowDataIds: ["LATITUDE"],
        title: "latitude",
        type: "text",
        width: "half",
        size: "single",
        required: false,
      },
      {
        ids: ["LONGITUDE"],
        rowDataIds: ["LONGITUDE"],
        title: "longitude",
        type: "text",
        width: "half",
        size: "single",
        required: false,
      },
    ],
  },
  {
    title: "inventory-management-settings",
    default: "expand",
    inputs: [
      {
        ids: ["WGT_YN"],
        rowDataIds: ["WGT_YN"],
        title: "weight-management",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "WGT_YN",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["WGT_TYPE"],
        rowDataIds: ["WGT_TYPE"],
        title: "wgt-type",
        type: "select",
        width: "half",
        size: "single",
        required: false,
        optionsKey: "WGT_TYPE",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["BEST_DATE_YN"],
        rowDataIds: ["BEST_DATE_YN"],
        title: "management-Y/N-of-expiration-date",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "BEST_DATE_YN",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["RFID_YN"],
        rowDataIds: ["RFID_YN"],
        title: "use-rfid-Y/N",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "RFID_YN",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["RACK_USE_YN"],
        rowDataIds: ["RACK_USE_YN"],
        title: "rack-usage-Y/N",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "RACK_USE_YN",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["RACK_TYPE"],
        rowDataIds: ["RACK_TYPE"],
        title: "rack-type",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "RACK_TYPE",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["MANAGE_PLT_YN"],
        rowDataIds: ["MANAGE_PLT_YN"],
        title: "logistics-container-management-Y/N",
        type: "select",
        width: "half",
        size: "single",
        required: false,
        optionsKey: "MANAGE_PLT_YN",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["STOCK_ALERT_YN"],
        rowDataIds: ["STOCK_ALERT_YN"],
        title: "notification-of-inventory-shortage",
        type: "select",
        width: "half",
        size: "single",
        required: false,
        optionsKey: "STOCK_ALERT_YN",
        options: [{ nameKey: "select", name: "", value: "" }],
        optionsAutoSelected: { autoSelectedKeyIndex: 1 },
      },
    ],
  },
  {
    title: "in/out-management-settings",
    default: "expand",
    inputs: [
      {
        ids: ["AUTO_RCV_STEP"],
        rowDataIds: ["AUTO_RCV_STEP"],
        title: "automatic-operation-stage-receiving",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "AUTO_RCV_STEP",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["AUTO_SND_STEP"],
        rowDataIds: ["AUTO_SND_STEP"],
        title: "automatic-operation-stage-shipping",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "AUTO_SND_STEP",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["MULTI_SHIP_YN"],
        rowDataIds: ["MULTI_SHIP_YN"],
        title: "multiple-shipping-Y/N",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "MULTI_SHIP_YN",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["CLIENT_ORD_YN"],
        rowDataIds: ["CLIENT_ORD_YN"],
        title: "automatic-registration-of-store-zone",
        type: "select",
        width: "half",
        size: "single",
        required: false,
        optionsKey: "CLIENT_ORD_YN",
        optionsAutoSelected: { autoSelectedKeyIndex: 1 },
      },
    ],
  },
  {
    title: "center-type-settings",
    default: "expand",
    inputs: [
      {
        ids: ["LC_TYPE"],
        rowDataIds: ["LC_TYPE"],
        title: "logistics-center-type",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "LC_TYPE",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["LC_USE_TY"],
        rowDataIds: ["LC_USE_TY"],
        title: "logistics-center-operation-type",
        type: "select",
        width: "half",
        size: "single",
        required: true,
        optionsKey: "LC_USE_TY",
        optionsAutoSelected: { autoSelectedKeyIndex: 0 },
      },
      {
        ids: ["RECEIVING_LC_CD", "RECEIVING_LC_ID", "RECEIVING_LC_NM"],
        rowDataIds: ["LC_CD", "LC_ID", "LC_NM"],
        title: "receiving-logistics-center",
        type: "triple-search",
        width: "full",
        size: "triple",
        required: false,
      },
      {
        ids: ["SHIPPING_LC_CD", "SHIPPING_LC_ID", "SHIPPING_LC_NM"],
        rowDataIds: ["LC_CD", "LC_ID", "LC_NM"],
        title: "shipping-logistics-center",
        type: "triple-search",
        width: "full",
        size: "triple",
        required: false,
      },
    ],
  },
  {
    title: "business-information",
    default: "expand",
    inputs: [
      {
        ids: ["CUST_CD"],
        rowDataIds: ["CUST_CD"],
        title: "logistics-company-code-3pl",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["CUST_NM"],
        rowDataIds: ["CUST_NM"],
        title: "logistics-company-3pl",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["BIZ_NO"],
        rowDataIds: ["BIZ_NO"],
        title: "company-number",
        type: "text",
        width: "half",
        size: "single",
        required: false,
      },
      {
        ids: ["REP_NM"],
        rowDataIds: ["REP_NM"],
        title: "president-name",
        type: "text",
        width: "half",
        size: "single",
        required: false,
      },
      {
        ids: ["TRADE_NM"],
        rowDataIds: ["TRADE_NM"],
        title: "company-name",
        type: "text",
        width: "half",
        size: "single",
        required: false,
      },
      {
        ids: ["ZIP"],
        rowDataIds: ["ZIP"],
        title: "zip-code",
        type: "text",
        width: "half",
        size: "single",
        required: false,
      },
      {
        ids: ["ADDR"],
        rowDataIds: ["ADDR"],
        title: "address",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
      {
        ids: ["TEL"],
        rowDataIds: ["TEL"],
        title: "tel",
        type: "text",
        width: "half",
        size: "single",
        required: false,
      },
      {
        ids: ["FAX"],
        rowDataIds: ["FAX"],
        title: "fax",
        type: "text",
        width: "half",
        size: "single",
        required: false,
      },
      {
        ids: ["E_MAIL"],
        rowDataIds: ["E_MAIL"],
        title: "email-address",
        type: "text",
        width: "full",
        size: "single",
        required: false,
      },
    ],
  },
];
