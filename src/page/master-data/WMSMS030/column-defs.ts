/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS030/column-defs.ts
 *  Description:    기준정보/물류센터정보관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { i18n } from "@/i18n";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const MODAL_COLUMN_DEFS: any = {
  "receiving-logistics-center": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "LC_CD",
      headerKey: "logistics-center-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "LC_NM",
      headerKey: "logistics-center-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "LC_ID",
      headerKey: "logistics-center-id",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  "shipping-logistics-center": [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      width: 50,
      cellStyle: { textAlign: "center" },
      headerCheckboxSelection: true,
      checkboxSelection: true,
    },
    {
      field: "LC_CD",
      headerKey: "logistics-center-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "LC_NM",
      headerKey: "logistics-center-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "LC_ID",
      headerKey: "logistics-center-id",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
};

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "",
    headerName: "No",
    minWidth: 80,
    width: 80,
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (Object.hasOwn(params.data, "STATE") && params.data.STATE === "C") {
        return "";
      }
      return params.node.rowIndex + 1;
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
    lockPosition: true,
  },
  {
    field: "STATE",
    headerName: "",
    cellStyle: { textAlign: "center" },
    hide: true,
  },
  {
    field: "LC_ID",
    headerName: t("grid-column-name.lc-id"),
    sortable: true,
    cellStyle: { textAlign: "right" },
    // valueGetter: (params: any) => {
    //   if (params.data.LC_ID.includes("temp")) {
    //     return "";
    //   } else {
    //     return params.data.LC_ID;
    //   }
    // },
    width: 140,
  },
  {
    field: "LC_NM",
    headerName: t("grid-column-name.logistics-center-name"),
    sortable: true,
    // export: true,
    flex: 1,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.creation-date"),
    cellStyle: { textAlign: "center" },
    sortable: true,
    // export: true,
    width: 170,
  },
  {
    field: "REG_NM",
    headerName: t("grid-column-name.writer"),
    sortable: true,
    // export: true,
    width: 130,
  },
  /** Hidden Column */
  //#region ::  기본정보
  {
    field: "LC_CD",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 영문
    field: "LC_ENG_NM",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 기준 시간
    field: "LC_TIME_ZONE",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // TAG
    field: "TAG_PREFIX",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // GLC 코드
    field: "GLN_CD",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 위도
    field: "LATITUDE",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 경도
    field: "LONGITUDE",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  //#endregion
  //#region ::  재고관리설정
  {
    // 무게관리 여부
    field: "WGT_YN",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "WGT_TYPE",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 유효기간관리여부
    field: "BEST_DATE_YN",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // RFID적용여부
    field: "RFID_YN",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 랙사용여부
    field: "RACK_USE_YN",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 랙타입
    field: "RACK_TYPE",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 물류용기관리여부
    field: "MANAGE_PLT_YN",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 재고부족알림
    field: "STOCK_ALERT_YN",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  //#endregion
  //#region ::  입출고관리설정
  {
    // 자동작업단계 (입고)
    field: "AUTO_RCV_STEP",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 자동작업단계(출고)
    field: "AUTO_SND_STEP",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 다중출고여부
    field: "MULTI_SHIP_YN",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 거래처ZONE자동등록
    field: "CLIENT_ORD_YN",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  //#endregion
  //#region ::  센터유형설정
  {
    // 물류센터타입
    field: "LC_TYPE",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 물류센터운영타입
    field: "LC_USE_TY",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 입고물류센터(코드)
    field: "RECEIVING_LC_CD",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 입고물류센터(ID)
    field: "RECEIVING_LC_ID",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 입고물류센터(명칭)
    field: "RECEIVING_LC_NM",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 출고물류센터(코드)
    field: "SHIPPING_LC_CD",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 출고물류센터(ID)
    field: "SHIPPING_LC_ID",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 출고물류센터(명칭)
    field: "SHIPPING_LC_NM",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  //#endregion
  //#region ::  사업자정보
  {
    // 물류사코드
    field: "CUST_CD",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 물류사명
    field: "CUST_NM",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 사업자번호
    field: "BIZ_NO",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 대표자성명
    field: "REP_NM",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 상호
    field: "TRADE_NM",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 우편번호
    field: "ZIP",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 주소
    field: "ADDR",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 전화번호
    field: "TEL",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 팩스번호
    field: "FAX",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    // 이메일주소
    field: "E_MAIL",
    editable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
  //#endregion
];
