/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSMS030/input.search.ts
 *  Description:    기준관리/물류센터정보관리 Meta data
 *  Authors:        dhkim
 *  Update History:
 *                  2024.07 : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import type { ISearchInput } from "@/types";

export const SEARCH_INPUT: ISearchInput[] = [
  {
    ids: ["LC_NAME"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: ["LC_NAME"],
    title: "logistics-center-name",
    width: "half",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["select-box"],
    optionsKey: "LCLIST",
    optionsReadOnly: true,
  },
];

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  unUsedRefreshButton: true,
  modalColumnDefs: [],
  searchModalInfo: [],
  searchInput: SEARCH_INPUT,
  pageInfo: "null",
};
