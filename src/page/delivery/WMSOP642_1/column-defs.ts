/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP642_1/column-defs.ts
 *  Description:    택배관리/택배접수관리(공통)_택배접수관리(API) 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import GridSelectBox from "@/components/renderer/grid-select-box.vue";
import { Format } from "@/lib/ag-grid/index";
import type { IColDef, IHasKeyColDef } from "@/types/agGrid";

export const FORM_COLUMN_DEFS: IColDef[] = [
  {
    field: "No",
    headerKey: "no",
    headerName: "",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    field: "SPDCOL_SELECT ",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
  },
  {
    field: "DENSE_FLAG",
    headerKey: "lc-name",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "CUST_NM",
    headerKey: "owner",
    headerName: "",
    headerClass: "header-center",
    width: 130,
  },
  {
    field: "WORK_STAT_NM",
    headerKey: "order-status(H)",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 130,
  },
  {
    field: "WORK_STAT_D_NM",
    headerKey: "order-status(D)",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 130,
  },
  {
    field: "OUT_REQ_DT",
    headerKey: "expected-delivery-date",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 110,
  },
  {
    field: "ITEM_CD",
    headerKey: "product-code",
    headerName: "",
    headerClass: "header-center",
    minWidth: 140,
  },
  {
    field: "ITEM_NM",
    headerKey: "product-name",
    headerName: "",
    headerClass: "header-center",
    minWidth: 200,
  },
  {
    field: "TYPE_S_YN",
    headerKey: "individual",
    headerName: "",
    headerClass: "header-center",
    editable: false,
    width: 80,
  },
  {
    field: "ORD_QTY",
    headerKey: "qty",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    editable: false,
    width: 110,
    valueFormatter: Format.NumberCount,
  },
  {
    // 박스번호
    field: "TRACKING_NO",
    headerKey: "box-no",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    cellRenderer: GridSelectBox,
    width: 110,
  },
  {
    // 택배박스
    field: "PARCEL_BOX_TY_NM",
    headerKey: "parcel-box",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 110,
  },
  {
    // 접수
    field: "RECEIVE_YN",
    headerKey: "receipt",
    headerName: "",
    headerClass: "header-center",
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    editable: false,
    width: 50,
  },
  {
    // 택배사
    field: "DLV_COMP_CD_NM",
    headerKey: "delivery-company",
    headerName: "",
    headerClass: "header-center",
    width: 150,
  },
  {
    // 송화인
    field: "SENDR_NM",
    headerKey: "sender-name",
    headerName: "",
    headerClass: "header-center",
    editable: false,
    width: 130,
  },
  {
    // 송장번호
    field: "INVC_NO",
    headerKey: "invoice-number",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "left" },
    width: 150,
  },
  {
    // 추가송장 여부
    field: "INVC_INVC_ADD_FLAG",
    headerKey: "add",
    headerName: "",
    headerClass: "header-center",
    editable: false,
    width: 50,
  },
  {
    // 반품(원송장)
    field: "PARCEL_ORD_TY_NM",
    headerKey: "return(original-invoice)",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 150,
  },
  {
    //접수구분
    field: "PARCEL_ORD_TY",
    headerKey: "return(original-invoice)",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    // 송장출력
    field: "PRINT_CNT",
    headerKey: "print-invoice",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 110,
  },
  {
    // 엑셀등록
    field: "EXCEL_IN_YN",
    headerKey: "excel-registration",
    headerName: "",
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    width: 100,
  },
  {
    // 차수
    field: "ORD_DEGREE",
    headerKey: "degree",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 80,
  },
  {
    field: "DATA_SENDER_NM",
    headerKey: "channel",
    headerName: "",
    headerClass: "header-center",
    width: 200,
  },
  {
    // 원주문번호
    field: "ORG_ORD_ID",
    headerKey: "original-order-number",
    headerName: "",
    headerClass: "header-center",
    width: 200,
  },
  {
    // 주문번호
    field: "ORD_ID",
    headerKey: "order-number",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 130,
  },
  {
    // 주문SEQ
    field: "ORD_SEQ",
    headerKey: "order-Seq",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 110,
  },
  {
    field: "RCVR_NM",
    headerKey: "customer-nm",
    headerName: "",
    headerClass: "header-center",
    width: 130,
  },
  {
    // 전화번호
    field: "RCVR_TEL",
    headerKey: "tel1",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 150,
  },
  {
    // 우편번호
    field: "ZIP_NO",
    headerKey: "zip-code",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 110,
  },
  {
    // 주소
    field: "ADDR",
    headerKey: "address",
    headerName: "",
    headerClass: "header-center",
    width: 350,
  },
  {
    // 배송메시지
    field: "DLV_MSG1",
    headerKey: "delivery-message",
    headerName: "",
    headerClass: "header-center",
  },
  {
    // 비고
    field: "ETC2",
    headerKey: "etc2",
    headerName: "",
    headerClass: "header-center",
  },
  {
    // 주문설비타입
    field: "EQUIPMENT_NM",
    headerKey: "ordered-equipment-type",
    headerName: "",
    headerClass: "header-center",
    width: 130,
  },
  {
    // 세부주문번호
    field: "LEGACY_ORG_ORD_NO",
    headerKey: "detailed-order-number",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
  },
  {
    // 접수일
    field: "REG_DT",
    headerKey: "date-of-receipt",
    headerName: "",
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 180,
  },
  {
    // 접수자
    field: "REG_NM",
    headerKey: "receptionist",
    headerName: "",
    headerClass: "header-center",
    width: 130,
  },
  {
    field: "POOL_CODE",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "RECOM_BOX_TY_NM",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "CUST_ID",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "RCVR_ADDR",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "RCVR_DETAIL_ADDR",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "PROC_FLAG",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "ROW_COLOR",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "TYPE_ST",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "PROC_SUM",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "PARCEL_ORD_TY",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "ROW_COLOR",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "TYPE_S_INVC_CNT",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "CBM_QTY",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "OPT_1",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "DLV_COMP_CD",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "RECOM_BOX_ID",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "POOL_PARCEL_BOX_TY",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "M_PARCEL_BOX_SYS_CD",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "PAGE_VAL",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "TOTALPAGE",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "PARCEL_BOX_TY",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "B_DENSE_FLAG",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "B_ROW_COLOR",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "SORT_FLAG",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "DENSE_CNT",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "FIRST_ITEM_NM",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "FIRST_ORD_QTY",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "PARCEL_COM_TY_SEQ",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
  {
    field: "END",
    headerKey: "",
    headerName: "",
    headerClass: "header-center",
    hide: true,
  },
];

// export const MODAL_COLUMN_DEFS: any = {
export const MODAL_COLUMN_DEFS: IHasKeyColDef = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      cellStyle: { textAlign: "right" },
      sortable: true,
    },
  ],
  product: [
    {
      field: "",
      headerKey: "",
      headerName: "No",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
      cellStyle: { textAlign: "center" },
    },
    {
      field: "CUST_NM",
      headerKey: "owner",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_GRP_ID",
      headerKey: "product-group",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_CODE",
      headerKey: "product-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ITEM_KOR_NM",
      headerKey: "product-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BOX_BAR_CD",
      headerKey: "box-barcode",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "MAKER_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "STOCK_QTY",
      headerKey: "stock",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "BAD_QTY",
      headerKey: "inferior-product",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UOM_NM",
      headerKey: "uom",
      headerName: "",
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "UNIT_PRICE",
      headerKey: "unit-price",
      headerName: "",
      valueFormatter: Format.NumberPrice,
      cellStyle: { textAlign: "right" },
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "WH_NM",
      headerKey: "warehouse",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};

export const DEVICELIST_FORM_COLUMN_DEFS: IColDef[] = [
  {
    field: "DAS_ORD_DT",
    headerKey: "das-data-date",
    headerName: "",
    cellStyle: { textAlign: "center" },
  },
  {
    field: "DAS_ORD_DEGREE",
    headerKey: "degree",
    headerName: "",
  },
  {
    field: "ITEM_CODE",
    headerKey: "product-code",
    headerName: "",
  },
  {
    field: "ITEM_NAME",
    headerKey: "product-name",
    headerName: "",
  },
  {
    field: "ORD_QTY",
    headerKey: "ord-qty",
    headerName: "",
    cellStyle: { textAlign: "right" },
  },
  {
    field: "INVC_NO",
    headerKey: "invoice-number",
    headerName: "",
    cellStyle: { textAlign: "right" },
  },
  {
    field: "ORG_ORD_ID",
    headerKey: "original-order-number",
    headerName: "",
    cellStyle: { textAlign: "right" },
  },
  {
    field: "REG_DT",
    headerKey: "registration-dt",
    headerName: "",
    cellStyle: { textAlign: "center" },
  },
];
