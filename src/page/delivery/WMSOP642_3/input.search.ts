/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP642_3/input.search.ts
 *  Description:    택배관리/택배송장발급이력 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import type { IModal, ISearchInput } from "@/types";
import { MODAL_COLUMN_DEFS } from "./column-defs";

export const SEARCH_INPUT: ISearchInput[] = [
  {
    // 작업일자
    ids: ["cmbSrchType3", "vrSrchReqDtFrom3", "vrSrchReqDtTo3"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    title: "",
    width: "quarter",
    isModal: false,
    required: true,
    isSearch: false,
    options: [{ name: "input-management-date", nameKey: "input-management-date", value: "100" }],
    optionsKey: "cmbSrchType3",
    optionsAutoSelected: { autoSelectedKeyIndex: 0 },
    optionMerge: false,
    types: ["select-box", "date", "date"],
  },
  {
    ids: ["vrSrchCustCd3", "vrSrchCustNm3"],
    hiddenId: "vrSrchCustId3",
    rowDataIds: ["CUST_CD", "CUST_NM"],
    rowDataHiddenId: "CUST_ID",
    searchApiKeys: ["vrSrchCustCd3", "vrSrchCustNm3"],
    srchKey: "CUST",
    title: "owner",
    width: "quarter",
    isModal: false,
    required: true,
    isSearch: true,
    types: ["text", "text"],
  },
  {
    ids: ["PARCEL_COM_TY3"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "delivery-classification",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    options: [{ name: "all", nameKey: "all", value: "" }],
    optionsKey: "PARCEL_COM_TY3",
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    types: ["select-box"],
  },
  {
    ids: ["vrSrchReceiveYn3"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "receive-Y/N",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "N", nameKey: "N", value: "N" },
      { name: "Y", nameKey: "Y", value: "Y" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    types: ["select-box"],
  },
  {
    ids: ["vrSrchOrgOrdId3"],
    hiddenId: "",
    rowDataIds: ["ORD_ID"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchOrgOrdId3"],
    srchKey: "ORD_ID",
    title: "order-number",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["vrSrchInvcNo3"],
    hiddenId: "",
    rowDataIds: ["INVC_NO"],
    rowDataHiddenId: "",
    searchApiKeys: ["vrSrchInvcNo3"],
    srchKey: "INVC_NO",
    title: "Invoice-Number",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    types: ["text"],
  },
  {
    ids: ["vrSrchInvcDelYn3"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "delete-Y/N",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "N", nameKey: "N", value: "N" },
      { name: "Y", nameKey: "Y", value: "Y" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    types: ["select-box"],
  },
  {
    ids: ["vrSrchInvcPrintYn3"],
    hiddenId: "",
    rowDataIds: [""],
    rowDataHiddenId: "",
    searchApiKeys: [""],
    srchKey: "",
    title: "whether-to-print-invoice-Y/N",
    width: "quarter",
    isModal: false,
    required: false,
    isSearch: false,
    options: [
      { name: "all", nameKey: "all", value: "" },
      { name: "N", nameKey: "N", value: "N" },
      { name: "Y", nameKey: "Y", value: "Y" },
    ],
    optionsAutoSelected: { autoSelectedKeyIndex: 0, allowAutoSelected: true },
    types: ["select-box"],
  },
];

export const SEARCH_MODAL_INFO: IModal = {
  owner: {
    page: "WMSCM011",
    id: "owner",
    title: "search-owner",
    gridTitle: "owner-list",

    apis: {
      url: "/WMSCM011/list_rn.action",
      params: {
        S_CUST_CD: "",
        S_CUST_NM: "",
        S_CUST_ID: "",
        S_CUST_TYPE: "12",
        S_LC_ALL: "",
        S_LC_ID: "",
      },
      data: {
        _search: false,
        nd: "",
        rows: "100",
        page: "1",
        sidx: "",
        sord: "asc",
      },
    },
    inputs: [
      {
        id: "S_CUST_TYPE",
        title: "customer-type",
        type: "select",
        width: "triple",
        options: [{ name: "all", nameKey: "all", value: "12" }],
        optionsReadOnly: true,
      },
      {
        id: "S_CUST_CD",
        searchContainerInputId: "vrSrchCustCd3",
        title: "shipper-code",
        type: "text",
        width: "triple",
      },
      {
        id: "S_CUST_NM",
        searchContainerInputId: "vrSrchCustNm3",
        title: "owner-name",
        type: "text",
        width: "triple",
      },
    ],
  },
};

export const SEARCH_CONTAINER_META = {
  useSetting: false,
  unUsedRefreshButton: false,
  modalColumnDefs: MODAL_COLUMN_DEFS,
  searchInput: SEARCH_INPUT,
  searchModalInfo: SEARCH_MODAL_INFO,
  pageInfo: "null",
};
