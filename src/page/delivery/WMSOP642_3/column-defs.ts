/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP642_3/column-defs.ts
 *  Description:    택배관리/택배접수관리(공통)_택배송장발급이력 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import { i18n } from "@/i18n";
import { Format } from "@/lib/ag-grid";
import type { IModalColDef } from "@/types";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    menuTabs: ["columnsMenuTab"],
    lockVisible: true,
    lockPosition: true,
    pinned: "left",
  },
  {
    field: "SPDCOL_SELECT",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    suppressColumnsToolPanel: true,
    pinned: "left",
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.Owner"),
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ORD_ID",
    headerName: t("grid-column-name.Order-Number"),
    headerClass: "header-center",
    width: 130,
    cellStyle: { textAlign: "right" },
    cellClass: "stringType",
    sortable: true,
  },
  {
    field: "ORD_SEQ",
    headerName: t("grid-column-name.order-Seq"),
    headerClass: "header-center",
    width: 100,
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "ORD_DATE",
    headerName: t("grid-column-name.Date-of-Receipt"),
    width: 130,
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
  {
    field: "SWEET_TRACKER_DLV_CD",
    headerName: t("grid-column-name.courier-company"),
    headerClass: "header-center",
    width: 150,
    sortable: true,
  },
  {
    field: "INVC_NO",
    headerName: t("grid-column-name.Invoice-Number"),
    headerClass: "header-center",
    width: 200,
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "PRINT_CNT",
    headerName: t("grid-column-name.print-count"),
    headerClass: "header-center",
    width: 120,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    sortable: true,
  },
  {
    field: "DLV_PRICE",
    headerName: t("grid-column-name.transportation-costs"),
    headerClass: "header-center",
    width: 120,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberPrice,
    sortable: true,
  },
  {
    field: "PARCEL_QTY",
    headerName: t("grid-column-name.quantity"),
    headerClass: "header-center",
    width: 120,
    cellStyle: { textAlign: "right" },
    valueFormatter: Format.NumberCount,
    sortable: true,
  },
  {
    field: "SWEET_TRACKER_IF_YN",
    headerName: t("grid-column-name.tracking-the-delivery"),
    headerClass: "header-center",
    width: 110,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    sortable: true,
  },
  {
    field: "PARCEL_BOX_TY",
    headerName: t("grid-column-name.box-type"),
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 110,
    sortable: true,
  },
  {
    field: "BOX_NO",
    headerName: t("grid-column-name.box-order"),
    headerClass: "header-center",
    width: 110,
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "EXCEL_IN_YN",
    headerName: t("grid-column-name.excel-registration-Y/N"),
    headerClass: "header-center",
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    width: 130,
    sortable: true,
  },
  {
    field: "REG_DT",
    headerName: t("grid-column-name.Registration-Date"),
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    width: 180,
    valueFormatter: Format.Date,
    sortable: true,
  },
  {
    field: "REG_NO",
    headerName: t("grid-column-name.Registration-No"),
    headerClass: "header-center",
    width: 130,
    sortable: true,
  },
  {
    field: "DEL_YN",
    headerName: t("grid-column-name.delete-Y/N"),
    headerClass: "header-center",
    width: 100,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    sortable: true,
  },
  {
    field: "RECEIVE_YN",
    headerName: t("grid-column-name.receive-Y/N"),
    headerClass: "header-center",
    width: 100,
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    sortable: true,
  },
  {
    field: "UPD_DT",
    headerName: t("grid-column-name.Modified-Date"),
    width: 180,
    valueFormatter: Format.Date,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "UPD_NO",
    headerName: t("grid-column-name.Modified-No"),
    headerClass: "header-center",
    width: 130,
    sortable: true,
  },
  {
    field: "SPDCOL_FLAG",
    headerName: "",
    headerClass: "header-center",
    sortable: true,
    hide: true,
    suppressColumnsToolPanel: true,
  },
];

export const MODAL_COLUMN_DEFS: IModalColDef = {
  owner: [
    {
      field: "No",
      headerName: "No",
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerName: t("grid-column-name.shipper-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerName: t("grid-column-name.owner-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerName: t("grid-column-name.address"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerName: t("grid-column-name.company-name"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerName: t("grid-column-name.owner-epc-code"),
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerName: t("grid-column-name.tel"),
      headerClass: "header-center",
      sortable: true,
    },
  ],
};
