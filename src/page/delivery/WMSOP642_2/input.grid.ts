/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP642_2/input.grid.ts
 *  Description:    택배관리/택배송장업로드 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.05. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import { IControlBtn } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "delivery-history-registration",
    colorStyle: "success",
    paddingStyle: "normal",
    authType: "EXC_AUTH",
    image: "excel",
  },
  {
    title: "modification-of-courier-invoice-number",
    colorStyle: "primary",
    paddingStyle: "normal",
    authType: "EXC_AUTH",
    image: "",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [100],
};

export const gridOptionsMeta = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  enableRangeSelection: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
};
