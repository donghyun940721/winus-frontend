/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSOP642_2/column-defs.ts
 *  Description:    택배관리/택배접수관리(공통)_택배송장업로드 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import GridCircleIconForYn from "@/components/renderer/grid-circle-icon-for-yn.vue";
import { i18n } from "@/i18n";
import { Format } from "@/lib/ag-grid";
import type { IColDef, IColGroupDef } from "@/types/agGrid";

const { t } = i18n.global;

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "NO",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    menuTabs: ["columnsMenuTab"],
    lockVisible: true,
    lockPosition: true,
    pinned: "left",
  },
  {
    field: "SPDCOL_SELECT",
    headerName: "",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    suppressColumnsToolPanel: true,
    lockVisible: true,
    lockPosition: true,
    pinned: "left",
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.Owner"),
    width: 140,
    flex: 1,
    headerClass: "header-center",
    sortable: true,
  },
  {
    field: "ORD_ID",
    headerName: t("grid-column-name.Order-Number"),
    headerClass: "header-center",
    width: 130,
    flex: 1,
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
  {
    field: "INPUT_ORD_SEQ",
    headerName: t("grid-column-name.order-Seq"),
    headerClass: "header-center",
    width: 100,
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
  {
    field: "ORD_SEQ",
    headerName: t("grid-column-name.turn"),
    headerClass: "header-center",
    width: 80,
    cellStyle: { textAlign: "center" },
    sortable: true,
  },
  {
    field: "SWEET_TRACKER_DLV_CD",
    headerName: t("grid-column-name.courier-company1"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 150,
    flex: 1,
    sortable: true,
  },
  {
    field: "DLV_COMP_CD",
    headerName: t("grid-column-name.courier-company2"),
    headerClass: "header-center",
    cellStyle: { textAlign: "center" },
    width: 150,
    sortable: true,
  },
  {
    field: "ORD_DATE",
    headerName: t("grid-column-name.Date-of-Receipt"),
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    width: 130,
    sortable: true,
  },
  {
    //ORG_INVC_NO, INVC_NO 다른 컬럼에서 같은 값을 가지고 있음
    field: "INVC_NO",
    headerName: t("grid-column-name.Invoice-Number"),
    headerClass: "header-center",
    sortable: true,
    // cellRenderer: gridTextInputVue,
    width: 200,
    flex: 1,
    editable: true,
  },
  {
    field: "DLV_PRICE",
    headerName: t("grid-column-name.transportation-costs"),
    headerClass: "header-center",
    cellStyle: { textAlign: "right" },
    width: 120,
    valueFormatter: Format.NumberPrice,
    sortable: true,
  },
  {
    field: "SWEET_TRACKER_IF_YN",
    headerName: t("grid-column-name.tracking-the-delivery"),
    width: 100,
    headerClass: "header-center",
    cellClass: "renderer-cell",
    cellRenderer: GridCircleIconForYn,
    sortable: true,
  },
  {
    field: "BOX_NO",
    headerName: t("grid-column-name.box-order"),
    headerClass: "header-center",
    width: 110,
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "PARCEL_QTY",
    headerName: t("grid-column-name.quantity"),
    width: 80,
    headerClass: "header-center",
    valueFormatter: Format.NumberCount,
    cellStyle: { textAlign: "right" },
    sortable: true,
  },
  {
    field: "SPDCOL_FLAG",
    headerClass: "header-center",
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "ORG_INVC_NO",
    headerClass: "header-center",
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "ORG_SWEET_TRACKER_DLV_CD",
    headerClass: "header-center",
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "END",
    headerClass: "header-center",
    hide: true,
    suppressColumnsToolPanel: true,
  },
];

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      cellStyle: { textAlign: "center" },
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};
