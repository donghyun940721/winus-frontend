/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSIF001/input.grid.ts
 *  Description:    인터페이스/오픈몰마스터관리 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.06. : Created by dhkim
 *
-------------------------------------------------------------------------------*/
import { IControlBtn } from "@/types";
import { FORM_COLUMN_DEFS } from "./column-defs";

export const CONTROL_BTN: IControlBtn[] = [
  {
    title: "new",
    colorStyle: "primary",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "save",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "INS_AUTH",
  },
  {
    title: "del",
    colorStyle: "danger",
    paddingStyle: "bold",
    image: "",
    authType: "DEL_AUTH",
  },
];

export const gridMetaData: any = {
  //페이징옵션
  pagingSizeList: [],
};

export const gridOptionsMeta = {
  defaultColDef: {
    resizable: true,
    menuTabs: [],
    lockVisible: true,
    lockPosition: true,
  },
  headerHeight: 32,
  rowHeight: 32,
  columnDefs: FORM_COLUMN_DEFS,
  rowSelection: "multiple",
  rowModelType: "clientSide",
  cellDataType: false,
  enableRangeSelection: true,
  suppressRowClickSelection: true, // row클릭시 체크박스 체크방지
  suppressContextMenu: true, // Context Menu 미사용
};
