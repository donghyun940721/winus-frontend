/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSIF001/input.meta.ts
 *  Description:    인터페이스/오픈몰마스터관리 META File
 *  Authors:        dhkim
 *  Update History:
 *                  2024.06. : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import type { info } from "@/types/index.ts";
import { gridMetaData, gridOptionsMeta } from "./input.grid.ts";
import { SEARCH_CONTAINER_META } from "./input.search.ts";

export * from "./input.grid.ts";
export * from "./input.search.ts";

export const commonSetting = {
  authPageGroup: "TMSYS",
  authPageId: "WMSDF001_3",
  serAuthField: "SER_AUTH",
};

export const SEARCH_META = {
  ...SEARCH_CONTAINER_META,
  ...commonSetting,
};

export const GRID_META = {
  ...gridMetaData,
  ...commonSetting,
};

export const GRID_OPTIONS_META = {
  ...gridOptionsMeta,
};

export const INFO: info = {
  autoModal: true,
  autoModalPage: "",
  pk: "RNUM",
};
