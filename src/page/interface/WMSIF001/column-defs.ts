/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	WMSIF001/column-defs.ts
 *  Description:    인터페이스/오픈몰 마스터 관리 컬럼 정의 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import gridSelect from "@/components/renderer/grid-select.vue";
import { i18n } from "@/i18n";
import { Format, Getter } from "@/lib/ag-grid";
import { GridUtils } from "@/lib/ag-grid/utils";
import type { IColDef, IColGroupDef } from "@/types/agGrid";
import { ICellEditorParams } from "ag-grid-community";

const { t } = i18n.global;

export const FORM_COLUMN_DEFS: (IColDef | IColGroupDef)[] | null = [
  {
    field: "No",
    headerName: "No",
    minWidth: 80,
    width: 80,
    cellStyle: { textAlign: "center" },
    valueGetter: (params: any) => {
      if (String(params.node.data.RNUM).includes("temp")) {
        return "";
      } else {
        return params.node.rowIndex + 1;
      }
    }, // 인덱스는 0이 아닌 1부터 시작
    pinned: "left",
  },
  {
    field: "SPDCOL_CHK_BOX",
    maxWidth: 50,
    cellStyle: { textAlign: "center" },
    headerCheckboxSelection: true,
    checkboxSelection: true,
    pinned: "left",
  },
  {
    field: "LC_NM",
    headerName: t("grid-column-name.lc-name"),
    headerClass: "header-center",
    sortable: true,
    pinned: "left",
  },
  {
    field: "CUST_CD",
    headerName: t("grid-column-name.shipper-code"),
    headerClass: "header-center",
    sortable: true,
    pinned: "left",
  },
  {
    field: "CUST_NM",
    headerName: t("grid-column-name.owner"),
    headerClass: "header-center",
    sortable: true,
    pinned: "left",
  },
  {
    field: "OPENMALL_CD",
    headerName: t("grid-column-name.openmall_cd"),
    headerClass: "header-require",
    width: 130,
    editable: true,
    cellEditor: gridSelect,
    cellEditorParams: (params: ICellEditorParams) => {
      return {
        values: GridUtils.getOptionsCode("OPENMALL_CD"),
        formatValue: (value: any) => {
          return Getter.convetCodeToNameByOptionData("OPENMALL_CD", value);
        },
      };
    },
    cellRenderer: function (params: any) {
      return Getter.convetCodeToNameByOptionData("OPENMALL_CD", params.value);
    },
    sortable: true,
    pinned: "left",
  },
  {
    field: "OPENMALL_CD_DESC",
    headerName: t("grid-column-name.openmall_cd(desc)"),
    headerClass: "header-require",
    width: 180,
    sortable: true,
    editable: true,
  },
  {
    field: "STRT_DT",
    headerName: t("grid-column-name.strt-dt"),
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    width: 110,
    sortable: true,
    editable: true,
  },
  {
    field: "REP_DELIVERY_VENDOR",
    headerName: t("grid-column-name.rep-delivery-vendor"),
    headerClass: "header-center",
    width: 150,
    sortable: true,
    editable: true,
  },
  {
    field: "OPENMALL_API_ID1",
    headerName: t("grid-column-name.openmall-api-id1"),
    headerClass: "header-center",
    width: 180,
    sortable: true,
    editable: true,
  },
  {
    field: "OPENMALL_API_PW1",
    headerName: t("grid-column-name.openmall-api-pw1"),
    headerClass: "header-center",
    width: 180,
    sortable: true,
    editable: true,
  },
  {
    field: "OPENMALL_API_KEY1",
    headerName: t("grid-column-name.openmall_api_key1"),
    headerClass: "header-require",
    width: 180,
    sortable: true,
    editable: true,
  },
  {
    field: "OPENMALL_API_ID2",
    headerName: t("grid-column-name.openmall-api-id2"),
    headerClass: "header-center",
    width: 180,
    sortable: true,
    editable: true,
  },
  {
    field: "OPENMALL_API_PW2",
    headerName: t("grid-column-name.openmall-api-pw2"),
    headerClass: "header-center",
    width: 180,
    sortable: true,
    editable: true,
  },
  {
    field: "OPENMALL_API_KEY2",
    headerName: t("grid-column-name.openmall-api-key2"),
    headerClass: "header-center",
    width: 180,
    sortable: true,
    editable: true,
  },
  {
    field: "OPENMALL_API_ID3",
    headerName: t("grid-column-name.openmall-api-id3"),
    headerClass: "header-center",
    width: 180,
    sortable: true,
    editable: true,
  },
  {
    field: "OPENMALL_API_PW3",
    headerName: t("grid-column-name.openmall-api-pw3"),
    headerClass: "header-center",
    width: 180,
    sortable: true,
    editable: true,
  },
  {
    field: "OPENMALL_API_KEY3",
    headerName: t("grid-column-name.openmall-api-key3"),
    headerClass: "header-center",
    sortable: true,
    editable: true,
  },
  {
    field: "COMPAYNY_GOODS_CD",
    headerName: t("grid-column-name.company-goods-cd"),
    headerClass: "header-center",
    width: 130,
    cellStyle: { textAlign: "center" },
    sortable: true,
    editable: true,
  },
  {
    field: "TOKEN1",
    headerName: t("grid-column-name.token1"),
    headerClass: "header-center",
    width: 250,
    cellStyle: { textAlign: "left" },
    sortable: true,
    editable: true,
  },
  {
    field: "TOKEN1_REG_DT",
    headerName: t("grid-column-name.token-reg-dt"),
    headerClass: "header-center",
    width: 180,
    valueFormatter: Format.Date,
    sortable: true,
    editable: false,
  },
  {
    field: "BF_OPENMALL_CD",
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "BF_OPENMALL_CD_DESC",
    hide: true,
    suppressColumnsToolPanel: true,
  },
  {
    field: "BF_OPENMALL_API_KEY1",
    hide: true,
    suppressColumnsToolPanel: true,
  },
];

export const MODAL_COLUMN_DEFS: any = {
  owner: [
    {
      field: "No",
      headerKey: "no",
      headerName: "",
      width: 60,
      valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
    },
    {
      field: "",
      headerKey: "",
      headerName: "",
      headerCheckboxSelection: true,
      checkboxSelection: true,
      width: 50,
    },
    {
      field: "CUST_CD",
      headerKey: "shipper-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_NM",
      headerKey: "owner-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "ADDR",
      headerKey: "address",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "EMP_NM",
      headerKey: "company-name",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "CUST_EPC_CD",
      headerKey: "owner-epc-code",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
    {
      field: "TEL",
      headerKey: "tel",
      headerName: "",
      headerClass: "header-center",
      sortable: true,
    },
  ],
};
