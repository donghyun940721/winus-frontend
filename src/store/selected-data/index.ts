/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	selected-data/index.ts
 *  Description:    화면 상 모든 입력/선택란의 선택 값 관리를 위한 스크립트 (Component, Grid, Modal)
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { defineStore } from "pinia";
import { reactive } from "vue";

import { UtilService } from "@/services/util-service";
import { useTabsStore } from "@/store";

export const useSelectedDataStore = defineStore("selected-data", () => {
  // Grid에서 포커싱된 행의 데이터
  const _selectedData = reactive({ value: {} as any });
  // Grid에서 선택된 행의 데이터
  const _selectedDataList = reactive({ value: {} as any });
  // SearchModal 창에서 선택한 값
  const _selectedModalData = reactive({ value: {} as any });
  // Info 패널을 갖는 화면에 바인딩된 데이터 관리
  const _selectedInfoData = reactive({ value: {} as any });

  /**
   * 현재 선택한 페이지 정보 조회
   * @returns 현재 선택한 페이지의 id
   */
  const getCurrentPageId = (): any => {
    if (useTabsStore().selectedTab.value) {
      return useTabsStore().selectedTab.value?.id;
    }
    return "unknownPage";
  };

  /**
   * 그리드에서 포커싱된의 행의 데이터 조회
   * @returns : 그리드에서 포커싱된 행의 데이터
   */
  const getSelectedData = (): any => {
    return _selectedData.value[getCurrentPageId()] || {};
  };

  /**
   * searchModal 창에서 선택한 데이터 조회
   * @param paramPageId : 데이터를 조회할 페이지 id
   * @returns           : searchModal 창에서 선택한 데이터
   */
  const getSelectedModalData = (paramPageId?: string): any => {
    let pageId;
    if (paramPageId) {
      pageId = paramPageId;
    } else {
      pageId = getCurrentPageId();
    }

    return _selectedModalData.value[pageId] || {};
  };

  /**
   * 그리드에서 선택된 행의 데이터 조회
   * @returns : 그리드에서 선택된 행 데이터
   */
  const getSelectedDataList = (): any => {
    return _selectedDataList.value[getCurrentPageId()] || {};
  };

  /**
   * info영역에서 선택된 데이터 조회
   * @returns : info영역에서 선택된 데이터
   */
  const getSelectedInfoData = (): any => {
    return _selectedInfoData.value[getCurrentPageId()] || {};
  };

  /**
   * info영역에서 선택된 데이터 변경 - 특정 key값의 value를 변경할 때 사용
   * @param key       : 변경할 info input의 key값
   * @param value     : 변경할 값
   * @param isDelete  : 삭제 여부 지정
   */
  function changeSelectedInfoData(key: string, value: string | any | any[], isDelete?: boolean) {
    _selectedInfoData.value[getCurrentPageId()] = _selectedInfoData.value[getCurrentPageId()] || {};
    _selectedInfoData.value[getCurrentPageId()][key] = value;
    if (isDelete) {
      delete _selectedInfoData.value[getCurrentPageId()];
    }
  }

  /**
   * info영역에서 선택된 데이터 행 데이터 변경
   *
   * @param data        : 지정할 데이터 정보
   * @param isChangeRow : 이전에 저장되어있는 store값과 새로 지정한 데이터 정보(data) merge 여부
   *                      grid와 info가 함께 있는 페이지에서 '신규' 행 생성 시 이전에 저장되어 있는 값은 지우고 신규 행에 바인딩될 input의 id와 value만 _selectedInfoData에 저장한다
   */
  function selectedInfoHandler(data: { [key: string]: any } | null, isChangeRow?: boolean) {
    if (isChangeRow) {
      if (!data) {
        delete _selectedInfoData.value[getCurrentPageId()];
      }
      _selectedInfoData.value[getCurrentPageId()] = data;
    } else {
      _selectedInfoData.value[getCurrentPageId()] = { ..._selectedInfoData.value[getCurrentPageId()], ...data };
    }
  }

  /**
   * 그리드에서 포커싱된 데이터 변경
   * @param data         : 변경할 데이터 정보
   * @param isOpenByInfo : 이전 데이터 정보와 변경할 데이터 정보 merge여부
   */
  function selectedHandler(data: { [key: string]: string | number }, isOpenByInfo?: Boolean) {
    if (isOpenByInfo) {
      _selectedData.value[getCurrentPageId()] = { ..._selectedData.value[getCurrentPageId()], ...data };
    } else {
      _selectedData.value[getCurrentPageId()] = data;
    }
  }

  /**
   * searchModal 데이터 변경
   * @param data        : 변경할 데이터
   * @param paramPageId : 변경 데이터를 지정할 페이지 id
   */
  function selectedModalHandler(data: { [key: string]: any }, paramPageId?: string) {
    let pageId;
    if (paramPageId) {
      pageId = paramPageId;
    } else {
      pageId = getCurrentPageId();
    }

    _selectedModalData.value[pageId] = { ..._selectedModalData.value[pageId], ...data };
  }

  /**
   * searchModal 데이터 삭제
   * @param key         : 삭제할 searchModal 데이터의 key값
   * @param initialData : 삭제 시 초기값으로 지정될 값
   */
  function removeSelectedModalData(key?: string, initialData?: any) {
    if (initialData) {
      _selectedModalData.value[getCurrentPageId()] = initialData;
    } else if (key) {
      _selectedModalData.value[getCurrentPageId()] = _selectedModalData.value[getCurrentPageId()] || {};
      // _selectedModalData.value[getCurrentPageId()][key] = undefined;
      delete _selectedModalData.value[getCurrentPageId()][key];
    }
  }

  /**
   * 그리드에서 포커싱된 데이터 변경
   * @param key   : 변경할 데이터의 key값
   * @param value : 변경할 값
   */
  function changeSelectedData(key: string, value: string) {
    _selectedData.value[getCurrentPageId()] = _selectedData.value[getCurrentPageId()] || {};
    _selectedData.value[getCurrentPageId()][key] = value;
  }

  /**
   * 그리드에서 선택된 행 데이터 변경
   * @param data        : 변경할 값
   * @param filteredKey : 선택된 행의 값을 초기화 할 때 사용되는 key값
   */
  function changeSelectedDataList(data: any, filteredKey?: string) {
    if (filteredKey) {
      if (data.length === 0 && !UtilService.isEmptyObject(_selectedDataList.value[getCurrentPageId()])) {
        _selectedDataList.value[getCurrentPageId()] = _selectedDataList.value[getCurrentPageId()].filter((row: any) => {
          return row.gridType !== filteredKey;
        });
      } else {
        if (UtilService.isEmptyObject(_selectedDataList.value[getCurrentPageId()])) {
          _selectedDataList.value[getCurrentPageId()] = data;
        } else {
          _selectedDataList.value[getCurrentPageId()] = [..._selectedDataList.value[getCurrentPageId()], ...data];
        }
      }
    } else {
      _selectedDataList.value[getCurrentPageId()] = data;
    }
  }

  /**
   * 선택된 행 데이터 중 특정 index에 값을 지정
   * @param index : 값을 변경할 행의 index
   * @param key   : 값을 변경할 input의 key값
   * @param data  : 변경할 값
   */
  function changeSelectedDataListItem(index: number, key: string, data: any) {
    _selectedDataList.value[getCurrentPageId()][index][key] = data;
  }

  /**
   * 모든 선택된 값 초기화
   * @param pageId : 선택된 값을 초기화 할 페이지 id
   */
  function resetAllSelectedData(pageId: string | null = null) {
    if (pageId == "all") {
      _selectedData.value = {};
      _selectedModalData.value = {};
      _selectedDataList.value = {};
      _selectedInfoData.value = {};
    } else {
      delete _selectedData.value[pageId || getCurrentPageId()];
      delete _selectedModalData.value[pageId || getCurrentPageId()];
      delete _selectedDataList.value[pageId || getCurrentPageId()];
      delete _selectedInfoData.value[pageId || getCurrentPageId()];
    }
  }

  return {
    _selectedData,
    _selectedModalData,
    _selectedDataList,
    _selectedInfoData,
    getSelectedData,
    getSelectedModalData,
    getSelectedDataList,
    getSelectedInfoData,
    selectedInfoHandler,
    selectedHandler,
    changeSelectedInfoData,
    changeSelectedDataListItem,
    changeSelectedData,
    selectedModalHandler,
    resetAllSelectedData,
    changeSelectedDataList,
    removeSelectedModalData,
  };
});
