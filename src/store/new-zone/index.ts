/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	new-zone/index.ts
 *  Description:    기준관리/거래처정보관리 - 거래처정보(info영역) - 입출고존 모달 - 신규 팝업 정보를 보관하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { defineStore } from "pinia";
import { reactive } from "vue";
import { useTabsStore } from "@/store";

export const useNewZoneModalStore = defineStore("new-zone", () => {
  // 상세로케이션정보 그리드(하단 그리드)의 검색모달 버튼 모달 호출 여부
  const isOpenNewZone = reactive({ value: {} as any });
  // 현재 선택한 로케이션의 rowIndex
  const currentLocationIndex = reactive({ value: {} as any });
  // 로케이션 조회 모달에서 선택한 데이터
  const locationData = reactive({ value: {} as any });
  // 로케이션 데이터
  const locationDataList = reactive([] as { index: string; data: { [key: string]: any } }[]);

  /**
   * 현재 선택한 페이지 id 정보 조회
   * @returns   : 현재 선택한 페이지 id
   */
  const getCurrentPageId = (): any => {
    if (useTabsStore().selectedTab.value) {
      return useTabsStore().selectedTab.value?.id;
    }
    return "unknownPage";
  };

  /**
   *  그리드 내 검색모달 버튼을 클릭한 상세로케이션정보 그리드의 rowIndex 지정
   * @param index   : 그리드 내 검색모달 버튼을 클릭한 상세로케이션정보 그리드의 rowIndex
   */
  function setCurrentLocationIndex(index: number) {
    currentLocationIndex.value[getCurrentPageId()] = index;
  }

  /**
   * 그리드 내 검색모달 버튼을 클릭한 상세로케이션정보 그리드의 rowIndex 조회
   * @returns       : 그리드 내 검색모달 버튼을 클릭한 상세로케이션정보 그리드의 rowIndex
   */
  function getCurrentLocationIndex() {
    return currentLocationIndex.value[getCurrentPageId()];
  }

  /**
   * 상세로케이션정보 그리드(하단 그리드)의 검색모달 버튼 클릭 시 조회되는 로케이션 조회 모달에서 선택한 데이터 지정
   * @param data    : 로케이션 조회 모달에서 선택한 데이터
   */
  function setLocationData(data: any) {
    locationData.value[getCurrentPageId()] = data;
  }

  /**
   * 상세로케이션정보 그리드(하단 그리드)의 검색모달 버튼 클릭 시 조회되는 로케이션 조회 모달에서 선택한 데이터 조회
   * @returns       : 로케이션 조회 모달에서 선택한 데이터
   */
  function getLocationData() {
    return locationData.value[getCurrentPageId()];
  }

  /**
   * 상세로케이션정보 그리드(하단 그리드)의 검색모달 버튼 모달 호출 여부 지정
   * @param value   : 모달 호출 여부
   *              true  - 호출
   *              false - 호출 안함
   */
  function setOpenNewZone(value: boolean) {
    isOpenNewZone.value[getCurrentPageId()] = value;
  }

  /**
   * 상세로케이션정보 그리드(하단 그리드)의 검색모달 버튼 모달 호출 여부 조회
   * @returns       : 모달 호출 여부
   *              true  - 호출
   *              false - 호출 안함
   */
  function getOpenNewZone() {
    return isOpenNewZone.value[getCurrentPageId()];
  }

  return {
    currentLocationIndex,
    isOpenNewZone,
    locationDataList,
    getCurrentLocationIndex,
    setCurrentLocationIndex,
    setLocationData,
    getLocationData,
    setOpenNewZone,
    getOpenNewZone,
  };
});
