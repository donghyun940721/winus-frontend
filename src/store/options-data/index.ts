/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	options-data/index.ts
 *  Description:    search 컴포넌트 내 select-box 타입 input의 option data 정보를 보관하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { reactive } from "vue";
import { defineStore } from "pinia";

import { useTabsStore } from "@/store";
import { UtilService } from "@/services/util-service";

export const useOptionDataStore = defineStore("option-data", () => {
  // option-data 정보를 저장할 변수
  const _optionData = reactive({ value: {} as any });
  // useOptionDataStore 를 watch하고 있을경우, watch로 인해 내부로직 실행여부 관리
  const _availableWatchBySearchCondition = reactive({ value: {} as any });

  /**
   * 현재 선택한 페이지 id 정보 조회
   * @returns : 현재 선택한 페이지 id
   */
  const getCurrentPageId = (): any => {
    if (useTabsStore().selectedTab.value) {
      return useTabsStore().selectedTab.value?.id;
    }
    return "unknownPage";
  };

  /**
   * option-data 정보 조회
   * @returns : 현재 선택한 페이지 id를 key값으로 지정된 option-data값
   */
  const getOptionData = (): any => {
    return _optionData.value[getCurrentPageId()] || {};
  };

  /**
   * option-data 정보 지정
   * @param data    : 지정할 option-data 정보
   */
  const setOptionData = (data: any) => {
    _optionData.value[getCurrentPageId()] = { ..._optionData.value[getCurrentPageId()], ...data };
    _optionData.value[getCurrentPageId()].S_ITEM_GRP_ID = _optionData.value[getCurrentPageId()].ITEMGRP;
  };

  /**
   * option-data 정보 합치기
   * @param key     : option-data를 저장할 key값
   * @param data    : 새로 합칠 option-data 정보
   */
  const mergeOptionData = (key: string, data: any) => {
    if (!UtilService.isEmptyObject(_optionData.value[getCurrentPageId()][key])) {
      _optionData.value[getCurrentPageId()][key] = [...data, ..._optionData.value[getCurrentPageId()][key]];
    }
  };

  /**
   * option-data 정보 변경하기
   * @param key     : option-data를 저장할 key값
   * @param data    : 변경할 option-data 정보
   */
  const changeOptionData = (key: string, data: any) => {
    _optionData.value[getCurrentPageId()][key] = [...data];
  };

  /**
   * search 컴포넌트 내에 select-box를 사용하는 input에 option의 자동선택 여부 조회
   * @returns       : 자동선택 여부
   *                  true  - 자동선택 실행
   *                  false - 자동선택 실행 안함
   */
  const getAvailableWatchBySearchCondition = (): boolean => {
    return _availableWatchBySearchCondition.value[getCurrentPageId()];
  };

  /**
   * search 컴포넌트 내에 select-box를 사용하는 input에 option의 자동선택 여부 지정
   * @param available : 자동선택 여부
   *                  true  - 자동선택 실행
   *                  false - 자동선택 실행 안함
   */
  const setAvailableWatchBySearchCondition = (available: boolean) => {
    _availableWatchBySearchCondition.value[getCurrentPageId()] = available;
  };

  /**
   * '모든 탭 닫기' 버튼 클릭 시 해당 option-data 정보를 초기화 한다
   * @param pageId : 현재 선택된 페이지의 id
   */
  const resetAllOptionData = (pageId: string | null = null) => {
    if (pageId == "all") {
      _optionData.value = {};
      _availableWatchBySearchCondition.value = {};
    } else {
      delete _optionData.value[pageId || getCurrentPageId()];
      delete _availableWatchBySearchCondition.value[pageId || getCurrentPageId()];
    }
  };

  return {
    _optionData,
    _availableWatchBySearchCondition,
    getOptionData,
    setOptionData,
    changeOptionData,
    mergeOptionData,
    resetAllOptionData,
    getAvailableWatchBySearchCondition,
    setAvailableWatchBySearchCondition,
  };
});
