/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	row-span-check-box/index.ts
 *  Description:    그리드 내 check-box에서 행병합 정보를 보관하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { useTabsStore } from "@/store";
import { defineStore } from "pinia";
import { reactive } from "vue";

export const useRowSpanCheckboxStore = defineStore("check", () => {
  // 왼쪽 체크박스 체크된 리스트
  const checkedSpanRow = reactive({ value: {} as any[] });
  // 오른쪽 체크박스 체크된 리스트
  const checkedArr = reactive({ value: {} as any[] });
  // rowspan 만들때 사용한 객체
  const gridRowSpanRule = reactive({ value: {} as { [key: string]: any } });

  /**
   * 왼쪽 헤더 체크박스 선택
   * @param allData       : 왼쪽 헤더 체크박스로 선택된 모든 행 데이터 리스트
   * @param allDataLength : 왼쪽 헤더 체크박스로 선택된 모든 행 데이터 리스트의 길이
   * @param uniqueKeyData : 왼쪽 체크박스 체크 유무에 따라 오른쪽 체크박스를 핸들링 하기 위해 기준이 되는 데이터를 담은 리스트
   */
  const allCheckHandler = (allData: any, allDataLength: number, uniqueKeyData: any) => {
    if (!Object.hasOwn(checkedArr.value, getCurrentPageId())) {
      checkedArr.value[getCurrentPageId()] = [];
    }
    if (!Object.hasOwn(checkedSpanRow.value, getCurrentPageId())) {
      checkedSpanRow.value[getCurrentPageId()] = [];
    }
    if (checkedArr.value[getCurrentPageId()].length === allDataLength) {
      checkedSpanRow.value[getCurrentPageId()] = [];
      checkedArr.value[getCurrentPageId()] = [];
    } else {
      checkedArr.value[getCurrentPageId()] = [...allData];
      checkedSpanRow.value[getCurrentPageId()] = uniqueKeyData;
    }
  };

  /**
   * 현재 선택한 페이지 id 정보 조회
   * @returns : 현재 선택한 페이지 id
   */
  const getCurrentPageId = (): any => {
    if (useTabsStore().selectedTab.value) {
      return useTabsStore().selectedTab.value?.id;
    }
    return "unknownPage";
  };

  /**
   * 오른쪽 체크박스 체크된 리스트 조회
   * @returns : 오른쪽 체크박스 체크된 리스트
   */
  function getCheckedArr() {
    return checkedArr.value[getCurrentPageId()] || [];
  }

  /**
   * 왼쪽 체크박스 체크된 리스트 조회
   * @returns : 왼쪽 체크박스 체크된 리스트
   */
  function getCheckedSpanRow() {
    return checkedSpanRow.value[getCurrentPageId()] || [];
  }

  /**
   * rowSpan 객체 조회
   *
   * @returns : rowSpan 객체 정보
   */
  function getGridRowSpanRule() {
    return gridRowSpanRule.value[getCurrentPageId()] || {};
  }

  /** 왼쪽 체크박스 클릭시 호출 */
  const selectRowSpanData = (rowSpanKey: string, selectData: any, rowData: any, startRowIndex: any) => {
    if (!Object.hasOwn(checkedArr.value, getCurrentPageId())) {
      checkedArr.value[getCurrentPageId()] = [];
    }
    if (!Object.hasOwn(checkedSpanRow.value, getCurrentPageId())) {
      checkedSpanRow.value[getCurrentPageId()] = [];
    }

    //내가 클릭한 rowdata의 id의 count
    const _gridRowSpanRule = gridRowSpanRule.value[getCurrentPageId()][`${selectData[rowSpanKey]}_${startRowIndex}`];

    // 내가 클릭한 spanrow영역의 데이터
    const filteredData = [];
    // 내가 클릭한 spanrow의 시작 index부터 클릭한sapn rowdata 의 count만큼 순회
    for (let i = _gridRowSpanRule.startIndex; i < _gridRowSpanRule.startIndex + _gridRowSpanRule.count; i++) {
      /* 
         내가 클릭한 spanrow영역의 모든 데이터를 filteredData에 push 
         rowData : 모든 rowdata
      */
      filteredData.push(rowData[i]);
    }
    // 왼쪽 체크박스 체크된 리스트에 포함 유무 확인
    const existingRowSpanIndex = checkedSpanRow.value[getCurrentPageId()].findIndex((row: any) => row[rowSpanKey] === selectData[rowSpanKey]);
    // 미포함
    if (existingRowSpanIndex === -1) {
      // 왼쪽 체크박스 체크리스트에 해당 row의 data를 push
      checkedSpanRow.value[getCurrentPageId()].push(selectData);
      // 오른쪽 체크박스에 같은 id값을 가진 데이터를 다 지우고
      checkedArr.value[getCurrentPageId()] = checkedArr.value[getCurrentPageId()].filter((row: any) => row[rowSpanKey] !== selectData[rowSpanKey]);
      // 다시 push
      checkedArr.value[getCurrentPageId()].push(...filteredData);
      // 포함
    } else {
      // 왼쪽 체크박스 리스트에서 제거
      checkedSpanRow.value[getCurrentPageId()] = checkedSpanRow.value[getCurrentPageId()].filter((row: any) => row[rowSpanKey] !== selectData[rowSpanKey]);
      // 오른쪽 체크박스 리스트에서 제거
      checkedArr.value[getCurrentPageId()] = checkedArr.value[getCurrentPageId()].filter((row: any) => row[rowSpanKey] !== selectData[rowSpanKey]);
    }
  };

  // 체크박스 (렌더러 x)
  const addCheckedArr = (rowSpanKey: string, uniqueKey: string, selectData: any, isSelected: boolean, startRowIndex: number) => {
    if (!Object.hasOwn(checkedArr.value, getCurrentPageId())) {
      checkedArr.value[getCurrentPageId()] = [];
    }
    if (!Object.hasOwn(checkedSpanRow.value, getCurrentPageId())) {
      checkedSpanRow.value[getCurrentPageId()] = [];
    }
    // 클릭시 checkedarr에 넣기
    if (isSelected) {
      if (checkedArr.value[getCurrentPageId()].findIndex((checkedItem: any) => checkedItem[uniqueKey] === selectData[uniqueKey]) === -1) {
        checkedArr.value[getCurrentPageId()].push(selectData);
        //checkedArr에서 내가 누른 rowdata.id와 같은 값들로 새 배열생성
        const filteredCheckedArr = checkedArr.value[getCurrentPageId()].filter((row: any) => row[rowSpanKey] === selectData[rowSpanKey]);
        // 내가 누른 rowdata의 개수와, 내가 누른 rowdata의 총 range가 같을때
        if (filteredCheckedArr.length === gridRowSpanRule.value[getCurrentPageId()][`${selectData[rowSpanKey]}_${startRowIndex}`].count) {
          checkedSpanRow.value[getCurrentPageId()].push(selectData);
        } else {
          checkedSpanRow.value[getCurrentPageId()] = checkedSpanRow.value[getCurrentPageId()].filter((row: any) => row[rowSpanKey] !== selectData[rowSpanKey]);
        }
      }
    } else {
      checkedArr.value[getCurrentPageId()] = checkedArr.value[getCurrentPageId()].filter((ckeckedItem: any) => {
        return ckeckedItem[uniqueKey] !== selectData[uniqueKey];
      });

      const filteredCheckedArr = checkedArr.value[getCurrentPageId()].filter((row: any) => row[rowSpanKey] === selectData[rowSpanKey]);
      // 필터링한 이후 range와 count가 다르면
      if (filteredCheckedArr.length !== gridRowSpanRule.value[getCurrentPageId()][`${selectData[rowSpanKey]}_${startRowIndex}`].count) {
        // 오른쪽 체크박스에서 내가 클릭한 체크박스와 같은 id를 가진 값을 제거
        checkedSpanRow.value[getCurrentPageId()] = checkedSpanRow.value[getCurrentPageId()].filter((row: any) => row[rowSpanKey] !== selectData[rowSpanKey]);
      }
    }
  };

  /**
   * rowSpan 객체 지정
   *  - 이전에 저장된 값과 새로 추가된 객체 정보를 merge
   *
   * @param rowSpanRuleObject : rowSpan생성을 위한 객체 정보
   */
  function setGridRowSpanRule(rowSpanRuleObject: any) {
    gridRowSpanRule.value[getCurrentPageId()] = { ...gridRowSpanRule.value[getCurrentPageId()], ...rowSpanRuleObject };
  }

  return {
    getCheckedArr,
    getCheckedSpanRow,
    getGridRowSpanRule,
    checkedSpanRow,
    checkedArr,
    setGridRowSpanRule,
    allCheckHandler,
    selectRowSpanData,
    addCheckedArr,
  };
});
