/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	confirm/index.ts
 *  Description:    사용자에개 정보를 확인 받거나 선택을 하게하는 기능들을 정의하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { defineStore } from "pinia";

export const useConfirmStore = defineStore("confirm", {
  state: () => ({
    // confirm 팝업(confirm.vue)에서 사용되는 메시지
    confirmMessage: null as string | null,
    // confirm 팝업(confirm.vue)에서 사용되는 버튼 이름(다국어 처리)
    confirmButtonLabel: null as string | null,
    // confirm 팝업(confirm.vue)에서 선택한 버튼 정보
    confirmDecision: null as boolean | null,
  }),
  actions: {
    /**
     * confirm 팝업 정보 정의
     * @param message             : confirm 팝업(confirm.vue)에서 사용되는 메시지
     * @param confirmButtonLabel  : confirm 팝업(confirm.vue)에서 사용되는 버튼 이름(다국어 처리)
     * @returns                   : confirm 팝업(confirm.vue)에서 선택한 버튼에 따른 결과값
     */
    confirm(message: string, confirmButtonLabel: string | null = null) {
      this.confirmDecision = null;
      this.confirmMessage = message;
      this.confirmButtonLabel = confirmButtonLabel;
      return new Promise((resolve, reject) => {
        const unsubscribeHandler = this.$subscribe(() => {
          unsubscribeHandler();
          const decision = this.confirmDecision;
          this.confirmDecision = null;
          this.confirmMessage = null;
          this.confirmButtonLabel = null;
          if (decision) {
            // confirm에서 확인(confirm)버튼을 클릭했을 때
            resolve(decision);
          } else {
            // confirm에서 취소(cancel) 버튼을 눌렀을 때 "Confirm canceled!!!" 오류 메시지 전달
            reject(new Error("Confirm canceled!!!"));
          }
        });
      });
    },
    /** confirm 팝업에서 선택한 버튼(decision)정보 설정
     * @param : confirm 팝업에서 선택한 버튼 정보
     *        - true  : 확인(confirm) 버튼
     *        - false : 취소(cancel) 버튼
     */
    setDecision(decision: any) {
      this.confirmDecision = decision;
    },
  },
});
