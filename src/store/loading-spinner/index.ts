/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	loading-spinner/index.ts
 *  Description:    로딩 스피너 정보를 보관하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { defineStore } from "pinia";
import { ref } from "vue";

export const useLoadingStore = defineStore("loading-state", () => {
  // 로딩 스피너 실행 횟수
  const spinners = ref(0 as number);
  // 로딩 스피너 실행 관리 변수(로딩 스피너 동작시킬 페이지 경로 저장)
  const spinnings = ref([] as any);

  /**
   * 로딩 스피너 실행
   * @param path : 현재 선택된 탭의 경로
   */
  function addSpinner(path: string | null = null) {
    if (path) {
      spinnings.value.push(path);
    } else {
      spinners.value++;
    }
  }

  /**
   * 로딩 스피너 제거
   * @param path : 현재 선택된 탭의 경로
   */
  function removeSpinner(path: string | null = null) {
    setTimeout(() => {
      if (path) {
        spinnings.value = spinnings.value.filter((spinning: string) => spinning != path);
      } else {
        spinners.value--;
      }
    }, 250);
  }

  /**
   * 로딩 스피너 실행 관리 변수에 path값 포함되어 있는지 확인
   * @param path  : 현재 선택된 탭의 경로
   * @returns     : 로딩 스피너 실행 관리 변수에 path값 포함되어 있을 때 'true' 아니면 'false'
   */
  function includes(path: string) {
    return spinnings.value.includes(path || "undefinedPath");
  }

  return { spinners, addSpinner, removeSpinner, includes };
});
