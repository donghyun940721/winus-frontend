/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	user/index.ts
 *  Description:    현재 사용자 정보를 보관하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { defineStore } from "pinia";
import { reactive } from "vue";

export const userStore = defineStore("user", () => {
  // 현재 로그인한 사용자 정보
  const currentUser = reactive({ value: null as any });
  // 서버에서 받아온 사용자 권한 정보
  const windowRoll = reactive({ value: null as any });
  // 로그인시 지정된 세션 만료 시간
  const expire = reactive({ value: null as any });
  // 기본 설정 언어
  const DEFAULT_LOCALE = "korean";

  /**
   * 현재 사용자 정보 지정
   *
   * @param dt  : 서버에서 받아온 사용자 상세정보
   */
  const setCurrentUser = (dt: any) => {
    currentUser.value = dt;
  };

  /**
   * 사용자 로케이션 정보 지정
   *
   * @param location  : 지정할 로케이션 정보
   */
  const setCurrentUserLocation = (location: any) => {
    const currentUserParse = JSON.parse(currentUser.value);
    currentUserParse.location = location;
    currentUserParse.LC_CUST_NM = location.LC_CUST_NM;
    currentUserParse.LC_ID = location.LC_ID;
    currentUserParse.LC_NM = location.LC_NM;
    currentUser.value = JSON.stringify(currentUserParse);
  };

  /**
   * 사용자 권한 정보 지정
   *
   * @param wr  : 서버에서 받아온 사용자 권한 정보
   */
  const setWindowRoll = (wr: any) => {
    windowRoll.value = wr;
  };

  /**
   * 사용자 세션 정보 지정
   *
   * @param expireTime  : 로그인시 지정할 세션 만료 시간
   */
  const setExpire = (expireTime: any) => {
    expire.value = expireTime;
  };

  /**
   * 현재 사용자 정보 조회
   *
   * @returns : 조회된 사용자 정보
   */
  const getCurrentUser = () => {
    if (!currentUser.value) {
      reload();
    }
    return currentUser.value;
  };

  /**
   * 사용자 권한 정보 조회
   *
   * @returns  : 조회된 사용자 권한 정보
   */
  const getWindowRoll = () => {
    return windowRoll.value;
  };

  /**
   * 사용자 세션 정보 조회
   *
   * @returns : 로그인 시 지정된 세션 만료 시간
   */
  const getExpire = () => {
    return expire.value;
  };

  /**
   * 로그인 시 사용자 정보 생성
   *
   * @param userDetail      : 서버에서 받아온 사용자 상세 정보
   * @param userWindowRoll  : 서버에서 받아온 사용자 권한 정보
   * @param expire          : 로그인 시 지정한 세션 유지 시간
   */
  const create = (userDetail: any, userWindowRoll: any, expire: any) => {
    const winusUserInfo = {
      userDetail: JSON.stringify(userDetail),
      userWindowRoll: JSON.stringify(userWindowRoll),
      expire,
    };
    localStorage.setItem("winusUserInfo", JSON.stringify(winusUserInfo));
    reload();
  };

  /**
   * 로컬스토리지에 사용자 상세 정보 저장
   *
   * @param userDetail  : 서버에서 받아온 사용자 상세 정보
   */
  const storeCurrentUser = (userDetail: any) => {
    const winusUserInfo = {
      userDetail: userDetail,
      userWindowRoll: windowRoll.value,
      expire: expire.value,
    };
    localStorage.setItem("winusUserInfo", JSON.stringify(winusUserInfo));
    reload();
  };

  /**
   * 사용자 정보 재조회
   */
  const reload = () => {
    const winusUserInfo = JSON.parse(localStorage.getItem("winusUserInfo")!);
    if (winusUserInfo) {
      setCurrentUser(winusUserInfo.userDetail);
      setWindowRoll(winusUserInfo.userWindowRoll);
      setExpire(winusUserInfo.expire);
    } else {
      setCurrentUser(null);
      setWindowRoll(null);
      setExpire(null);
    }
  };

  /**
   * 다국어 설정
   *
   * @returns  : 현재 선택된 다국어 정보
   */
  const getUserLocale = () => {
    if (!currentUser.value) {
      reload();
    }
    if (!currentUser.value) {
      return DEFAULT_LOCALE;
    }
    const user = JSON.parse(currentUser.value);
    if (user.locale) {
      return user.locale;
    }
    user.locale = DEFAULT_LOCALE;
    storeCurrentUser(JSON.stringify(user));
    return user.locale;
  };

  /**
   * 현재 사용자 로케이션 정보 조회
   * @returns  : 사용자 로케이션 정보
   */
  const getUserLocation = () => {
    if (!currentUser.value) {
      reload();
    }
    if (!currentUser.value) {
      return null;
    }
    const user = JSON.parse(currentUser.value);
    return user.location;
  };

  return {
    getCurrentUser,
    getWindowRoll,
    getExpire,
    setCurrentUser,
    setCurrentUserLocation,
    setWindowRoll,
    setExpire,
    create,
    reload,
    storeCurrentUser,
    getUserLocale,
    getUserLocation,
  };
});
