/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	menu/index.ts
 *  Description:    메뉴 정보를 보관하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { defineStore } from "pinia";
import { reactive } from "vue";

export const menusStore = defineStore("menus", () => {
  // 서버에서 받아온 메뉴 정보를 저장할 변수
  const menus = reactive({ value: null as any });

  /**
   * 서버에서 받아온 메뉴 정보 설정
   * @param mns   : 서버에서 받아온 메뉴 정보
   */
  const setMenus = (mns: any) => {
    menus.value = mns;
  };

  /**
   * 서버에서 받아온 메뉴 정보 조회
   * @returns    : 서버에서 받아온 메뉴 정보
   */
  const getMenus = () => {
    return menus.value;
  };

  return {
    setMenus,
    getMenus,
  };
});
