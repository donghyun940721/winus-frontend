/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	form-height/index.ts
 *  Description:    form 컴포넌트 높이 설정에 관한 기능들을 정의하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { defineStore } from "pinia";
import { reactive } from "vue";

import { useTabsStore } from "@/store";

export const useFormHeightStore = defineStore("form-height", () => {
  // search 컴포넌트 높이
  const _searchCompHeight = reactive({ value: {} as any });

  /**
   * 현재 선택한 페이지 정보 조회
   * @returns 현재 선택한 페이지의 id
   */
  const getCurrentPageId = (): any => {
    if (useTabsStore().selectedTab.value) {
      return useTabsStore().selectedTab.value?.id;
    }
    return "unknownPage";
  };

  /**
   * search 컴포넌트 높이 지정
   * @param value : search-area영역의 높이값
   */
  const setSearchCompHeight = (value: number) => {
    // 페이지의 header 태그 영역과 메뉴 탭 영역 높이의 합
    const layoutHeight = 114;
    // control 버튼 영역
    const controlHeight = 48;
    // form 컴포넌트 padding-bottom
    const formBottomPadding = 32;

    // zoom: 0.88 맞춤 height 조절.
    value += -128;

    const total = layoutHeight + controlHeight + formBottomPadding;
    // 페이지 컴포넌트에서 search 컴포넌트 높이와 form 컴포넌트 영역을 제외한 레이아웃 높이의 합을 현재 페이지 id를 key값으로 _searchCompHeight로 지정
    _searchCompHeight.value[getCurrentPageId()] = Number(total + value);
  };

  /**
   * form 컴포넌트 높이 지정
   * @returns 계산된 form 컴포넌트 높이 값
   */
  const getFormHeight = () => {
    // setSearchCompHeight를 통해 계산된 높이의 합
    _searchCompHeight.value[getCurrentPageId()] = _searchCompHeight.value[getCurrentPageId()] || 0;
    // 실행 중인 스크린 크기 기준으로 계산된 search 영역 높이값을 뺀 높이값을 form 컴포넌트의 높이로 지정
    const resultFormHeight = `calc(100vh - ${_searchCompHeight.value[getCurrentPageId()]}px)`;

    return resultFormHeight;
  };

  /**
   * '모든 탭 닫기' 버튼 클릭 시 해당 페이지의 높이 값을 초기화 한다
   * @param pageId : 현재 선택된 페이지의 id
   */
  const resetAllHeightData = (pageId: string | null = null) => {
    if (pageId == "all") {
      _searchCompHeight.value = {};
    } else {
      delete _searchCompHeight.value[pageId || getCurrentPageId()];
    }
  };

  return {
    _searchCompHeight,
    setSearchCompHeight,
    getFormHeight,
    resetAllHeightData,
  };
});
