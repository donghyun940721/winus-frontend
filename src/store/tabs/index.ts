/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	tabs/index.ts
 *  Description:    사용자 메뉴 댑에 관한 정보를 보관하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { defineStore } from "pinia";
import { reactive, ref } from "vue";
import { useRouter } from "vue-router";

import { TAB_ITEM_MARGIN } from "@/constants/tab-item-margin";
import { useFormHeightStore, useOptionDataStore, useSearchConditionStore, useSelectedDataStore } from "@/store";
import { ISelectedTab } from "@/types";

export const useTabsStore = defineStore("tabs", () => {
  const router = useRouter();

  // 추가된 tab list
  const newTabs = reactive({
    value: [] as ISelectedTab[] | undefined,
  });
  // 선택된 tab 정보
  const selectedTab = reactive({
    value: {} as ISelectedTab | undefined,
  });
  // 사용자의 tab list
  const tabList = reactive({
    value: [] as ISelectedTab[],
  });
  // 화면 상 숨겨진 tab list
  const hiddenTabList = reactive({
    value: [] as ISelectedTab[],
  });
  // 삭제된 tab의 id를 담은 list 정규식
  const removedTabIdsRegex = reactive({
    value: null as any,
  });
  // 삭제된 tab의 id를 담은 list
  const removedTabIds = reactive({
    value: [] as string[],
  });

  // 탭 전환 시 중지 시킬 watch의 집합
  const watchList = reactive({
    value: [] as Array<Function>
  });    

  const errorMessage = ref("");

  /**
   * 다른 페이지로 넘어갈시, grid row 더블클릭하여 다른 페이지로 넘어가는 로직인지, 일반적인 menu tab 클릭하여 넘어가는지 구분
   * forSelectBox: search-container-input의 select box 설정 동작 여부
   * forFormGrid: 각 페이지 form의 그리드 데이터 가져오기 동작 여부
   * */
  const _changedSelectedTabByNotClickedTabMenu = reactive({ forSelectBox: {} as boolean, forFormGrid: {} as boolean });
  _changedSelectedTabByNotClickedTabMenu.forSelectBox = false;
  _changedSelectedTabByNotClickedTabMenu.forFormGrid = false;

  /**
   * 에러 발생 시 보여질 메시지 지정
   * @param message : 에러 발생 시 보여질 메시지
   */
  const setErrorMessage = (message: string) => {
    errorMessage.value = message;
  };

  /**
   * 에러 발생 시 보여질 메시지 조회
   * @returns : 에러 발생 시 보여질 메시지
   */
  const getErrorMessage = () => {
    return errorMessage.value || "";
  };

  /**
   * 사용자가 삭제한 탭을 제거된 탭 id 목록에 추가
   *
   * @param id  : 삭제된 tab의 페이지 id
   * @returns
   */
  const addToRemovedTabIdsRegex = (id: string | null = null) => {
    if (id == "welcome") {
      return;
    }
    if (id == null) {
      for (const tab of hiddenTabList.value) {
        removedTabIds.value.push(tab.id);
      }
      for (const tab of tabList.value) {
        removedTabIds.value.push(tab.id);
      }
    } else {
      removedTabIds.value.push(id);
    }
    removedTabIdsRegex.value = new RegExp(removedTabIds.value.join("|"));
  };

  /**
   * 제거된 탭 id 목록에서 사용자가 사용한 탭을 제거
   *
   * @param id  : 현재 tab의 페이지 id
   * @returns
   */
  const removeFromRemovedTabIdsRegex = (id: string) => {
    if (id == "welcome") {
      return;
    }
    removedTabIds.value = removedTabIds.value.filter((tabId) => tabId != id);
    if (removedTabIds.value.length == 0) {
      removedTabIdsRegex.value = null;
    } else {
      removedTabIdsRegex.value = new RegExp(removedTabIds.value.join("|"));
    }
  };

  /**
   * 모든 탭 닫기 버튼 클릭 시 사용한 모든 탭 정보를 초기화
   * @param pageId : 탭 정보를 초기화 할 페이지 id
   */
  const resetAllTabData = (pageId: string = "all") => {
    useFormHeightStore().resetAllHeightData(pageId);
    useOptionDataStore().resetAllOptionData(pageId);
    useSearchConditionStore().resetAllSearchData(pageId);
    useSelectedDataStore().resetAllSelectedData(pageId);
  };

  /**
   * 해당 path가 tab 정보에 포함되어 있는지 여부 조회
   * @param path : 조회할 path 정보
   * @returns    : tab 정보에 해당 path 포함 여부
   */
  const isPathIncluded = (path: string) => {
    for (const tab of tabList.value) {
      if ("/" + tab.path === path) {
        return true;
      }
    }
    for (const tab of hiddenTabList.value) {
      if ("/" + tab.path === path) {
        return true;
      }
    }
    return false;
  };

  /**
   * 사용자가 사용한 tab 정보를 로컬스토리지에 저장
   * @param userId : 사용자 id
   */
  const saveLocalStorage = (userId: string) => {
    localStorage.setItem("userTabs" + userId, JSON.stringify(tabList.value));
    localStorage.setItem("userHiddenTabs" + userId, JSON.stringify(hiddenTabList.value));
  };

  /**
   * 현재 사용자가 접속한 탭 지정
   *
   * @param userId : 사용자 id
   * @param path   : 현재 사용자가 접속한 페이지의 경로
   */
  const loadTabs = (userId: string, path: string) => {
    tabList.value = JSON.parse(localStorage.getItem("userTabs" + userId)!) || [];
    hiddenTabList.value = JSON.parse(localStorage.getItem("userHiddenTabs" + userId)!) || [];
    selectedTab.value = tabList.value.find((tab) => "/" + tab.path === path);
    if (!selectedTab.value) {
      const hiddenTab = hiddenTabList.value.find((tab) => "/" + tab.path === path);
      if (hiddenTab) {
        addTab(hiddenTab, userId);
      }
    }
  };

  /**
   * 탭 선택
   *
   * @param tab                        : 현재 선택한 tab
   * @param changeOnlySelectedTabValue : 다중 탭 팝업을 위한 파라미터
   */
  const selectTab = (tab: ISelectedTab, changeOnlySelectedTabValue?: boolean) => {
    stopWatch(); // 등록된 watch 제거

    selectedTab.value = tab;
    // changeOnlySelectedTabValue 스토어 사용을 위해 다중 탭 팝업을 위한 파라미터, 라우팅은 쿼리스트링을 사용하기때문에 multi-tab-modal-layout.vue에서 진행
    if (!changeOnlySelectedTabValue) {
      router.push("/" + tab.path);
    }
  };

  /**
   *  탭 너비 조정
   *
   * @param userId : 사용자 id
   */
  const resizeTabs = async (userId: string) => {
    const $tabList = document.querySelector("#menu-tab-page .tab-list") as HTMLElement;
    const $tabItems = document.querySelectorAll("#menu-tab-page .tab-list > li:not(.hidden-for-sizing)") as NodeListOf<HTMLElement>;
    const $buttonItems = document.querySelectorAll("#menu-tab-page .tab-list > button") as NodeListOf<HTMLElement>;
    if (!$tabList) {
      return;
    }
    const tabListWidth = $tabList.offsetWidth;
    let actualTabItemsWidth = 40; // default margin
    for (const tabItem of $tabItems) {
      actualTabItemsWidth += tabItem.offsetWidth + TAB_ITEM_MARGIN;
    }
    for (const buttonItem of $buttonItems) {
      actualTabItemsWidth += buttonItem.offsetWidth + TAB_ITEM_MARGIN;
    }
    if (actualTabItemsWidth > tabListWidth) {
      if ($buttonItems.length === 1) {
        actualTabItemsWidth += actualTabItemsWidth;
      }
      hiddenTabList.value = hiddenTabList.value || [];
      for (let i = 0; i < tabList.value.length; i++) {
        if (selectedTab.value && selectedTab.value.id == tabList.value[i].id) {
          continue;
        }
        hiddenTabList.value.push(tabList.value.splice(i, 1)[0]);
        actualTabItemsWidth -= $tabItems[i].offsetWidth + TAB_ITEM_MARGIN;
        if (actualTabItemsWidth <= tabListWidth) {
          break;
        }
      }
      saveLocalStorage(userId);
    } else if (hiddenTabList.value && hiddenTabList.value.length > 0) {
      newTabs.value = hiddenTabList.value;
      setTimeout(() => {
        const $newTabItems = document.querySelectorAll("#menu-tab-page .tab-list > li.hidden-for-sizing") as NodeListOf<HTMLElement>;
        for (let i = 0; i < hiddenTabList.value.length; i++) {
          actualTabItemsWidth += $newTabItems[i].offsetWidth + TAB_ITEM_MARGIN;
          if (actualTabItemsWidth > tabListWidth) {
            break;
          }
          tabList.value.push(hiddenTabList.value.splice(i, 1)[0]);
        }
        newTabs.value = [];
        saveLocalStorage(userId);
      });
    }
  };

  /**
   * 탭 추가
   *
   * @param menu   : 추가된 탭 정보
   * @param userId : 사용자 id
   */
  const addTab = (menu: ISelectedTab, userId: string) => {
    selectedTab.value = menu;
    removeFromRemovedTabIdsRegex(menu.id);
    if (tabList.value.find((tab) => tab.id === menu.id)) {
      return;
    }
    const existingIndex = hiddenTabList.value.findIndex((hMenu) => hMenu.id === menu.id);
    if (existingIndex > -1) {
      hiddenTabList.value.splice(existingIndex, 1);
    }
    if (tabList.value.length == 0) {
      tabList.value.push(menu);
      saveLocalStorage(userId);
      return;
    }
    newTabs.value = [menu];
    setTimeout(() => {
      const $tabList = document.querySelector("#menu-tab-page .tab-list") as HTMLUListElement;
      const $tabItems = document.querySelectorAll("#menu-tab-page .tab-list > li") as NodeListOf<HTMLElement>;
      const $buttonItems = document.querySelectorAll("#menu-tab-page .tab-list > button") as NodeListOf<HTMLElement>;
      if (!$tabList) {
        return;
      }
      const tabListWidth = $tabList.offsetWidth;
      let actualTabItemsWidth = 0;
      for (const tabItem of $tabItems) {
        actualTabItemsWidth += tabItem.offsetWidth + TAB_ITEM_MARGIN;
      }
      for (const buttonItem of $buttonItems) {
        actualTabItemsWidth += buttonItem.offsetWidth + TAB_ITEM_MARGIN;
      }
      for (let i = $tabItems.length - 2; i > 0; i--) {
        if (actualTabItemsWidth <= tabListWidth) {
          break;
        }
        hiddenTabList.value.push(tabList.value.splice(i, 1)[0]);
        actualTabItemsWidth -= $tabItems[i].offsetWidth + TAB_ITEM_MARGIN;
      }
      tabList.value.push(menu);
      saveLocalStorage(userId);
      newTabs.value = [];
    });
  };

  /**
   * 탭 제거
   *
   * @param menu   : 제거할 탭 정보
   * @param userId : 사용자 id
   */
  const deleteTab = (menu: ISelectedTab, userId: string) => {
    if (tabList.value.length == 1 && menu.path === "welcome") {
      return;
    }
    addToRemovedTabIdsRegex(menu.id);

    resetAllTabData(menu.id);
    const deleteTabIndex = tabList.value.findIndex((tab) => tab.id === menu.id);
    tabList.value = tabList.value.filter((tab) => tab.id !== menu.id);

    if (tabList.value.length === 0) {
      router.push("/welcome");
      localStorage.setItem("userTabs" + userId, JSON.stringify([]));
      return;
    }

    if (menu.id === selectedTab.value?.id) {
      if (tabList.value.length === deleteTabIndex) {
        selectedTab.value = tabList.value[tabList.value.length - 1];
      } else {
        selectedTab.value = tabList.value[deleteTabIndex];
      }
      router.push("/" + selectedTab.value?.path);
    }

    if (hiddenTabList.value.length > 0) {
      tabList.value.push(...hiddenTabList.value.splice(0, 1));
    }
    saveLocalStorage(userId);
  };

  /**
   * 탭 놓기(드래그 놓았을 때)
   *
   * @param index    : 드래그 이벤트로 변경될 tab의 index
   * @param tabIndex : 드래그 이벤트 전 tab의 index
   * @param userId   : 사용자 id
   */
  const dropTab = (index: number, tabIndex: number, userId: string) => {
    const newTabMenus = tabList.value.splice(tabIndex, 1)[0];
    const droppedTabs = tabList.value.splice(index, 0, newTabMenus);
    if (droppedTabs.length == 1) {
      addToRemovedTabIdsRegex(droppedTabs[0].id);
      resetAllTabData(droppedTabs[0].id);
    }
    saveLocalStorage(userId);
  };

  /**
   * 화면 상에서 숨겨진 탭 제거
   * (menu-tab.vue에서 더보기(more-btn)버튼으로 보여지는 숨겨진 탭 리스트에서 리스트를 지울 때)
   *
   * @param menuId : 제거할 숨겨진 탭 정보
   * @param userId : 사용자 id
   */
  const deleteHiddenTab = (menuId: string, userId: string) => {
    const menu = tabList.value.find((tab) => "/" + tab.id === menuId) as ISelectedTab;
    hiddenTabList.value = hiddenTabList.value.filter((tab) => tab.id !== menuId);
    addToRemovedTabIdsRegex(menu.id);
    resetAllTabData(menu.id);
    saveLocalStorage(userId);
  };

  /**
   * 모든 탭 닫기 버튼 클릭 시 사용한 모든 탭을 삭제
   *
   * @param userId : 사용자 id
   */
  const deleteAllTabs = (userId: string) => {
    addToRemovedTabIdsRegex();
    hiddenTabList.value = [];
    tabList.value = [];
    resetAllTabData();
    saveLocalStorage(userId);
    router.push("/welcome");
  };

  /**
   * 사용자가 탭을 클릭하는 것이 아닌 다른 방법으로 탭 간 이동을 할 때
   *
   * @param type : 업데이트 할 대상 타입
   * @param val  : 해당 타입의 상태
   */
  const setChangedSelectedTabByNotClickedTabMenu = (type: string, val: boolean) => {
    if (type === "select-box") {
      _changedSelectedTabByNotClickedTabMenu.forSelectBox = val;
    } else if (type === "form-grid") {
      _changedSelectedTabByNotClickedTabMenu.forFormGrid = val;
    }
  };

  /**
   * 등록된 watch 전체를 비활성화
   */
  const stopWatch = () => {
    console.log("Stop Watch ... " + watchList.value.length);

    // 이전 화면의 watch 사용 중지
    if(watchList.value.length > 0) {
      for(const fnc of watchList.value){
        fnc();
      }
    }

    watchList.value = new Array<Function>();
  }

  return {
    watchList,
    newTabs,
    selectedTab,
    tabList,
    hiddenTabList,
    removedTabIdsRegex,
    isPathIncluded,
    loadTabs,
    selectTab,
    resizeTabs,
    addTab,
    deleteTab,
    dropTab,
    deleteHiddenTab,
    deleteAllTabs,
    setChangedSelectedTabByNotClickedTabMenu,
    _changedSelectedTabByNotClickedTabMenu,
    getErrorMessage,
    setErrorMessage,
    stopWatch,
  };
});
