/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	menu/index.ts
 *  Description:    메뉴의 사용자 설정 값을 보관하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { ISearchInput } from "@/types";
import { defineStore } from "pinia";
import { useTabsStore } from "../tabs";

type SearchInputData = Map<string, ISearchInput[]>;

export const useUserSettingStore = defineStore("userSetting", {
  
  state: () => {
    return {
      searchInput: new Map as SearchInputData,
      originSearchInput: new Map as SearchInputData,
    }
  },

  actions: {
    registUserSearchInput (searchInput: ISearchInput[]) {
      const getCurrentPageId = (): any => {
        if (useTabsStore().selectedTab.value) {
          return useTabsStore().selectedTab.value?.id;
        }
        return "unknownPage";
      };
      
      this.searchInput.set(getCurrentPageId(), searchInput);
    },

    registOriginSearchInput (searchInput: ISearchInput[]) {
      const getCurrentPageId = (): any => {
        if (useTabsStore().selectedTab.value) {
          return useTabsStore().selectedTab.value?.id;
        }
        return "unknownPage";
      };
      
      this.originSearchInput.set(getCurrentPageId(), searchInput);
    }

  },

  getters: {
    getUserSearchInput: (state) => {
      const getCurrentPageId = (): any => {
        if (useTabsStore().selectedTab.value) {
          return useTabsStore().selectedTab.value?.id;
        }
        return "unknownPage";
      };

      return state.searchInput.get(getCurrentPageId());
    },

    getOriginSearchInput: (state) => {
      const getCurrentPageId = (): any => {
        if (useTabsStore().selectedTab.value) {
          return useTabsStore().selectedTab.value?.id;
        }
        return "unknownPage";
      };

      return state.originSearchInput.get(getCurrentPageId());
    },
  }
});