/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	search-condition-data/index.ts
 *  Description:    Search Area에서 사용되는 입력 값 관리를 위한 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/
import { defineStore } from "pinia";
import { reactive } from "vue";

import { useTabsStore } from "@/store";

export const useSearchConditionStore = defineStore("search-condition-data", () => {
  // 검색조건에 입력된 값
  const _searchInputValue = reactive({ value: {} as any });
  // 모달 창의 검색조건에 입력된 값 (모달 닫힐 때 해제)
  const _modalSearchInputValue = reactive({ value: {} as any });
  // 검색조건초기화 시 초기값
  const _initialSearchInputValue = reactive({ value: {} as any });
  // 상단 검색영역의 검색 버튼을 통해 하단 grid 데이터 조회 가능 여부 설정 값
  const _availableSearchBySearchCondition = reactive({ value: {} as { [key: string]: boolean } });
  // 검색조건초기화 시 select-box input autoSelected 여부
  const _availableInitializeBySearchInputValue = reactive({ value: {} as { [key: string]: boolean } });
  // search 영역 검색 버튼 아래에있는 초기화 버튼시 사용. 초기화인지 검색인지 구별하기 위한 용도
  const _reset = reactive({ value: {} as { [key: string]: boolean } });
  // _reset 와 같은 용도 + select box option 선택에 따라 다른 select box option 값이 달라지는 페이지에서 추가로 사용
  const _reset2 = reactive({ value: {} as { [key: string]: boolean } });

  /**
   * 현재 선택한 페이지 정보 조회
   * @returns : 현재 선택한 페이지의 id
   */
  const getCurrentPageId = (): any => {
    if (useTabsStore().selectedTab.value) {
      return useTabsStore().selectedTab.value?.id;
    }
    return "unknownPage";
  };

  /**
   * search input의 값 조회
   * @param paramPageId : 지정한 페이지 id
   * @returns           : 현재 페이지에서 사용되는 search area의 input들의 값
   */
  const getSearchInputValue = (paramPageId?: string): any => {
    let pageId;
    if (paramPageId) {
      pageId = paramPageId;
    } else {
      pageId = getCurrentPageId();
    }
    return _searchInputValue.value[pageId] || {};
  };

  /**
   * search input의 값 변경
   * @param key         : 값을 변경할 input의 key값
   * @param value       : 변경할 값
   * @param paramPageId : 값을 변경 할 페이지 id
   */
  const changeSearchInputValue = (key: string, value: any, paramPageId?: string) => {
    let pageId;
    if (paramPageId) {
      pageId = paramPageId;
    } else {
      pageId = getCurrentPageId();
    }
    _searchInputValue.value[pageId] = _searchInputValue.value[pageId] || {};
    _searchInputValue.value[pageId][key] = value;
    if (value === "option value empty") {
      _searchInputValue.value[pageId][key] = "";
    }
  };

  /**
   * search input 값 삭제
   * @param key : 삭제할 search input의 key값
   */
  const removeSearchInputValue = (key: string) => {
    changeSearchInputValue(key, undefined);
  };

  /**
   * search input 값 초기화
   * @param data : search input 초기화 시 셋팅될 초기값
   */
  function resetSearchInputValue(data: any) {
    _searchInputValue.value[getCurrentPageId()] = data;
  }

  /**
   * search input 중 모달을 사용하는 input의 값 조회
   * @returns : 현재 페이지에서 사용되는 search input중 모달을 사용하는 input들의 값
   */
  const getModalSearchInputValue = (): any => {
    return _modalSearchInputValue.value[getCurrentPageId()] || {};
  };

  /**
   * search input 중 모달을 사용하는 input의 값 변경
   * @param key   : 값을 변경할 모달 input의 key값
   * @param value : 변경할 값
   */
  const changeModalInputValue = (key: string, value: string) => {
    _modalSearchInputValue.value[getCurrentPageId()] = _modalSearchInputValue.value[getCurrentPageId()] || {};
    if (value) {
      _modalSearchInputValue.value[getCurrentPageId()][key] = value;
    } else {
      _modalSearchInputValue.value[getCurrentPageId()][key] = "";
    }
  };

  /**
   * search input 중 모달을 사용하는 input의 값 초기화
   */
  const resetModalInputValue = () => {
    delete _modalSearchInputValue.value[getCurrentPageId()];
  };

  /**
   * search input에서 사용되는 모든 값 초기화
   * @param pageId : 지정한 페이지 id
   */
  const resetAllSearchData = (pageId: string | null = null) => {
    if (pageId == "all") {
      _searchInputValue.value = {};
      _modalSearchInputValue.value = {};
    } else {
      delete _searchInputValue.value[pageId || getCurrentPageId()];
      delete _modalSearchInputValue.value[pageId || getCurrentPageId()];
    }
    _reset.value[pageId || getCurrentPageId()] = true;
    _reset2.value[pageId || getCurrentPageId()] = true;
  };

  /**
   * search 영역 초기화 버튼 클릭 시 초기화 상태 지정
   * @param reset : 지정할 초기화 상태
   *              - true  : 초기화
   *              - false : 초기화 종료
   */
  const setResetValue = (reset: boolean): any => {
    _reset.value[getCurrentPageId()] = reset;
  };

  /**
   * search 영역 초기화 버튼의 초기화 상태 조회
   * @returns : 초기화 상태
   */
  const getResetValue = (): any => {
    return _reset.value[getCurrentPageId()];
  };

  /**
   * select box option 선택에 따라 다른 select box option 값이 달라지는 페이지에서 초기화 시 초기화 상태 지정
   * @param reset : 지정할 초기화 상태
   *              - true  : 초기화
   *              - false : 초기화 종료
   */
  const setResetValue2 = (reset: boolean): any => {
    _reset2.value[getCurrentPageId()] = reset;
  };

  /**
   * select box option 선택에 따라 다른 select box option 값이 달라지는 페이지에서 초기화 시 초기화 상태 조회
   * @returns : 초기화 상태
   */
  const getResetValue2 = (): any => {
    return _reset2.value[getCurrentPageId()];
  };

  /**
   * 검색조건초기화 버튼 클릭 시 지정될 초기값 조회
   * @returns : 검색조건초기화 버튼 클릭 시 지정될 초기값
   */
  const getInitialSearchInputValue = (): any => {
    return _initialSearchInputValue.value[getCurrentPageId()] || {};
  };

  /**
   * 검색조건초기화 버튼 클릭 시 지정될 초기값 지정
   * @param initialInputValue : 검색조건초기화 버튼 클릭 시 지정할 초기값
   */
  const setInitialSearchInputValue = (initialInputValue: any) => {
    _initialSearchInputValue.value[getCurrentPageId()] = initialInputValue;
  };

  /**
   * 검색조건초기화 시 select-box autoSelected 여부 지정
   * @param available : autoSelected 여부
   *                   - true  : autoSelected 지정
   *                   - false : autoSelected 미지정
   */
  const setAvailableInitializeBySearchInputValue = (available: boolean) => {
    _availableInitializeBySearchInputValue.value[getCurrentPageId()] = available;
  };

  /**
   * 검색조건초기화 시 select-box autoSelected 여부 조회
   * @returns : 검색조건초기화 시 select-box autoSelected 여부
   */
  const getAvailableInitializeBySearchInputValue = (): boolean => {
    return _availableInitializeBySearchInputValue.value[getCurrentPageId()];
  };

  /**
   * 그리드 조회 여부 지정
   * @param available : 지정할 그리드 조회 여부
   *                  - true  : 그리드 조회
   *                  - false : 그리드 조회 종료
   */
  const setAvailableSearchBySearchCondition = (available: boolean) => {
    _availableSearchBySearchCondition.value[getCurrentPageId()] = available;
  };

  /**
   * 그리드 조회 여부 조회
   * @returns : 그리드 조회 여부
   */
  const getAvailableSearchBySearchCondition = (): boolean => {
    return _availableSearchBySearchCondition.value[getCurrentPageId()];
  };

  return {
    _searchInputValue,
    _modalSearchInputValue,
    _initialSearchInputValue,
    _availableSearchBySearchCondition,
    _availableInitializeBySearchInputValue,
    _reset,
    _reset2,
    getSearchInputValue,
    getModalSearchInputValue,
    getInitialSearchInputValue,
    changeSearchInputValue,
    removeSearchInputValue,
    resetSearchInputValue,
    changeModalInputValue,
    resetModalInputValue,
    resetAllSearchData,
    setResetValue,
    getResetValue,
    setResetValue2,
    getResetValue2,
    setInitialSearchInputValue,
    setAvailableSearchBySearchCondition,
    getAvailableSearchBySearchCondition,
    setAvailableInitializeBySearchInputValue,
    getAvailableInitializeBySearchInputValue,
  };
});
