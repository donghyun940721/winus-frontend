/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	EventBus.ts
 *  Description:    Application내 전역 이벤트를 처리하기 위한 EventBus
 *  사용법       :
  ```ts
    const eventBus = useEventBusStore();
    const message = ref<string>('');

    const emitEvent = () => {
      eventBus.emit('my-event', { message: 'Hello from Emitter!' });
    };

    const handleEvent = (payload?: { message: string }) => {
      if (payload) {
        message.value = payload.message;
      }
    };

    const handlestopDefaultEvent = (payload?: { message: string }) => {
      if (payload) {
        message.value = "stopDefault: " + payload.message;
      }
    };

    onMounted(() => {
      // 일반 우선순위 이벤트 리스너 등록
      eventBus.on('my-event', handleEvent);
      // 기존에 등록된 다른 이벤트 리스너가 실행되지 않도록 처리
      eventBus.on('my-event', handlestopDefaultEvent, true);
    });

    onUnmounted(() => {
      eventBus.off('my-event', handleEvent);
      eventBus.off('my-event', handlestopDefaultEvent);
    });
  ```
 *  Authors:        lyonghwan
 *  Update History:
 *                  2023.08. : Created by lyonghwan
 *
------------------------------------------------------------------------------*/

import { useTabsStore } from "@/store";
import { defineStore } from "pinia";
import { ref } from "vue";

type Callback = (payload?: any) => void;

interface EventItem {
  callback: Callback;
  stopDefault: boolean; //다른 컴포넌트에서 정의된 로직을 실행하지 않는다.
}

type Events = Record<string, EventItem[]>;

export const useEventBusStore = defineStore("eventBus", () => {
  const events = ref<Events>({});

  /**
   * Pinia Event를 발생 시키는 기능
   * @param eventName event 이름, 이 이벤트 이름은 Listen하는 곳에서 동일하게 사용된다. 화면별 이벤트는 기본적으로 화면 ID를 Prefix로 사용한다.
   *                  예) eventName='clicked' pageId='WMSOP910_4'인 경우 자동으로 EventName은 WMSOP910_4-clicked로 변경된다.
   * @param payload Event에서 전달하고자 하는 데이터를 제공하고 형식은 any이다
   */
  const emit = (eventName: string, payload?: any) => {
    const pageId = useTabsStore().selectedTab.value?.id;
    if (pageId) {
      eventName = pageId + "-" + eventName;
    }
    if (events.value[eventName]) {
      // stopDefault가 true인 이벤트를 우선 실행
      const sortedEvents = events.value[eventName].sort((_a, b) => {
        return b.stopDefault ? -1 : 1;
      });
      for (let i = 0; i < sortedEvents.length; i++) {
        const eventItem = sortedEvents[i];
        eventItem.callback(payload);
        // 실행한 Event가 stopDefault가 true인 이벤트를 우선 실행하고 이렇게 정의된 이벤트가 있을 경우 그 후의 콜백은 처리하지 않음
        if (eventItem.stopDefault) {
          break;
        }
      }
    }
  };

  /**
   * Pinia Event Listner
   * @param eventName event 이름, 이 이벤트 이름은 발생시키는 곳과 동일하게 사용되어야 한다. 화면별 이벤트는 기본적으로 화면 ID를 Prefix로 사용한다.
   *                  예) eventName='clicked' pageId='WMSOP910_4'인 경우 자동으로 EventName은 WMSOP910_4-clicked로 변경된다.
   * @param callback event의 데이터를 받아서 처리하는 로직, 형식은 (payload?: any) => void;
   * @param stopDefault 기타 컴포넌트에서 정의된 로직을 실행할 것인지를 나타내는 boolean 값, 기본값은 false이다.
   */
  const on = (eventName: string, callback: Callback, stopDefault: boolean = false) => {
    const pageId = useTabsStore().selectedTab.value?.id;
    if (pageId) {
      eventName = pageId + "-" + eventName;
    }
    if (!events.value[eventName]) {
      events.value[eventName] = [];
    }
    events.value[eventName].push({ callback, stopDefault });
  };

  /**
   * Pinia Store의 listner를 해지시키는 로직으로서 컴포넌트의 로직에서 이벤트가 사용됬을 경우 unmount될때 필수로 호출되어야 한다.
   * @param eventName event 이름, 이 이벤트 이름은 Listen하는 곳에서 동일하게 사용된다. 화면별 이벤트는 기본적으로 화면 ID를 Prefix로 사용한다.
   *                  예) eventName='clicked' pageId='WMSOP910_4'인 경우 자동으로 EventName은 WMSOP910_4-clicked로 변경된다.
   * @param callback event의 데이터를 받아서 처리하는 로직, 형식은 (payload?: any) => void;
   */
  const off = (eventName: string, callback: Callback) => {
    const pageId = useTabsStore().selectedTab.value?.id;
    if (pageId) {
      eventName = pageId + "-" + eventName;
    }
    if (!events.value[eventName]) return;

    events.value[eventName] = events.value[eventName].filter((eventItem) => eventItem.callback !== callback);
  };

  return {
    emit,
    on,
    off,
  };
});
