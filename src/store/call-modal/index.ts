/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	call-modal/index.ts
 *  Description:    특정 모달을 호출할 때 호출을 위한 정보를 정의하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { defineStore } from "pinia";
import { reactive } from "vue";

export const useCallModalIdStore = defineStore("call-modal", () => {
  // 호출 할 모달 정보 key값
  const callModalId = reactive({ value: { key: "" } });
  // grid-search-input에서 호출 할 모달 정보 key값
  const cellRendererCallModalId = reactive({ value: { key: "", rowIndex: null as null | number } });

  /**
   * 파라미터로 넘어온 모달 정보를 구별하는 key값을 가지고 해당 모달을 호출
   * @param value : 모달 정보 key값
   */
  function setCallModalId(value: string) {
    callModalId.value.key = "";
    callModalId.value.key = value;
  }

  /**
   * grid-text-input 컴포넌트에서 focus out검색 시 결과값이 1개 이상인 경우 파라미터로 넘어온 key값과 일치하는 모달 호출을 위한 정보 변경
   * @param value - key       : 모달 정보 key값
   *              - rowIndex  : grid-text-input이 위치한 rowIndex(grid-search-button과 상호작용 하기 위해 사용)
   *                            cellRendererCallModalId값을 grid-search-button에서 watch로 변경을 감지하면 grid-text-input의 rowIndex와 key값이 grid-search-button의 rowIndex와 key값과
   *                            일치할 때 모달 호출
   */
  function setCellRendererCallModalId(value: { key: string; rowIndex: null | number }) {
    cellRendererCallModalId.value.key = "";
    cellRendererCallModalId.value.rowIndex = null;

    cellRendererCallModalId.value.key = value.key;
    cellRendererCallModalId.value.rowIndex = value.rowIndex;
  }

  return { callModalId, cellRendererCallModalId, setCallModalId, setCellRendererCallModalId };
});
