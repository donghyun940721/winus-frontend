/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	constants/proxy.ts
 *  Description:    BackEnd Service가 로컬 실행환경이 아닌 경우 적용 (vite.config.ts)
 *                  신규 컨트롤러 추가 시, 경로 추가 필요.
 *                  (localhost, 127.0.0.1) 
 *  Authors:        dhkim
 *  Update History:
 *                  2024.04 : Created by dhkim
 *
-------------------------------------------------------------------------------*/

import { ProxyOptions } from "vite";

interface ProxyDefinition {
  [key: string]: ProxyOptions;
}

/**
 * Proxy 설정 정보 반환
 *
 * @param env: 목적지 URL
 * @returns
 */
export function GetProxDef(env: ImportMetaEnv): ProxyDefinition {
  const backendUrl = env.VITE_TARGET_URL;
  const prefix = env.VITE_URL_PREFIX;
  const proxy: ProxyDefinition = {};
  proxy[prefix] = {
    target: backendUrl,
    changeOrigin: true,
    secure: false,
    ws: true,
    rewrite: (path) => path.replace(/^\/api/, ""),
  };
  return proxy;
}
