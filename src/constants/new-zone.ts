import RemoveBtnVue from "@/components/renderer/RemoveBtn.vue";
import NewZoneSearchVue from "@/components/renderer/new-zone-search.vue";

export const LOCATION_MODAL_COLUMN_DEFS = [
  {
    field: "",
    headerName: "",
    width: 80,
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 100,
  },
  {
    field: "LOC_CD",
    headerName: "로케이션",
    width: 600,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
  },
];

export const NEW_ZONE_COLUMN_DEFS = [
  {
    field: "zone_name",
    headerKey: 'zone-name',
    width: 860,
    headerClass: "header-center",
    sortable: true,
    editable: true,
  },
];

export const LOCATION_INFO_COLUMN_DEFS = [
  {
    field: "",
    headerName: "",
    width: 100,
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerName: "",
    headerCheckboxSelection: true,
    checkboxSelection: true,
    width: 100,
  },
  {
    field: "LOC_ID",
    hide: true
  },
  {
    field: "LOC_CD",
    headerKey: 'location',
    width: 350,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    editable: true
  },
  {
    headerName: "",
    width: 100,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    cellRenderer: NewZoneSearchVue,
  },
  {
    field: "",
    headerKey: "work-order",
    width: 100,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerKey: "delete",
    width: 100,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    cellRenderer: RemoveBtnVue,
  },
];
