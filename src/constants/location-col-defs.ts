export const LOCATION_MODAL_COLUMN_DEFS = [
  {
    field: "",
    headerName: "",
    width: 80,
    valueGetter: (params: any) => params.node.rowIndex + 1, // 인덱스는 0이 아닌 1부터 시작
  },
  {
    field: "",
    headerName: "",
    checkboxSelection: true,
    width: 100,
  },
  {
    field: "LOC_CD",
    headerName: "로케이션",
    width: 600,
    cellStyle: { textAlign: "center" },
    headerClass: "header-center",
    sortable: true,
  },
];
