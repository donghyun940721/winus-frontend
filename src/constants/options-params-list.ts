// params를 요구하는 api에는 배열로 값들을 넣어주고, 특정 파라미터를 받지 않는 api에는 빈 객체를 할당해준다
export const OPTION_PARAMS_LIST: any = {
  WMSOP999T6_rn: [
    {
      id: "vrOrdSubType",
      group: "WORK01",
    },
    {
      id: "vrPayYn",
      group: "COM09",
    },
    {
      id: "vrKinOutYn",
      group: "COM09",
    },
  ],
  WMSOP999T2_rn: [
    {
      id: "vrOrdSubType",
      group: "WORK01",
    },
    {
      id: "vrPayYn",
      group: "COM09",
    },
    {
      id: "vrKinOutYn",
      group: "COM09",
    },
    {
      id: "CNTR_TYPE",
      group: "CAR05",
    },
  ],
  TMSYS020_rn: [
    {
      id: "BA_CODE_CD",
      group: "PGB",
    },
    {
      id: "VIEW_MENU_CD",
      group: "PGB",
    },
  ],
  WMSTG020: [],
  WMSTG040: [
    {
      id: "vrSrchItemType",
      group: "POOL_YN",
    },
  ],
  WMSTG070_rn: [
    {
      id: "vrSrchItemType",
      group: "ITEM_TY",
    },
    {
      id: "vrSrchLocType",
      group: "LOC01",
    },
  ],
  WMSTG090: [
    {
      id: "vrSrchItemType",
      group: "ITEM_TY",
    },
  ],
  WMSTG200: [
    {
      id: "WORK_STAT",
      group: "EPC_STAT",
    },
  ],
  WMSTG700: [
    {
      id: "vrSrchItemOrdGb",
      group: "ORD01",
    },
    {
      id: "vrSrchOrdSubtype",
      group: "WORK01",
    },
  ],
  TMSYS040_rn: [
    {
      id: "vrTopMenuCd",
      group: "PGB",
    },
  ],
  TMSYS010_rn: [
    {
      id: "USER_GB",
      group: "USER",
    },
  ],
  TMSYS010: [
    {
      id: "USER_GB",
      group: "USER",
    },
  ],
  TMSYS030_rn: [
    {
      id: "PROC_AUTH",
      group: "PROC_AUTH",
    },
    {
      id: "ITEM_FIX_YN",
      group: "USEYN",
    },
    {
      id: "MULTI_USE_GB",
      group: "MUG",
    },
    {
      id: "LANG",
      group: "LANG",
    },
    {
      id: "LC_CHANGE_YN",
      group: "LC_CHN_YN",
    },
    {
      id: "PROC_AUTH",
      group: "PROC_AUTH",
    },
  ],
  WMSST076_rn: [
    {
      id: "vrSrchItemType",
      group: "POOL_YN",
    },
    {
      id: "vrLotAttribute",
      group: "LOT02",
    },
    {
      id: "S_CUST_TYPE",
      group: "ORD08",
    },
  ],
  WMSST160: [
    {
      id: "SBCYCLSTOCKID",
      group: "COM09",
    },
  ],
  WMSST120: [
    //TODO: 문제 : Velocity로직에는 ROTSTATE로 공통코드 확인 중이나 , 실제 디비 확인해 본 결과 ROTSTATE없음. 우선은 CYCL02로 적용
    {
      id: "ROTSTATE",
      group: "CYCL02",
    },
    {
      id: "CYCL_STOCK_TYPE",
      group: "CYCL01",
    },
    {
      id: "TYPE_ST",
      group: "TYPEST",
    },
    {
      id: "WORK_STAT",
      group: "CYCL02",
    },
  ],
  WMSMS030: {},
  WMSMS040: {},
  WMSST020_rn: [
    {
      id: "vrSrchPoolYn",
      group: "ITM15",
    },
    {
      id: "vrSrchSetItemYn",
      group: "SETITEM_YN",
    },
  ],
  WMSMS030_rn: [
    { id: "WGT_YN", group: "COM09" },
    { id: "RACK_USE_YN", group: "COM09" },
    { id: "BEST_DATE_YN", group: "COM09" },
    { id: "RACK_TYPE", group: "LOC02" },
    { id: "RFID_YN", group: "COM09" },
    { id: "AUTO_RCV_STEP", group: "WORK03" },
    { id: "LC_TIME_ZONE", group: "TIME" },
    { id: "AUTO_SND_STEP", group: "WORK04" },
    { id: "WGT_TYPE", group: "WGT01" },
    { id: "MULTI_SHIP_YN", group: "COM09" },
    { id: "LC_TYPE", group: "LC_TYPE" },
    { id: "MANAGE_PLT_YN", group: "COM09" },
    { id: "LC_USE_TY", group: "LC_USE_TY" },
    { id: "STOCK_ALERT_YN", group: "USEYN" },
    { id: "CLIENT_ORD_YN", group: "COM09" },
    { id: "groupOUTLOC", group: "OUTLOC" },
  ],
  WMSMS150: [
    {
      id: "ITEM_TY",
      group: "ITEM_TY2",
    },
    {
      id: "CYCL_STOCK_TYPE",
      group: "CYCL01",
    },
    {
      id: "TYPE_ST",
      group: "TYPEST",
    },
  ],
  WMSST051_rn: [
    {
      id: "vrSrchItemType",
      group: "ITEM_TY",
    },
    {
      id: "S_SET_ITEM_YN",
      group: "COM09",
    },
    {
      id: "vrSrchItemType2",
      group: "ITEM_TY",
    },
    {
      id: "vrSrchItemType4",
      group: "ITEM_TY",
    },
    {
      id: "S_SET_ITEM_YN_4",
      group: "COM09",
    },
    {
      id: "vrSrchItemOrdGb",
      group: "ORD01",
    },
    {
      id: "S_SET_ITEM_YN_5",
      group: "COM09",
    },
  ],
  WMSST010_rn: [
    {
      id: "vrSrchSetItemYn",
      group: "SETITEM_YN",
    },
    {
      id: "S_CUST_TYPE",
      group: "CUST01",
    },
    {
      id: "vrSrchItemOrdGb",
      group: "ORD01",
    },
    {
      id: "vrSrchLocType",
      group: "LOC01",
    },
    {
      id: "vrSrchLockYn",
      group: "COM09",
    },
  ],
  WMSOP910_rn: [
    {
      id: "vrSrchOrderPhase",
      group: "ORD03",
    },
    {
      id: "vrSrchOrdSubtype",
      group: "WORK01",
    },
    {
      id: "vrSrchInsertSource",
      group: "INS01",
    },
    {
      id: "vrSrchOrdIncomEtcType",
      group: "P_INCOM_TYPE",
    },
    {
      id: "vrSrchLocationType",
      group: "LOC08",
    },
    {
      id: "vrSrchStockQtyEmpty",
      group: "MAPPINGYN",
    },
    {
      id: "vrSrchSetItemType",
      group: "COM11",
    },
    {
      id: "S_CUST_TYPE",
      group: "ORD08",
    },
  ],
  WMSOM100: [
    {
      id: "UOM",
      group: "UOM",
    },
    {
      id: "ITEMGRP",
      group: "ITEMGRP",
    },
  ],
  WMSOM120_rn: [
    {
      id: "vrOrdRegisterType",
      group: "STS01",
    },
    {
      id: "vrOrdSubtype",
      group: "WORK01",
    },
  ],
  WMSST400: [],
  WMSMS010_rn: [
    {
      id: "BIZ_NO_TYPE",
      group: "ACC22",
    },
    {
      id: "DEAL_TYPE",
      group: "ACC21",
    },
    {
      id: "MANAGE_EVENT",
      group: "ACC06",
    },
    {
      id: "MANAGE_BIZ_CONT",
      group: "ACC07",
    },
    {
      id: "BIZ_COND",
      group: "ACC22",
    },
    {
      id: "BIZ_NO_TYPE",
      group: "ACC23",
    },
    {
      id: "BIZ_TYPE",
      group: "ACC24",
    },
    {
      id: "MANAGE_GRADE",
      group: "ACC26",
    },
    {
      id: "SETTLE_TERM",
      group: "ACC12",
    },
    {
      id: "USE_CAL_YN",
      group: "COM09",
    },
    {
      id: "CAL_TYPE",
      group: "CAL_TYPE",
    },
    {
      id: "ACCOUNT_CD1",
      group: "ACC25",
    },
    {
      id: "NOW_USE_YN",
      group: "USEYN",
    },
    {
      id: "SAFE_STOCK_TY",
      group: "SSTY",
    },
    {
      id: "DLV_CNTL",
      group: "COM09",
    },
    {
      id: "LOT_NO_USE_YN",
      group: "COM09",
    },
    {
      id: "CREDIT_GRADE",
      group: "ITM08",
    },
    {
      id: "CUST_TYPE",
      group: "CUST01",
    },
    {
      id: "CLIENT_NEC_YN",
      group: "COM09",
    },
    {
      id: "RCV_RCM_TY",
      group: "RCV_RCM_TY",
    },
    {
      id: "TYPICAL_CUST_GB",
      group: "COM09",
    },
    {
      id: "SAP_USE_YN",
      group: "COM09",
    },
    {
      id: "SET_RELEASE_YN",
      group: "COM09",
    },
    {
      id: "PARCEL_TRACE_YN",
      group: "COM09",
    },
    {
      id: "PARCEL_BOX_RECOM_YN",
      group: "COM09",
    },
  ],
  WMSMS011_rn: [
    {
      id: "vrSrchCustInOutType",
      group: "CUST02",
    },
    {
      id: "vrSrchCustGrpId",
      group: "CUST03",
    },
    {
      id: "BIZ_COND",
      group: "ACC06",
    },
    {
      id: "BIZ_TYPE",
      group: "ACC07",
    },
    {
      id: "CREDIT_GRADE",
      group: "ITM08",
    },
    {
      id: "DLV_CNTL",
      group: "COM09",
    },
    {
      id: "VAT_YN",
      group: "COM09",
    },
    {
      id: "BEST_DATE_YN",
      group: "COM09",
    },
    {
      id: "ASN_YN",
      group: "COM09",
    },
    {
      id: "ASN_MAKE_TYPE",
      group: "ASN_STEP",
    },
    {
      id: "CUST_TYPE",
      group: "CUST01",
    },
    {
      id: "CUST_INOUT_TYPE",
      group: "CUST02",
    },
    {
      id: "ASN_TYPE",
      group: "ASNTYPE",
    },
    {
      id: "CUST_GRP_ID",
      group: "CUST03",
    },
  ],
  WMSMS090_rn: [
    {
      id: "S_SET_ITEM_YN",
      group: "COM09",
    },
    {
      id: "S_SET_ITEM_YN",
      group: "COM09",
    },
    {
      id: "S_USE_YN",
      group: "COM09",
    },
    {
      id: "IN_OUT_BOUND",
      group: "ITM16",
    },
    {
      id: "CURRENCY_NAME",
      group: "CRCY",
    },
    {
      id: "SET_ITEM_YN",
      group: "COM09",
    },
    {
      id: "SET_ITEM_TYPE",
      group: "COM11",
    },
    {
      id: "TYPE_ST",
      group: "TYPE",
    },
    {
      id: "LOT_USE_YN",
      group: "COM09",
    },
    {
      id: "SHIP_ABC",
      group: "ITM08",
    },
    {
      id: "WEIGHT_CLASS",
      group: "ITM13",
    },
    {
      id: "BEST_DATE_TYPE",
      group: "BDT",
    },
    {
      id: "BEST_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "USE_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "AUTO_OUT_ORD_YN",
      group: "USEYN",
    },
    {
      id: "OUT_BEST_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "OUT_LOC_REC",
      group: "OUTLOC",
    },
    {
      id: "IN_BEST_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "AUTO_CAL_PLT_YN",
      group: "USEYN",
    },
    {
      id: "PLT_YN",
      group: "COM09",
    },
    {
      id: "REWORK_ITEM_YN",
      group: "COM09",
    },
    {
      id: "USE_YN",
      group: "USEYN",
    },
    {
      id: "ITEM_TMP",
      group: "ITEM_TMP",
    },
    {
      id: "ITEM_TYPE",
      group: "ITEM_TYPE",
    },
    {
      id: "DEV_ITEM_TMP",
      group: "ITEM_TMP",
    },
    {
      id: "IN_LOC_REC",
      group: "RCV_RCM_TY",
    },
    {
      id: "SERIAL_CHK_YN",
      group: "USEYN",
    },
    {
      id: "PARCEL_COMP_CD",
      group: "SWEET_DCD",
    },
    {
      id: "PARCEL_COM_TY",
      group: "USEYN",
    },
    {
      id: "MACHINE_TYPE",
      group: "EQ001",
    },
    {
      id: "S_SET_ITEM_YN",
      group: "COM09",
    },
    {
      id: "S_USE_YN",
      group: "COM09",
    },
    {
      id: "IN_OUT_BOUND",
      group: "ITM16",
    },
    {
      id: "CURRENCY_NAME",
      group: "CRCY",
    },
    {
      id: "SET_ITEM_YN",
      group: "COM09",
    },
    {
      id: "SET_ITEM_TYPE",
      group: "COM11",
    },
    {
      id: "TYPE_ST",
      group: "TYPE",
    },
    {
      id: "LOT_USE_YN",
      group: "COM09",
    },
    {
      id: "SHIP_ABC",
      group: "ITM08",
    },
    {
      id: "WEIGHT_CLASS",
      group: "ITM13",
    },
    {
      id: "BEST_DATE_TYPE",
      group: "BDT",
    },
    {
      id: "BEST_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "USE_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "AUTO_OUT_ORD_YN",
      group: "USEYN",
    },
    {
      id: "OUT_BEST_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "OUT_LOC_REC",
      group: "OUTLOC",
    },
    {
      id: "IN_BEST_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "AUTO_CAL_PLT_YN",
      group: "USEYN",
    },
    {
      id: "PLT_YN",
      group: "COM09",
    },
    {
      id: "REWORK_ITEM_YN",
      group: "COM09",
    },
    {
      id: "USE_YN",
      group: "USEYN",
    },
    {
      id: "ITEM_TMP",
      group: "ITEM_TMP",
    },
    {
      id: "ITEM_TYPE",
      group: "ITEM_TYPE",
    },
    {
      id: "DEV_ITEM_TMP",
      group: "ITEM_TMP",
    },
    {
      id: "IN_LOC_REC",
      group: "RCV_RCM_TY",
    },
    {
      id: "SERIAL_CHK_YN",
      group: "USEYN",
    },
    {
      id: "PARCEL_COMP_CD",
      group: "SWEET_DCD",
    },
    {
      id: "PARCEL_COM_TY",
      group: "USEYN",
    },
    {
      id: "MACHINE_TYPE",
      group: "EQ001",
    },
    {
      id: "S_SET_ITEM_YN",
      group: "COM09",
    },
    {
      id: "S_USE_YN",
      group: "COM09",
    },
    {
      id: "IN_OUT_BOUND",
      group: "ITM16",
    },
    {
      id: "CURRENCY_NAME",
      group: "CRCY",
    },
    {
      id: "SET_ITEM_YN",
      group: "COM09",
    },
    {
      id: "SET_ITEM_TYPE",
      group: "COM11",
    },
    {
      id: "TYPE_ST",
      group: "TYPE",
    },
    {
      id: "LOT_USE_YN",
      group: "COM09",
    },
    {
      id: "SHIP_ABC",
      group: "ITM08",
    },
    {
      id: "WEIGHT_CLASS",
      group: "ITM13",
    },
    {
      id: "BEST_DATE_TYPE",
      group: "BDT",
    },
    {
      id: "BEST_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "USE_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "AUTO_OUT_ORD_YN",
      group: "USEYN",
    },
    {
      id: "OUT_BEST_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "OUT_LOC_REC",
      group: "OUTLOC",
    },
    {
      id: "IN_BEST_DATE_UNIT",
      group: "YMD",
    },
    {
      id: "AUTO_CAL_PLT_YN",
      group: "USEYN",
    },
    {
      id: "PLT_YN",
      group: "COM09",
    },
    {
      id: "REWORK_ITEM_YN",
      group: "COM09",
    },
    {
      id: "USE_YN",
      group: "USEYN",
    },
    {
      id: "ITEM_TMP",
      group: "ITEM_TMP",
    },
    {
      id: "ITEM_TYPE",
      group: "ITEM_TYPE",
    },
    {
      id: "DEV_ITEM_TMP",
      group: "ITEM_TMP",
    },
    {
      id: "IN_LOC_REC",
      group: "RCV_RCM_TY",
    },
    {
      id: "SERIAL_CHK_YN",
      group: "USEYN",
    },
    {
      id: "PARCEL_COMP_CD",
      group: "SWEET_DCD",
    },
    {
      id: "PARCEL_COM_TY",
      group: "USEYN",
    },
    {
      id: "MACHINE_TYPE",
      group: "EQ001",
    },
  ],
  WMSMS080: {},
  WMSMS092_rn: [
    {
      id: "ITEMGRP",
      group: "ITEMGRP",
    },
    {
      id: "USE_YN_E1",
      group: "COM09",
    },
    {
      id: "USE_YN_E5",
      group: "COM09",
    },
    {
      id: "S_CUST_TYPE",
      group: "ORD08",
    },
  ],
  WMSMS095: [
    {
      id: "ITEM_TY",
      group: "ITEM_TY",
    },
    {
      id: "ITEMGRP",
      group: "ITEMGRP",
    },
    {
      id: "vrSrchLockYn",
      group: "COM09",
    },
  ],
  WMSCM091_rn: [{ id: "CNTR_TYPE", group: "CAR05" }],
  WMSCM091Q8_rn: {},
  WMSCM162_rn: [
    {
      id: "CNTR_TYPE",
      group: "CAR05",
    },
  ],
  WMSST055_rn: {},
  WMSST057: [
    {
      id: "vrSrchItemType",
      group: "ITEM_TY",
    },
    {
      id: "vrSrchLocType",
      group: "LOC01",
    },
  ],
  WMSST058: [
    {
      id: "vrSrchItemType",
      group: "ITEM_TY",
    },
    {
      id: "vrSrchLocType",
      group: "LOC01",
    },
  ],
  WMSST059: [
    {
      id: "vrSrchItemType",
      group: "ITEM_TY",
    },
    {
      id: "vrSrchItemGrpId",
      group: "ITEMGRP",
    },
    {
      id: "S_SET_ITEM_YN",
      group: "COM09",
    },
  ],
  WMSYS030_rn: [
    {
      id: "vrTempleteType",
      group: "TT",
    },
    {
      id: "S_CUST_TYPE",
      group: "ORD08",
    },
  ],
  WMSOP010: [
    {
      id: "vrSrchOrderPhase",
      group: "ORD03",
    },
    {
      id: "vrSrchApproveYn",
      group: "ORD04",
    },
    {
      id: "vrSrchItemOrdGb",
      group: "ORD01",
    },
    {
      id: "WORK_STAT",
      group: "STS01",
    },
    {
      id: "vrSrchOrdSubtype",
      group: "WORK01",
    },
  ],
  WMSOP021: [
    {
      id: "vrSrchOrdSubtype",
      group: "WORK01",
    },
    {
      id: "vrSrchOrderPhase",
      group: "ORD03",
    },
  ],
  WMSOP031: [
    {
      id: "vrSrchOrderPhase",
      group: "ORD03",
    },
  ],
  WMSOP030SE5_rn: [
    {
      id: "vrSrchOrdSubType",
      group: "WORK01",
    },
  ],
  WMSOP642_rn: [
    {
      id: "vrSrchOrderPhase",
      group: "ORD03",
    },
    {
      id: "vrSrchOrderPhaseDetail",
      group: "ORD03",
    },
    {
      id: "vrSrchOrdWorkType",
      group: "EQ001",
    },
    {
      id: "vrSrchDlvCompCd",
      group: "SWEET_DCD",
    },
    {
      id: "PARCEL_COM_TY",
      group: "SWEET_DCD",
    },
    {
      id: "PARCEL_ORD_TY",
      group: "PARCEL_ORD",
    },
    // {
    //   id: "PARCEL_PAY_TY",
    //   group: "PARCEL_PAY",
    // },
    // {
    //   id: "PARCEL_BOX_TY",
    //   group: "PARCEL_BOX",
    // },
    // {
    //   id: "PARCEL_ETC_TY",
    //   group: "PARCEL_ETC",
    // },
    // {
    //   id: "PARCEL_ORD_TY",
    //   group: "PARCEL_ORD",
    // },
    // {
    //   id: "PARCEL_PAY_TY",
    //   group: "PARCEL_PAY2",
    // },
    // {
    //   id: "PARCEL_BOX_TY",
    //   group: "PARCEL_BOX_LG",
    // },
    // {
    //   id: "PARCEL_ORD_TY",
    //   group: "PARCEL_ORD",
    // },
    // {
    //   id: "PARCEL_PAY_TY",
    //   group: "PARCEL_PAY",
    // },
    // {
    //   id: "PARCEL_BOX_TY",
    //   group: "PARCEL_BOX_L",
    // },
    // {
    //   id: "PARCEL_ETC_TY",
    //   group: "PARCEL_ORD_2",
    // },
    // {
    //   id: "PARCEL_ORD_TY",
    //   group: "PARCEL_ORD",
    // },
    {
      id: "COM_DLV_DIV",
      group: "CAR04_HJ",
    },
    {
      id: "COM_PAY_CON",
      group: "HANJIN_PAY",
    },
    {
      id: "COM_BOX_TYP",
      group: "PARCEL_BOX_HJ",
    },
    {
      id: "PARCEL_COM_TY2",
      group: "SWEET_DCD",
    },
    {
      id: "PARCEL_COM_TY3",
      group: "SWEET_DCD",
    },
    {
      id: "S_CUST_TYPE",
      group: "ORD08",
    },
  ],
  WMSST014: [
    {
      id: "vrSrchItemOrdGb",
      group: "ORD01",
    },
    {
      id: "vrSrchSetItemYn",
      group: "SETITEM_YN",
    },
    {
      id: "vrSrchLockYn",
      group: "COM09",
    },
    {
      id: "vrSrchItemType",
      group: "ITEM_TY",
    },
  ],
  WMSST040T1_rn: [
    {
      id: "vrSrchItemType",
      group: "ITEM_TY",
    },
    {
      id: "vrSrchReason",
      group: "REASON",
    },
  ],
  WMSST040E3_rn: [
    {
      id: "vrSrchItemType",
      group: "ITEM_TY",
    },
    {
      id: "vrSrchHexGb",
      group: "HEX_GB",
    },
  ],
  WMSST210: [
    {
      id: "vrSrchOrderGb",
      group: "ORD07",
    },
    {
      id: "vrSrchHexGb",
      group: "HEX_GB",
    },
    {
      id: "vrSrchUserByLcId",
      group: "LCCODE",
    },
  ],
  WMSST065: [],
  WMSMS096: [
    {
      id: "ITEM_TYPE_NAME",
      group: "ITEM_TYPE_NAME",
    },
    {
      id: "CUST_ID",
      group: "CUST_ID",
    },
    {
      id: "STAT",
      group: "STAT",
    },
    {
      id: "ITEMGRP",
      group: "ITEMGRP",
    },
  ],

  WMSMS093: [
    {
      id: "WORK_STAT",
      group: "WORK05",
    },
  ],

  WMSMS191: [
    {
      id: "ZONE",
      group: "ZONE",
    },
  ],

  WMSMS094: [
    {
      id: "ZONE",
      group: "ZONE",
    },
  ],
  WMSIT020: [
    {
      id: "SWEET_DCD",
      group: "SWEET_DCD",
    },
    {
      id: "PARCEL_ORD",
      group: "PARCEL_ORD",
    },
    {
      id: "PARCEL_COM_TY_SEQ",
      group: "PARCEL_COM_TY_SEQ",
    },
    {
      id: "PARCEL_PAY",
      group: "PARCEL_PAY",
    },
    {
      id: "PARCEL_BOX",
      group: "PARCEL_BOX",
    },
    {
      id: "PARCEL_ETC",
      group: "PARCEL_ETC",
    },
  ],
  WMSST070: [
    {
      id: "ORD05",
      group: "ORD05",
    },
    {
      id: "COMPLETE",
      group: "COMPLETE",
    },
  ],
};
