/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	router/index.ts
 *  Description:    Page Routing 정의를 위한 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { welcomeMenu, winusMenus } from "@/constants/winus-menus";
import { UtilService } from "@/services/util-service";
import { useTabsStore, userStore } from "@/store";
import { ISelectedTab } from "@/types";
import { createRouter, createWebHistory } from "vue-router";

const routes = [
  {
    path: "/POP-WMSOP640",
    name: "WMSOP640",
    meta: { title: "WMSOP640" },
    component: () => import("@/page/delivery/WMSOP642_1/popups/WMSOP640.vue"),
  },
  {
    path: "/POP-WMSDF111",
    name: "POP-WMSDF111",
    meta: { title: "WMSDF111" },
    component: () => import("@/page/delivery/WMSOP642_1/popups/WMSDF111.vue"),
  },
  {
    path: "/POP-WMSOP330pop",
    name: "POP-WMSOP330pop",
    meta: { title: "WMSOP330pop" },
    component: () => import("@/page/operation/WMSOP910_4/popups/WMSOP330pop.vue"),
  },
  {
    path: "/POP-WMSOP630pop",
    name: "POP-WMSOP630pop",
    meta: { title: "WMSOP630pop" },
    component: () => import("@/page/operation/WMSOP910_3/popups/WMSOP630pop.vue"),
  },
  {
    path: "/POP-WMSOP630pop5",
    name: "POP-WMSOP630pop5",
    meta: { title: "WMSOP630pop5" },
    component: () => import("@/page/operation/WMSOP910_3/popups/WMSOP630pop5.vue"),
  },
  {
    path: "/POP-WMSCM310",
    name: "POP-WMSCM310",
    meta: { title: "WMSCM310" },
    component: () => import("@/page/master-data/WMSMS085/WMSCM310.vue"),
  },
  {
    path: "/POP-WMSRP030Q2",
    name: "POP-WMSRP030Q2",
    meta: { title: "WMSRP030Q2" },
    component: () => import("@/page/operation/WMSOP910_2/popups/WMSRP030Q2.vue"),
  },
  // TODO :: 삭제예정 (미사용)
  {
    path: "/POP-WMSOP030pop",
    name: "POP-WMSOP030pop",
    meta: { title: "WMSOP030pop" },
    component: () => import("@/page/operation/WMSOP910_2/popups/WMSOP030pop.vue"),
  },
  {
    path: "/POP-TMSYS030Q3",
    name: "POP-TMSYS030Q3",
    meta: { title: "POP-TMSYS030Q3" },
    component: () => import("@/page/operation/WMSOP910_2/popups/tmsys030q3.vue"),
  },
  {
    path: "/POP-WMSRP030Q4",
    name: "WMSRP030Q4",
    meta: { title: "WMSRP030Q4" },
    component: () => import("@/page/operation/WMSOP910_2/popups/wmsrp030q4.vue"),
  },
  {
    path: "/WMSOP000Q9_2",
    name: "WMSOP000Q9_2",
    meta: { title: "WMSOP000Q9_2" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q9.vue"),
  },
  {
    path: "/WMSOP000Q9_3",
    name: "WMSOP000Q9_3",
    meta: { title: "WMSOP000Q9_3" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q9.vue"),
  },
  {
    path: "/POP-WMSOP000Q9_1",
    name: "WMSOP000Q9_1",
    meta: { title: "WMSOP000Q9_1" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q9.vue"),
  },
  {
    path: "/POP-WMSOP000Q5_1",
    name: "WMSOP000Q5_1",
    meta: { title: "WMSOP000Q5_1" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q5.vue"),
  },
  {
    path: "/POP-WMSOP000Q2_1",
    name: "WMSOP000Q2_1",
    meta: { title: "WMSOP000Q2_1" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q2.vue"),
  },
  {
    path: "/POP-WMSOP000Q5V2_1",
    name: "WMSOP000Q5V2_1",
    meta: { title: "WMSOP000Q5V2_1" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q5v2.vue"),
  },
  {
    path: "/POP-WMSOP000Q12_1",
    name: "WMSOP000Q12_1",
    meta: { title: "WMSOP000Q12_1" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q12.vue"),
  },
  {
    path: "/POP-WMSOP000Q4_1",
    name: "WMSOP000Q4_1",
    meta: { title: "WMSOP000Q4_1" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q4.vue"),
  },
  {
    path: "/POP-WMSOP000Q3_1",
    name: "WMSOP000Q3_1",
    meta: { title: "WMSOP000Q3_1" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q3.vue"),
  },
  {
    path: "/POP-WMSOP000Q8_1",
    name: "WMSOP000Q8_1",
    meta: { title: "WMSOP000Q8_1" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q8.vue"),
  },
  {
    path: "/POP-WMSOP000Q1_1",
    name: "WMSOP000Q1_1",
    meta: { title: "WMSOP000Q1_1" },
    component: () => import("@/page/operation/WMSOP910_1/popups/wmsop000q1.vue"),
  },
  {
    path: "/wmsop030t",
    name: "wmsop030t",
    meta: { title: "wmsop030t" },
    component: () => import("@/page/operation/WMSOP910_3/WMSOP030T.vue"),
  },
  {
    path: "/wmsst050-inventory-adjustment-popup",
    name: "wmsst050-inventory-adjustment-popupe",
    meta: { title: "wmsst050-inventory-adjustment-popup" },
    component: () => import("@/page/stock/WMSST050/popup/wmsst050-inventory-adjustment-popup.vue"),
  },
  {
    path: "/wmstg200t1-new-popup",
    name: "wmstg200t1-new-popup",
    meta: { title: "wmstg200t1-new-popup" },
    component: () => import("@/page/statistics/WMSTG200/wmstg200t1-new-popup.vue"),
  },
  {
    path: "/wmstg733_2-enter-template",
    name: "wmsstg733_2-enter-template",
    meta: { title: "wmsstg733_2-enter-template" },
    component: () => import("@/page/statistics/WMSTG733_2/wmstg733_2-enter-template.vue"),
  },
  {
    path: "/shipping-order-popup",
    name: "shipping-order-popup",
    meta: { title: "shipping-order-popup" },
    component: () => import("@/popups/shipping-order-multi-tab/page.vue"),
    children: [
      {
        path: "/shipping-order",
        name: "shipping-order",
        meta: { title: "shipping-order" },
        component: () => import("@/popups/shipping-order-multi-tab/shipping-order.vue"),
      },
      {
        path: "/simple-shipping-order",
        name: "simple-shipping-order",
        meta: { title: "simple-shipping-order" },
        component: () => import("@/popups/shipping-order-multi-tab/simple-shipping-order.vue"),
      },
    ],
  },
  {
    path: "/receiving-order-popup",
    name: "receiving-order-popup",
    meta: { title: "receiving-order-popup" },
    component: () => import("@/popups/receiving-order-multi-tab/page.vue"),
    children: [
      {
        path: "/receiving-order",
        name: "receiving-order",
        meta: { title: "receiving-order" },
        component: () => import("@/popups/receiving-order-multi-tab/receiving-order.vue"),
      },
      {
        path: "/simple-receiving-order",
        name: "simple-receiving-order",
        meta: { title: "simple-receiving-order" },
        component: () => import("@/popups/receiving-order-multi-tab/simple-receiving-order.vue"),
      },
    ],
  },
  {
    path: "/logistics-container-receiving-order",
    name: "logistics-container-receiving-order",
    meta: { title: "logistics-container-receiving-order" },
    component: () => import("@/popups/logistics-container-receiving-order/logistics-container-receiving-order.vue"),
  },
  {
    path: "/logistics-container-shipping-order",
    name: "logistics-container-shipping-order",
    meta: { title: "logistics-container-shipping-order" },
    component: () => import("@/popups/logistics-container-shipping-order/logistics-container-shipping-order.vue"),
  },
  {
    path: "/template-input-for-sap",
    name: "template-input-for-sap",
    meta: { title: "template-input-for-sap" },
    component: () => import("@/popups/sap-template.vue"),
  },
  {
    path: "/sales-customer-registration",
    name: "sales-customer-registration",
    meta: { title: "sales-customer-registration" },
    component: () => import("@/popups/sales-customer-registration.vue"),
  },
  {
    path: "/wmsop910_3-new-popup",
    name: "wmsop910_3-new-popup",
    meta: { title: "wmsop910_3-new-popup" },
    component: () => import("@/popups/wmsop910_3-new-popup.vue"),
  },
  {
    path: "/wmsom120-new-popup",
    name: "wmsom120-new-popup",
    meta: { title: "wmsom120-new-popup" },
    component: () => import("@/popups/wmsom120-new-popup.vue"),
  },
  {
    path: "/upload-file",
    name: "upload-file",
    meta: { title: "Upload File" },
    component: () => import("@/popups/upload-file.vue"),
  },
  {
    path: "/template-input-for-b2c",
    name: "upload-template",
    meta: { title: "Upload Template" },
    component: () => import("@/popups/b2c-template.vue"),
  },
  {
    path: "/integrated-template-input",
    name: "integrated-template-input",
    meta: { title: "Upload Template" },
    component: () => import("@/popups/integrated-template.vue"),
  },
  {
    path: "/enter-template",
    name: "enter-template",
    meta: { title: "Upload Template" },
    component: () => import("@/popups/enter-template.vue"),
  },
  {
    path: "/re-location",
    name: "re-location",
    meta: { title: "Re Location" },
    component: () => import("@/popups/re-location-modal.vue"),
  },
  {
    path: "/excel-import",
    name: "excel-import",
    meta: { title: "Excel Import" },
    component: () => import("@/popups/excel-import.vue"),
  },
  {
    path: "/date-change",
    name: "date-change",
    meta: { title: "Date Change" },
    component: () => import("@/popups/date-change.vue"),
  },
  {
    path: "/shipment-control",
    name: "shipment-control",
    meta: { title: "Shipment Control" },
    component: () => import("@/popups/shipment-control.vue"),
  },
  {
    path: "/new-zone",
    name: "new-zone",
    meta: { title: "New Zone" },
    component: () => import("@/page/new-zone/new-zone.vue"),
  },
  {
    path: "/POP-WMSST040T2",
    name: "WMSST040T2",
    meta: { title: "WMSST040T2" },
    component: () => import("@/page/stock/WMSST040/popups/wmsst040t2.vue"),
  },
  {
    path: "/POP-WMSST040T1",
    name: "WMSST040T1",
    meta: { title: "WMSST040T1" },
    component: () => import("@/page/stock/WMSST040/popups/wmsst040t1.vue"),
  },
  {
    path: "/POP-WMSST040E3",
    name: "WMSST040E3",
    meta: { title: "WMSST040E3" },
    component: () => import("@/page/stock/WMSST040/popups/wmsst040e3.vue"),
  },
  {
    path: "/POP-WMSST040E6",
    name: "WMSST040E6",
    meta: { title: "WMSST040E6" },
    component: () => import("@/page/stock/WMSST040/popups/wmsst040e6.vue"),
  },
  {
    path: "/login",
    name: "login",
    meta: { title: "환영합니다." },
    component: () => import("@/page/login.vue"),
  },
  {
    path: "/error",
    name: "error",
    meta: { title: "error" },
    component: () => import("@/page/error.vue"),
  },
  {
    path: "/user-report",
    name: "user-report",
    meta: { title: "user-report" },
    component: () => import("@/popups/user-report/user-report.vue"),
  },
];
const home = {
  path: "/",
  name: "Home",
  meta: { title: "맞춤형 물류서비스" },
  component: () => import("@/page/home.vue"),
  children: [
    {
      path: "/welcome",
      name: "welcome",
      meta: { title: "맞춤형 물류서비스" },
      component: () => import("@/page/welcome.vue"),
    },
  ],
};

for (const mainMenu of winusMenus) {
  for (const menu of mainMenu.subMenus) {
    if (menu.hidden) {
      continue;
    }
    home.children.push({
      path: "/" + mainMenu.id + "/" + menu.id,
      name: mainMenu.id + "/" + menu.id,
      meta: { title: menu.title },
      component: () => import(`@/page/${mainMenu.id}/${menu.id}/page.vue`),
    });
  }
}
routes.push(home);
const router = createRouter({
  history: createWebHistory(),
  routes: routes,
});

const root_title = "WINUS";
router.beforeEach((to, from) => {
  document.title = `${root_title}${to.meta?.title ? " - " + to.meta?.title : ""}`;
  userStore().reload();
  if ((UtilService.isEmptyObject(userStore().getCurrentUser()) || UtilService.isEmptyObject(userStore().getWindowRoll())) && to.path !== "/login") {
    return "/login";
  } else if (!UtilService.isEmptyObject(userStore().getCurrentUser()) && !UtilService.isEmptyObject(userStore().getWindowRoll()) && userStore().getExpire() >= Date.now() && to.path === "/login") {
    return from;
  } else if (userStore().getExpire() < Date.now() && to.path !== "/error" && from.path !== "/" && to.path !== "/login") {
    return "/error";
  }

  if (routes.findIndex((route) => route.path == to.path || (route.children && route.children.findIndex((child) => child.path == to.path) != -1)) === -1) {
    return "/";
  }
  const currentUser = JSON.parse(userStore().getCurrentUser());
  if (currentUser && (!useTabsStore().newTabs.value || useTabsStore().newTabs.value!.length == 0)) {
    useTabsStore().loadTabs(currentUser.USER_ID, to.path);
    if (!useTabsStore().selectedTab.value) {
      if (to.path === "/" + welcomeMenu.path) {
        useTabsStore().addTab(welcomeMenu, currentUser.USER_ID);
      } else {
        for (const mainMenu of winusMenus) {
          for (const subMenu of mainMenu.subMenus) {
            if ("/" + subMenu.path === to.path) {
              const menu: ISelectedTab = subMenu;
              menu.mainMenuTitle = mainMenu.title;
              useTabsStore().addTab(subMenu, currentUser.USER_ID);
              return;
            }
          }
        }
      }
    }
  }
});

export default router;
