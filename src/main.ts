/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	main.ts
 *  Description:    Vue App 메인을 구성하는 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { i18n } from "@/i18n";
import { LicenseManager } from "ag-grid-enterprise";
import { createPinia } from "pinia";
import { createApp } from "vue";
import Toast, { POSITION, PluginOptions } from "vue-toastification";
import App from "./App.vue";
import router from "./router";
// @ts-ignore : type지원이 되지 않는 라이브러리인 경우 해당 주석을 통해 에러 메시지 제거 가능
// @ts-ignore
import vClickOutside from "click-outside-vue3";

import 'tippy.js/dist/tippy.css';
import VueTippy from 'vue-tippy';

import Aura from '@primevue/themes/aura';
import PrimeVue from 'primevue/config';

import { definePreset } from "@primevue/themes";
import "vue-toastification/dist/index.css";
import "/public/assets/scss/_reset.scss";

const pinia = createPinia();

LicenseManager.setLicenseKey(import.meta.env.VITE_GRID_KEY);

const options: PluginOptions = {
  position: POSITION.BOTTOM_RIGHT,
  hideProgressBar: true,
  timeout: 5000,
  // For the actual toast, including different toast types:
  toastClassName: "winus-toast",
  // For the toast body when using strings or a custom component as content
  bodyClassName: ["winus-toast-body", "winus-toast-component-body"],
  closeButtonClassName: "btn-winus-toast-close",
  icon: {
    iconClass: "winus-icon", // Optional
    iconTag: "i", // Optional
  },
};

const tippyOptions = {
  directive: 'tippy', // => v-tippy
  component: 'tippy', // => <tippy/>
  componentSingleton: 'tippy-singleton', // => <tippy-singleton/>,
  defaultProps: {
    placement: 'auto-end',
    allowHTML: true,
  }, // => Global default options * see all props
}

const winusPreSet = definePreset(Aura, {
  primitive: {
    green: { 50: 'rgba(28, 198, 89, 0.3)', 100: '#dcfce7', 200: '#bbf7d0', 300: '#86efac', 400: '#4ade80', 500: '#22c55e', 600: '#16a34a', 700: '#15803d', 800: '#166534', 900: '#14532d', 950: '#052e16' },
    sky: { 50: 'rgba(61, 97, 225, 0.3)', 100: '#e0f2fe', 200: '#bae6fd', 300: '#7dd3fc', 400: '#38bdf8', 500: '#3d61e1', 600: '#0284c7', 700: '#0369a1', 800: '#075985', 900: '#0c4a6e', 950: '#082f49' }
  },

  components: {
      // ... other components
  }
});

createApp(App).use(router).use(i18n).use(Toast, options).use(pinia).use(vClickOutside).use(VueTippy, tippyOptions).use(PrimeVue,{ theme: {preset: winusPreSet} }).mount("#app");
