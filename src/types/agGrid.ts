/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	types/agGrid.ts
 *  Description:    AG-Grid 관련 타입스크립트 인터페이스 확장
 *  Authors:        dhkim
 *  Update History:
 *                  2024.03. : Created by dhkim
 *
------------------------------------------------------------------------------*/
import type { ColDef, ColGroupDef, ICellEditorParams } from "ag-grid-community";

/**
 * AG-Grid 'ColDef' 참조 정의
 */
export interface IColDef extends ColDef {
  /** 열 헤더 정의 구분 */
  headerKey?: string;

  /** 엑셀 추출 여부 */
  export?: boolean;

  children?: any;
}

/**
 * AG-Grid 'ColDef' 참조 정의
 */
export interface IColGroupDef extends ColGroupDef, ColDef {
  /** 열 헤더 정의 구분 */
  headerKey: string | undefined;

  /** 엑셀 추출 여부 */
  export?: boolean;

  children: (IColDef | ColDef | ColGroupDef)[];
}

/**
 * 모달 창의 AG-Grid 컬럼 정보 (ColDef)
 */
export interface IHasKeyColDef {
  [key: string]: IColDef[];
}

export interface ColorPickerParams extends ICellEditorParams {
  value: string; // 현재 셀의 값, 색상 코드가 들어올 것입니다.
}
