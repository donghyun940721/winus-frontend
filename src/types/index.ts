/*------------------------------------------------------------------------------
 *  Copyright (c) 2023 LogisSystemsAll. All Rights Reserved
 *
 *  Use of this software is controlled by the terms and conditions found in the
 *  license agreement under which this software has been supplied.
 *------------------------------------------------------------------------------
 *
 *  Source Name:   	types/index.ts
 *  Description:    Type Class들 Export 정의를 위한 스크립트
 *  Authors:        J. I. Cho
 *  Update History:
 *                  2023.08. : Created by J. I. Cho
 *
------------------------------------------------------------------------------*/

import { ColDef } from "ag-grid-community";
import { IColDef, IColGroupDef } from "./agGrid";

/** 그리드 상단에 위치한 그리드 통계수치 정보 정의 */
export interface IGridStatisticsInfo {
  /** 서버에서 받아온 통계수치의 key값 */
  id: string;
  /** 그리드 통계수치의 이름 */
  title: string;
  /**
   * 같은 이름을 가진 통계수치에서 함께 보여져야 할 통계수치의 key값
   * subId도 서버에서 받아온 통계수치의 key값 중 하나
   * ex ) WMSOP910_3 페이지 참조
   */
  subId?: string;
}

/**
 * ag-grid 컬럼 정보 정의
 */
export interface IHasKeyColumDef {
  [key: string]: ColDef[];
}

/** 검색영역 상단 날짜 정보 정의 */
export interface IDateRange {
  /** 날짜 input의 key값 */
  dateInputIds: [string, string];
  /** 날짜 input 옆 설정 옵션 정보 */
  checkBoxes: {
    /** 옵션 input의 key값 */
    id: string;
    /** 옵션 input 선택 시 적용되어야 할 value값 */
    value: string;
    /** 옵션 input의 이름 */
    title: string;
    /** 기본 선택 설정 */
    defaultChecked?: boolean;
  }[];
}

/** control 영역 버튼 정의 */
export interface IControlBtn {
  /** 버튼 이름 */
  title: string;
  /** 버튼 색 설정 */
  colorStyle: string;
  /**
   * 버튼 여백 설정
   * normal, bold
   */
  paddingStyle: string;
  /**
   * 버튼 이미지 설정
   * '엑셀' 관련 버튼일 때 'excel'이미지 설정
   */
  image: string;
  /**
   * disabled 처리
   * value: 'disabled'
   */
  disabled?: string;

  display?: string;

  authType?: string;
}

export interface ISplitButton {
  /** 버튼 명 */
  title: string;

  /** 이벤트 핸들러 명칭 */
  eventHandler: string;

  /** 버튼 색상 */
  colorStyle: string;

  /** 이미지 */
  image?: string;
}

export interface IControlBtnProps {
  /** 버튼 전체 정보 */
  data: IControlBtn;
  /** control 버튼별 기능 정의 */

  /** 신규 */
  addRowData?: () => void;
  /** 저장 */
  saveRowData?: () => void;
  /** 삭제 */
  deleteRowData?: () => void;
  /** 엑셀 다운로드 */
  excelExport?: () => void;
  /** 엑셀 입력 */
  excelImport?: () => void;
  /** 메뉴얼 다운로드 */
  clickManual?: () => void;
  /** 엑셀입력(간편) */
  simpleExcelImport?: () => void;
  /**
   * 바코드 출력
   * ex ) 기준관리/상품정보관리(WMSMS090)
   */
  printBarcode?: () => void;
  /**
   * 템플릿 업로드
   * ex ) 통계관리/출고검수조회_시리얼조회(WMSTG733_2)
   */
  uploadTemplate?: () => void;
  /**
   * LOT 삭제
   * ex ) 통계관리/LOT별재고현황(WMSTG070)
   */
  deleteLot?: () => void;
  /**
   * 출고주문등록
   * ex ) 통계관리/LOT별재고현황(WMSTG070)
   */
  registrationShippingOrder?: () => void;
  /**
   * 수불재생성
   * ex ) 재고관리/현재고조회(WMSST010)
   */
  InOutOrderRegeneration?: () => void;
  /** 검색조건초기화 */
  resetSearchInput?: () => void;
  /** 출고일 변경 */
  changeShippingDate?: () => void;
  /** 엑셀(일자별Loc합계) */
  excelLocTotalByDate?: () => void;
  /** 명의 변경 */
  changeOfOwner?: () => void;
  /** 로케이션 재등록 */
  reLocation?: () => void;
  /** 시스템관리/템플릿관리 - 템플릿 삭제 */
  deleteTemplate?: () => void;
  /** 시스템관리/템플릿관리 - 시작 행 저장 */
  saveStartRow?: () => void;
  /** 시스템관리/템플릿관리 - api로 받아온 StartRow(시작행(값)) */
  startRowValue?: any;
  /** B2C 전용 템플릿 업로드 */
  templateInputForB2C?: () => void;
  /** 인쇄 */
  printExport?: () => void;
  /** 화주통합템플릿(기능 개발 보류 (11/16)) */
  integratedTemplateInput?: () => void;
  /**
   * 입고일 변경
   * ex ) 운영관리 - 입/출고관리통합/입고관리(WMSOP910_1)
   */
  changeReceivingDate?: () => void;
  /** 출고 식별표 출력 */
  shippingIdentificationLabelPrint?: () => void;
  /** 템플릿 입력 */
  enterTemplate?: () => void;
  /**
   * 박스바코드변경
   * ex ) 기준관리/상품정보관리(WMSMS090)
   */
  changeBoxBarcode?: () => void;
  /** 대표로케이션지정 */
  designateRepresentativeLocation?: () => void;
  /** 중복바코드조회
   * ex ) 기준관리/상품정보관리(WMSMS090)
   */
  setOnlyGridViewParams?: () => void;
  /** 바코드 생성
   * ex ) 기준관리/상품정보관리(WMSMS090)
   */
  createBarcode?: () => void;
  /** 상품재등록
   * ex ) 기준관리/상품정보관리(WMSMS090)
   */
  productReRegistration?: () => void;
  /** 엑셀(전체) */
  excelAllExport?: () => void;
  /** 엑셀(선택) */
  excelSelectExport?: () => void;
  /** 파일업로드 - 기능 개발 보류
   * ex ) 운영관리 - 입/출고관리(통합)/출고관리(B2C)(WMSOP910_3)
   */
  uploadFile?: () => void;
  /** 템플릿복사
   * ex ) 시스템관리/템플릿관리(WMSYS030)
   */
  copyTemplate?: () => void;
  /** 센터동기화 */
  changeIsOpenLcSyncModal?: (value: boolean) => void;
  /** 운영관리 - 입/출고관리(통합), 입고주문관리 : 신규 모달 내 초기화 */
  resetGridRowData?: () => void;
  /** 입고템플릿업로드
   * ex ) 운영관리/입고주문관리(WMSOM120)
   */
  uploadReceivingTemplate?: () => void;
  /** barcode-multi-input.vue에서 사용되는 함수인데 개발 보류된 항목 */
  close?: () => void;
  /** barcode-multi-input.vue에서 사용되는 함수인데 개발 보류된 항목 */
  sync?: () => void;
  /** 신규베송처 */
  newDestinations?: () => void;
  /** sap템플릿입력 -> 개발 보류 항목 */
  templateInputForSap?: () => void;
  /** 행삭제 (sap템플릿입력 -> 개발 보류 항목) */
  lineDel?: () => void;
  /** 전체엑셀
   * ex ) 재고관리/현재고 조회(WMSST010)
   */
  allExcelExport?: () => void;
  /** 필터
   * ex ) 재고관리/현재고 조회(WMSST010)
   */
  filter?: () => void;
  /** 전체통합버튼 */
  totalIntegration?: () => void;
  /** 설정
   * ex ) 재고관리/재고조정(WMSST050)
   */
  setting?: () => void;
  /** 재고이동지시서
   * ex ) 재고관리/재고이동(WMSST040)
   */
  stockMoveReport?: () => void;
  /** 작업상태조회
   * ex ) 재고관리/재고이동(WMSST040)
   */
  wmsst040t2?: () => void;
  /** 재고조정
   * ex ) 재고관리/재고조정(WMSST050)
   */
  inventoryAdjustment?: () => void;
  /**
   * 출력
   * ex ) 재고관리/현재고조회(WMSST010) - 상세현재고조회
   */
  printing?: () => void;
  /**
   * 통합조회
   * ex ) 재고관리/재고이동(WMSST040)
   */
  wmsst040t1?: () => void;
  /**
   * 신규(팝업)
   * ex ) 재고관리/재고이동(WMSST040) - 신규팝업
   */
  wmsst040e3?: () => void;
  /**
   * 작업취소
   * ex ) 재고관리/재고이동(WMSST040) - 신규/신규(행추가)팝업 내 버튼
   */
  cancelWork?: () => void;
  /**
   * 신규(행추가)
   * ex ) 재고관리/재고이동(WMSST040) - 신규(행추가) 팝업
   */
  wmsst040e6?: () => void;
  /**
   * 바코드 출력
   * ex ) 기준관리/로케이션정보관리(WMSMS080)
   */
  barcodeOutput?: () => void;
  /**
   * 권한복사
   * ex ) 시스템관리/사용자관리상세(TMSYS030_2)
   */
  copyPermission?: () => void;
  /**
   * 출고확정
   * ex ) 운영관리 - 입/출고관리(통합)/출고관리(B2C) - 작업지시 팝업 내 버튼
   */
  complete?: () => void;
  /**
   * 작업지시
   * ex ) 운영관리 - 입/출고관리(통합)/출고관리(B2C) - 작업지시 팝업 내 버튼
   */
  workOrder?: () => void;
  /**
   * 미배차내역조회
   * ex ) 운영관리 - 입/출고관리(통합) context menu - 일반배차 팝업 내 버튼
   */
  nonVehicleDispatch?: () => void;
  /**
   * 해제
   * ex ) 운영관리 - 입/출고관리(통합)/입고관리(WMSOP910_1) context menu - 입하 - DOCK장 지정 팝업 내 버튼
   */
  dockRelease?: () => void;
  /**
   * 초기화
   * ex ) 운영관리 - 입/출고관리(통합)(WMSOP910_1 ~ 3) context menu - 로케이션 지정 팝업 내 버튼
   */
  locationReset?: () => void;
  /**
   * 로케이션 추천
   * ex ) 운영관리 - 입/출고관리(통합)(WMSOP910_1 ~ 3) context menu - 로케이션 지정 팝업 내 버튼
   */
  locationRecommendation?: () => void;
  /**
   * 사용자관리상세
   * ex ) 시스템관리/사용자관리(TMSYS030_1)
   */
  userManagementDetail?: () => void;
  /**
   * 승인, 취소
   * ex ) 운영관리 - 입/출고관리(통합)/입고관리 - 입하 - 입차확인 팝업 내 버튼
   * @param vrApproveYn 승인, 취소를 구별하는 파라미터 / '승인'일 때 'Y', '취소'일 때 'N'
   */
  requestSaveOrCancel?: (vrApproveYn: "Y" | "N") => void;
  /**
   * 탭이동시에 쓰이는 함수
   * ex ) 기준관리/센터별작업정책(WMSMS020) (2차 개발 페이지)
   */
  fn_search?: () => void;
  /**
   * 소속사 ZONE 변경
   * ex ) 기준관리/거래처ZONE정보관리(WMSMS085) (2차 개발 페이지)
   */
  fn_zoneChange?: () => void;
  /**
   * 설정
   * ex ) 택배관리/택배접수관리(API)(WMSOP642_1) - 설정 모달
   * @param value 모달 여닫힘 설정(모달 클릭 시 true로 변경되면서 모달이 열리고 기본값 false로 지정)
   */
  changeIsShowDisplaySetting?: (value: boolean) => void;
  /**
   * 피킹리스트엑셀
   * ex ) 운영관리 - 입/출고관리(통합)/출고관리(H/D)
   */
  pickingListExcel?: () => void;
}

/**
 * 택배관리(WMSOP642_1 ~ 3) 페이지 버튼 정의
 */
export interface IDeliveryBtnProps {
  /** 버튼 정보 */
  data: {
    /** 버튼 이름 */
    title: string;
    /** 버튼 색 */
    colorStyle: string;
    /** 버튼 이미지 */
    image: string;
    /** 버튼 이름 앞에 위치할 숫자 */
    number: string;
    /** disabled 처리. value: 'disabled'*/
    disabled?: string;
  };
  /** 택배송장번호 수정
   * ex ) 택배관리/택배송장업로드(WMSOP642_2)
   */
  saveRowData?: () => void;
  /** 엑셀
   * 엑셀다운로드
   * ex) 택배관리/택배송장발급이력(WMSOP642_3)
   */
  excelExport?: () => void;
  /** 택배이력등록
   *  ex ) 택배관리/택배송장업로드(WMSOP642_2)
   */
  excelImport?: () => void;
  /** 택배배송접수
   * ex ) 택배관리/택배접수관리(API)(WMSOP642_1)
   */
  courierDeliveryReception?: any;
  /** 송장추가접수
   * ex ) 택배관리/택배접수관리(API)(WMSOP642_1)
   */
  additionalInvoiceSubmission?: () => void;
  /** 택배송장출력
   * ex ) 택배관리/택배접수관리(API)(WMSOP642_1)
   */
  printDeliveryInvoice?: () => void;
  /** 고객주소수정
   * ex ) 택배관리/택배접수관리(API)(WMSOP642_1)
   */
  editCustomerAddress?: () => void;
  /** DAS 자료조회
   * ex ) 택배관리/택배접수관리(API)(WMSOP642_1)
   */
  dasDataSearch?: () => void;
  /** DAS 자료생성
   * ex ) 택배관리/택배접수관리(API)(WMSOP642_1)
   */
  dasDataGeneration?: () => void;
  /** 택배송장삭제
   * ex ) 택배관리/택배접수관리(API)(WMSOP642_1)
   */
  fn_dlvShipDelete?: () => void;
  /** [선]송장출력
   *  ex ) 택배관리/택배접수관리(API)(WMSOP642_1)
   */
  fn_dlvInvcNoPrintPrev?: (addFlag?: string, divFlag?: string) => void;
  /** [후]송장접수
   *  ex ) 택배관리/택배접수관리(API)(WMSOP642_1)
   */
  fn_dlvShipmentPost?: (addFlag?: string) => void;
}

/**
 * 엑셀 입력 팝업에서 사용되는 정보
 */
export interface IExcelImportModal {
  /** 팝업 이름*/
  modalTitle: string;
  /** '저장' 버튼 클릭 시 사용되는 url */
  saveUrl: string;
  /** '양식다운로드' 버튼 클릭 시 사용되는 url */
  templateUrl: string;
  /**
   * 엑셀 입력 팝업인지 엑셀입력(간편)인지 구분하는 값
   * uploadType : "SIMPLE" => '엑셀입력(간편)'
   * uploadType : "NORMAL" or 생략된 경우 => '엑셀입력'
   */
  uploadType?: string;
}

/**
 * 페이지 정보
 * input.ts에서 정의
 */
export interface info {
  /** 페이지 id */
  pageId?: string;
  /**
   * 화면 진입 시, 모달 창 출력 여부 지정.
   * (store 값과 중복되지 않도록 주의: call-modal.callModalId)
   */
  autoModal: boolean;
  /** 화면 접속 시 자동으로 띄울 모달의 pageId
   * input.ts에서 SEARCH_MODAL_INFO(IModal타입)의 page 속성과 일치해야 함
   */
  autoModalPage: string;
  /** 그리드의 pk */
  pk: string;
  /**
   * 하단 그리드의 pk
   * ex ) 기준관리 - 임가공상품구성정보관리/세트상품정보관리(WMSMS092_1)
   * 하단 그리드의 신규 행을 추가할 때 그리드의 pk값으로 사용
   */
  pk2?: string;
}

/**
 * info영역 input 정의
 */
export interface IInfoInput {
  /**
   * info영역에서 사용되는 input의 type들
   * "text" : 텍스트 input
   * "search" : 모달 input
   * "triple-search" : triple search input(모달을 사용하고 모달에서 선택한 값이 3개의 input에 바인딩 됨)
   * "select" : select-box input
   * "upload" : 이미지 업로드 input
   * "color" : 색상 지정 input
   * "date" : 날짜 지정 input
   * "number" : 숫자 지정 input
   * "middle-title" : info영역 내에 title이 필요할 때 사용되는 input
   *  ex) WMSMS011 - form.vue 참고
   * "check-box" : 체크박스 input
   */
  type: "text" | "search" | "triple-search" | "select" | "upload" | "color" | "date" | "number" | "middle-title" | "check-box";
  /** input key name */
  ids: string[];
  /** input key id */
  hiddenId?: string;
  /** 실제 data key name (search modal key name) */
  rowDataIds?: string[];
  /** 실제 data key id (search modal key id) */
  rowDataHiddenId?: string;
  /** input의 title(명칭) */
  title: string;
  /** 너비
   * half : 한 행에 2개의 input 지정
   * full : 한 행에 1개의 input 지정
   */
  width?: "half" | "full";
  /** input 간에 간격
   * single : 기본 간격
   * double : gap 10px
   * triple : gap 5px
   * single -> double -> triple 순으르 간격이 좁아짐
   */
  size?: "single" | "double" | "triple";
  /** Email 형식을 사용하는 input 인지 설정 */
  isEmail?: boolean;
  /** 필수값 설정 */
  required?: boolean;
  /** 중복 value 체크 */
  overapCheck?: boolean;
  /** 입력 정보 규칙 */
  innerTexts?: string[];
  /** input readonly설정 */
  isReadonly?: boolean;
  /** 히든 인풋들이 참조할 인풋인지 아닌지 정의 */
  isHiddenInputParent?: boolean;
  /** 참조할 input의 ids[0]값 */
  isHiddenInput?: string;
  /** 참조할 인풋의 value가 showHiddenInputValue:value value일때 인풋이 view에 보여짐 */
  showHiddenInputValue?: string;
  /** 서버에서 가져오는 select-box options key */
  optionsKey?: string;
  /** api로 options를 받아오지 않고 하드코딩 할때 사용 */
  options?: { nameKey: string; name: string; value: string }[];
  /** info 영역에 있는 select box option이며 최초 false. 신규 데이터 생성 후 최초로 default 값 세팅 이후에 true를 입력하여 다시 default값이 셋팅 안되도록 하는 용도. */
  optionInit?: boolean;
  /** 자동선택될 option 의 key value 혹은, autoSelectedKeyIndex 으로 값 셋팅.
   * (autoSelectedKeyIndex 경우 ex - { autoSelectedKeyIndex: 0 })
   */
  optionsAutoSelected?: { [key: string]: any; autoSelectedKeyIndex?: number };
  /** 인풋의 타입이 check-box일 경우 해당 체크박스가 checked될 때 스토어에 저장 될 값 */
  checkboxValue?: { checkedValue: string[]; unCheckedValue: string[] };
  /** 비활성화 처리 */
  disabled?: boolean;
  /** 숨김 처리 */
  hidden?: boolean;
  /** option을 button으로 관리해야 하는 경우 */
  optionControlName?: string;
}
/**
 * info input container 정의
 * 하나의 info 영역을 정의
 */
export interface IInfoInputContainer {
  /** info header 명칭 */
  title: string;
  /**
   * 기본 메뉴 셋팅 설정
   * expand : 페이지 접속 시 info영역을 확장한 상태로 설정
   * fold : 페이지 접속 시 info영역이 접힌 상태로 설정
   */
  default: "expand" | "fold";
  /** info input 정의 */
  inputs: IInfoInput[];
}

/**
 * 모달 value 정보 정의
 */
export interface IModalValue {
  /** 조회할 모달의 pagingOptions 지정 key값
   *  UtilService.getGridPagingOptions(type) 참고
   *  type에 key값이 들어가야함
   */
  page: string;
  /** 모달에서 선택한 값(행)을 저장할 때 사용하는 key값 */
  id: string;
  /** 모달의 title */
  title: string;

  /** 모달이 cellRenderer을 통해 열렸는지 아닌지를 구분하기 위함, 그리드 데이터 선택시 바인딩되는 방법이 다름 */
  isCellRenderer?: boolean;

  /** 모달이 템플릿복사 기능의 용도로 사용되었는지를 구분하기 위함, 데이터 선택시 스토어에 저장이 아닌 api호출 */
  templateId?: string;

  /**
   * 모달에서 선택된 값을 세션에 저장해야될때 사용
   * ex)재고관리-재고명의변경(신규)_부분재고 / 저장시 key값을 id로 저장
   */
  saveSession?: boolean;

  /** search-modal 에서 검색할때, 다른 search-modal의 데이터를 참조하여 검색이 필요할때 */
  defaultParamsData?: {
    /** input.ts 에 정의한 search modal 데이터의 key */
    storeSaveKey: string;

    /** 참조할 search modal 데이터의 row data key */
    rowDataKeys: string[];

    /** 참조한 search modal 데이터를 자신의 어떤 param 데이터로 입힐지 */
    paramsKeys: string[];
  };

  /** info영역에서 특정 데이터가 있거나, 특정데이터의 값이여야지만 열리는 경우가 있음
   * rowDataKey : 그리드에서 선택된 로우의 키값
   * rowDataValue : 그리드에서 선택된 로우의 value값
   */
  modalOpenCondition?: { rowDataKey: string; rowDataValue: string };

  /** 검색시 하드코딩된 특정 값을 필요로 할때 */
  infoDefaultParamsData?: {
    /** key값 */
    paramsKeys: string[];
    /** value깂 */
    rowDataKeys: string[];
  };
  /** 그리드 title */
  gridTitle: string;
  /** search-modal 에서 grid row 선택시 single or multiple 선택 결정 (설정안할경우 default single) */
  gridRowSelection?: string;
  /** search-modal 에서 grid 선택 후 일반적인 '선택' 버튼 기능이 아닌, 다른 기능을 실행시켜야할 경우 */
  differentFunc?: {
    /** 버튼이름으로 사용할 title. ex) 재등록 */
    title: string;
    /** 해당기능 실행할 api url */
    url: string;
  };
  /**  모달에서 새로운 window팝업을 띄울때*/
  isNewBtn?: string;
  /** 모달에서 사용하는 api 정보 */
  apis: {
    /** 모달 조회 시 사용되는 api url */
    url: any;
    /** 모달 조회 시 사용되는 params */
    params: any;
    /** 모달 조회 시 사용되는 bodyData */
    data: any;
  };
  /** 모달 input 정보 정의 */
  inputs: IModalInput[];
}

/**
 * 모달 input 정보 정의
 * 바인딩 되어야할 search container input의 id를 적어준다., search-container-input의 모달에서만 입력
 */
export interface IModalInput {
  /** 모달에서 검색시 전달되는 파라미터에 해당 인풋의 값이 바인딩 되어야할 key값 */
  id: string;
  /** 상호작용 되어야 하는 searchInput의 key값 */
  searchContainerInputId?: string;
  /** 모달 인풋의 title */
  title: string;
  /** 너비 지정
   * single : 한 행에 1개의 input 지정
   * half : 한 행에 2개의 input 지정
   * triple ; 한 행에 3개의 input 지정
   */
  width: "single" | "half" | "triple";
  /**
   * 모달 input의 타입 설정
   * select : select-box 인풋
   * text : 일반 텍스트 인풋
   * search : 모달을 사용하는 인풋
   */
  type: "select" | "text" | "search";
  /** type이 'search'일 때 띄어진 자식 모달에서 선택한 데이터의 key값 */
  searchRowDataIds?: string[];
  /** type이 'search'일 때 띄어진 자식 모달에서, 검색시 전달되는 파라미터에 해당 인풋의 값이 바인딩 되어야할 key값 */
  searchIds?: string[];
  /** api로 options를 받아오지 않고 하드코딩 할 때 사용 */
  options?: { value: string; nameKey: string; name: string }[];
  /** 서버에서 가져오는 select-box options key */
  optionsKey?: string;
  /** input readOnly 속성 설정 */
  optionsReadOnly?: boolean;
  /** 기본값 설정 옵션
   * (autoSelectedKeyIndex: 자동선택될 option의 인덱스 번호)
   */
  optionsAutoSelected?: { autoSelectedKeyIndex?: number };
  /** 다중조회 팝업을 위한 컬럼정의*/
  childrenColumnDefs?: any[];
  /** 다중조회 팝업을 위한 모달정의 */
  childrenModalData?: IModalValue;
}

/**
 * 모달 전체 정보 정의
 */
export interface IModal {
  /** 모달 정보 구별하는 key값 : 모달 value 정보 정의
   * SEARCH_INPUT(ISearchInput타입)의 title 속성과 일치시켜 주어야 함
   */
  [key: string]: IModalValue;
}

/**
 * search 영역 input 정의
 */
export interface ISearchInput {
  /** search input에서 사용하는 key */
  ids: string[];

  /** search input에서 사용하는 hiddenId (hidden Id가 없을경우 ids를 id로 사용) */
  hiddenId: string;

  /** search-modal 에서 띄어진 input key */
  rowDataIds: string[];

  /** search-modal 에서 띄어진 input hiddenId */
  rowDataHiddenId: string;

  /** 하나의 search input에서 같은 input안에 있는 select box 값에 따라 띄어야할 modal 이 다를때 사용 */
  rowData2ndIds?: string[];

  /** 하나의 search input에서 같은 input안에 있는 select box 값에 따라 띄어야할 modal 이 다를때 사용 */
  rowData2ndHiddenId?: string;

  /** focus 아웃하면서 api 요청할때 보낼 params key */
  searchApiKeys: string[];

  /** 포커스아웃시 하드코딩으로 추가된 params key 정보 */
  defaultSearchApiKeys?: { [key: string]: any };

  /** search영역에서 특정 selected modal data가 존재해야지만 모달이 열리는 경우가 있음, 참조해야할 modalData에 저장된 key값 */
  modalOpenCondition?: string;

  /** search타입의 인풋에서 포커스 아웃시 다른 search타입의 값을 참조할때 사용 */
  defaultParamsData?: {
    /** 참조할 search타입의 인풋의 데이터가 스토어에 저장된 key값(참조할 인풋의 title) */
    storeSaveKey: string;

    /** 포커스아웃시 bodyData의 키값 */
    paramsKeys: string[];

    /**
     * paramsKeys에 바인딩 되어야할 스토어의 저장된 object의 value값
     * (modalStore[storeSaveKey][rowDataKeys[i]]이와 같은 형식으로 참조)
     */
    rowDataKeys: string[];
  };

  /**
   * 포커스 아웃 시, 대부분 params는 해당 searchApiKey, searKey로 구성.
   * 그러나 인풋과 관계 없는 params도 존재하는 경우가 있음.
   * focusOutSearchEvent 함수 참고
   *
   * 모달내에서 검색시 defaultParamsData 사용되는 타입
   * 다른 모달의 검색결과값을 참조할때,
   */
  isAllUseSearchApiKey?: boolean;

  /** focus 아웃하면서 api 요청할때 보낼 params key */
  srchKey?: string;

  /** 표기명칭 (다국어 키값) */
  title: string;

  types?: ("text" | "date" | "select-box" | "select-box2" | "check-box" | "exclusive-checkbox" | "textarea" | "multi-select" | "month")[];

  /** search 검색이 고정이 아닌, select-box 선택을 통해 어떤 search modal을 띄울지 달라지는 경우 사용 */
  modalTypes?: string[];

  // TODO :: 미사용 항목 확인
  apiUrl?: string;

  /** search-container 내 열 구성 단위 */
  width: "entire" | "half" | "triple" | "quarter" | "hidden";

  /** 특정 input에 다른 style을 적용해야할때 사용 */
  style?: string;

  // TODO :: 용도 확인 필요 (true 조건의 동작방식?)
  isModal: boolean;

  /** 필수값 표시 */
  required: boolean;

  /** 화면 숨김 */
  hidden?: boolean;

  /** 자동완성 API 사용 여부 */
  isSearch: boolean;

  /** 인풋들의 정렬을 맞추기 위해, 기본 정렬이 space-between으로 되어있어서 한 행에 꽉차지 않는 경우 강제적으로 flex-start처럼 정렬하기 위해 사용 */
  isUnused?: boolean;

  /** 비활성화 처리. */
  disabled?: "disabled" | "";

  /** 입력 힌트. ids 인덱스 순서에 맞춰 기입 */
  placeholder?: string[];

  //#region :: ChecktBox 옵션

  /** 체크박스(토글)이 아닌 라디오 버튼으로 사용되는 경우:true, 체크박스와 정의는 동일하게 해주고, 해당 키값만 true로 해주어 사용됨 */
  isRadioBox?: boolean;

  /** 체크박스 라벨 title정의. ids와 인덱스를 일치시켜준다. */
  checkboxLabelTitles?: string[];

  /** 체크박스의 API Param value 값 정의, 매칭되는 checkboxLabelTitles 과 index를 동일하게 해준다 */
  checkboxCheckedValue?: string[];

  /** 체크박스 컴포넌트인 경우 기본값 checked를 갖을 요소의 인덱스 */
  checkboxAutoChecked?: string[];

  //#endregion

  //#region :: ComboBox 옵션

  /** select-box클릭시 기본 동작은 options가 아래로 내려오지만, 특정 화면에서 options가 짤리게 되어 위로 열려야되는 경우가 있음 */
  optionsPosition?: "top";

  /** server에서 가져오는 select-box options key */
  optionsKey?: string;

  /** 하나의 input 영역에 select-box가 2개이고, 2개다 server에서 가져오는 select-box options을 사용할 경우 두번째 select-box의 options key */
  optionsKey2?: string;

  /** select-box option을 직접 정의할때 사용 */
  options?: { [key: string]: any }[];

  /** 하나의 input 영역에 select-box가 2개이고, 2번째 select-box option을 직접 정의할때 사용 */
  options2?: { [key: string]: any }[];

  /** 비활성화 */
  optionsReadOnly?: boolean;

  /**
   * 컴포넌트의 기본값 설정 옵션1
   */
  optionsAutoSelected?: {
    /** 자동선택될 option 의 key value */
    [key: string]: any;

    /** 자동선택될 option의 인덱스 번호 */
    autoSelectedKeyIndex?: number;

    /**
     * 자동선택 가능 여부 옵션.
     *
     * (특정 select box 선택으로 또 다른 select box option을 새로 가져올 경우, watch로 인해 자동선택이 다시 작동되어 문제가 생김. 그 현상 방지용.)
     *
     * _ex- WMSYS030 - search.vue 참조_
     */
    allowAutoSelected?: boolean;
  };

  /**
   * 컴포넌트의 기본값 설정 옵션2
   *
   * (하나의 input 영역의 두번째 select-box에 적용)
   */
  optionsAutoSelected2?: {
    /** 자동선택될 option 의 key value */
    [key: string]: any;

    /** 자동선택될 option의 인덱스 번호 */
    autoSelectedKeyIndex?: number;

    /**
     * 자동선택 가능 여부 옵션.
     *
     * (특정 select box 선택으로 또 다른 select box option을 새로 가져올 경우, watch로 인해 자동선택이 다시 작동되어 문제가 생김. 그 현상 방지용.)
     *
     * _ex- WMSYS030 - search.vue 참조_
     */
    allowAutoSelected?: boolean;
  };

  /**
   * 첫번째 option에서 server에서 받은 목록과 직접 정의한 목록의 merge 가능 여부.
   *
   * (오직 false일때만 merge 안함. true, null, undefined 일 경우 merge함.)
   */
  optionMerge?: boolean;

  /**
   * 두번째 option에서 server에서 받은 목록과 직접 정의한 목록의 merge 가능 여부.
   *
   * (오직 false일때만 merge 안함. true, null, undefined 일 경우 merge함.)
   */
  optionMerge2?: boolean;

  //#endregion
}

/** 선택된 tab에 대한 정보 정의 */
export interface ISelectedTab {
  /** 선택된 tab에 해당하는 페이지 id */
  id: string;
  /** 선택된 tab에 해당되는 페이지명 */
  title: string;
  /** 선택된 tab에 해당하는 페이지가 위치한 경로 */
  path: string;
  /** 선택된 tab에 해당하는 페이지의 대메뉴명 */
  mainMenuTitle?: string;
}

/** 하위 메뉴 정보 정의 */
export interface ISubMenu {
  /** 하위 메뉴 페이지 id */
  id: string;
  /** 하위 메뉴 페이지명 */
  title: string;
  /** 하위 메뉴 페이지가 위치한 경로 */
  path: string;
  /** 즐겨찾기 설정 */
  favorite: boolean;
  /** 하위 메뉴 숨김 여부 설정 */
  hidden: boolean;
}

/** 메뉴 내비게이션 정보 정의 */
export interface IWinusMenu {
  /** 메뉴 내비게이션에서 선택한 메뉴의 대메뉴 id값
   * ex ) 시스템관리 선택 시 id = 'setting'
   */
  id: string;
  /** 메뉴 내비게이션에서 선택한 메뉴의 대메뉴명
   * ex ) 시스템관리 선택 시 id = '시스템관리'
   */
  title: string;
  /** 메뉴 내비게이션 하단 메뉴 전체보기에서 각 대메뉴별 소메뉴 리스트 높이 설정
   * 기본값 true
   */
  tooMany: boolean;
  /** 하위 메뉴 정보 정의 */
  subMenus: ISubMenu[];
}

/** 서버에서 받아온 options중 일부만 보여주는 경우 사용되는 UtilService.setAvailableOptions의 두번째 인자로 들어가는 값 */
export interface IAvailableOptionsUtilParams {
  /** 받아온 options의 키값  */
  inputId: string;
  /** 사용할 option의 value값 */
  availableList: string[];
}

/** 그리드에서 사용하는 select-box정의 */
export interface IGridCellSelectBox {
  /** select-box가 지정되어야 하는 컬럼의 field명 */
  [key: string]: {
    /** 그리드의 pk */
    pk: string;
    /** optionsStore에서 options값을 참조할때 사용되는 키 */
    optionsKey?: string;
    /** api로 options를 받아오지 않고 하드코딩 할때 사용 */
    options?: { name: string; nameKey: string; value: string }[];
    /**
     * 특정 select-box를 참조하여 해당 요소의 값이 변경될때마다 options가 달라질때, 참조하고 있는 select-box의 키값을 value로 넣어준다
     * ex ) WMSDF001_1 참고
     */
    dynamicOptionsKey?: string;
    /**
     * 기본적으로 select-box가 위치한 컬럼의 값만 변경해주지만, 다른 필드의 값도 변경해줄때 사용 / 바꾸고자 하는 필드명을 넣어준다
     * ex ) WMSDF001_1 참고
     */
    otherRowDataKeys?: string[];
    /**
     * otherRowDataKeys요소에 options의 어떠한 값을 할당해줄지 정해준다. otherRowDataKeys와 할당할 options요소의 인덱스를 맞추어준다
     *  ex ) WMSDF001_1 참고
     */
    otherOptionsKeys?: string[];
    /**
     * select-box값을 선택하면 value키값의 value가 그리드에 할당되지만 nameKey가 value로 할당되는 경우가 있음
     *  ex ) WMSDF001_1 참고
     */
    valueType?: string;
    /**
     * 기본적으로 code===code return name형식이지만, name===name return name인 경우 true를 사용
     * ex ) WMSST076 참고
     */
    useOnlyName?: boolean;
  };
}

/** 그리드에서 사용하는 select-box정의
 * ex ) 택배관리/택배접수관리(API)(WMSOP642_1) 페이지
 */
export interface IGridCellOptionalSelectBox {
  /** select-box가 지정되어야 하는 컬럼의 field명 */
  [key: string]: {
    /** 그리드의 pk */
    pk: string;
    /** optionsStore에서 options값을 참조할때 사용되는 키 */
    optionsKey?: string;
    /** api로 options를 받아오지 않고 하드코딩 할때 사용 */
    options?: { name: string; nameKey: string; value: string }[];
    /**
     * 특정 select-box를 참조하여 해당 요소의 값이 변경될때마다 options가 달라질때, 참조하고 있는 select-box의 키값을 value로 넣어준다
     * ex ) WMSDF001_1 참고
     */
    dynamicOptionsKey?: string;
    /**
     * 기본적으로 select-box가 위치한 컬럼의 값만 변경해주지만, 다른 필드의 값도 변경해줄때 사용 / 바꾸고자 하는 필드명을 넣어준다
     * ex ) WMSDF001_1 참고
     */
    otherRowDataKeys?: string[];
    /**
     * otherRowDataKeys요소에 options의 어떠한 값을 할당해줄지 정해준다. otherRowDataKeys와 할당할 options요소의 인덱스를 맞추어준다
     *  ex ) WMSDF001_1 참고
     */
    otherOptionsKeys?: string[];
    /**
     * select-box값을 선택하면 value키값의 value가 그리드에 할당되지만 nameKey가 value로 할당되는 경우가 있음
     *  ex ) WMSDF001_1 참고
     */
    valueType?: string;
    /**
     * 기본적으로 code===code return name형식이지만, name===name return name인 경우 true를 사용
     * ex ) WMSST076 참고
     */
    useOnlyName?: boolean;
    /** 편집기ㅣ 활성화 여부 */
    editable?: Function;
  };
}

/** 그리드에서 사용하는 search-button정의 */
export interface IGridCellSearchButton {
  /** search-button이 지정되어야 하는 컬럼의 field명 */
  [key: string]: {
    /** 모달에서 선택한 데이터를 해당 로우에 바인딩할 위치(field) */
    fieldList: string[];
    /** 모달에서 어떠한 값을 fieldList에 할당시킬지 (fieldList와 매칭되는 요소의 인덱스를 맞추어준다 ) */
    rowDataKeys: string[];
    /** 검색 모달에서 api요청시 검색버튼이 위치한 로우의 데이터를 필요로 하는는 경우 두 배열의 index를 매칭시켜준다 */
    setParamsFromGridRowData?: {
      /** 검색버튼이 위치한 로우에서 필요한 키값 */
      rowDataKeys: string[];
      /** 로우의 키값을 바인딩할 params의 키값 */
      paramsKeys: string[];
    };
    /** grid-search-button에 정의된 모달을 열기 전 특정 데이터를 필요로 할 때 */
    callApiCondition?: {
      /** 필요로 하는 특정 데이터 타입(모달 데이터) */
      dataType: "modal";
      /** 필요로 하는 특정 데이터의 title 값 */
      key: string;
    };
    /** 기존 ModalDef */
    modalData: IModalValue;
    /** 컬럼 정의(columnDef) */
    colDef: { [key: string]: any }[];
  };
}

/** 그리드 컬럼 정의시 change-index-button컴포넌트 랜더러가 위치한 컬럼의 객체의 타입 */
export interface IGridCellChangeIndexColDef {
  /** field value는 order-up(위쪽 삼각형), order-down(아래쪽 삼각형) 두가지만 가능 */
  field: "order-up" | "order-down";
  /** 필드명(다국어key값)
   * 현재 해당 타입에서는 ""으로 사용중
   */
  headerKey?: string;
  /** cellClass 스타일 지정 */
  cellClass?: string;
  /** 필드명(다국어 적용된된 값)
   *  현재 해당 타입에서는 ""으로 사용중
   */
  headerName?: string;
  /** 해당 컬럼에서 쓰일 cellRenderer 지정 */
  cellRenderer: any;
  /** 너비 지정 */
  width?: number;
}

/** 그리드에서 사용하는 check-box정의 */
export interface IGridCellCheckBox {
  /** check-box가 지정되어야 하는 컬럼의 field명 */
  [key: string]: {
    /** 그리드의 pk */
    pk: string;
    /** 체크되었을때 value */
    checkedValue: string;
    /** 체크되지 않았을때의 value */
    unCheckedValue: string;
  };
}

/** 그리드에서 사용하는 dateInput정의 */
export interface IGridCellDateInput {
  /** 그리드의 pk */
  gridPk: string;
}

/** TemplatePromptModalInfo 정의 */
export interface ITemplatePromptModalInfo {
  /** 키값은 control버튼의 버튼의 title */
  [key: string]: {
    /** 모달의 헤더영역에 들어갈 제목 */
    header: string; // 모달의 헤더영역에 들어갈 제목
    /** 모달에서 입력할 정보 설명 */
    content: string;
    /** [key: string]와 동일하게 일치시켜준다 */
    title: string;
  };
}

/** 센터동기화 기능 사용시 필요 정의 */
export interface ILcSyncModalProps {
  /** 센터동기화 모달에 보낼 파라미터 데이터 */
  params: { [key: string]: any };
  /** 센터동기화 모달에 보낼 ui 데이터 */
  uiOption: { title: string; useAsnCheckbox: boolean };
  /** 센터동기화 모달 '저장'버튼 클릭 시 요청되는 url */
  apiUrl: string;
}

/** 운영관리 메뉴의 전체통합버튼 리스트 정의 */
export interface IGridTotalIntegrationMenuInfo {
  /** 버튼 이름 */
  title: string;
  /** 버튼 클릭 시 발생하는 이벤트 정의 */
  clickFunction: () => void;
}

/** cellRenderer : gridTextInput 자동완성 정의 타입(grid-text-input.vue) */
export interface ICellRendererSearchTextInputParams {
  /** 자동완성 되어야 할 모달 input의 key 값 */
  searchModalInputKey: string;
  /** 검색버튼과 상호작용되는 text input인 경우 change시 호출되는 함수 */
  focusOutEventKey: string;
  /** focus out 검색으로 api 요청 보낼 때 사용되는 params */
  srchKey: string;
  /** cellRenderer : GridSearchButton이 사용되는 컬럼의 field명과 일치시켜 주어야 한다 */
  searchButtonFieldName: string;
  /** * 모달의 결과값이 1개 이상일 경우 호출될 모달의 이름(title) */
  callModalId: string;
  /** search-modal에서 검색할 때, 다른 search-modal의 데이터를 참조하여 검색이 필요할 때 */
  defaultParamsData?: {
    /** input.ts 에 정의한 search modal 데이터의 key */
    storeSaveKey: string;
    /** 참조할 search modal 데이터의 row data key */
    paramsKeys: string[];
    /** 참조한 search modal 데이터를 넣을 자신의 param 데이터 key */
    rowDataKeys: string[];
  };
}

/** 택배관리 > 택배접수관리(API)(WMSOP642_1) '접수구분' 설정 정의
 * ex ) option-range.vue('접수구분' 설정 컴포넌트)
 */
export interface IOptionRange {
  /** check-box 정보 */
  checkBoxes: {
    /** 옵션 input의 key값 */
    id: string;
    /** 옵션 input 선택 시 적용되어야 할 value */
    value: string;
    /** 옵션 input의 이름 */
    title: string;
  }[];
}

export type IModalColDef = Record<string, (IColDef | IColGroupDef)[] | null>;
